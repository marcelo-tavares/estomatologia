<%
select case pr1_wk
	case 0
	%>
		<p class="Titulo" align="center">CAMPANHAS APOIADAS PELO CONGRESSO</p>
		<p class="TextoComum">
			&#149; <a href="javascript:submeter(2,9,1,1);">Campanha de preven��o de C�ncer de Boca em Florian�polis</a>.
		</p>
		<p class="TextoComum">
			&#149; <a href="javascript:submeter(2,9,1,2);">Campanha de preven��o de C�ncer de Boca em Tubar�o</a>. 
		</p>
	<%
	case 1
	%>
		<p class="Titulo" align="center">Campanha Apoiada pelo Congresso<br>Campanha de preven��o de C�ncer de Boca em Florian�polis</p>
		<p class="TextoComum">
			Evento realizado no dia do Cirurgi�o-Dentista (25 de outubro de 2004), promovido pelo SESC-Fpolis, em parceria com CEPON (Centro de Pesquisas Oncol�gicas), Secretaria Municipal de Sa�de de Florian�polis, Secretaria Estadual de Sa�de de Santa Catarina, Disciplinas de Patologia Bucal e Estomatologia do Curso de Odontologia da UFSC e Ambulat�rio de Estomatologia do Hospital Universit�rio/UFSC.
		</p>
		<p class="TextoComum">
			O p�blico de aproximadamente 500 associados do SESC (comerci�rios e dependentes), foram informados sobre preven��o do C�ncer de Boca e auto-exame e submetidos ao exame bucal objetivando o diagn�stico precoce. Tamb�m fez parte do evento a preven��o da doen�a c�rie e periodontal, al�m da distribui��o de escovas e pastas de dente.
		</p>
		<p class="TextoComum">
			A campanha contou com a colabora��o e participa��o dos alunos do curso de Odontologia/UFSC e dos membros da Comiss�o Organizadora do XIII Congresso Brasileiro de Estomatologia, marcando a presen�a da SOBE neste evento.
		</p>
		<p class="TextoComum" align="center">
			<img SRC="images/floripa1.jpg" border="0" WIDTH="500" HEIGHT="321"><br><br>
			<img SRC="images/floripa2.jpg" border="0" WIDTH="500" HEIGHT="338"><br><br>
			<img SRC="images/floripa3.jpg" border="0" WIDTH="500" HEIGHT="315"><br><br>
			<img SRC="images/floripa4.jpg" border="0" WIDTH="500" HEIGHT="559">
		</p>
	<%
	case 2
	%>
		<p class="Titulo" align="center">Campanha Apoiada pelo Congresso<br>Campanha de preven��o de C�ncer de Boca em Tubar�o</p>
		<p class="TextoComum">
			Neste evento foram examinados mais de 150 moradores da cidade de Tubar�o, que receberam orienta��es individualizadas e folders sobre o auto-exame e a preven��o do C�ncer de Boca. Esta campanha foi realizada pela Disciplina de Estomatologia e Curso de Odontologia da UNISUL e pela ABO-Tubar�o, contando com o apoio da Secretaria Municipal de Sa�de de Tubar�o e pelo Ex�rcito. Tamb�m estavam presentes a Comiss�o Organizadora do XIII Congresso Brasileiro de Estomatologia e alunos de Gradua��o em Odontologia da UNISUL.
		</p>
		<p class="TextoComum" align="center">
			<img SRC="images/tubarao1.jpg" border="0" WIDTH="500" HEIGHT="344"><br><br>
			<img SRC="images/tubarao2.jpg" border="0" WIDTH="500" HEIGHT="337"><br><br>
			<img SRC="images/tubarao3.jpg" border="0" WIDTH="500" HEIGHT="371"><br><br>
			<img SRC="images/tubarao4.jpg" border="0" WIDTH="500" HEIGHT="375"><br><br>
			<img SRC="images/tubarao5.jpg" border="0" WIDTH="500" HEIGHT="375"><br><br>
		</p>
	<%
end select
%>