<%@ LANGUAGE=vbscript enablesessionstate=true LCID=1046 %>
<!--#include file="adovbs.inc"-->
<%

Response.Expires = -1
Response.Buffer = true

desenvolvimento = false
if Request.ServerVariables("SERVER_NAME") = "marcelo" then desenvolvimento = true

md_wk = request("md")

if md_wk = "" then 
	md_wk = 0
end if

if session("avaliacao") = "" then session("avaliacao") = False

if (not session("avaliacao") and (md_wk <> 0 ))then Response.Redirect("default.asp?md=0") 

it_wk = request("it")
if it_wk = "" then 
	it_wk = 1
end if

pr1_wk = request("pr1")
if pr1_wk = "" then 
	pr1_wk = 0
end if

' -------------- Qual o Navegador ? ---------------
navegador = 0

'response.write Request.ServerVariables("HTTP_USER_AGENT") & "<BR>"

strNetscape = instr(Request.ServerVariables("HTTP_USER_AGENT"), "Netscape")
if strNetscape > 0 then
	'Response.Write "NETSCAPE" & " Vers�o " & mid(Request.ServerVariables("HTTP_USER_AGENT"),Cint(strNetscape)+9,1)
	navegador=2
else
	strMSIE = instr(Request.ServerVariables("HTTP_USER_AGENT"), "MSIE")
	if strMSIE > 0 then
		strOpera = instr(Request.ServerVariables("HTTP_USER_AGENT"), "Opera")
			if strOpera > 0 then
				'Response.Write "Opera" & " Vers�o " & mid(Request.ServerVariables("HTTP_USER_AGENT"), Cint(strOpera)+6,1)
				navegador=3
			else
				'Response.Write "INTERNET EXPLORER" & " Vers�o " & mid(Request.ServerVariables("HTTP_USER_AGENT"), Cint(strMSIE)+5,1)
				navegador=1
			end if
	end if
end if
' -----------------------------------------------------
%>

<html>
<head>
<link REL="stylesheet" HREF="../estilos.css">
<script>
	function submeter(md, it, pr1){
		<% if desenvolvimento then %>
		document.Form1.action = 'default.asp';
		<% else %>
		document.Form1.action = 'default.asp';
		<% end if %>
		document.Form1.md.value = md;
		document.Form1.it.value = it;
		document.Form1.pr1.value = pr1;
		
		document.Form1.submit();
	}

	function acende(elemento){
		elemento.style.cursor='hand';
		elemento.style.color='#ff0000';
	}

	function apaga(elemento){
		elemento.style.cursor='default';
		if (elemento.ativado == 0) 
			elemento.style.color='#000000'
		else
			elemento.style.color='#ff0000'
	}
	
	function acendeSM(elemento){
		elemento.style.cursor='hand';
		elemento.style.fontWeight = 700;
	}

	function apagaSM(elemento){
		elemento.style.cursor='default';
		elemento.style.fontWeight = 400;

	}
	
	function AtivaDesativaSubmenu(sm){
		submenu = document.getElementById('submenu'+sm);
		menu = document.getElementById('menu'+sm);
		
		if (submenu.ativado == 0){
		
			for (i=1; i<=1; i++){
				sm_wk = document.getElementById('submenu'+i);
				m_wk = document.getElementById('menu'+i);
				sm_wk.style.display = 'none';
				sm_wk.ativado = 0;
				if (i != sm){
					m_wk.style.color = '#000000';
				}
				else {
					m_wk.style.color = '#FF0000';
				}
				m_wk.ativado = 0;
			}		
		
			submenu.ativado = 1;
			menu.ativado = 1;
			submenu.style.display='inline';
			menu.style.color = "#ff0000"
		}
		else {
			submenu.ativado = 0;
			menu.ativado = 0;
			submenu.style.display='none';
			menu.style.color = "#FFFFFF";
		}
	}

	function fechaSubmenu(sm){
		document.getElementById('submenu'+sm).className = 'submenu';
	}
	
	
	function load(md){
		if (md == 6) md = 1;
		if (md != 0){
			// Abrir o Submenu
			submenu = 'submenu' + md
			menu = 'menu' + md
			document.getElementById(submenu).style.display = 'inline';
			document.getElementById(menu).style.color='#ff0000';
		}
	}

	function attrAtivado(){
		for (i=1; i<=1; i++) {
			nm_menu = 'menu' + i;
			nm_submenu = 'submenu' + i;
			nm_submenuitem1 = sm + i + 1;
			nm_submenuitem2 = sm + i + 2;
			menu = document.getElementById(nm_menu)
			submenu = document.getElementById(nm_submenu)
			sm1 = document.getElementById(nm_submenuitem1)
			sm2 = document.getElementById(nm_submenuitem2)
			if ((menu.ativado != 0) &&  (menu.ativado != 1)) {
				menu.ativado=0;
				submenu.ativado=0;
				sm1.ativado=0;
				sm2.ativado=0;
			}
		}
		
	}

</script>

</head>
<body leftmargin="0" topmargin="0" onload="<%if navegador <> 1 then %>attrAtivado();<% end if %>load(<%=md_wk%>);"  vlink="#ff0000" alink="#ff0000" link="#ff0000">
	<form name="Form1" method="POST">
	<input name="md" type="hidden" value=<%=md_wk%>>
	<input name="it" type="hidden" value=<%=it_wk%>>
	<input name="pr1" type="hidden" value=<%=pr1_wk%>>
	<table WIDTH="780" ALIGN="left" BORDER="0" CELLSPACING="0" CELLPADDING="0">
		<tr bgcolor=#e8e8e8>
			<td width="780" colspan="2">
				<img height="70" src="../images/ponto.gif" width="10" align="right" border="0">
				<br><center><span class="titulo">XIII Congresso Brasileiro de Estomatologia<br>M�dulo de Avalia��o de Trabalhos</span></center>
			</td>
		</tr>
		<tr>
			<td width="120" valign="top">
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">
					<tr>
						<td valign="top" height="4000" bgcolor=#EDF0FF>
							<table BORDER="0" CELLSPACING="0" CELLPADDING="0" width="100">
								<tr>
									<td>
										<img height="10" src="../images/ponto.gif" width="1" align="left" border="0">
									</td>
								</tr>
								
								<% if session("avaliacao") then %>

								<tr>
									<td class="textoMenu">
										<span id="menu1" ativado="1" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="AtivaDesativaSubmenu(1);">
											TRABALHOS
										</span>
										<span id="submenu1" class="submenuAceso" ativado="1">
											<br>
											<span id="sm11" ativado="1" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(1,1,0);">
												Resumos
											</span>
											<br>
											<span id="sm12" ativado="1" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="alert('Op��o n�o dispon�vel.')">
												Alterar Senha
											</span>
											<br>
										</span>
										<br>&nbsp;
									</td>
								</tr>
								
								<% end if %>
								
								<tr>
									<td class="textoMenu">
										<span id="menu9" ativado="0" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="submeter(0,99,0);">
												VOLTAR
										</span>
										<br><br>
										<%'= md_wk & " - " & it_wk & " - " & pr1_wk%>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" rowspan="2" align=center>
				<%
				select case md_wk
					case 0
						%><!--#include file="ava_md0.asp"--><%
					case 1
						select case it_wk
							case 1
								%><!--#include file="ava_md1it1.asp"--><%
							case 2
								%><!--#include file="ava_md1it2.asp"--><%
						end select
				end select
				%>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	</form>
</body>
</html>