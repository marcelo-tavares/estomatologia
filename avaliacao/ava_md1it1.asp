<%
function testaZero(q)
	if isnull(q) then
		testaZero = """"""
	else
		if q = 0 then
			testaZero = """"""
		else
			testaZero = """" & replace(Cstr(q),",",".") & """"
		end if
	end if
end function
%>
<script>
	function ordenar(pr1, ordem) {
		document.Form1.h_ordem.value = ordem;
		submeter(1,1,0);
	}
			
	function visualizar(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(1,1,1);
	}

	function  registrarNota(cd_trabalho) {
		nota = document.Form1.nota.value
		if (nota == '') nota = 'a';
		if (!(isFinite(nota))){
			alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
		}
		else {
			if (confirm('Confirma a atribui��o da nota ' + nota + ' para o trabalho N� ' + cd_trabalho + ' ?')) {
					document.Form1.hcd_trabalho.value = cd_trabalho;
					submeter(1,1,2);
				}
		}
	}

	function paginar(codPag){
		document.Form1.paginacao.value = codPag;
		submeter(1,1,0);
	}
</script>
<p class="Titulo" align="center">AVALIA��O DE TRABALHOS</p>
<form name="Form1" method="Post">
	<input type="hidden" name="h_ordem" value="<%=ordenacao_wk%>">
	<input type="hidden" name="paginacao">
	<input type="hidden" name="hcd_trabalho">
	<input type="hidden" name="hcd_modo">
	<%
	select case pr1_wk
		case 0		
		
		' Conectar com o banco
		%>
		<!--#include file=conn.asp -->
		<%
		Set Conn = Server.CreateObject("ADODB.Connection") 
		Conn.Open cnpath

		set rsD = server.CreateObject("ADODB.Recordset")
		set rsD = Conn.Execute("Select vl_descricao from TB_Config where cd_codigo = 5")
		tm_pagina_wk = cdbl(rsD("vl_descricao"))
		%>
		
			<table BORDER="0" CELLSPACING="0" CELLPADDING="0" class=tabelaProgramacao>
				<tr>
					<td>
					<%
					' Recuperar vari�veis de formul�rio
					ordenacao_wk = request("h_ordem")
					if ordenacao_wk = "" then ordenacao_wk = 1
							
					filtroNome_wk = request("filtroNome")
					filtroTitle_wk = request("filtroTitle")
					filtroModo_wk = request("filtroModo")

					select case pr1_wk
						case 0
							' Tratamento de Paginacao
							paginacao_wk = request("paginacao")
							if paginacao_wk = "" then paginacao_wk = 0
								
							select case paginacao_wk
								case 0
									session("paginaAtual")=1
								case 1
									session("paginaAtual")=1
								case 2
									if session("paginaAtual") > 1 then
										session("paginaAtual") = session("paginaAtual") - 1
									end if
								case 3
									if session("paginaAtual") < session("tp") then
										session("paginaAtual") = session("paginaAtual") + 1
									end if			
								case 4
									session("paginaAtual") = session("tp")
							end select
							
							' Criar o recordSet
							set rsT = server.CreateObject("ADODB.RecordSet")	
								
							sqlStr = "SELECT t.*, ta.dt_avaliacao, ta.vl_media, ta.cd_aprovacao FROM (TB_Trabalhos t inner join" _
							& " TB_TrabalhoAvaliadores ta on t.cd_trabalho = ta.cd_trabalho)" _
							& " WHERE ta.cd_avaliador = " & session("cd_avaliador") _
							& " ORDER BY " & ordenacao_wk & ";"

							'Response.Write  sqlStr
							'Response.Flush
							
							' pagina��o
							rsT.PageSize = tm_pagina_wk
							rsT.CacheSize = 5
							rsT.Open sqlStr, Conn, adOpenKeyset
							totalRegistros_wk = rsT.recordcount
							totalPaginas_wk = CInt(rsT.PageCount)
							tamanhoPagina_wk = rsT.PageSize
							session("tp") = totalPaginas_wk
							reg=0	
								
							%>
							<table width="100%" border="0">
								<tr class="CelulaTitulo">
									<b>
										<td width="100%" colspan="4" align=center>Carteira de Trabalhos para <%=session("nm_avaliador")%></td>
									</b>
								</tr>
							<% if rsT.RecordCount = 0 then %>
								<tr><td align=center><h5>Nenhum Trabalho encontrado.</h5></td><tr>
							<% else %>
								<% ' linha de t�tulo %>
								<tr class="CelulaCorpo">
									<b>
									<td width="5%">&nbsp;</td>
									<td width="5%"><a href="javascript:ordenar(0,1)">C�d.</a></td>
									<td width="85%"><a href="javascript:ordenar(0,11)">Titulo</a></td>
									<td width="5%" align="left">A��es</td>
									</b>
								</tr>
								<% ' linhas de detalhes %>
								<%
								flagLinha_wk = 1
								rsT.AbsolutePage = Cint(session("paginaAtual"))			
								while ((not rsT.EOF) and (reg < tamanhoPagina_wk))
									if flagLinha_wk then 
										nomeClasse = "txtTabelaLida"
									else	
										nomeClasse = "txtTabela"
									end if
									flagLinha_wk = abs(flagLinha_wk - 1)
									select case rsT("cd_modo")
										case 1
											strImg = "CCC"
											strCat = "Caso Cl�nico Consultivo"
										case 2
											strImg = "PP "
											strCat = "Painel de Pesquisa"
										case 3
											strImg = "PC"
											strCat = "Painel Consultivo"
										case 4
											strImg = "O  "
											strCat = "Apresenta��o Oral de Caso Cl�nico"
									end select

									strClasse = "NaoAvaliado"
									strAceito = "Aguardando Avalia��o."
									strNota = "<BR><BR>(Aguardando avalia��o)"
									sn_avaliacao = false
									if rsT("dt_avaliacao") < now() then
										select case rsT("cd_aprovacao")
											case 1
												strAprovacao = "Aprova��o"
											case 2
												strAprovacao = "Altera��o para Painel"
											case 3
												strAprovacao = "Exclus�o"
										end select
										strCarinha = "parc_vd.gif"
										strClasse = "Avaliado"
										strAceito = "Avaliado."
										strNota = "<BR><br><b>M�DIA: " & formatnumber(rsT("vl_media"),2) & "<b>" _
										& " - Recomenda��o: <b>" & strAprovacao & "</b>"
									end if
									%>
									<tr class="CelulaCorpo">
										<td width="100%" colspan="4"><hr></td>
									</tr>
									<tr class="CelulaCorpo">
										<td width="5%"><strong><%=strImg%></strong></td>
										<td width="5%" align="center" class="<%=strClasse%>"><%=rsT("cd_trabalho")%></td>
										<td width="85%" class="<%=strClasse%>"><%=Ucase(rsT("de_titulo"))%><%=strNota%></td>
										<td width="5%" align="center">
											<img src="../images/ico_lupa.gif" border="0" onclick="visualizar(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Visualizar Trabalho" WIDTH="22" HEIGHT="15">
										</td>
								</tr>
								<%
									rsT.MoveNext
									reg = reg + 1
								wend
							end if 
							%>
								<tr class="CelulaTitulo">
									<b>
										<td width="100%" colspan="4">&nbsp;</td>
									</b>
								</tr>
							</table>

							<%
							' Barra de Pagina��o	%>
							<table width="100%">
								<tr class="CelulaCorpo" valign="middle">
									<td width="25%">
										P�gina <%=session("paginaAtual")%> de <%=totalPaginas_wk%>
									</td>
									<td width="25%">
										Registros : <%=rsT.RecordCount%>
									</td>
									<td width="50%" align="right">
										<% 
										if session("paginaAtual") > 1 then %>
											<a href="javascript:paginar(1);"><img src="../images/Ico_PrimeiraOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(2);"><img src="../images/Ico_AnteriorOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_PrimeiraOff.gif" border="0" WIDTH="12" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_AnteriorOff.gif" border="0" WIDTH="8" HEIGHT="15">
										<% 
										end if %>
										&nbsp;
										<% 
										if session("paginaAtual") < session("tp") then %>
											<a href="javascript:paginar(3);"><img src="../images/Ico_ProximaOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(4);"><img src="../images/Ico_UltimaOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_ProximaOff.gif" border="0" WIDTH="8" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_Ultimaoff.gif" border="0" WIDTH="12" HEIGHT="15">
										<% 
										end if %>
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			<%
			end select
			set rsT = nothing
			set rsD = nothing
			set rsCat = nothing
			set Conn = nothing
		case 1

			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
			
			strSQL = "select * from TB_Trabalhos " _
			& " where cd_trabalho = " & request("hcd_trabalho")

			set rsT = server.CreateObject("ADODB.Recordset")
			set rsT = Conn.Execute(strSQL)
			cd_trabalho_wk = rsT("cd_trabalho")
			cd_modo_wk = rsT("cd_modo")

			strSQL = "select * from TB_TrabalhoAvaliadores " _
			& " where cd_trabalho = " & request("hcd_trabalho") _
			& " and cd_avaliador = " & session("cd_avaliador")

			set rsA = server.CreateObject("ADODB.Recordset")
			set rsA = Conn.Execute(strSQL)

			justificativaA_wk = ""
			justificativaE_wk = ""
			select case rsA("cd_aprovacao")
				case 2
					justificativaA_wk = rsA("de_justificativa")
					justificativaE_wk = ""
				case 3
					justificativaA_wk = ""
					justificativaE_wk = rsA("de_justificativa")
			end select

			select case rsT("cd_modo")
				case 1
					modo_wk = "Caso Cl�nico Consultivo"
					recomendo1_wk = "Aprova��o"
					recomendo2_wk = ""
					recomendo3_wk = "Exclus�o"
					quesito01 = "As normas de apresenta��o do resumo foram cumpridas ?"
					quesito02 = "O resumo cont�m os itens necess�rios para que se possa fazer a avalia��o do trabalho, e est� pronto para publica��o nos anais do congresso ? (sugest�es no fim da ficha)"
					quesito03 = "O caso est� claramente descrito ?"
					quesito04 = "As possibilidades de diagn�stico foram levantadas, explorando de forma suficiente as informa��es dispon�veis no momento ?"
					quesito05 = "As dificuldades no diagn�stico e/ou manejo do caso foram apontadas ?"
					quesito06 = "As refer�ncias bibliogr�ficas levantadas s�o adequadas e satisfat�rias ?"
					quesito07 = "Os exames laboratoriais, imaginol�gicos e outros foram adequadamente indicados, explorados e interpretados no intuito de elucidar o caso cl�nico ?"
					quesito08 = "As imagens apresentadas apresentam boa qualidade, s�o relevantes e suficientes para ilustrar o caso ?"
					quesito09 = "Relev�ncia da apresenta��o do trabalho no Congresso."
					quesito10 = ""
				case 2
					modo_wk = "Painel de Pesquisa"
					recomendo1_wk = "Aprova��o"
					recomendo2_wk = ""
					recomendo3_wk = "Exclus�o"
					quesito01 = "As normas de apresenta��o do resumo foram cumpridas ?  (deve conter: Introdu��o, Justificativa, Objetivos, Metodologia, Resultados, Conclus�es e Bibliografia)."
					quesito02 = "O t�tulo do trabalho est� adequado e claro ?"
					quesito03 = "O resumo cont�m os itens necess�rios para que se possa fazer a avalia��o da validade, import�ncia e aplicabilidade do trabalho ? Est� pronto para publica��o nos anais do congresso ? (sugest�es no fim da ficha)"
					quesito04 = "A metodologia est� adequadamente descrita ?"
					quesito05 = "Os resultados do estudo s�o v�lidos ?"
					quesito06 = "A discuss�o/conclus�o est� baseada em evid�ncias ou em opini�o pessoal ?"
					quesito07 = "Os resultados suportam as conclus�es ?"
					quesito08 = "As refer�ncias bibliogr�ficas s�o adequadas e satisfat�rias ?"
					quesito09 = "As palavras-chave est�o coerentes e em bom n�mero ?"
					quesito10 = "Relev�ncia cient�fica do trabalho."
				case 3
					modo_wk = "Painel Cl�nico"
					recomendo1_wk = "Aprova��o"
					recomendo2_wk = ""
					recomendo3_wk = "Exclus�o"
					quesito01 = "As normas de apresenta��o do resumo foram cumpridas ?"
					quesito02 = "O t�tulo do trabalho est� adequado e claro ?"
					quesito03 = "O resumo cont�m os itens necess�rios para que se possa fazer a avalia��o do trabalho, e est� pronto para publica��o nos anais do congresso? (sugest�es no fim da ficha)"
					quesito04 = "O caso cl�nico est� claramente descrito ?"
					quesito05 = "Os m�todos de diagn�stico / tratamento est�o adequadamente aplicados e descritos ?"
					quesito06 = "O desfecho do caso foi especificado ? "
					quesito07 = "O caso apresenta acompanhamento cl�nico ?"
					quesito08 = "As refer�ncias bibliogr�ficas s�o adequadas e satisfat�rias ?"
					quesito09 = "As palavras-chave est�o coerentes e em bom n�mero ?"
					quesito10 = "Relev�ncia da apresenta��o do trabalho no Congresso."
				case 4
					modo_wk = "Apresenta��o Oral Caso Cl�nico"
					recomendo1_wk = "Aprova��o para Apresenta��o Oral"
					recomendo2_wk = "Altera��o para Painel"
					recomendo3_wk = "Exclus�o"
					quesito01 = "As normas de apresenta��o do resumo foram cumpridas ?"
					quesito02 = "O t�tulo do trabalho est� adequado e claro ?"
					quesito03 = "O resumo cont�m os itens necess�rios para que se possa fazer a avalia��o do trabalho ?"
					quesito04 = "O caso cl�nico est� claramente descrito ? "
					quesito05 = "Os m�todos de diagn�stico est�o adequadamente aplicados e descritos ?"
					quesito06 = "O desfecho do caso foi especificado, ou seja, o caso foi conclu�do? Apresenta acompanhamento cl�nico ?"
					quesito07 = "As refer�ncias bibliogr�ficas s�o adequadas e satisfat�rias ?"
					quesito08 = "As palavras-chave est�o coerentes e em bom n�mero ?"
					quesito09 = "As imagens apresentadas apresentam boa qualidade, s�o relevantes e suficientes para ilustrar o caso ?"
					quesito10 = "Relev�ncia da apresenta��o do trabalho no Congresso."
			end select
			%>
			<script>
				function abreFechaFormAvaliacao(){
					if (document.Form1.statusForm.value == 0) {
						document.getElementById("form_avaliacao").style.display = 'inline';
						document.getElementById("linkAbreFecha").innerText = "Fechar o Formul�rio de Avalia��o";
						document.Form1.statusForm.value = 1;
					}
					else {
						document.getElementById("form_avaliacao").style.display = 'none';
						document.getElementById("linkAbreFecha").innerText = "Abrir o Formul�rio de Avalia��o";
						document.Form1.statusForm.value = 0;
					}
				}
				
				function calculaMediaAvaliacao(){

					q1 = document.Form1.q1.value;
					if (!(isFinite(q1))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."');
						document.Form1.q1.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q1)) 
							document.Form1.q1.focus();
					
					q2 = document.Form1.q2.value;
					if (!(isFinite(q2))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
						document.Form1.q2.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q2)) 
							document.Form1.q2.focus();
					
					q3 = document.Form1.q3.value;
					if (!(isFinite(q3))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
						document.Form1.q3.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q3)) 
							document.Form1.q3.focus();

					q4 = document.Form1.q4.value;
					if (!(isFinite(q4))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."');
						document.Form1.q4.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q4)) 
							document.Form1.q4.focus();
					
					q5 = document.Form1.q5.value;
					if (!(isFinite(q5))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
						document.Form1.q5.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q5)) 
							document.Form1.q5.focus();
					
					q6 = document.Form1.q6.value;
					if (!(isFinite(q6))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
						document.Form1.q6.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q6)) 
							document.Form1.q6.focus();

					q7 = document.Form1.q7.value;
					if (!(isFinite(q7))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."');
						document.Form1.q7.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q7)) 
							document.Form1.q7.focus();
					
					q8 = document.Form1.q8.value;
					if (!(isFinite(q8))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
						document.Form1.q8.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q8)) 
							document.Form1.q8.focus();
					
					q9 = document.Form1.q9.value;
					if (!(isFinite(q9))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
						document.Form1.q9.focus();
						return false;
					}
					else
						if (!entreUm_e_Dez(q9)) 
							document.Form1.q9.focus();
							
							
					q10=0;
					quesitos = 9;
					<% if rsT("cd_modo") <> 1 then %>
					quesitos = 10;
					q10 = document.Form1.q10.value;
					if (!(isFinite(q10))){
						alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."');
						document.Form1.q10.focus();
						return false;
					}
					else 
						if (!entreUm_e_Dez(q10)) 
							document.Form1.q10.focus();
					<% end if %>
				}
				
				function entreUm_e_Dez(q){
					if (q != '')
						if ((q < 1)||(q>10)){
							alert('O valor deve estar entre 1 e 10');
							return false;
						}
					return true;
				}
				
				function painelAvaliacao(modo, recomendacao){
					switch(modo){
						case 1:
							switch(recomendacao){
								case 1:
									document.getElementById('justificativa1').style.display='none';
									break;
								case 3:
									document.getElementById('justificativa1').style.display='inline';
									break;
							}
							break;
						case 2:
						case 3:
							switch(recomendacao){
								case 1:
									document.getElementById('justificativa1').style.display='none';
									break;
								case 3:
									document.getElementById('justificativa1').style.display='inline';
									break;
							}
							break;
						case 4:
							switch(recomendacao){
								case 1:
									document.getElementById('justificativa1').style.display='none';
									document.getElementById('justificativa2').style.display='none';
									break;
								case 2:
									document.getElementById('justificativa1').style.display='none';
									document.getElementById('justificativa2').style.display='inline';
									break;
								case 3:
									document.getElementById('justificativa1').style.display='inline';
									document.getElementById('justificativa2').style.display='none';
									break;
							}
							break;
					}
				}
				
				function registrarAvaliacao(trabalho, modo){
					document.Form1.hcd_trabalho.value = trabalho;
					document.Form1.hcd_modo.value = modo;
					submeter(1,1,2);
				}
			</script>
			<table class="TabelaProgramacao" width="100%" border=0>
				<tr class="CelulaTitulo">
					<td colspan=2 width=100%>
						<table width="100%" border=0>
							<tr class="CelulaTitulo">
								<td width=30% align="left">
									TRABALHO <%=cd_trabalho_wk%>
								</td>
								<td width=70% align="right">
									<a id=linkAbreFecha href="javascript:abreFechaFormAvaliacao();">Abrir o Formul�rio de Avalia��o</a>
									<input type=hidden name=statusForm value=0>
								</td>
							</tr>
						</table>
					</td>
				<tr>
				
				<tr id=form_avaliacao class="CelulaCorpo" style="display='none';">
					<td>
						<hr>
						<center>
						<b>Formul�rio de Avalia��o de <%=modo_wk%></b><br><br>
						Pontue com valores de 1 a 10 para cada t�pico avaliado
						</center>
						<br>
						<table width=100% border=0>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito01%>
								</td>
								<td>
									<input type=textbox name=q1 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q1"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito02%>
								</td>
								<td>
									<input type=textbox name=q2 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q2"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito03%>
								</td>
								<td>
									<input type=textbox name=q3 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q3"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito04%>
								</td>
								<td>
									<input type=textbox name=q4 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q4"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito05%>
								</td>
								<td>
									<input type=textbox name=q5 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q5"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito06%>
								</td>
								<td>
									<input type=textbox name=q6 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q6"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito07%>
								</td>
								<td>
									<input type=textbox name=q7 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q7"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito08%>
								</td>
								<td>
									<input type=textbox name=q8 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q8"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td>
									<%=quesito09%>
								</td>
								<td>
									<input type=textbox name=q9 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q9"))%> onkeyup="calculaMediaAvaliacao();">
								</td>
							</tr>
							<% if (rsT("cd_modo") <> 1) then %>
								<tr class="CelulaCorpoCinza">
									<td>
										<%=quesito10%>
									</td>
									<td>
										<input type=textbox name=q10 size=5 class=textbox02 value=<%=testaZero(rsA("vl_q10"))%> onkeyup="calculaMediaAvaliacao();">
									</td>
								</tr>
							<% end if %>
							<tr class="CelulaCorpo">
								<td colspan=2>
									<b>AVALIA��O</b>
								</td>
							</tr>
							<tr class="CelulaCorpoCinza">
								<td colspan=2 width=100%>
									<% 
									if (rsT("cd_modo") = 1) then 
										%>
										<br>
										<input type=checkbox <%if rsA("sn_questoesSociais") then%>checked <%end if%>name=questoesSociais value="ON">&nbsp;
										Quest�es sociais (falta de recursos financeiros do paciente ou dos servi�os de Estomatologia) e culturais podem ter prejudicado o andamento do caso ?
										<br>
										<br>
									<% end if %>
									Recomendo:&nbsp;&nbsp;<br>
									<input type=radio <%if rsA("cd_aprovacao") = 1 then%>checked<%end if%> name=aprova class=textbox01 value=1 onclick="painelAvaliacao(<%=rsT("cd_modo")%>,1);">&nbsp;<%=recomendo1_wk%><br>

									<% if rsT("cd_modo") = 4 then
										strChecked = ""
										strDisplay = "none"
										if rsA("cd_aprovacao") = 2 then
											strChecked = "checked"
											strDisplay = "inline"
										end if
										%>
										<input type=radio  <%=strChecked%> name=aprova class=textbox01 value=2 onclick="painelAvaliacao(<%=rsT("cd_modo")%>,2);">&nbsp;<%=recomendo2_wk%><br>
   										<span id=justificativa2 style="display='<%=strDisplay%>';">
											Justificativa para a Altera��o de Categoria:
											<textarea name=justificativaAlteracao cols=75 rows=3 class=textarea02><%=justificativaA_wk%></textarea>
											<br>
										</span>
									<% end if %>
									
									<%
									strChecked = ""
									strDisplay = "none"
									if rsA("cd_aprovacao") = 3 then
										strChecked = "checked"
										strDisplay = "inline"
									end if
									%>
									<input type=radio <%=strChecked%> name=aprova class=textbox01 value=3 onclick="painelAvaliacao(<%=rsT("cd_modo")%>,3);">&nbsp;<%=recomendo3_wk%><br>
									
									<span id=justificativa1 style="display='<%=strDisplay%>';">
										Justificativa para a Exclus�o:
										<textarea name=justificativaExclusao cols=75 rows=3 class=textarea02><%=justificativaE_wk%></textarea>
										<br>
									</span>
									<% if (rsT("cd_modo") <> 4) then %>
										<br>Observa��es / Sugest�es:<br>
										<textarea name=Observacoes cols=75 rows=3 class=textarea02><%=rsA("de_observacoes")%></textarea>
									<% end if %>
								</td>
							</tr>
						</table>
						<br>
						<input class=botaoPadrao120 type=button name=registraNota value="Registrar Avalia��o" onclick="registrarAvaliacao(<%=cd_trabalho_wk%>, <%=cd_modo_wk%>);">
						<hr>
					</td>
				</tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>T�tulo :&nbsp;</b><br>
						<%=replace(rsT("de_titulo"),chr(10),"<br>")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>Resumo :&nbsp;</b><br>
						<%=rsT("de_sumario")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>Palavras Chaves :&nbsp;</b><br>
						<%=rsT("de_palavrasChaves")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>Refer�ncias Bibliogr�ficas :&nbsp;</b><br>
						<%=rsT("de_referencias")%>
					</td>
				<tr>

				<% 
				if rsT("cd_modo") = 1 then
					%>
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Exames imaginol�gicos :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Exames anatomopatol�gicos :&nbsp;</b><br>
							<%=rsT("de_anatomopatologico")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Outros exames complementares :&nbsp;</b><br>
							<%=rsT("de_outrosExames")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Informa��es sobre o tratamento :&nbsp;</b><br>
							<%=rsT("de_infoTratamento")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Informa��es sobre seguimento :&nbsp;</b><br>
							<%=rsT("de_infoSeguimento")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Diagn�stico ou hip�teses diagn�sticas :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					<%
					select case cint(rsT("cd_dificuldade"))
						case 1
							strDificuldade_wk = "No diagn�stico"
						case 2
							strDificuldade_wk = "No tratamento"
						case 3
							strDificuldade_wk = "Em Ambos (diagn�stico e tratamento)"
					end select
					%>
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Dificuldade :&nbsp;</b><br>
							<%=strDificuldade_wk%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Relev�ncia da apresenta��o do trabalho no congresso :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					
					<%
				end if
				%>

				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<hr>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" colspan="2">
						<table width="100%">
							<tr class="CelulaCorpo">
								<td width="40%" align="center">
									<b>Modo</b><br>
									<%=modo_wk%>
								</td>
								<td width="60%" align="left" >
									<% 
									if rsT("cd_modo") = 4 then 
										%>
										<b>Retirada</b><br>
										<%
										if rsT("cd_naoOral") = 1 then
											%>Desejo retirar o trabalho<br>caso n�o seja aprovado<%
										else
											%>Caso n�o seja aprovado desejo<br>inscrever este trabalho como<br>Painel Consultivo<%
										end if
									end if
									%>
								</td>
							<tr>
						</table>
					</td>
				<tr>
				<% if ((rsT("cd_modo") = 1) or (rsT("cd_modo") = 4))  then  %>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<hr><b>Imagens</b><br><br>
						<%
						strSQL = "select de_prefixoImagens from TB_Trabalhos where cd_trabalho = " & request("hcd_trabalho")
						set rs = Conn.Execute(strSQL)
							
						strSQL = "select * from TB_ImagensTrabalhos where cd_trabalho = " & rs("de_prefixoImagens") & " order by nu_seqImagem"
						
						set rs = Conn.Execute(strSQL)
						
						if not rs.eof then
							while not rs.EOF
								%>
								<img src="http://www.tavares.eti.br/upload_admin/estomatologia/<%=rs("cd_trabalho")%>_<%=rs("de_nomeArquivo")%>">
								<br>
								<%=rs("de_legenda")%>
								<hr>
								<%
								rs.MoveNext
							wend
						else
							Response.Write "<br>Nenhuma Imagem encontrada para este trabalho."
						end if
						%>
					</td>
				<tr>
				<% end if %>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="2">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
					</td>
				<tr>
			<%
		case 2
			' Atribui nota
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			cd_trabalho_wk = request("hcd_trabalho")
			cd_modo_wk = request("hcd_modo")
			
			'Response.write "atribuir avaliacao " & session("cd_avaliador") & " - " & cd_trabalho_wk & "<BR><BR>"
			
			vl_q1_wk = Request("q1")
			if vl_q1_wk = "" then vl_q1_wk = 0

			vl_q2_wk = Request("q2")
			if vl_q2_wk = "" then vl_q2_wk = 0

			vl_q3_wk = Request("q3")
			if vl_q3_wk = "" then vl_q3_wk = 0

			vl_q4_wk = Request("q4")
			if vl_q4_wk = "" then vl_q4_wk = 0

			vl_q5_wk = Request("q5")
			if vl_q5_wk = "" then vl_q5_wk = 0

			vl_q6_wk = Request("q6")
			if vl_q6_wk = "" then vl_q6_wk = 0

			vl_q7_wk = Request("q7")
			if vl_q7_wk = "" then vl_q7_wk = 0

			vl_q8_wk = Request("q8")
			if vl_q8_wk = "" then vl_q8_wk = 0

			vl_q9_wk = Request("q9")
			if vl_q9_wk = "" then vl_q9_wk = 0

			vl_q10_wk = Request("q10")
			if vl_q10_wk = "" then vl_q10_wk = 0

			justificativaAlteracao_wk = Request("justificativaAlteracao")
			if justificativaAlteracao_wk = "" then justificativaAlteracao_wk = "-"
			
			justificativaExclusao_wk = Request("justificativaExclusao")
			if justificativaExclusao_wk = "" then justificativaExclusao_wk = "-"
			
			observacoes_wk = Request("observacoes")
			if observacoes_wk = "" then observacoes_wk = "-"
			
			aprova_wk = Request("aprova")
			
			justificativa_wk = ""
			if aprova_wk = 1 then
				justificativa_wk = justificativaAlteracao_wk
			else
				if aprova_wk = 3 then
					justificativa_wk = justificativaExclusao_wk
				end if
			end if
			
			questoesSociais_wk = request("questoesSociais")

			if questoesSociais_wk = "on" then 
				sn_questoesSociais_wk = 1
			else
				sn_questoesSociais_wk = 0
			end if
			
			qt_quesitos = 10
			if cd_modo_wk = 1 then qt_quesitos = 9
			
			vl_media_wk = CDbl(replace(vl_q1_wk,".",",")) + CDbl(replace(vl_q2_wk,".",",")) + CDbl(replace(vl_q3_wk,".",",")) + CDbl(replace(vl_q4_wk,".",",")) + CDbl(replace(vl_q5_wk,".",",")) + CDbl(replace(vl_q6_wk,".",",")) + CDbl(replace(vl_q7_wk,".",",")) + CDbl(replace(vl_q8_wk,".",",")) + CDbl(replace(vl_q9_wk,".",",")) + CDbl(replace(vl_q10_wk,".",","))
			vl_media_wk = vl_media_wk / qt_quesitos
			
			if false then
			Response.Write " 1: " & CDbl(vl_q1_wk) & "<BR>"
			Response.Write " 2: " & CDbl(vl_q2_wk) & "<BR>"
			Response.Write " 3: " & CDbl(vl_q3_wk) & "<BR>"
			Response.Write " 4: " & CDbl(vl_q4_wk) & "<BR>"
			Response.Write " 5: " & CDbl(vl_q5_wk) & "<BR>"
			Response.Write " 6: " & CDbl(vl_q6_wk) & "<BR>"
			Response.Write " 7: " & CDbl(vl_q7_wk) & "<BR>"
			Response.Write " 8: " & CDbl(vl_q8_wk) & "<BR>"
			Response.Write " 9: " & CDbl(vl_q9_wk) & "<BR>"
			Response.Write "10: " & CDbl(vl_q10_wk) & "<BR><BR>"

			Response.Write " M: " & vl_media_wk & "<BR><BR>"

			Response.Write "JA: " & justificativaAlteracao_wk & "<BR><BR>"
			Response.Write "JE: " & justificativaExclusao_wk & "<BR><BR>"
			Response.Write "OB: " & observacoes_wk & "<BR><BR>"
			Response.Write "QS: " & questoesSociais_wk & "<BR><BR>"
			Response.Write "AP: " & aprova_wk & "<BR><BR>"
			end if
			
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			on error resume next
		
			set rsD = server.CreateObject("ADODB.Recordset")
			
			strSQL = "update TB_TrabalhoAvaliadores " _
			& " set vl_q1 = " & vl_q1_wk _
			& ", vl_q2 = " & vl_q2_wk _
			& ", vl_q3 = " & vl_q3_wk _
			& ", vl_q4 = " & vl_q4_wk _
			& ", vl_q5 = " & vl_q5_wk _
			& ", vl_q6 = " & vl_q6_wk _
			& ", vl_q7 = " & vl_q7_wk _
			& ", vl_q8 = " & vl_q8_wk _
			& ", vl_q9 = " & vl_q9_wk _
			& ", vl_q10 = " & vl_q10_wk _
			& ", vl_media = " & replace(vl_media_wk, ",", ".") _
			& ", cd_aprovacao = " & aprova_wk _
			& ", sn_questoesSociais = " & sn_questoesSociais_wk _
			& ", de_justificativa = '" & justificativa_wk _
			& "', de_observacoes = '" & observacoes_wk _
			& "', dt_avaliacao = '" & now() & "'" _
			& " where cd_trabalho = " & cd_trabalho_wk _
			& " and cd_avaliador = " & session("cd_avaliador")
			
			'if session("cd_avaliador") = 23 then
			'	Response.Write strSQL
			'	Response.Flush
			'end if
			
			set rsD = Conn.Execute(strSQL)
			on error goto 0
			%>
			<table class="TabelaProgramacao" width="564">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						AVALIA��O DO TRABALHO N� <%=cd_trabalho_wk%>
					</td>
				<tr>
			<%
			if err.Number = 0 then
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Avalia��o conclu�da com sucesso !</h5>
					</td>
				<tr>
				<%
			else
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Problema no registro da avalia��o !</h5>
					<%
					Response.Write "Erro N� " & err.number & "<br>"
					Response.Write err.description
					%>
					</td>
				<tr>
				<%
			end if
			%>
			<tr class="CelulaTitulo">
				<td align="middle" colspan="3">
					<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
				</td>
			<tr>
			<%
		end select
		%>
	</table>
</form>