<p class="Titulo" align="center">FLORIAN�POLIS</p>

<script>
	function ativaDefinicao(elemento){
		elemento.style.cursor='hand';
		elemento.style.color='#FFD359';
		elemento.style.fontWeight = 700;
	}

	function ativaTabela(id){
		for  (i=1; i<=7; i++) {
			tabela='tab' + i
			document.getElementById(tabela).style.display = "none";
		}
		
		tabela='tab' + id
		document.getElementById(tabela).style.display = "inline";

		document.getElementById("hr1").style.display = "inline";
		document.getElementById("imgPonte").style.display = "none";
	}

	function desativaDefinicao(elemento){
		elemento.style.cursor='default'; 
		elemento.style.color='#000000';
		elemento.style.fontWeight = 700;
	}

	function desativaTabela(id){
		tabela='tab' + id
		document.getElementById(tabela).style.display = "none";
	}

	function abrirJanelaNews(URL,nomeJanela,props){
		window.open(URL,nomeJanela,props);
	}
		 
	function ampliarFoto(nm_foto, de_foto,cd_idioma){
		url = 'ampliaFoto.asp?nm_foto='+nm_foto+'&de_foto='+de_foto+'&cd_idioma='+cd_idioma
		id = 'Imagem';
		params = 'scrollbars=no,width=546,height=396';
		abrirJanelaNews(url,id,params);
	}
</script>

<table class="Visivel" border="0" cellpadding="5" cellspacing="0" WIDTH="100%">
	<tr>
		<td align="middle" class="textocomum">
		  <b>
			<span id="sp1" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(1);">FLORIAN�POLIS</span>&nbsp;&#149;&nbsp;

			<span id="sp2" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(2);">ILHA DE SANTA CATARINA</span>&nbsp;&#149;&nbsp;

			<span id="sp3" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(3);">HIST�RIA</span><br><br>
			
			<span id="sp4" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(4);">PRAIAS</span>&nbsp;&#149;&nbsp;

			<span id="sp5" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(5);">CONTINENTE</span>&nbsp;&#149;&nbsp;

			<span id="sp6" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(6);">FORTALEZAS</span>&nbsp;&#149;&nbsp;

			<span id="sp7" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(7);">CULIN�RIA</span>
		  </b>
		</td>
	</tr>
</table>

<p align="center">
	<img src="images/PontesVistaAerea.jpg" id="imgPonte" WIDTH="408" HEIGHT="227">
</p>

<hr id="hr1" style="DISPLAY: none">

<table style="DISPLAY: none" border="0" id="tab1">
	<tr>
		<td class="textoComum">
			<p>
				<b>FLORIAN�POLIS � SOL, MAR, TRADI��O E HIST�RIA</b>
				<br>
			</p>
			<p>
				Seus primeiros habitantes foram os �ndios tupis guaranis, que lhe deram as primeiras denomina��es: Miembipe e Jurer� Mirim. O primeiro a aportar nela foi o navegador espanhol Cabeza de Vaca em 1541. Os bandeirantes chegaram mais de um s�culo depois com Francisco Dias Velho, que fundou a povoa��o de Nossa Senhora do Desterro. Para mant�-la, o governo portugu�s enviou para ela, de 1748 � 1756, cerca de 5 mil colonos sa�dos das ilhas dos A�ores e Madeira. 
			</p>
			<p>
				Em 1894, numa homenagem ao marechal Floriano Peixoto, a cidade recebeu a atual denomina��o &#150; Florian�polis, a musa de milhares de turistas atra�dos n�o apenas pela hist�ria, mas tamb�m pelos atributos naturais da Ilha de Santa Catarina, onde fica localizada a maior parte da cidade. Com 370 mil habitantes, Florian�polis � a �nica capital de Estado brasileiro cuja popula��o � sobrepujada, numericamente, por outra do interior, Joinville, com cerca de 440 mil habitantes.
			</p>
			<p>
				Riqueza e fartura s�o uma esp�cie de marca de Florian�polis quando o assunto � praia. Com 42 , algumas selvagens, outras de beleza incomum e voltadas para o turismo sofisticado e internacional, algumas de �guas quentes e calmas, outras agitadas e frias. 
			</p>
			<p>
				Entre as mais conhecidas est�o Canasvieiras, a mais internacionalizada, preferida pelos argentinos e uruguaios; Ribeir�o da Ilha, onde a heran�a a�oriana � vista nas casas, igrejas e rostos; Lagoa da Concei��o, um de seus mais reverenciados cart�es postais; Joaquina, o palco do surf do centro sul brasileiro; e a badalada Jurer�, not�vel pelos esportes n�uticos e vida noturna.
			</p>
			<p>
			<br>
			<p>
				<b>CAPITAL DA DIVERSIDADE</b>
			</p>
			<p>
				O visitante que chega � Florian�polis em qualquer �poca do ano depara-se com um intrigante problema: o excesso de diversidade. Por isso o bom turista deve eleger algumas prioridades. Se a oferta de praias pode ser considerada exagerada, h� tamb�m hist�ria, como as v�rias fortalezas que cercam a Ilha de Santa Catarina. Elas n�o passam despercebidas por ningu�m. Algumas restauradas, outras em ru�nas, s�o um espet�culo � parte e representam hoje o mais significativo patrim�nio da Marinha brasileira em todo o Atl�ntico.
			</p>
			<p>
				Para defender Florian�polis de invas�es, Portugal edificou v�rios fortes em locais estrat�gicos para controlar o acesso pelo norte e sul � vila do Desterro. De 1738 � 1761, foram constru�das cinco fortalezas. A de S�o Jos� de Ponta Grossa foi a primeira a ser erguida. A mais imponente � a de Anhatomirim, onde foram investidos mais de US$ 1 milh�o na sua recupera��o.
			</p>
			<p>
				V�rios outros monumentos est�o incorporados � hist�ria de Florian�polis. Um dos principais � a ponte Hercilio Luz, por d�cadas tido como principal cart�o postal da cidade, interditada ao tr�fego por estar com suas estruturas avariadas. Com 819 metros de extens�o, foi inaugurada em 1926, com duas torres de 75 metros de altura. � um raro exemplar de ponte p�nsil existente no mundo.
			</p>
			<p>
				Outro ponto que merece visita��o � a Catedral Metropolitana, localizada na parte mais central da cidade e no mesmo local onde Francisco Dias Velho construiu, em 1675, uma capela em homenagem � Nossa Senhora do Desterro. Nela, foi morto num ataque de piratas. Posteriormente foi substitu�da por uma pequena igreja que mais tarde transformou-se em catedral. Distante poucos metros fica o Pal�cio Cruz e Sousa, constru�do no s�culo XVIII, inicialmente utilizado como resid�ncia e local de trabalho do Presidente da Prov�ncia, passando a abrigar os governadores de Santa Catarina at� a metade deste s�culo. Hoje sedia o Museu Hist�rico de Santa Catarina.
			</p>
			<p>
				O pr�dio da Alf�ndega � outro belo exemplar da hist�ria de Florian�polis. Foi constru�do entre 1875 e 1876, com uma arquitetura neo-cl�ssica, rica em detalhes. Nas proximidades da Alf�ndega fica o mais tradicional centro comercial da cidade &#150; o Mercado P�blico, com 140 boxes abrigando estabelecimentos que vendem produtos t�picos ( peixes, carnes, frutos do mar, m�veis em vime e lou�as de barro ), onde transitam cerca de 10 mil pessoas por dia. O pr�dio foi inaugurado em 1898. Al�m do centro da cidade, h� v�rios monumentos de import�ncia hist�rica e arquitet�nica no interior da Ilha, especialmente nas localidades do Ribeir�o da Ilha e Lagoa da Concei��o.
			</p>
			<p>
				� especialmente no ver�o que o visitante de Florian�polis pode ver algumas das mais significativas manifesta��es folcl�ricas da cidade, todos com forte influ�ncia a�oriana e espanhola, como o boi-de-mam�o.
			</p>
			<br><br>&nbsp;
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab2">
	<tr>
		<td class="textoComum">
			<p>
				<b>ILHA DE SANTA CATARINA: UM MUNDO A SER DESCOBERTO</b>
				<br>
			</p>
			<p>
				Dos seus 1.045 km2, a Ilha de Santa Catarina reserva muito o que descobrir. A p� pelas trilhas de mata fechada, a cavalo, de barco at� comunidades isoladas, e pelo c�u, de asa-delta ou &quot;paraglider&quot;.A paisagem nada mon�tona revela uma diversidade que enobrece o territ�rio das 42 praias mais encantadoras da regi�o sul. Tem plan�cies, dunas, lagoas, mangues, montanhas, vegeta��o atl�ntica nativa. Al�m disso, na sua vizinhan�a, existem outras 30 pequenas ilhas paradis�acas.
			</p>
			<p>
				Florian�polis, que ocupa toda a Ilha de Santa Catarina e mais um trecho do continente, guarda at� hoje tra�os dos colonizadores do s�culo XVIII. Na arquitetura , na pesca, na agricultura, na gastronomia e at� no jeit�o do seu povo. � uma terra desenhada para exploradores dos mais variados interesses. 
			</p>
			<p>
				As trilhas mais selvagens est�o no sul da ilha. As tr�s que levam � praia do Saquinho, partindo da praia da Solid�o, da Caieira da Barra do Sul ou de Naufragados, s�o �ngremes e exigem um certo f�lego, mas pagam o esfor�o ao revelar um peda�o imaculado da Mata Atl�ntica ou, para quem sai da praia da Solid�o, uma costa magnificamente recortada e enfeitada de gaivotas. No Saquinho, a praia em formato de concha � pouco habitada, uma casa aqui e outra acol�.
			</p>
			<p>
				Uma das mais espetaculares praias desertas da ilha, que os &quot;habitu�s&quot; fazem quest�o de manter no anonimato, � a Lagoinha do Leste. Suas �guas s�o um pouco agitadas e boas para o surf, sua areia � fina e fofa, e tem pouco mais de um quil�metro de extens�o. � cercada por um morro e h� uma lagoa perto da praia, com vegeta��o farta, em cujas margens os campistas costumam armar suas barracas. Pode-se chegar � Lagoinha do Leste a partir do P�ntano do Sul, depois de uma escalada de pouco mais de uma hora, ou pela trilha que sai da praia da Arma��o percorrendo o que os ilh�us chamam de &quot;cost�o&quot;, a costa irregular formada por grandes pedras. 
			</p>
			<p>
				Entre Arma��o e Lagoinha fica a mi�da praia de Matadeiro, com apenas 200 metros, que tem ondas melhores para o surf do que a agitada Joaquina, segundo muitos surfistas. Mas as trilhas nativas n�o revelam apenas praias. A que sai um pouco ao sul do centrinho do Ribeir�o da Ilha, passa pela Lagoa do Peri e termina na praia da Arma��o, oculta numa de suas vertentes o �ltimo engenho de cangalha ativo de Florian�polis. Por uma estradinha intercalada por meia d�zia de porteiras chega-se ao Engenho do Chico, na regi�o curiosamente denominada de Sert�o do Ribeir�o. As nascentes brotam por toda parte, n�o faltam pessegueiros e o pasto para o gado � de um verde que impressiona. Enfim, nada se parece com o sert�o de verdade. H� o engenho da farinha movido a boi, que segundo a sabedoria dos antigos m�i durante os meses que n�o t�m a letra &quot;r&quot; de maio a agosto . E h� o alambique, que produz por semana 500 litros da melhor cacha�a da Ilha. 
			</p>
			<p>
				Seguindo a mesma trilha, a 8 km do Engenho do Chico, est� o Parque Municipal da Lagoa do Peri, a maior reserva de �gua doce da Ilha de Santa Catarina, com 5 km2. O lugar � excelente para pescas e caminhadas. Caminhar nas trilhas antigas exige cuidados e muita aten��o. Apesar do esfor�o, s�o garantidos o suor e o prazer dos aventureiros.
			</p>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab3">
	<tr>
		<td class="textoComum">
			<p>
				<b>HIST�RICO DA OCUPA��O HUMANA DA ILHA DE SANTA CATARINA</b>
				<br>
			</p>
			<p>
				A constata��o de presen�a humana na Ilha de Santa Catarina pode ser considerada recente quando comparada � antiga civiliza��o amer�ndia, que t�m ossadas datando de 30 mil anos, aproximadamente. Assim, como em todo litoral catarinense, os vest�gios humanos mais antigos encontrados e catalogados na Ilha remontam para 5 mil anos de ocupa��o, sempre diretamente ligados com a cultura de sambaquis.
			</p>
			<p>
				Tamb�m conhecido como casqueiro, concheiro e berbigueiro, entre outros nomes, trata-se de um s�tio arqueol�gico que em sua origem guarani significa monte de conchas. Durante centenas ou milhares de anos os primitivos habitantes, que eram naturalmente dependentes da coleta de frutos do mar, iam acumulando em locais apropriados, os restos e cascas de moluscos. Estes montes cresciam tanto a cada gera��o que passaram a ser um local bastante apropriado para suas habita��es. Al�m da proximidade do mar, os sambaquis eram locais secos e seguros.
			</p>
			<p>
				At� o ano de 1989 haviam sido registrados 120 sambaquis s� na Ilha de Santa Catarina em pesquisas arqueol�gicas realizadas em 20% dos s�tios existentes. Por�m sabe-se que muitos deles foram destru�dos pela ocupa��o hist�rica que se sucedeu. Transformaram-se em mat�ria-prima para o fabrico de cal. Suspeita-se que nas partes mais elevadas dos sambaquis tenha havido a presen�a de um novo grupo humano, os itarar�s, que apresentavam h�bitos diferentes dos primeiros habitantes dos sambaquis, revelados em vest�gios cer�micos e uma suposta pr�tica agr�cola. Houve, neste segundo grupo que ocupou a Ilha, uma sens�vel diminui��o no consumo de moluscos em sua dieta alimentar. Sup�e-se tamb�m que estes povos n�o tiveram contato entre si, habitando a Ilha em momentos que se sucederam historicamente.
			</p>
			<p>
				O terceiro grupo ind�gena que migrou para a Ilha, no s�culo XIV, foi o dos carij�s, da fam�lia lingu�stica tupi-guarani, tradicional no litoral sul do Brasil. Instalaram-se aproximadamente dois s�culos antes da chegada dos primeiros europeus. Suas aldeias variaram de 30 a 80 habita��es, o que imprimiu uma maior e mais densa ocupa��o efetiva da Ilha de Santa Catarina. Os primeiros contatos entre os carij�s e estrangeiros ocorreu por volta de 1556, quando expedi��es deixavam na Ilha n�ufragos ou desertores. Desde o princ�pio, este conv�vio deu-se de forma pac�fica. Por um lado, os amistosos carij�s abasteciam os visitantes espanh�is de alimentos e forneciam seguras informa��es acerca de caminhos que os levassem a rios como Igua�� e Itapoc�, para alcan�arem o Paraguai; por outro, os estrangeiros regalavam os �ndios com muitos presentes.
			</p>
			<p>
				Entretanto, esse conv�vio durou menos de um s�culo pois tornou-se prejudicial a cultura dos carij�s, que migravam continente adentro.
			</p>
			<p>
				Suspeita-se que j� em 1600 n�o haviam tribos na Ilha de Santa Catarina. A fuga dos carij�s do lugar onde estavam e que chamavam Meiembipe, que traduzido do tupi significa &quot;montanha ao longo do canal&quot;, n�o implica em rompimento mas simplesmente como uma a��o defensiva. Os nativos passaram a sentir-se muito vulner�veis na medida em que as necessidades dos visitantes, cada vez mais numerosos, exigiam maior produ��o de alimentos, o que alterava profundamente seus h�bitos, cren�as e at� mesmo a liberdade f�sica.
			</p>
			<p>
				At� meados do s�culo XVIII, embora o Tratado de Tordesilhas, em 1494, garantisse legalmente a posse das terras da Ilha de Santa Catarina � coroa portuguesa, Portugal n�o havia tomado nenhuma medida efetiva de povoamento. Percebia-se apenas a a��o espanhola. Depois de algumas ocupa��es inst�veis e de v�rias disputas pela posse das terras, vai caber a Francisco Dias Velho, nobre de S�o Vicente, fixar-se na Ilha. � prov�vel que o assentamento tenha se dado por volta de 1675, uma vez que tr�s anos ap�s essa data Dias Velho ir� requerer do governo da capitania paulista duas l�guas em quadro na Ilha, justificando ter instalado uma igreja em devo��o a Nossa Senhora do Desterro, benfeitorias e culturas na referida �rea. 
			</p>
			<p>
				Cresce a import�ncia estrat�gica da Ilha de Santa Catarina, exigindo que fosse melhor defendida e ocupada de modo mais contundente. Desta forma, em 1726, Desterro � elevada � categoria de vila e iniciam-se projetos militares de fortifica��es na Ilha, estabelecendo-se a coloniza��o definitiva, feita por a�orianos.
			</p>
			<p>
				No dia 6 de janeiro de 1748 desembarcaram na Ilha de Santa Catarina 461 pessoas provenientes da ilha de A�ores. At� 1756, cerca de 6 mil a�orianos e madeirenses j� estavam instalados na Ilha, mesmo que precariamente. Foram distribu�das �s fam�lias colonizadoras sementes, armas e ferramentas, cavalos e touros para o arado, o que dificultou ainda mais a adapta��o dos colonos. Al�m disso sua cultura de trigo n�o se adequou ao clima da regi�o, o que os obrigou a produzir culturas herdadas dos �ndios.
			</p>
			<p>
				Assim, a mandioca tornou-se a base da alimenta��o a�oriana e em pouco tempo (cerca de 30 anos) j� existiam 300 pequenos engenhos, sendo alguns de cana-de-a��car). Muitos colonos abandonaram suas terras e passaram a se dedicar a of�cios urbanos e � pesca artesanal. Outros tantos eram recrutados para treinamentos militares, o que de certa forma serviu de entrave e comprometeu o desenvolvimento da pequena produ��o mercantil na promissora Desterro. Na virada do s�culo XVIII para o s�culo XIX j� se podia perceber um in�cio de �xodo rural motivado pelo recrutamento, of�cios da pesca e trabalhos urbanos que, al�m de aumentar o n�cleo da cidade permitia a forma��o dos primeiros latif�ndios.
			</p>
			<p>
				<b>DE DESTERRO A FLORIAN�POLIS</b>
				<br>
			</p>
			<p>
				Numa homenagem prestada pelo ent�o governador Herc�lio Pedro da Luz ao marechal Floriano Peixoto, largamente contestada por muitos catarinenses e conhecedores de sua hist�ria, Desterro teve seu nome substitu�do, em 10 de outubro de 1894, por Florian�polis. Com fortalecimento de uma emergente burguesia comercial, a administra��o da cidade saiu do comando de militares e passou �s m�os dos comerciantes, que acumularam capital intermediando as exporta��es locais e escoando produtos no principal porto de Santa Catarina, que nesta �poca ainda existia na Ilha.
			</p>
			<p>
				Devido a crescente e not�vel concentra��o populacional propiciada pelo com�rcio, a capital catarinense entra no s�culo XX com caracter�sticas bem peculiares aos grandes centros urbanos.
			</p>
			<p>
				O per�metro urbano sofreu grandes agress�es, aumentando tamb�m seus problemas. Em 1926 foi inaugurada a primeira das tr�s pontes que viriam a ligar a Ilha ao Continente, a ponte Herc�lio Luz. Isto representou um marco consider�vel para o circuito econ�mico, pois imprimiu � cidade situa��o privilegiada de p�lo regional em rela��o ao interior do Estado, deflagrando um franco processo de moderniza��o.
			</p>
			<p>
				Com a Revolu��o de 30 houve ainda mais incremento ao com�rcio, agora estabelecido por meio de rodovias, haja vista a plena decad�ncia do antigo porto de Desterro. Entretanto, as principais transforma��es urbanas na cidade de Florian�polis vieram a acontecer com mais intensidade a partir da segunda metade deste s�culo. No final da d�cada de 50 foi implantada a Universidade Federal de Santa Catarina que aliada � cria��o da Eletrosul e de v�rias outras empresas estatais, atraiu um grande fluxo migrat�rio, n�o s� do interior como de outros Estados. Multiplicaram-se as �reas loteadas, os bairros residenciais, os pr�dios de apartamentos, as empresas e o com�rcio.
			</p>
			<p>
				Paralelamente ao crescimento das classes m�dias (que deu-se gra�as ao crescimento do setor p�blico devido � inje��o de recursos federais e estaduais), houve tamb�m a invas�o de grande n�mero de migrantes pobres expropriados do campo e evadidos de outras cidades, que atra�dos pelas possibilidades promissoras de emprego que passaram a existir em Florian�polis acabaram por multiplicar as �reas de periferia urbana.
			</p>
			<p>
				Cercada por �guas cristalinas, com praias de �guas claras e agrad�vel clima, Florian�polis fica localizada a mais ou menos um quil�metro do continente, no ponto mais pr�ximo.Com suas condi��es clim�ticas e geogr�ficas favor�veis, � natural que seus habitantes dediquem-se as atividades n�uticas, como pesca esportiva, pesca submarina, vela, surf e remo. Com um calend�rio ativo durante todo ano, al�m da Fenaostra (Festa Nacional da Ostra) e da Festa da Tainha, onde h� diferentes pratos deste peixe, destacam-se a festa do marisco e o carnaval, com barcos fantasiados e muita alegria.
			</p>
			<p>
				Na Ilha encontram-se praias para todos os gostos, desde as mais movimentadas, com hot�is internacionais, boa infra-estrutura e vida noturna, �s virgens, onde o meio ambiente � totalmente preservado.
			</p>
			<p>
				Chamada pelos �ndios guaranis &quot;jurer� mirim&quot; (pequena &quot;boca d&#146;�gua&quot; pelo estreito que a separa do continente), a Ilha de Santa Catarina � conhecida desde o s�culo XVI, descoberta pelos veleiros que procuravam o caminho das �ndias. Quando os portugueses decidiram colonizar a costa sul brasileira, v�rias vilas foram instaladas, entre elas, Nossa Senhora do Desterro, hoje Florian�polis. Ap�s v�rios ataques de cors�rios, as pequenas vilas quase desapareceram.
			</p>
			<p>
				Nessa �poca, o governo portugu�s resolveu facilitar a vinda de fam�lias das ilhas dos A�ores para repovoar a costa e preservar a coloniza��o. Tamb�m para garantir a posse da llha, durante a coloniza��o os portugueses constru�ram v�rias fortifica��es. Entre elas destaca-se Anhatomirim, a maior e melhor preservada. Foi constru�da em uma pequena ilha, � 5 milhas da cidade, em 1744.
			</p>
			<p>
				Hoje faz parte do patrim�nio hist�rico de Santa Catarina. No local encontra-se um centro de pesquisa marinha, um museu e um aqu�rio mar�timo. 
			</p>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab4">
	<tr>
		<td class="textoComum">
			<p>
				<b>CADA PRAIA TEM SUA PR�PRIA CARACTER�STICA</b>
				<br>
			</p>
			<p>
				Os munic�pios lim�trofes � Ilha de Santa Catarina &#150; Governador Celso Ramos, ao norte; Palho�a, ao sul; e S�o Jos� e Bigua�u, a oeste - tem em comum a coloniza��o a�oriana e o fato de sediarem ind�strias da regi�o metropolitana. No quesito praias, as grandes concorrentes de Florian�polis s�o Palho�a e Governador Celso Ramos.
			</p>
			<p>
				Em Palho�a, Praia de Fora, Enseada do Brito, Praia do Sonho e Pinheira s�o comunidades pesqueiras bastante procuradas pelos turistas. Pedras Altas � a praia oficial do naturismo no munic�pio, e a Guarda do Emba� � reconhecida como uma das melhores para a pratica do surf. Governador Celso Ramos � mais conhecida pela pol�mica farra do boi do que por sua bela costa, composta por enseadas, ba�as e pen�nsulas. O munic�pio � tamb�m o maior produtor de mariscos cultivados do Brasil. Al�m das praias de Palmas, Praia Grande, Arma��o, e Costeira, pertence a seu territ�rio a ilha de Anhatomirim e a Ba�a dos Golfinhos. Ainda pouco explorado turisticamente e sem muitas op��es de lazer al�m das praias, Governador Celso Ramos � um passeio que vale pela natureza bastante preservada.
			</p>
			<p>
				No centro de todas as atra��es est� a Ilha de Santa Catarina, com seu misto de plan�cies, dunas, lagoas, mangues e montanhas com vegeta��o atl�ntica que se evidenciam de variadas formas. Al�m disso, na sua vizinhan�a, existem outras 30 pequenas ilhas, dentre elas as que sediam as fortalezas de Anhatomirim e Ratones Grande, al�m de Ratones Pequena, do Franc�s, Guar�s Grande, Diamante e dos Noivos, todas na ba�a norte. Na baia sul encontra-se a das Vinhas, Laranjeiras, Maria Francisca, do Largo, do Andrade e ilha dos Carlos. Ao norte, sobressaem-se as ilhas Deserta e a do Arvoredo, que constitui uma reserva biol�gica marinha. A leste localizam-se as ilhas de Moleques do Norte, Mata-Fome, Badejo, das Aranhas, do Xavier e Campeche. Nesta �ltima, a natureza exuberante e os vest�gios de uma antiga civiliza��o pr�-hist�rica s�o preservados. Com 1.600 metros de extens�o, a ilha � um passeio imperd�vel. Com ajuda de um bom guia d� para percorr�-la num dia. Nela � poss�vel ver inscri��es rupestres, visitar a caverna dos morcegos- que corta a ilha numa extens�o de 30 metros - fazer treking e ainda mergulhar ou aproveitar sua linda praia.
			</p>
			<p>
				Ao sul localizam-se as ilhas Tr�s Irm�s, Moleques do Sul, do Coral, Ara�atuba e dos Papagaios. Na costa norte da Ilha de Santa Catarina ficam as praias com maior infra-estrutura tur�stica, �guas calmas e mornas, ideais para a fam�lias com crian�as, com exce��o da praia Brava. 
			</p>
			<p>
				<b>COSTA NORTE</b>
				<br>
			</p>
			<p>
				<b>Santinho</b> - De mar agitado, igualmente bom para o surf, fica a 40 km do centro e possui uma extens�o de 2,5 km de praia. Uma de suas atra��es s�o os petr�glifos junto ao cost�o. S�o inscri��es feitas nas pedras h� mais de 5 mil anos pelos �ndios carij�s. A faixa de areia clara e macia � bastante larga. Com suas dunas, formam um ecossistema de rara beleza.
			</p>
			<p>
				<b>Ingleses</b> - A praia, a 36 quil�metros do centro, � a que fica mais ao norte da Ilha de Santa Catarina. Seu nome prov�m de um navio ingl�s que naufragou ali em 1700. � uma das que tem infra-estrutura tur�stica mais completa. Apesar de n�o ter uma �gua t�o quente, proporciona bons momentos aos banhistas que buscam lazer. Tem aproximadamente 5 quil�metros de extens�o e � uma das mais solicitadas no ver�o.
			</p>
			<p>
				<b>Brava</b> - O mar agitado e frio, geralmente com ondas fortes, � �timo para o surf e proporciona excelentes banhos. Distante 38 quil�metros do centro, a Brava fica atr�s de um morro que permite uma vis�o magn�fica de seu alto, antes da chegada � praia. Sua extens�o � de 1,7 quil�metros de areia macia. A faixa de areia inclinada faz com que o mar tenha uma profundidade consider�vel logo na beira. As op��es de com�rcio nesta praia, que foi recentemente urbanizada com a constru��o de um imponente conjunto de pr�dios em estilo mediterr�neo, vem aumentando ano a ano.
			</p>
			<p>
				<b>Lagoinha</b> - De mar calmo e �guas muito frias, a Lagoinha est� localizada a 37 quil�metros do centro. Sua faixa de areia � de 900 metros e vem recebendo um p�blico cada vez maior nos �ltimos anos. Seu acesso ocorre antes da entrada de Ponta das Canas. � um dos recantos mais agrad�veis da Ilha de Santa Catarina. � considerada uma �tima op��o para fam�lias com crian�as.
			</p>
			<p>
				<b>Ponta das Canas</b> - Segue a tend�ncia natural do norte da Ilha que, historicamente, em fun��o da necessidade de defesa contra os ataques espanh�is, foi ocupado e urbanizado em escala muito maior do que o sul. Distante 34 quil�metros do centro, se caracteriza por apresentar �guas mansas e geralmente quentes. Sua extens�o � de 1,9 quil�metro de areia clara e fina. Boa op��o para a pr�tica de esportes n�uticos, � um dos balne�rios mais procurados por turistas argentinos e uruguaios.
			</p>
			<p>
				<b>Cachoeira do Bom Jesus</b> - Localizada entre Ponta das Canas e Canasvieiras, esta praia de mar calmo e quase 3 quil�metros de extens�o de areia branca, situa-se a 27 quil�metros do centro. � um dos balne�rios onde se encontram belas resid�ncias e uma rede de servi�os bem estruturada. 
			</p>
			<p>
				<b>Canasvieiras</b> - Principal reduto dos argentinos em Florian�polis, a praia de Canasvieiras � a mais cosmopolita dentre todas as da Ilha de Santa Catarina a ponto de ter &quot;portunhol&quot; como l�ngua oficial durante os meses de ver�o. � um dos pontos mais agitados da Ilha, tanto durante o dia como � noite. S�o bares, restaurantes, hot�is, boates, fliperamas, campings. Enfim, toda a estrutura necess�ria a uma estada confort�vel e repleta de op��es. Tamb�m s�o oferecidos equipamentos para a pr�tica de diversos esportes e h� uma grande quantidade de boas quadras de t�nis nos hot�is , clubes e pousadas. Com uma extens�o de 3 quil�metros, Canasvieiras possui �guas extremamente agrad�veis e quentes, al�m de calmas. No balne�rio pode-se alugar baleeiras que, conforme as condi��es do vento, demoram 40 minutos para chegar at� a ilha de Anhatomirim. Mais perto da praia est� a ilha do Franc�s, quase toda destinada ao uso de seu �nico propriet�rio. Outras op��es nesta praia, distante 27 quil�metros do centro, s�o os passeios de escuna e o aluguel de pedalinhos.
			</p>
			<p>
				<b>Daniela</b> - Com uma extens�o de 3 quil�metros e distante 26 quil�metros do centro, esta praia � ocupada essencialmente por casas de veraneio. De �guas claras e mar calmo, � muito procurada por fam�lias com crian�as. N�o oferece muitas op��es em servi�os.
			</p>
			<p>
				<b>COSTA OESTE</b>
				<br>
			</p>
			<p>
				A Costa Oeste da Ilha de Santa Catarina, denominada Mar de Dentro, fica de frente para o continente, dividido entre as ba�as norte e sul. Na ba�a norte encontram-se as praias de Saco Grande, Santo Ant�nio de Lisboa e Sambaqui, formadas por comunidades localizadas em enseadas de �guas abrigadas que conservam caracter�sticas dos imigrantes a�orianos. 
			</p>
			<p>
				<b>Sambaqui</b> &#150; Pertencente ao distrito de Santo Ant�nio de Lisboa, Sambaqui � uma op��o para quem procura tranq�ilidade numa das �reas mais agrad�veis para o lazer e o descanso. Distante 17 km do centro, sua praia tem 1,10 km de extens�o. O nome Sambaqui teve origem no amontoado de conchas dispersas em suas areias, chamando a aten��o dos pesquisadores por revelar, de tempos em tempos, algum animal ou vegetal fossilizado, al�m de resqu�cios de comunidades ind�genas que habitavam a regi�o. Suas �guas claras s�o calmas, quase paradas. A linha de praia apresenta um relevo formado por cost�es. Local da Ilha onde ainda sobrevive a dan�a folcl�rica do boi-de- mam�o, Sambaqui tem v�rios bares na praia e bons restaurantes.
			</p>
			<p>
				<b>Santo Ant�nio de Lisboa</b> - Freguesia mais antiga da Ilha, Santo Ant�nio � um importante reduto da cultura a�oriana. Sua praia tem �guas calmas e quentes. � um local distante 13 quil�metros do centro, not�vel por sua extrema beleza e tranq�ilidade, com praia de areias grossas. Em substitui��o � pesca, uma das atividades que vem ganhando cada vez mais espa�o � o cultivo de ostras e mariscos. Outro recanto ideal para quem busca paz � o vizinho Cacup�. Tem uma extens�o de 900 metros. Al�m de v�rios restaurantes, � uma �tima op��o para um piquenique ou passeio familiar. 
			</p>
			<p>
				<b>SUL DA ILHA</b>
				<br>
			</p>
			<p>
				<b>Tapera</b> - Localizada na costa sul da Ilha, nas proximidades do aeroporto, fica � 29 km do centro e possui �guas limpas e paradas. Em seu 1,5 km de extens�o a areia � branca, dura e grossa. A praia tem in�meros bares e � uma op��o de passeio.
			</p>
			<p>
				<b>Ribeir�o da Ilha</b> - Localizado a 36 km do centro, o distrito do Ribeir�o da Ilha � um dos mais antigos povoados de Florian�polis. Registros indicam que em 1526 o espanhol Sebasti�o Caboto aportou ali e alguns de seus comandados se juntaram aos n�ufragos de uma expedi��o anterior, de Dias de Solis, de 1515. O Ribeir�o � um composto de v�rias pequenas praias de �guas calmas e areia grossa. � um dos locais mais t�picos da Ilha, como o casario a�oriano, a Igreja Nossa Senhora da Lapa do Ribeir�o e o Museu Etnol�gico, com acervo de pe�as raras que retratam a coloniza��o a�oriana.
			</p>
			<p>
				<b>COSTA LESTE</b>
				<br>
			</p>
			<p>
				Na Costa Leste situam-se as praias voltadas para o mar aberto, de frente para o Oceano Atl�ntico, assentadas acima do cord�o arenoso. Boas para o surf, suas �guas s�o limpas e frias.
			</p>
			<p>
				<b>Joaquina</b> - Mundialmente famosa por suas excelentes condi��es para a pr�tica do surf e pela infinidade de competi��es deste esporte nela sediadas, as dunas da Joaquina oferecem condi��es para uma nova modalidade de esporte &#150; o sandbord. Seu nome origina de uma lenda na qual uma velha rendeira, de nome Joaquina, estava em suas areias realizando seu of�cio quando foi tragada por uma enorme onda. Praia de mar agitado, distante 17 km do centro, � chamada de &quot;Joaca&quot; pelos surfistas. Com extens�o de 3,5 km, � uma das que oferece melhor infra-estrutura aos visitantes, com hot�is, restaurantes, bares, terminal tur�stico com posto policial e de salva- vidas, chuveiros p�blicos e sanit�rios. A pr�tica da nata��o exige aten��o devido as grandes ondas e correntes. Invariavelmente a Joaquina est� repleta de surfistas e bodyboarders com suas roupas coloridas. O futebol de areia e o v�lei de praia s�o constantemente praticados ali. Dotada de um amplo estacionamento pago, tamb�m possui sorveterias e lojas de artesanato. Um eficiente sistema de ilumina��o permite o surf e a pr�tica de outros esportes � noite.
			</p>
			<p>
				<b>Mole</b> - �tima op��o para o surf, a Mole n�o � recomendada para crian�as. H� buracos logo na beira. Localizada � 15 km do centro, entre a Lagoa da Concei��o e a Barra da lagoa, tem tal nome por causa da maciez de suas areias. Tamb�m � dotada de estacionamento p�blico, chuveiros, sanit�rios e posto de salva-vidas. H� bares que servem frutos do mar e lanches. Com suas �guas sempre frias, � o &quot;point&quot; da moda na cidade. Freq�entada essencialmente por jovens, agita-se nas noites de ver�o com seus bares de m�sica ao vivo. Tamb�m � uma das preferidas pelos praticantes de frescobol.
			</p>
			<p>
				<b>Lagoa da Concei��o</b> - A Lagoa da Concei��o disputa com a Ponte Herc�lio Luz a condi��o de principal cart�o postal da cidade. Pode-se dizer tamb�m que diferentes mundos que habitam Florian�polis- o cosmopolita e o interiorano- se cruzam na Lagoa. Apesar de n�o ter ondas, ela � o lugar onde mora a maioria dos surfistas. Apesar de n�o ter bibliotecas, teatros e cinemas, ela � o reduto de artistas e intelectuais. Apesar de n�o ter um grande palco para shows, abriga em seus bares os mais distintos ritmos para embalar suas noites. Quem procura divers�o e arte n�o pode perder a Lagoa de vista. Ao seu redor ficam as melhores praias para o surf e outros esportes radicais. Dos morros que a circundam surgem asas e paraglides, colorindo o c�u. Nas dunas, desde a Avenida das Rendeiras &#150; onde encontram-se os �ltimos redutos do artesanato da renda de bilro- at� a Joaquina, pratica-se o sandboard. Em suas �guas imperam as lanchas, jet-skis, pranchas de windsurf e barcos � vela de todos os tipos e tamanhos. A Lagoa esteve durante muito tempo ligada � v�rios n�cleos pr�ximos. Dentre estes est�o a Barra da Lagoa, a Costa da Lagoa e o Canto da Lagoa, comunidades de pescadores que preservam muitas das caracter�sticas culturais. No Canto da Lagoa, descobriu-se um sambaqui (cemit�rio ind�gena), onde foram recolhidos machados semi-polidos, cer�mica e outros vest�gios arqueol�gicos.
			</p>
			<p>
				A Costa da Lagoa � uma t�pica col�nia de pescadores. Sua origem est� ligada � ocupa��o da pr�pria Lagoa da Concei��o. Nos s�culos XVIII e XIX a Costa da Lagoa alcan�ou prest�gio pelo forte interc�mbio de mercadorias que ali eram produzidas e levadas para a Lagoa e outros n�cleos. A grande quantidade de cultivos e os muitos engenhos que ali existiram podem ser avaliados atualmente. H� ainda alguns engenhos t�picos, casar�es e sobrados ao longo do secular caminho de pedras que liga as duas comunidades. Aquele n�cleo de pescadores e rendeiras ainda desenvolve suas pr�ticas artesanalmente, como nas gera��es passadas. Um decreto municipal tombou a Costa da Lagoa como patrim�nio hist�rico cultural. 
			</p>
			<p>
				<b>Galheta</b> - A praia da Galheta, com acesso pela praia Mole, � o outro exemplo de praia selvagem que s� pode ser alcan�ada a p�. Para chegar at� ela � preciso seguir uma trilha de cerca de 300 metros localizada junto ao cost�o de pedras do lado esquerdo da Mole. A Galheta � um local paradis�aco e n�o apresenta nenhum tipo de edifica��o. Recentemente a C�mara dos Vereadores oficializou esse local como praia de nudismo. Al�m da bela paisagem que proporciona, o ambiente tamb�m � favor�vel � pr�tica do surf.
			</p>
			<p>
				<b>Barra da Lagoa</b> - Maior n�cleo pesqueiro da Ilha, a Barra da Lagoa tamb�m � uma das praias mais procuradas de Florian�polis com seus 2,50 km de faixa de areia. Distante 20 km do centro, a Barra � bem servida em bares e restaurantes. Suas �guas n�o chegam a ser violentas, embora em alguns dias seja poss�vel a pr�tica do surf, principalmente junto ao cost�o de pedras, do lado direito da praia. O lugar tem v�rias op��es de pousadas e casas de aluguel, al�m de campings. Em julho o local vive uma grande agita��o, com a Festa da Tainha, uma grande quermesse organizada pela comunidade local e que atrai visitantes de v�rias partes.
			</p>
			<p>
				<b>Mo�ambique</b> - Quase deserta, � a maior praia da ilha, com 12,5 km de areias macias. A quase inexist�ncia de ocupa��o urbana se deve ao fato de que esta praia situa-se pr�xima �s �reas de preserva��o permanente, como as dunas do Santinho e o Parque Florestal do Rio Vermelho, uma reserva de 400 mil m2 implantada nos anos 70. Tamb�m boa op��o para o surf, Mo�ambique ou Praia Grande, possui �guas frias. Seu nome adv�m do fato de suas areias serem repletas de mo�ambiques, um molusco semelhante � ostra. Distante 35 km do centro, tem uma diminuta estrutura comercial.
			</p>
			<p>
				<b>COSTA SUL</b>
				<br>
			</p>
			<p>
				Na Costa Sul encontram-se as praias com menor infra-estrutura tur�stica e que abrigam comunidades t�picas, al�m de v�rias trilhas selvagens que levam a praias de mar grosso quase desertas.
			</p>
			<p>
				<b>Campeche</b> - Localizada a 20 km do centro da cidade, � uma praia bastante larga, circundada por pequenas dunas e vegeta��o rasteira. � uma das boas op��es para o surf na Ilha. Sua extens�o � de 11,4 km e a estrada que d� acesso ao mar � bem servida em termos de bares e restaurantes. Em frente � praia est� a Ilha do Campeche, com exuberante vegeta��o nativa. De 1926 a 1939, o escritor e aviador franc�s Saint-Exup�ry (&quot;O Pequeno Pr�ncipe&quot;) fazia escala no aer�dromo natural do lugar. 
			</p>
			<p>
				<b>Naufragados</b> - Para se chegar at� esta praia percorre-se 28 km desde o centro at� o Ribeir�o da Ilha e mais 12 km por estrada de ch�o at� a Caieira da Barra do Sul. Depois � necess�ria uma caminhada de 3 km para se chegar ao destino final, que corresponde ao ponto mais extremo do sul da Ilha. Apesar do esfor�o, o passeio � agrad�vel. O nome da praia est� ligado a um naufr�gio ali ocorrido em 1715 com um barco de imigrantes a�orianos. Com uma extens�o de 1500 metros de areias claras, � uma praia de mar bravio e �guas geralmente frias. Muito procurada pelos adeptos do camping selvagem e surfistas. Para quem prefere maior comodidade, h� um acesso pelo mar a partir do Ribeir�o da Ilha. O passeio de barco � de rara beleza, pois o trajeto ocorre ao longo dos cost�es onde a vegeta��o da Ilha se apresenta muito preservada.
			</p>
			<p>
				<b>Morro das Pedras</b> - Igualmente de mar agitado, onde o banho exige alguns cuidados, esta praia, com 3.200 metros de extens�o e distante 20 km do centro, � liga��o entre o Campeche e a Arma��o. Paralela a ela est� a Lagoa do Peri. Na estrada que d� acesso � praia pode-se ver o choque das ondas contra o cost�o de pedras. No alto do morro fica a casa de retiro dos padres jesu�tas, constru�da com pedras extra�das do pr�prio local. Dali se tem uma bela vis�o da praia da Arma��o e do Parque da Lagoa do Peri.
			</p>
			<p>
				<b>Arma��o</b> - Com uma extens�o de 2,25 km, j� foi uma forte esta��o de ca�a �s baleias. Mesmo com boas ondas, n�o chega a ser perigosa para o banho. Possui restaurantes muito procurados, localizados junto � pra�a e � centen�ria capela a�oriana. Distante 25 km do centro, � uma das tradicionais col�nias de pescadores da Ilha. Ali tamb�m se localiza um dos mais importantes s�tios arqueol�gicos do Estado.
			</p>
			<p>
				<b>Matadeiro</b> - Separada da praia da Arma��o por um pequeno rio, esta praia possui apenas 200 metros e s� pode ser alcan�ada a p�. Al�m do surf, o local � excelente para a pr�tica do v�lei, frescobol e futebol etc. Possui bares que servem refei��es e lanches variados.
			</p>
			<p>
				<b>Lagoinha do Leste</b> - Outra praia selvagem, que s� pode ser alcan�ada a p�, a Lagoinha do Leste merece ser visitada. � acess�vel ap�s uma caminhada de aproximadamente uma hora pela trilha existente no morro. N�o possui bares ou restaurantes, mas � um para�so natural, de areias macias e mar consideravelmente agitado. Al�m disso tem uma lagoa localizada no lado esquerdo em cujas margens os campistas se instalam. Com uma extens�o de 1,25 km, � um aut�ntico santu�rio da natureza.
			</p>
			<p>
				<b>Saquinho</b> - Existem tr�s trilhas que levam � praia do Saquinho, partindo da Solid�o, da Caieira da Barra do Sul ou de Naufragados. S�o localizadas em �reas �ngremes e exigem um certo f�lego, mas vale o esfor�o ao revelar um peda�o de Mata Atl�ntica bastante preservado, ou, para quem sai da Solid�o, uma costa magnificamente recortada onde a presen�a das gaivotas � marcante. Esta praia tem formato de concha e apenas 200 metros de extens�o. � freq�entada por surfistas.
			</p>
			<p>
				<b>P�ntano do Sul</b> - Esta praia � uma das mais ricas col�nias de pescadores da Ilha, com boas op��es em restaurantes. Seu mar � relativamente agitado, mas com ondas n�o muito grandes e sem maiores riscos para banho. Distante 31 km do centro, ali est� localizada uma das reservas arqueol�gicas da Ilha, al�m do p�ntano, ecossistema que originou seu nome. A faixa de areia, na sua maioria �mida, � bastante larga. 
			 </p>
		</td>
		<td class="textoComum" width="120" valign="top" align="right">
			<%
			dim nm_figuras(28)
			nm_figuras( 1) = "Armacao"
			nm_figuras( 2) = "Barra da Lagoa"
			nm_figuras( 3) = "Canasvieiras1"
			nm_figuras( 4) = "Canasvieiras2"
			nm_figuras( 5) = "Canasvieiras3"
			nm_figuras( 6) = "Costao da Ilha da Armacao"
			nm_figuras( 7) = "Costao do Santinho"
			nm_figuras( 8) = "Daniela"
			nm_figuras( 9) = "Hotel da Praia Brava"
			nm_figuras(10) = "Ilha da Armacao"
			nm_figuras(11) = "Ilha da Fortaleza da Aracatuba"
			nm_figuras(12) = "Ilha do Campeche"
			nm_figuras(13) = "Joaquina 1"
			nm_figuras(14) = "Joaquina"
			nm_figuras(15) = "Pantano do sul"
			nm_figuras(16) = "Piscina Natural na Praia Mole"
			nm_figuras(17) = "Ponta das Canas"
			nm_figuras(18) = "Ponta de Naufragados"
			nm_figuras(19) = "Ponta do Sambaqui"
			nm_figuras(20) = "Praia Brava 1"
			nm_figuras(21) = "Praia Brava 2"
			nm_figuras(22) = "Praia Deserta"
			nm_figuras(23) = "Praia do Matadeiro"
			nm_figuras(24) = "Praia do Ribeirao da Ilha"
			nm_figuras(25) = "Praia Mole 1"
			nm_figuras(26) = "Ribeirao da Ilha"

			dim alt_figuras(28)
			alt_figuras( 1) = "Praia da Arma��o"
			alt_figuras( 2) = "Barra da Lagoa"
			alt_figuras( 3) = "Praia de Canasveiras"
			alt_figuras( 4) = "Praia de Canasveiras"
			alt_figuras( 5) = "Praia de Canasveiras"
			alt_figuras( 6) = "Cost�o da Ilha da Arma��o"
			alt_figuras( 7) = "Cost�o do Santinho"
			alt_figuras( 8) = "Balne�rio Daniela"
			alt_figuras( 9) = "Hotel da Praia Brava"
			alt_figuras(10) = "Ilha da Arma��o"
			alt_figuras(11) = "Ilha da Fortaleza da Ara�atuba"
			alt_figuras(12) = "Ilha do Campeche"
			alt_figuras(13) = "Praia da Joaquina"
			alt_figuras(14) = "Praia da Joaquina"
			alt_figuras(15) = "Praia do P�ntano do Sul"
			alt_figuras(16) = "Piscina Natural na Praia Mole"
			alt_figuras(17) = "Ponta das Canas"
			alt_figuras(18) = "Ponta de Naufragados"
			alt_figuras(19) = "Ponta do Sambaqui"
			alt_figuras(20) = "Praia Brava"
			alt_figuras(21) = "Praia Brava"
			alt_figuras(22) = "Praia Deserta"
			alt_figuras(23) = "Praia do Matadeiro"
			alt_figuras(24) = "Praia do Ribeir�o da Ilha"
			alt_figuras(25) = "Praia Mole"
			alt_figuras(26) = "Ribeirao da Ilha"
			
			for i=1 to 26
				%>
				<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - Clique para almpliar" vspace="50" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>);">
				<br>
				<%
			next
			%>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab5">
	<tr>
		<td class="textoComum">
			<p>
				<b>CONTINENTE TAMB�M MERECE UMA VISITA</b>
				<br>
			</p>
				No Continente, de frente para a Ilha, ficam belas praias de mar calmo. D�o nome a bairros da cidade e foram coqueluche nos anos 50 e 60. As dos bairros Coqueiros e Estreito n�o s�o habitualmente utilizadas para banhos. Em compensa��o, no caso de Coqueiros, oferecem �timas paisagens, como a praia da Saudade, do Sorriso, do Meio, Itagua�u e das Palmeiras, al�m da do Bom Abrigo, localizada no bairro hom�nimo. No Estreito encontra-se a praia do Balne�rio, uma op��o para caminhadas e pr�tica de esportes.
			<p>
			<p>
				<b>ENTORNO DE FLORIAN�POLIS</b>
				<br>
			</p>
			<p>
				Numa regi�o de transi��o entre o litoral e a serra, com clima ameno e temperado, Florian�polis tem na sua vizinhan�a duas cidades famosas por suas fontes minerais: �guas Mornas e Santo Amaro da Imperatriz, distantes 35 quil�metros do centro da capital catarinense. A paisagem campestre, que mistura-se � montanha, guarda a rusticidade de um tempo em que esses lugares eram caminhos de tropeiros entre a regi�o serrana e o litoral.
			</p>
			<p>
				De coloniza��o germ�nica, �guas Mornas foi emancipada politicamente em 1961, levada � categoria de munic�pio. Suas vilas e povoados tipicamente rurais criados pelos imigrantes alem�es tem tra�os ainda bem marcantes e facilmente percept�veis na arquitetura, idioma, h�bitos e costumes de seus habitantes.
			</p>
			<p>
				O visitante tem, al�m das op��es oferecidas pelos hot�is da regi�o, alternativas diversas, como passeios ao parque Estadual da Serra do Tabuleiro- local de preserva��o da fauna e flora, com destaque para um mini-zool�gico de esp�cies em extin��o. Tamb�m pr�ximas est�o algumas praias, como as da Pinheira e Guarda do Emba�, no munic�pio de Palho�a, distante 39 quil�metros de Florian�polis. 
			</p>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab6">
	<tr>
		<td class="textoComum">
			<p>
				<b>FORTALEZAS: TURISMO EDUCATIVO DESPERTA EM FLORIAN�POLIS</b>
				<br>
			</p>
			<p>
				Seculares, elas circundam a Ilha de Santa Catarina, distribu�das ao longo de sua costa de 172 quil�metros, recentemente revitalizadas e abertas � visita��o p�blica. S�o monumentais fortalezas que constitu�ram at� recentemente o principal projeto na �rea de patrim�nio cultural do Brasil. A restaura��o completa da maior delas, a de Santa Cruz de Anhatomirim, vem consolidando estes monumentos hist�ricos como principais pontos de atra��o tur�stica de Florian�polis. S�o milhares de visitantes de todo o pa�s e do exterior, principalmente de pa�ses vizinhos. A m�dia nos meses de janeiro e fevereiro ( alta temporada de ver�o ) tem sido de 400 mil pessoas.
			</p>
			<p>
				Tais monumentos constituem um conjunto de seis unidades que remontam ao s�culo XVIII, quando os espanh�is e portugueses entraram em conflito na regi�o do Prata e o governo portugu�s decidiu manter um comando �nico em toda costa sul brasileira at� a Col�nia de Sacramento, tendo a Ilha de Santa Catarina, ent�o chamada Nossa Senhora do Desterro, como ponto estrat�gico no Atl�ntico Sul.
			</p>
			<p>
				Dessa forma, o rei de Portugal, D. Jo�o V, determinou em 1738 a ida do brigadeiro Jos� da Silva Paes � Desterro para construir em seu redor um sistema de fortifica��es visando defend�-la. Engenheiro militar, Silva Paes projetou quatro: Santo Ant�nio, na ilha de Ratones Grande; Santa Cruz, na ilha de Anhatomirim; S�o Jos�, no norte da ilha de Santa Catarina e Nossa Senhora da Concei��o, ao sul desta, na pequena ilha de Ara�atuba.
			</p>
			<p>
				Do conjunto, a maior e a que sofreu menos danos ao longo dos s�culos foi a de Santa Cruz, que juntamente com as de Santo Ant�nio e S�o Jos�, formava o sistema defensivo triangular que guardava a entrada da barra norte da Ilha de Santa Catarina. Esse sistema de defesa tinha como base o cruzamento de fogos. Cada canh�o da fortaleza deveria atingir a metade da dist�ncia entre as outras duas de forma que fosse poss�vel o bloqueio do acesso aos inimigos.
			</p>
			<p>
				Segundo os historiadores, esta fortaleza n�o foi efetivamente utilizada do ponto de vista b�lico, nem mesmo durante a invas�o espanhola em 1777. Ap�s este epis�dio, o sistema entrou em descr�dito e Santa Cruz passou a ser progressivamente abandonada. Em 1894, durante a Revolu��o Federalista, serviu de pris�o e base de fuzilamento de revoltosos contra o governo de Floriano Peixoto. Em 1907, passou a pertencer ao Minist�rio da Marinha e voltou a ser utilizada como pris�o no desfecho da Revolu��o Constitucionalista de 1932. Funcionou como fortaleza at� o final da Segunda Guerra Mundial, quando o aparecimento de novas tecnologias b�licas tornou-a completamente obsoleta como unidade militar. Ent�o foi desativada.
			</p>
			<p>
				Apesar de tombada em 1938, somente em 1970 Santa Cruz veio � sofrer as primeiras interven��es de restauro feitos pela Universidade Federal de Santa Catarina, que reabriu-a oficialmente � visita��o p�blica em 1984. A restaura��o completa foi terminada em 1993.
			</p>
			<p>
				Como as demais edificadas no s�culo XVIII no Brasil, Santa Cruz tem tra�os de influ�ncia renascentista em sua arquitetura. Na ilha circundada por cost�es, seus edif�cios distribuem-se de maneira esparsa em diferentes n�veis, contidos por espessas muralhas, em cujo v�rtices se abrigam as proeminentes guaritas circulares de vigia. Entre os edif�cios mais significativos da fortaleza destacam-se a portada, que comp�e-se de um corredor formado por uma ab�boda de tijolos sobre os quais se elevam dois maci�os de alvenaria escalonadas; a casa do comandante, tipo c�mara e cadeia; e uma esp�cie de sobrado que foi a primeira sede do governo de Santa Catarina, onde residiu Silva Paes. O quartel da tropa � uma constru��o de grande destaque, representando o auge da impon�ncia das obras de Silva Paes. O estilo cl�ssico da constru��o � determinado por contornos retos, telhas coloniais e doze arcadas t�rreas de tal apuro que raramente deixavam de ser mencionadas por viajantes europeus em seus di�rios.
			</p>
			<p>
				Al�m desses edif�cios, encontram-se na ilha o paiol de p�lvora, a casa da farinha e o armaz�m da praia. Outros edif�cios foram constru�dos posteriormente, como a usina de eletricidade e a casa do tel�grafo. Para o visitante desta fortaleza h� o lado natural para ser apreciado. A maior parte do contorno da ilha de Anhatomirim � de cost�es que albergam uma fauna marinha muito peculiar. Tr�s pequenas praias arenosas completam o seu litoral. O rel�vo � modesto e a altitude m�xima � de 31 metros. Entre a ilha e o continente, que s�o separados por duas milhas, a profundidade � de no m�ximo cinco metros. Nas redondezas, numa ba�a, abrigam-se cerca de 500 golfinhos.
			</p>
			<p>
				A fortaleza de Santo Ant�nio, na ilha de Ratones Grande, � a segunda maior da regi�o. Sua constru��o teve in�cio em 1740, conclu�da quatro anos depois. Permaneceu em ru�nas at� 1990, quando foi iniciada sua recupera��o. Tamb�m com tra�os renascentistas, os principais edif�cios est�o implantados em linha, guarnecidos pela costa e voltados para o mar. O paiol de p�lvora, situado no ponto mais proeminente do t�rreo � a �nica constru��o com dois pavimentos. Os edif�cios, bem como as muralhas que os resguardam, foram constru�dos � base de cal. Entre os elementos arquitet�nicos mais significativos da fortaleza destacam-se a portada, a fonte de �gua e o aqueduto. Este, que une a casa do comandante ao quartel da tropa, integra um interessante e original sistema de capta��o, condu��o e aproveitamento das �guas pluviais provenientes dos telhados dos edif�cios principais.
			</p>
			<p>
				A fortaleza de S�o Jos� da Ponta Grossa, fica distante 25 quil�metros do centro de Florian�polis, na ilha de Santa Catarina. Estrategicamente situada no alto de um morro, emoldurada pela beleza dos cost�es e areia branca da praia do Forte, constitu�a o terceiro v�rtice do sistema triangular de defesa. A maioria de seus edif�cios completou sua restaura��o na Segunda metade da d�cada de 90. Entre os edif�cios, o mais significativo � a casa do comandante, constru��o curiosamente geminada ao paiol de p�lvora, formando o �nico conjunto com dois pavimentos no local. A for�a e a sobriedade da composi��o geradora do p�tio principal, delineado pelo sobrado colonial e a austera capela, espelham bem a import�ncia e a inter-rela��o dos poderes do Estado e a Igreja no s�culo XVIII.
			</p>
			<p>
				Al�m da pr�pria Universidade Federal de Santa Catarina, que organiza passeios com estudantes e estudiosos, empresas particulares exploram passeios tur�sticos para fortalezas que se localizam em ilhas (Anhatomirim e Ratones Grande). As excurs�es s�o feitas em escunas e a principal empresa do ramo � a Scuna Sul. Mesmo fora da temporada de ver�o, h� excurs�es di�rias em diferentes hor�rios e com servi�o de bordo. Alem destes servi�os, baleeiras s�o encontradas em diversas praias e na Lagoa da Concei��o para passeios nas proximidades.
			</p>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab7">
	<tr>
		<td class="textoComum">
			<p>
				<b>CULIN�RIA DA ILHA DE SANTA CATARINA PRIVILEGIA FRUTOS DO MAR</b>
				<br>
			</p>
			<p>
				A culin�ria da Ilha de Santa Catarina privilegia os frutos do mar. Peixes, camar�es e mariscos s�o alguns dos pratos servidos numa j� numerosa rede de restaurantes especializados, localizados tanto no centro da cidade como na maioria das praias mais freq�entadas. Os peixes s�o preparados nas mais variadas formas, dependendo da esp�cie. A tainha, o bagre e o linguado, por exemplo, podem ser servidos em fil�s ou cortados em postas. Os mariscos s�o servidos refogados logo ap�s seu recolhimento ou mesmo depois de secos.
			</p>
			<p>
				Os pratos mais comuns s�o o peixe ensopado, s� servido com sal e ervas de cheiro (salsa, cebolinha e louro) e consumido com pir�o de farinha de mandioca cozida no mesmo caldo de peixe; e o peixe frito, cortado em postas e envolto em farinha de mandioca ou trigo antes de fritar. A pr�pria gordura � aproveitada para fritura. Normalmente come-se o peixe frito com pir�o de mandioca. Outra variedade de prato muito comum � o peixe assado. Retiram-se apenas as v�sceras e conservam-se as escamas. H� tamb�m o peixe grelhado sobre brasas, enrolado em folhas de bananeira (n�o necessariamente). Quando pronto, o couro se desprende da carne com facilidade. Come-se com pir�o de feij�o . As ovas da tainha s�o outro prato muito apreciado, fritas e servidas com lim�o. No card�pio t�pico entra tamb�m o camar�o, frito, refogado, � milanesa e ao bafo, acompanhado de pir�o de peixe ou arroz. O caldo de camar�o, muito parecido com a sopa de legumes e verduras, tem largo consumo, quando n�o servido de base para o pir�o.
			</p>
		</td>
	</tr>
</table>
