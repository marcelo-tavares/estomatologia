<input type=hidden name=pr3 value=1>
strProgCientifica = "Programa��o Cient�fica"
strProgSocial = "Programa��o Social"
strProgTuristica = "Programa��o Tur�stica"
strPeriodo = "Per�odo"
strDia16 = "16 de julho (S�bado)"
strDia17 = "17 de julho (Domingo)"
strDia18 = "18 de julho (2� Feira)"
strDia19 = "19 de julho (3� Feira)"
strDia20 = "20 de julho (4� Feira)"
strCursoPreCongresso = "Curso Pr�-Congresso"
strConferencia = "Confer�ncias"
strSessaoCientifica = "Sess�o Cient�fica"
strPaineis = "Sess�o de Pain�is"
strSimposio = "Simp�sio"
strCurso = "Curso"
strRadDigital = "Radiologia Digital"
strCerEncerramento = "Cerim�nia de Encerramento"
strAlmoco = "Almo�o"
strPremio = "Pr�mio IADMFR"
strAssembleia = "ASSEMBL�IA GERAL"
strAdvanced = "Interpreta��o Radiogr�fica Avan�ada"
strJoaquina = "Sala Joaquina"
strSambaqui = "Sala Sambaqui"
strCampeche = "Sala Campeche"
else
strProgCientifica = "Scientific Program"
strProgSocial = "Social Program"
strProgTuristica = "Tour Program"
strPeriodo = "Time"
strDia19 = "May 19 (Monday)"
strDia20 = "May 20 (Tuesday)"
strDia21 = "May 21 (Wednesday)"
strDia22 = "May 22 (Thrusday)"
strDia23 = "May 23 (Friday)"
strCursoPreCongresso = "Pre-Congress Course"
strConferencia = "Conferences"
strSessaoCientifica = "Scientific Session"
strPaineis = "Poster Session" 
strSimposio = "Symposium"
strCurso = "Special Lecture"
strRadDigital = "Digital Imaging"
strCerEncerramento = "Closing Ceremony"
strAlmoco = "Lunch"
strPremio = "IADMFR Award"
strAssembleia = "GENERAL ASSEMBLY"
strAdvanced = "Advanced Imaging of the Salivary Glands"
strJoaquina = "Room Joaquina"
strSambaqui = "Room Sambaqui"
strCampeche = "Room Campeche"
end if 
%>
<p class="Titulo" align="center"><%=strProgCientifica%></p>

<p class="textoComum" align="center">
	<A class=Link href="javascript:submeter(2,1,<%=id_wk%>,1)"><%=strProgSocial%></A>
	&nbsp;&nbsp;&nbsp;&nbsp;�&nbsp;&nbsp;&nbsp;&nbsp;
	<A class=Link href="javascript:submeter(2,1,<%=id_wk%>,2)"><%=strProgTuristica%></A>
</p>


<table class=tabelaProgramacao>
  <tr class=CelulaTitulo>
    <td width=100 align=middle><%=strPeriodo%></td>
    <td colspan=2 align=middle><%=strDia19%></td>
  </tr>
  <tr class=CelulaCorpo>
    <td width=100 align=middle>08:30 - 10:15</td>
    <td width=250>
		<b>
			<%=strCursoPreCongresso%><br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,8)">
				REJANE RIBEIRO-ROTTA
			</A>
		</b>
    </td>
    <td><%=strSambaqui%></td>
  </tr>
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>10:15 - 10:45</td>
    <td colspan=2>Coffee break</td>
  </tr>
  
	<tr class=CelulaCorpo>
		<td width=100 align=middle>10:45 - 12:30</td>
		<td width=250>
			<b>
				<%=strCursoPreCongresso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,6)">
					DALE MILES
				</A>
			</b>
		</td>
		<td><%=strSambaqui%></td>
	</tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>12:30 - 14:00</td>
    <td colspan=2><%=strAlmoco%></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>14:00 - 15:45</td>
		<td width=250>
			<b>
				<%=strCursoPreCongresso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,2)">
					MARCELO CAVALCANTI
				</A>
			</b>
		</td>
    <td><%=strSambaqui%></td>
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>15:45 - 16:15</td>
    <td colspan=2>Coffee break</td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>16:15 - 18:00</td>
		<td width=250>
			<b>
				<%=strCursoPreCongresso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,3)">
					ALLAN FARMAN
				</A>
			</b>
		</td>
    <td><%=strSambaqui%></td>
  </tr>
  
  <!-- ************************************ -->
  
  <tr class=CelulaTitulo>
    <td width=100 align=middle><%=strPeriodo%></td>
    <td colspan=2 align=middle><%=strDia20%></td>
  </tr>
  <tr class=CelulaCorpo>
    <td width=100 align=middle>08:30 - 10:15</td>
    <td width=250>
		<b>
			<%=strSimposio%> 1<br>
			(<%=strRadDigital%>)<br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,3)">
				FARMAN
			</A>,&nbsp;
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,6)">
				MILES
			</A>&nbsp;&&nbsp; 
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,11)">
				WENZEL
			</A>
		</b>
	</td>
    <td><%=strSambaqui%></td>
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>10:15 - 10:45</td>
    <td colspan=2>Coffee break</td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>10:45 - 12:30</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
    </td>
    <td>
		
		<%=strJoaquina%>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
	</td>    
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>12:30 - 14:00</td>
    <td colspan=2><%=strAlmoco%></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>14:00 - 15:00</td>
    <td width=250>
			<b>
				<%=strCurso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,7)">
					TOHRU KURABAYASHI
				</A>
			</b>
    </td>
    <td>
		<%=strSambaqui%>
	 </td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td colspan=3><hr noshade color="#e8e8e8"></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>15:00 - 15:45</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b> (15:05-15:50)<br>(<%=strPremio%>)
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b>
    </td>
    <td>
		<%=strJoaquina%><br>&nbsp;
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>

  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>15:45 - 16:15</td>
    <td colspan=2>Coffee break</td>
  </tr>

  <tr class=CelulaCorpo>
    <td width=100 align=middle>16:15 - 18:00</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b> (at� as 17:00h)
    </td>
    <td>
		<%=strJoaquina%>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>
  
  <!-- ************************************ -->
  
  <tr class=CelulaTitulo>
    <td width=100 align=middle><%=strPeriodo%></td>
    <td colspan=2 align=middle><%=strDia21%></td>
  </tr>

  <tr class=CelulaCorpo>
    <td width=100 align=middle>08:30 - 09:30</td>
    <td width=250>
			<b>
				<%=strCurso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,4)">
					NEIL FREDERIKSEN
				</A>
			</b>
    </td>
    <td>
		<%=strSambaqui%>
	 </td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td colspan=3><hr noshade color="#e8e8e8"></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>09:35 - 10:20</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b>
    </td>
    <td>
		<%=strJoaquina%>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>

  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>10:15 - 10:45</td>
    <td colspan=2>Coffee break</td>
  </tr>

  <tr class=CelulaCorpo>
    <td width=100 align=middle>10:45 - 11:45</td>
    <td width=250>
			<b>
				<%=strCurso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,12)">
					ERIC WHAITES
				</A>
			</b>
    </td>
    <td>
		<%=strSambaqui%>
	 </td>
  </tr>

  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>12:30 - 14:00</td>
    <td colspan=2><%=strAlmoco%></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>14:00 - 15:45</td>
    <td width=250>
		<b>
				<%=strCurso%> (Portugu�s)<br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,9)">
					IZABEL RUBIRA
				</A>
		</b><br>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b>
    </td>
    <td>
		<%=strJoaquina%><br>&nbsp;<br>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>15:45 - 16:15</td>
    <td colspan=2>Coffee break</td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>16:15 - 18:00</td>
    <td width=250>
		<b>
				<%=strCurso%> (Espanhol)<br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,10)">
					JAIME VALENZUELA
				</A>
		</b><br>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b> (at� as 17:00h)
    </td>
    <td>
		<%=strJoaquina%><br>&nbsp;<br>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>
  
  <!-- ************************************ -->
  
  <tr class=CelulaTitulo>
    <td width=100 align=middle><%=strPeriodo%></td>
    <td colspan=2 align=middle><%=strDia22%></td>
  </tr>
  <tr class=CelulaCorpo>
    <td width=100 align=middle>08:30 - 09:30</td>
    <td width=250>
			<b>
				<%=strCurso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,5)">
					CHRISTINA LINDH
				</A>
			</b>
    </td>
    <td>
		<%=strSambaqui%>
	 </td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td colspan=3><hr noshade color="#e8e8e8"></td>
  </tr>

  <tr class=CelulaCorpo>
    <td width=100 align=middle>09:30 - 10:15</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b> (09:35-10:15)
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b>
    </td>
    <td>
		<%=strJoaquina%>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>10:15 - 10:45</td>
    <td colspan=2>Coffee break</td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>10:45 - 11:30</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b>
    </td>
    <td>
		<%=strJoaquina%>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td colspan=3><hr noshade color="#e8e8e8"></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>11:30 - 12:30</td>
    <td width=250>
			<b>
				<%=strCurso%><br>
				<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,11)">
					ANN WENZEL
				</A>
			</b>
    </td>
    <td>
		<%=strSambaqui%>
	 </td>
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>12:30 - 14:00</td>
    <td colspan=2><%=strAlmoco%></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>14:00 - 15:45</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b> (14:00-14:45)
    </td>
    <td>
		<%=strJoaquina%>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>15:45 - 16:15</td>
    <td colspan=2>Coffee break</td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>16:15 - 18:00</td>
    <td width=250>
		<b><%=strAssembleia%></b>
    </td>
    <td>
		<%=strSambaqui%>
	</td>
  </tr><!-- ************************************ -->
  <tr class=CelulaTitulo>
    <td width=100 align=middle><%=strPeriodo%></td>
    <td colspan=2 align=middle><%=strDia23%></td>
  </tr>

  <tr class=CelulaCorpo>
    <td width=100 align=middle>08:30 - 10:15</td>
    <td width=250>
		<b>
			<%=strSimposio%><br>
			(<%=strAdvanced%>)<br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,1)">
				BROWN
			</A>&nbsp;&&nbsp;
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,7)">
				KURABAYASHI
			</A>
		</b>
	</td>
    <td><%=strSambaqui%></td>
  </tr>
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>10:15 - 10:45</td>
    <td colspan=2>Coffee break</td>
  </tr>
  
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>10:45 - 11:45</td>
    <td width=250>
		<b><%=strSessaoCientifica%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strConferencia%></b>
		<hr noshade color="#e8e8e8">
		<b><%=strPaineis%></b>
    </td>
    <td>
		<%=strJoaquina%>
		<hr noshade color="#e8e8e8">
		<%=strSambaqui%>
		<hr noshade color="#e8e8e8">
		<%=strCampeche%>
	</td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td colspan=3><hr noshade color="#e8e8e8"></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>11:50 - 12:30</td>
    <td width=250>
		<b><%=strCerEncerramento%></b>
    </td>
    <td>
		<%=strSambaqui%>
	</td>
  </tr>
  
  
  <tr class=CelulaCorpoCinza>
    <td width=100 align=middle>12:30 - 14:00</td>
    <td colspan=2><%=strAlmoco%></td>
  </tr>
  
  <tr class=CelulaCorpo>
    <td width=100 align=middle>14:00 - 18:00</td>
    <td width=200 colspan=2>
		<b><%=ucase(strProgTuristica)%></b>
    </td>
  </tr>

</table>
&nbsp;<br>
&nbsp;<br>
&nbsp;<br>
