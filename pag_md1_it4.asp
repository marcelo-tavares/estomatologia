<p class="Titulo" align="center">CONVITE DA PRESIDENTE DA SOBE</p>

<p class="textoComum">
	<img SRC="images/Foto_Lelia.jpg" align="right" hspace="5" WIDTH="173" HEIGHT="200">Em julho pr�ximo passado assumimos a Presid�ncia da Sociedade Brasileira de Estomatologia (SOBE), convocada que fomos para esta nobre miss�o. Para n�s que fazemos a atual Diretoria, constitui motivo de grande satisfa��o estarmos � frente de uma sociedade que enobrece a todos que dela participam pelo seu perfil cient�fico e de valoriza��o de profissionais da Odontologia Brasileira.
</p>
<p class="textoComum">
Ficamos felizes em ter como sede do pr�ximo XIII Congresso e  XXXI Jornada Brasileira de Estomatologia a cidade de Florian�polis-SC. Estamos acompanhando  o trabalho da equipe que est� sob a lideran�a da ilustre e competente colega_Professora Liliane Janete Grando. Sabemos do seu entusiasmo e, acreditamos que o evento ser� coroado de �xito pela forma dedicada e respons�vel com que est� sendo encarada a organiza��o do mesmo, haja vista que teremos, � disposi��o dos congressistas que dele participarem, uma excelente programa��o cient�fica contando com a participa��o de nomes exponenciais da Estomatologia brasileira e internacional.
</p>
<p class="textoComum">
Do ponto de vista social a programa��o est� sendo cuidadosamente elaborada oferecendo momentos de laser e descontra��o � quantos estiverem presentes ao evento, tudo feito com carinho, especialmente para voc� sobiano e seus familiares.
</p>
<p class="textoComum">
Esperamos que este seja mais um momento de congrega��o de Estomatologistas, Patologistas, Cirurgi�es, Radiologistas,alunos de p�s-gradua��o, cl�nicos e acad�micos, todos imbu�dos dos bons prop�sitos de elevar o nome de uma sociedade que atingiu a sua maior idade mostrando que sabe crescer nas adversidades.
</p>
<p class="textoComum">
Portanto, estimados colegas, conclamamos a todos para participarem n�o s� do  evento, mas, tamb�m, da confraterniza��o entre amigos que a SOBE, maduramente, saber� oferecer aos presentes, sem esquecer, � claro, os momentos especialmente preparados para o congra�amento cient�fico com troca de id�ias e de experi�ncias nos cursos, palestras, casos cl�nicos e pain�is.  
</p>
<p class="textoComum">
Cordialmente,
</p>
<p class="textoComum">
<b>Professora L�lia Batista de Souza</b><br>
Presidente da Sociedade Brasileira de Estomatologia-SOBE
</p>
