<input type=hidden name=pr3 value=1>
<%
strProgCientifica = "Programa��o Cient�fica"
strProgSocial = "Programa��o Social"
strProgTuristica = "Programa��o Tur�stica"
strPeriodo = "Per�odo"
strDia16 = "16 de julho (S�bado)"
strDia17 = "17 de julho (Domingo)"
strDia18 = "18 de julho (2� Feira)"
strDia19 = "19 de julho (3� Feira)"
strDia20 = "20 de julho (4� Feira)"
strCursoPreCongresso = "Curso Pr�-Congresso"
strConferencia = "Confer�ncias"
strSessaoCientifica = "Sess�o Cient�fica"
strPaineis = "Sess�o de Pain�is"
strSimposio = "Simp�sio"
strCurso = "Curso"
strRadDigital = "Radiologia Digital"
strCerEncerramento = "Cerim�nia de Encerramento"
strAlmoco = "Almo�o"
strPremio = "Pr�mio IADMFR"
strAssembleia = "ASSEMBL�IA GERAL"
strAdvanced = "Interpreta��o Radiogr�fica Avan�ada"
strJoaquina = "Sala Joaquina"
strSambaqui = "Sala Sambaqui"
strCampeche = "Sala Campeche"
%>
<p class="Titulo" align="center"><%=strProgCientifica%></p>


<table class=tabelaProgramacao>
	<tr class=CelulaTitulo>
		<td width=100 align=middle><%=strPeriodo%></td>
		<td colspan=2 align=middle><%=strDia16%></td>
	</tr>
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>MANH�</td>
		<td colspan=2 valign=middle>
			Abertura da Secretaria do Congresso
			<hr noshade>
			Pr�-congresso: <b>III Encontro Internacional de Odontogeriatria</b>
			<br><br>
			<b>Enfermidades sist�micas cr�nicas mais freq�entes nos idosos</b>: repercuss�es na cavidade bucal e implica��es no procedimento odontol�gico <A class=Link href="javascript:submeter(2,8,<%=id_wk%>,2)">Ver conte�do do curso</A>
			<br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,11)">
				Prof. Guillermo Machuca Portillo (Espanha)
			</A>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td width=100 align=center valign=middle>TARDE</td>
		<td colspan=2 valign=middle>
			Pr�-congresso
		</td>
	</tr>
  
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>NOITE</td>
		<td colspan=2 valign=middle>
			Abertura Oficial do evento seguida de Coquetel
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td colspan=3>&nbsp</td>
	</tr>
  
  <!-- ************************************ -->
  
  <tr class=CelulaTitulo>
    <td width=100 align=middle>&nbsp;</td>
    <td colspan=2 align=middle><%=strDia17%></td>
  </tr>
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>MANH�</td>
		<td colspan=2 valign=middle>
			Curso Nacional<br>
			<b>Examinando o paciente e interpretando imagens : um grande passo para o diagn�stico</b><br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,3)">
				Prof. Jos� Humberto Damante
			</A>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td width=100 align=center valign=middle>TARDE</td>
		<td colspan=2 valign=middle>
			Apresenta��o de Casos Cl�nicos
		</td>
	</tr>
  
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>NOITE</td>
		<td colspan=2 valign=middle>
			<b>F�rum de Ensino em Estomatologia</b>
		</td>
	</tr>

	<tr class=CelulaCorpo>
		<td colspan=3>&nbsp</td>
	</tr>
  
  <!-- ************************************ -->
  
  <tr class=CelulaTitulo>
    <td width=100 align=middle>&nbsp;</td>
    <td colspan=2 align=middle><%=strDia18%></td>
  </tr>
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>MANH�</td>
		<td colspan=2 valign=middle>
			Curso Internacional<br>
			Tema: <b>C�ncer Bucal</b><br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,1)">
				Prof. Sol Silverman (EUA)
			</A>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td width=100 align=center valign=middle>TARDE</td>
		<td colspan=2 valign=middle>
			Curso Internacional<br>
			Tema: <b>C�ncer Bucal</b><br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,1)">
				Prof. Sol Silverman (EUA)
			</A>
		</td>
	</tr>
  
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>NOITE</td>
		<td colspan=2 valign=middle>
			Festa  de Lan�amento do livro Fundamentos de Medicina Oral	
		</td>
	</tr>

	<tr class=CelulaCorpo>
		<td colspan=3>&nbsp</td>
	</tr>
  
  <!-- ************************************ -->
  
  <tr class=CelulaTitulo>
    <td width=100 align=middle>&nbsp;</td>
    <td colspan=2 align=middle><%=strDia19%></td>
  </tr>
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>MANH�</td>
		<td colspan=2 valign=middle>
			Apresenta��o de Casos Cl�nicos
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td width=100 align=middle>TARDE</td>
		<td colspan=2 valign=middle>
			Curso Nacional<br>
			Tema: <b>Quimioterapia</b><br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,4)">
				Prof. Celso Massumoto (Brasil)
			</A>
			<hr noshade>
			Apresenta��o de Casos Cl�nicos
		</td>
	</tr>
  
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>NOITE</td>
		<td colspan=2 valign=middle>
			Assembl�ia Geral da SOBE
		</td>
	</tr>

	<tr class=CelulaCorpo>
		<td colspan=3>&nbsp</td>
	</tr>
  
  <!-- ************************************ -->
  
  <tr class=CelulaTitulo>
    <td width=100 align=middle>&nbsp;</td>
    <td colspan=2 align=middle><%=strDia20%></td>
  </tr>
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>MANH�</td>
		<td colspan=2 valign=middle>
			<b>Semin�rio de Histopatologia</b><br><br>
			Curso Nacional<br>
			<b>Diagn�stico diferencial e conduta cl�nica face a les�es branco-avermelhadas de boca</b><br>
			<A class=Link href="javascript:submeter(2,4,<%=id_wk%>,2)">
				Prof. Abel Silveira Cardoso (Brasil)
			</A>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td width=100 align=center valign=middle>TARDE</td>
		<td colspan=2 valign=middle>
			Casos Consultivos
		</td>
	</tr>
  
	<tr class=CelulaCorpo>
		<td width=100 align=center valign=middle>NOITE</td>
		<td colspan=2 valign=middle>
			Jantar de Encerramento
		</td>
	</tr>

	<tr class=CelulaCorpo>
		<td colspan=3>&nbsp</td>
	</tr>

</table>
&nbsp;<br>
&nbsp;<br>
&nbsp;<br>
