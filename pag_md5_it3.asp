<% 
if id_wk = 1 then 
	%>
	<p class="Titulo" align="center">Forma de Pagamento</p>
	<p class="textoComum">
	A <a href="javascript:submeter(5,4,<%=id_wk%>,0);" class=link>ades�o</a> ao Congresso e Pr�-Congresso poder� ser paga em at� <b>5 parcelas iguais</b>, sendo a 1� venc�vel no dia da inscri��o e as demais no mesmo dia dos meses subsequentes. A �ltima parcela n�o poder� ultrapassar o dia <b>30/04/2003</b>.
	</p><!--default.asp?md=5&it=4-->
	<p class="textoComum">
	<b>PARA OS S�CIOS DA ABRO</b>:<br>O valor a ser pago como ades�o ao pr�-congresso poder� ser convertido em afilia��o � IADMFR, dando-lhe direito a:
	</p>
	<p class="textoComum">
	1) Pagamento de US$ 200,00 pelo Congresso, com direito ao Pr�-Congresso;<br>
	2) 6 edi��es da <b>Revista DentoMaxillofacial Radiology</b>;<br>
	3) 2 edi��es da <b>Newsletter</b>;<br>
	4) Participa��o em eventos da IADMFR com descontos significativos.
	<br><br>
	<b>Obs.:</b> * O "d�lar congresso" (U$1,00 = R$2,00) se aplica <b>APENAS</b> aos valores de ades�o ao Congresso/Pr�-Congresso, n�o podendo ser utilizado em rela��o � filiac�o a IADMFR (atrav�s do site: www.iadmfr.org) nem aos valores de hospedagem ou da programa��o tur�stica.
	</p>
	<% 
else 
	%>
	<p class="Titulo" align="center">Registration Procedures</p>
	<p class="textoComum">
	The preferred option is to make your <a href="javascript:submeter(5,4,<%=id_wk%>,0);" class=link>registration via the website</a>, where you will find further information, including those related to payment. Alternatively, you can provide the information contained in the registration form to the Secretariat via regular mail, e-mail or fax.
	<% 
end if 
%>
