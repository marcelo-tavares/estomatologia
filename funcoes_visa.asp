<%

function Moeda(valor)
	valor=valor*100
    pparte=left(valor,len(valor)-2)
    decima=right(valor,2)
    Moeda=pparte & "," & decima
end function

function DadosParticipante()
%>
<!--#include file="conn.asp"-->
<%
	
	set rs = server.CreateObject("ADODB.Recordset")
	set rs = conn.Execute("SELECT * FROM TB_Categoria WHERE cd_categoria = " & session("cd_categoria"))
	strCategoria = trim(rs("de_categoriaPt")) & " ("
	pc = false
	if session("sn_precongresso") then
		strCategoria = strCategoria & "Pr�-Congresso"
		pc = true
	end if
	
	if session("sn_congresso") then
		if pc then strCategoria = strCategoria & " / "
		strCategoria = strCategoria & "Congresso"
		pc = true
	end if
	
	strCategoria = strCategoria & ")"

	' DADOS DO PARTICIPANTE
	pedido="Dados do participante:" & chr(13)
	pedido = pedido & "------------------------------------------------------------" & chr(13)
	pedido=pedido & "Nome: " & session("nm_participante") & " - " & session("de_email") & " - " & session("nr_fone") & chr(13)& chr(13)

	' DADOS DO PEDIDO DE COMPRA.
	pedido=pedido & "Detalhes da Inscri��o:" & chr(13)

	'Participante
	pedido = pedido & "Categoria: " & strCategoria
	pedido = pedido & chr(13)
	
	'Acompanhantes
	if session("nr_acompanhantes") > 0 then
		if session("nr_acompanhantes") = 1 then
			strAcomp = "acompanhante"
		else
			strAcomp = "acompanhantes"
		end if
		pedido = pedido & session("nr_acompanhantes") & " " & strAcomp & chr(13)
	end if
	pedido = pedido & "------------------------------------------------------------" & chr(13)
	pedido = pedido & "Total: R$: " & moeda(session("vl_inscricaoR")) & chr(13)
	pedido = pedido & "------------------------------------------------------------" & chr(13)

	set conn = nothing
	set rs = nothing
	
	DadosParticipante = pedido
end function
'
%>
