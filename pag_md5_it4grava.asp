
<% 
function buscaCodigo()
	buscaCodigo = ""
	%>
	<!--#include file="conn.asp"-->
	<%
	Set oConn = Server.CreateObject("ADODB.Connection") 
	set oRs = server.CreateObject("ADODB.Recordset")

	'Response.Write cnpath
	'response.end

	on error resume next
	oConn.Open cnpath
	
	if err.number <> 0 then
		Response.Write "<h5>N�o foi poss�vel conectar com o banco de dados. Por favor tente mais tarde.</h5>"
		Response.Write err.number & " - " & err.description & "<BR>"
		buscaCodigo = -9
		return
	end if
	
	oConn.BeginTrans

	with oRs
		.ActiveConnection = oConn
		.CursorType = 2 'adOpenDynamic 
		.LockType = 2 'adLockOptmistic

		.Open "TB_Inscricao"

		'On Error Resume Next
		.AddNew
		retorno = oRs("cd_inscricao").value
		.Update
		
		if err.number <> 0 then
			.Close
			buscaCodigo = -1
			oConn.RollbackTrans
		else
			.Close
			buscaCodigo = retorno
			oConn.CommitTrans
		end if
		oConn.Close
	end with
	
	set oConn = nothing
	set oRs = nothing
end function

'-------------------------------------------------------------------------------

function gravaParcelas(cd_participante, nr_parcelas, vl_parcela, dt_inscricao)

	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn = Server.CreateObject("ADODB.Connection") 
	Conn.Open cnpath

	set rs = server.CreateObject("ADODB.Recordset")
	'Response.Write "<BR>" & cd_participante & ", " & nr_parcelas & ", " & vl_parcela &  ", " & dt_inscricao & "<BR>"
	'Response.Flush

	with rs
		.ActiveConnection = Conn
		.CursorType = 2 'adOpenDynamic 
		.LockType = 2 'adLockOptmistic
		.Open "TB_Parcelas"
		
		for i=1 to  nr_parcelas
			.AddNew
			.Fields("cd_participante").Value = cd_participante
			.Fields("cd_parcela").Value = i
			.Fields("vl_parcela").Value = cdbl(replace(vl_parcela,".",","))
			if i = 1 then
				dt_vencimento = Cdate(dt_inscricao) + 15
			else
				dt_vencimento = Cdate(dt_inscricao) + (i-1) * 30 + 15
			end if	
			.Fields("dt_parcela").Value = cdate(dt_vencimento)
			.Fields("sn_pago").Value = false
			.Update
		next
		.Close
	end with

	gravaParcelas = true
	
	set Conn = nothing
	set rs = nothing
end function

'-------------------------------------------------------------------------------

function gravaInscricao(cd_obtido,idioma, de_participante)
	dim ac(6)

	if cd_obtido = 0 then
		cd_obtido = buscaCodigo
	end if
	
	if cd_obtido > 0 then
		' Gravar Inscri��o
		%>
		<!--#include file=conn.asp -->
		<%
		Set Conn1 = Server.CreateObject("ADODB.Connection") 

		if instr(de_participante, "|") > 0 then
			ar_wk = split(de_participante, "|")
			ar_participante = split(ar_wk(0), "#")
			ar_cartaoVisa = split(ar_wk(1), "#")
		else
			ar_participante = split(de_participante, "#")
			redim ar_cartaoVisa(2)
			ar_cartaoVisa(0) = ""
			ar_cartaoVisa(1) = ""
		end if
		
		Conn1.Open cnpath
		Conn1.BeginTrans
		
		
		if cint(ar_participante(15)) = 1 then 
			nr_membro = ar_participante(16)
		end if
		
		if len(trim(nr_membro)) = 0 then nr_membro = "-"
		
		' 1- Gravar Dados do Participante
		
		sn_pc = "true"
		if not ar_participante(18) then sn_pc = "false"

		sn_c = "true"
		if not ar_participante(19) then sn_c = "false"
		
		sn_og = "true"
		if not ar_participante(30) then sn_og = "false"
		
		strSQL = _
		"INSERT INTO TB_Participantes (" _
		& "cd_participante, nm_participante, de_endereco, de_cidade, cd_estado, " _
		& "cd_CEP, de_pais, nr_fone, nr_fax, de_email, cd_cpf, cd_identidade, de_emissorIdentidade, " _
		& "cd_categoria, dt_inscricao, dt_confirmacao, vl_inscricao, vl_inscricaoR, " _
		& "tp_pagamento, sn_precongresso, sn_congresso, nr_membro, sn_socioOdontogeriatria) " _
		& "VALUES (" _
		& cd_obtido & ", '" & ucase(ar_participante(0)) & "', '" _
		& ar_participante(7) & "', '" & ar_participante(8) & "', " _
		& ar_participante(9) & ", '" & ar_participante(11) & "', '" _
		& ar_participante(10) & "', '"  _
		& ar_participante(12) & "', '" & ar_participante(13) & "', '" _
		& ar_participante(14) & "', '" & ar_cartaoVisa(1) & "', '-', '" _
		& ar_cartaoVisa(0) & "', " _
		& ar_participante(15) & ", '" & ar_participante(25) & "', '" _
		& ar_participante(26) & "', " & ar_participante(28) & ", " _
		& ar_participante(28) & ", " _
		& ar_participante(21) & ", " & sn_pc & ", " _
		& sn_c & ", '" & nr_membro & "', " & sn_og & ")"
		
		'Response.Write strSQL
		'Response.End
		
		'set objEmail = server.CreateObject("CDONTS.NewMail")
		'objEmail.Send "estomatologia@tavares.eti.br","estomatologia@tavares.eti.br","DEBUG CONGRESSO",strSQL
		'set objEmail=nothing
		
		'on error resume next
		
		Conn1.Execute strSQL

		if err.number <> 0 then
			gravaInscricao = -1 * (10 * cd_codigo + 1)
			Conn1.RollbackTrans
			set Conn1 = nothing
			return
		else
			if Cint(ar_participante(1)) > 0 then
				ac(1) = ar_participante(2)
				ac(2) = ar_participante(3)
				ac(3) = ar_participante(4)
				ac(4) = ar_participante(5)
				ac(5) = ar_participante(6)
				set rs1 = server.CreateObject("ADODB.Recordset")
				with rs1
					.ActiveConnection = Conn1
					.CursorType = 2
					.LockType = 2
					.Open "TB_Acompanhantes"
					
					for i = 1 to Cint(ar_participante(1))
						.AddNew
						rs1("cd_participante") = cd_obtido
						rs1("nm_acompanhante") = ac(i)
					next
					.Update
				end with
				if err.number <> 0 then
					Response.Write "<h5>Erro ao registrar acompanhantes. Favor tentar mais tarde.</h5>"
					gravaInscricao = -1 * (10 * cd_codigo + 2)
					Conn1.RollbackTrans
					set rs1 = nothing
					set Conn1 = nothing
					return
				end if
				set rs1 = nothing
			end if
		end if
	else
		Response.Write "<h5>Erro ao obter c�digo.  Favor tentar mais tarde.</h5>"
		gravaInscricao = -1 * (10 * cd_codigo + 3)
		return 
	end if
	Conn1.CommitTrans
	set Conn1 = nothing
	gravaInscricao = cd_obtido

	on error goto 0
	gravaParcelas cd_obtido, Cint(ar_participante(20)), ar_participante(29), Cdate(ar_participante(25))

end function

'-------------------------------------------------------------------------------
function removeInscricao(cd_participante)

	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn33 = Server.CreateObject("ADODB.Connection") 
	set rs = server.CreateObject("ADODB.Recordset")

	Conn33.Open cnpath
	
	strSQL = "DELETE TB_Participantes WHERE cd_participante = " & cd_participante
	Conn33.Execute strSQL
	set Conn33 = nothing
	set rs = nothing
end function

'-------------------------------------------------------------------------------
%>