<% 
strTitulo = "Comiss�o Organizadora"
%>

<p class="Titulo" align="center"><%=strTitulo%></p>
<p class="textoComum">

<table width=100% class=TabelaProgramacao>
	<tr class="CelulaCorpoCinza">
		<td>
			Presidente
		</td>
		<td>
			<b>Liliane Janete Grando</b><br>
			Mestre em Odontopediatria pela UFSC<br>
			Doutora em Estomatologia pela PUCRS<br>
			Fellowship na State University of New York at Stony Brook<br>
			Profa da Disciplina de Patologia Bucal e do Ambulat�rio de Estomatologia do Hospital Universit�rio da UFSC, Fpolis, SC<br>
			Profa Consultora da Disciplina de Estomatologia da UNISUL, Tubar�o, SC<br>
		</td>
	</tr>

	<tr class="CelulaCorpo">
		<td>
			Vice-Presidente
		</td>
		<td>
			<b>In�s Beatriz da Silva Rath</b><br>
			Doutora em Odontopediatria pela UFSC<br>
			Profa Adjunto da Disciplina de Estomatologia I da UFSC, Florian�polis, SC<br>
		</td>
	</tr>

	<tr class="CelulaCorpoCinza">
		<td>
			Tesouraria
		</td>
		<td>
			<b>M�rcio Corr�a</b><br>
			Doutor em Estomatologia pela PUCRS<br>
			Professor da disciplina de Radiologia do Departamento de Estomatologia da UFSC<br>
		</td>
	</tr>

	<tr class="CelulaCorpo">
		<td>
			Secretaria
		</td>
		<td>
			<b>Ana Paula Soares Fernandes</b><br>
			Mestre e Doutora pela UFSC<br>
			Especialista em Odontopediatria ABOSC<br>
			Profa Adjunto II da Disciplina de Estomatologia e Estomatologia para Pacientes com Necessidades Especiais da UFSC, Fpolis, SC<br>
		</td>
	</tr>


	<tr class="CelulaCorpoCinza">
		<td>
			Coordena��o Cient�fica
		</td>
		<td>
			<b>Maria In�s Meurer</b><br>
			Especialista em Radiologia Odontol�gica e Imaginologia pela UFSC<br>
			Doutora em Estomatologia pela PUCRS<br>
			Professora da Disciplina de Patologia Geral e do Ambulat�rio de Estomatologia da Hospital Universit�rio da UFSC, Florian�polis, SC.<br>
			<br><br>
			<b>Elena Riet</b><br>
			Mestre em Patologia Odontol�gica pela UFMG<br>
			Doutora em Patologia Bucal pela USP<br>
			<br><br>
			<b>Christine Kalvelage Philippi</b><br>
			Mestre em Patologia Bucal pela UFRGS<br>
			Professora da Disciplina de Histologia nos Cursos de Odontologia e Enfermagem da Univali<br>
			Respons�vel pelo Servi�o de Diagn�stico Histopatol�gico de Les�es Bucais da Univali<br>
			<br><br>
			<b>S�nia Maria L�ckmann Fabro</b><br>
			Mestre em Odontopediatria pela UFSC<br>
			Professora Adjunto da Disciplina de Patologia Geral e do Ambulat�rio de Estomatologia do HU-UFSC<br>
		</td>
	</tr>

	<tr class="CelulaCorpo">
		<td>
			Coordena��o Social e Turistica
		</td>
		<td>
			<b>Jos� Carlos Oleiniski</b><br>
			Professor das disciplinas de Pr�tese Total e Odontogeriatria pela UFSC<br>
			Doutor pela Universidad Complutense de Madrid<br>
			Presidente da Socidade Brasileira de Odontogeriatria<br>
			<br><br>
			<b>Denise Maria Belliard Oleiniski</b><br>
			Professora das disciplinas de Estomatologia e Odontogeriatria pela UFSC<br>
			Doutora pela Universidad Complutense de Madrid<br>
			Diretora Cient�fica da Sociedade Brasileira de Odontogeriatria
		</td>
	</tr>

	<tr class="CelulaCorpoCinza">
		<td>
			Coordena��o Empresarial e de Capta��o de Recursos
		</td>
		<td>
			<b>M�rcio Corr�a</b><br>
			Doutor em Estomatologia pela PUCRS<br>
			Professor da disciplina de Radiologia do Departamento de Estomatologia da UFSC<br>
			<br><br>
			<b>In�s Beatriz da Silva Rath</b><br>
			Doutora em Odontopediatria pela UFSC<br>
			Profa Adjunto da Disciplina de Estomatologia I da UFSC, Florian�polis, SC<br>
		</td>
	</tr>

	<tr class="CelulaCorpo">
		<td>
			Coordena��o de Recep��o e Hospedagem
		</td>
		<td>
			<b>Henrique Tavares</b><br>
			Especialista em Radiologia Odontol�gica e Imaginologia pela UFSC<br>
			Mestre em Diagn�stico Bucal (Radiologia) pela USP<br>
			<br><br>
			<b>Aira Maria Santos Bonfim</b>
			Especialista, Mestre e Doutora em CTBMF<br>
			Colaborateur Service d�Implantologie, Universit� de G�n�ve, Suisse-CH<br>
			Consultora Cient�fica da Revista Brasileira de Cirurgia<br>
			Membro da Equipe de Cirurgia Bucomaxilofacial do Hospital Florian�polis<br>
			Estagi�ria do Servi�o de Estomatologia do Hospital Universit�rio - UFSC
		</td>
	</tr>

	<tr class="CelulaCorpoCinza">
		<td>
			Coordena��o de Apoio F�sico e Log�stico
		</td>
		<td>
			<b>Filipe Ivan Daniel</b><br>
			Especializando em Radiologia Odontol�gica e Imaginologia pela UFSC<br>
			Estagi�rio do Servi�o de Estomatologia do Hospital Universit�rio da UFSC<br>
			<br><br>
			<b>In�s Beatriz da Silva Rath</b><br>
			Doutora em Odontopediatria pela UFSC<br>
			Profa Adjunto da Disciplina de Estomatologia I da UFSC, Florian�polis, SC<br>
		</td>
	</tr>

	<tr class="CelulaCorpo">
		<td>
			Coordena��o de Divulga��o
		</td>
		<td>
			<b>Filipe Ivan Daniel</b><br>
			Especializando em Radiologia Odontol�gica e Imaginologia pela UFSC<br>
			Estagi�rio do Servi�o de Estomatologia do Hospital Universit�rio da UFSC<br>
			<br><br>
			<b>Michella Dinah Zastrow</b><br>
			Especialista em Radiologia pela UFSC<br>
			Mestranda em Radiologia pela UFSC<br>
			Professora das Disciplinas de Radiologia e Estomatologia da UNISUL, Tubar�o/SC
		</td>
	</tr>

	<tr class="CelulaCorpoCinza">
		<td colspan=2>
		</td>
	</tr>

</table>



