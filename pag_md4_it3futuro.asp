<% 
if id_wk = 1 then
	strTitulo = "Hospedagem"
	strH1Sgl = "US$ 69.00"
	strH1Dbl = "US$ 73.00"
	strH1Tpl = "-"
	
	strH2Sgl = "US$ 58.00"
	strH2Dbl = "US$ 62.50"
	strH2Tpl = "US$ 77.00"
	
	strH3Sgl = "US$ 50.00"
	strH3Dbl = "US$ 60.00"
	strH3Tpl = "US$ 70.00"
	
	strH4Sgl = "US$ 38.00"
	strH4Dbl = "US$ 48.00"
	strH4Tpl = "-"
	
	strH5Sgl = "US$ 20.00"
	strH5Dbl = "US$ 25.00"
	strH5Tpl = "-"
	
	strH6Sgl = "US$ 38.00"
	strH6Dbl = "US$ 45.00"
	strH6Tpl = "-"
	
	strH7Sgl = "US$ 33.00"
	strH7Dbl = "US$ 47.00"
	strH7Tpl = "US$ 56.00"
	
	strH8Sgl = "US$ 30.00"
	strH8Dbl = "US$ 35.00"
	strH8Tpl = "US$ 50.00"
	
	strH9Sgl = "US$ 28.00"
	strH9Dbl = "US$ 32.00"
	strH9Tpl = "-"
	
	strDistancia = "Distancia<br>at� o congresso"
	strCaminhar = "a p�"
	strDirigir = "em carro"
	strObs = "Obs.:<br>As tarifas acima representam valores aproximados em d�lar devido �s recentes flutua��es cambiais do Real, est�o sujeitas a altera��es sem aviso pr�vio e devem ser confirmadas na reserva."
	strObs2 = "Para garantir os melhores pre�os de di�rias, as reservas de hotel devem ser feitas diretamente com a ag�ncia oficial de turismo do 14th ICDMFR:"	
	strLocalizacao = "Localiza��o dos hot�is pr�ximos ao Centro de Conven��es (CentroSul)"
else
	strTitulo = "Accommodation"
	strH1Sgl = "US$ 69.00"
	strH1Dbl = "US$ 73.00"
	strH1Tpl = "-"
	
	strH2Sgl = "US$ 58.00"
	strH2Dbl = "US$ 62.50"
	strH2Tpl = "US$ 77.00"
	
	strH3Sgl = "US$ 50.00"
	strH3Dbl = "US$ 60.00"
	strH3Tpl = "US$ 70.00"
	
	strH4Sgl = "US$ 38.00"
	strH4Dbl = "US$ 48.00"
	strH4Tpl = "-"
	
	strH5Sgl = "US$ 20.00"
	strH5Dbl = "US$ 25.00"
	strH5Tpl = "-"
	
	strH6Sgl = "US$ 38.00"
	strH6Dbl = "US$ 45.00"
	strH6Tpl = "-"
	
	strH7Sgl = "US$ 33.00"
	strH7Dbl = "US$ 47.00"
	strH7Tpl = "US$ 56.00"
	
	strH8Sgl = "US$ 30.00"
	strH8Dbl = "US$ 35.00"
	strH8Tpl = "US$ 50.00"
	
	strH9Sgl = "US$ 28.00"
	strH9Dbl = "US$ 32.00"
	strH9Tpl = "-"
	
	strDistancia = "Distance<br>to the venue"
	strCaminhar = "walk"
	strDirigir = "drive"
	strObs = "P.S.:<br>� Rates, given in US dollars, include full breakfast and are subject to changes without prior notice, especially due to recent fluctuations in the Brazilian currency (Real). Rates should be confirmed when making your reservation"	
	strObs2 = "To get the best rates, hotel reservations should be made directly with the Official Travel Agency of the 14th ICDMFR:"	
	strLocalizacao = "Location of the hotels nearby the Convention Center (CentroSul)"
end if 
%>

<p class="Titulo" align="center"><%=strTitulo%></p>

<p class="textoComum" align="left">
	<% if id_wk = 1 then %>
		O Centro de Conven��es escolhido para sediar o 14th ICDMFR � CentroSul � est� localizado no cora��o de Florian�polis. Devido a sua localiza��o estrat�gica no centro da cidade, facilitar� a hospedagem na rede hoteleira pr�xima, al�m de fornecer aos congressistas a chance de conviverem, por alguns dias, com os �manezinhos da ilha� e conhecerem a cultura, cren�as e h�bitos deste povo conhecido por sua grande hospitalidade.
		<br><br>
		Os hot�is conveniados ao 14th ICDMFR permitem uma diversidade de op��es aos Congressistas oriundos dos mais diversos cantos do Brasil e do exterior. O <b>Hotel Intercity</b> foi escolhido como o <b>Hotel Oficial</b> do Congresso.	
	<% else %>
		The venue for the 14th ICDMFR (CentroSul Convention Center) is located in the very heart of Florian�polis. Because of its strategic location in the downtown area of the city, it will facilitate accommodation in the nearby hotels, also giving the participants a chance to be in close contact with the typical residents of Florian�polis and to get familiar with the culture, beliefs and habits of this hospitable people.
		<br><br>
		To provide you with more affordable rates, the 14th ICDMFR has made arrangements with the following hotels, which will give you many options to choose from, as far as accommodation is concerned. The <b>Intercity Hotel</b> has been selected the <b>official hotel</b> for the 14th ICDMFR.
	<% end if %>
</p>

<% if id_wk = 1 then %>
<table width="100%" class="TabelaProgramacao">
	<tr class="CelulaTitulo">
		<td><b>HOTEL</b></td>
		<td align="center"><%=strDistancia%></td>
		<td align="center"><b>SGL</b></td>
		<td align="center"><b>DBL</b></td>
		<td align="center"><b>TPL</b></td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.intercityhotel.com.br" target="_topo">Intercity Hotel</a> (3*)</td>
		<td align="center">10-15 <%=strCaminhar%></td>
		<td align="center"><%=strH6Sgl%></td>
		<td align="center"><%=strH6Dbl%></td>
		<td align="center"><%=strH6Tpl%></td>
	</tr>
	<tr class="CelulaCorpo">
		<td><a href="http://www.accorhotels.com.br" target="_topo">Ibis</a> (3*)</td>
		<td align="center">20 <%=strCaminhar%></td>
		<td align="center"><%=strH5Sgl%></td>
		<td align="center"><%=strH5Dbl%></td>
		<td align="center"><%=strH5Tpl%></td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.accorhotels.com.br" target="_topo">Parthenon Lindacap</a> (3*)</td>
		<td align="center">20 <%=strCaminhar%></td>
		<td align="center"><%=strH2Sgl%></td>
		<td align="center"><%=strH2Dbl%></td>
		<td align="center"><%=strH2Tpl%></td>
	</tr>
	<tr class="CelulaCorpo">
		<td><a href="http://www.westcoral.com.br" target="_topo">Coral Plaza</a> (3*)</td>
		<td align="center">20 <%=strCaminhar%></td>
		<td align="center"><%=strH3Sgl%></td>
		<td align="center"><%=strH3Dbl%></td>
		<td align="center"><%=strH3Tpl%></td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.bluetree.com.br" target="_topo">Blue Tree Towers</a> (4*)</td>
		<td align="center">5-10 <%=strDirigir%></td>
		<td align="center"><%=strH1Sgl%></td>
		<td align="center"><%=strH1Dbl%></td>
		<td align="center">-</td>
	</tr>
	<tr class="CelulaCorpo">
		<td><a href="http://www.portodailha.com.br" target="_topo">Porto da Ilha</a> (3*)</td>
		<td align="center">25-30 <%=strCaminhar%></td>
		<td align="center"><%=strH4Sgl%></td>
		<td align="center"><%=strH4Dbl%></td>
		<td align="center">-</td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.baianorte.com.br" target="_topo">Baia Norte</a> (4*)</td>
		<td align="center">30 <%=strCaminhar%></td>
		<td align="center"><%=strH7Sgl%></td>
		<td align="center"><%=strH7Dbl%></td>
		<td align="center"><%=strH7Tpl%></td>
	</tr>
	<tr class="CelulaCorpo">
		<td><a href="http://www.bluetree.com.br" target="_topo">Faial</a> (3*)</td>
		<td align="center">5-10 <%=strCaminhar%></td>
		<td align="center"><%=strH8Sgl%></td>
		<td align="center"><%=strH8Dbl%></td>
		<td align="center"><%=strH8Tpl%></td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.iaccess.com.br/valerim" target="_topo">Valerim</a> (3*)</td>
		<td align="center">5-10 <%=strCaminhar%></td>
		<td align="center"><%=strH9Sgl%></td>
		<td align="center"><%=strH9Dbl%></td>
		<td align="center"><%=strH9Tpl%></td>
	</tr>
</table>
<% else %>
<table width="100%" class="TabelaProgramacao">
	<tr class="CelulaTitulo">
		<td><b>HOTEL</b></td>
		<td align="center"><%=strDistancia%></td>
		<td align="center"><b>SGL</b></td>
		<td align="center"><b>DBL</b></td>
		<td align="center"><b>TPL</b></td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.intercityhotel.com.br" target="_topo">Intercity Hotel</a> (3*)</td>
		<td align="center">10-15 <%=strCaminhar%></td>
		<td align="center"><%=strH6Sgl%></td>
		<td align="center"><%=strH6Dbl%></td>
		<td align="center"><%=strH6Tpl%></td>
	</tr>
	<tr class="CelulaCorpo">
		<td><a href="http://www.accorhotels.com.br" target="_topo">Parthenon Lindacap</a> (3*)</td>
		<td align="center">20 <%=strCaminhar%></td>
		<td align="center"><%=strH2Sgl%></td>
		<td align="center"><%=strH2Dbl%></td>
		<td align="center"><%=strH2Tpl%></td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.westcoral.com.br" target="_topo">Coral Plaza</a> (3*)</td>
		<td align="center">20 <%=strCaminhar%></td>
		<td align="center"><%=strH3Sgl%></td>
		<td align="center"><%=strH3Dbl%></td>
		<td align="center"><%=strH3Tpl%></td>
	</tr>
	<tr class="CelulaCorpo">
		<td><a href="http://www.bluetree.com.br" target="_topo">Blue Tree Towers</a> (4*)</td>
		<td align="center">5-10 <%=strDirigir%></td>
		<td align="center"><%=strH1Sgl%></td>
		<td align="center"><%=strH1Dbl%></td>
		<td align="center">-</td>
	</tr>
	<tr class="CelulaCorpoCinza">
		<td><a href="http://www.portodailha.com.br" target="_topo">Porto da Ilha</a> (3*)</td>
		<td align="center">25-30 <%=strCaminhar%></td>
		<td align="center"><%=strH4Sgl%></td>
		<td align="center"><%=strH4Dbl%></td>
		<td align="center">-</td>
	</tr>
</table>
<% end if %>

<p class="textoComum" align="left">
	<b><%=strObs%></b>
	<br><br>
	<%=strObs2%>
</p>


<% if id_wk = 1 then %>
	<p class="textoComum" align="left">
		<img SRC="images/bbtur_logo.gif" border="0" WIDTH="120" HEIGHT="20"><br><b>Ag�ncia de Viagens e Turismo</b><br>
		Rua Trajano, 265 - Loja 3<br>
		Fone: + 55 (48) 229-8500 / Fax: + 55 (48) 322-0460
		e-mail: <a href="mailto:andredutra@bbtur.com.br">andredutra@bbtur.com.br</a>
	</p>
<% else %>
	<p class="textoComum" align="left">
		<img SRC="images/bbtur_logo.gif" border="0" WIDTH="120" HEIGHT="20"><br><b>Travel Agency</b><br>
		Mailing address: Rua Trajano, 265 - Loja 3 - Florian�polis - SC - Brasil - 88010-010<br>
		Phone: + 55 (48) 229-8500 / Fax: + 55 (48) 322-0460
		e-mail: <a href="mailto:andredutra@bbtur.com.br">andredutra@bbtur.com.br</a>
	</p>
<% end if %>


<p class="textoComum" align="left">
	<b><%=strLocalizacao%></b>
	<br><br>
	<img src="images/mapaCentro.jpg" WIDTH="519" HEIGHT="750">
</p>

