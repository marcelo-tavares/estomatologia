<%
if pr1_wk = 0 then
%>
<script>
function mostraAtividade(atividade){
	for (i=1; i<=3; i++) {
		document.getElementById('atividade'+i).style.display = 'none';
		if (atividade == i){
			document.getElementById('atividade'+i).style.display = 'inline';
		}
	}
}

</script>
<p class="Titulo" align="center">F�rum de Ensino em Estomatologia</p>
<table width=100% border=0>
	<tr>
		<td width=70%>
			<span id=atividade1 style="display:'inline'" class="TextoComum">
				<b>� OBJETIVOS DO F�RUM</b><br><br>
				O objetivo do <b>F�rum sobre o Ensino em Estomatologia </b>� discutir e recomendar � Assembl�ia da SOBE um curr�culo m�nimo da especialidade no Brasil
			</span>

			<span id=atividade2 style="display:'none'" class="TextoComum">
				<b>� RESPONS�VEIS PELO F�RUM</b><br><br>
				<b>Cassius Torres-Pereira</b>
				<ul class="TextoComum">
					<li>Prof Adjunto das Disciplinas de Diagn�stico Bucal e Metodologia Cient�fica da UFPR
					<li>Doutor em Estomatologia pela PUC-RS
					<li>Especialista em Periodontia pela USP-Bauru
					<li>Membro do Grupo de Trabalho de Assessoria para a Estomatologia do Minist�rio da Sa�de - Governo Federal
				</ul>
				<b>Jair Carneiro Le�o</b>
				<ul class="TextoComum">
					<li>Professor Adjunto Estomatologia, UFPE
					<li>Mestre (MSc), em Medicina Oral, Eastman Dental Institute, Universidade de Londres, 1996
					<li>Doctor of Philosophy (PhD), Eastman Dental Institute, Universidade de Londres, 1999
					<li>P�s-Doutorado, Eastman Dental Institute, Universidade de Londres, 2003-2004
				</ul>
				<b>Celso Augusto Lemos J�nior</b>
				<ul class="TextoComum">
					<li>Cirurgi�o Dentista FOUSP - SP
					<li>Especialista Mestre e Doutor Estomatologia FOUSP - SP
					<li>Professor Universidade Ibirapuera
					<li>Secret�rio Executivo SBPqO
					<li>Membro do Grupo de Trabalho de Assessoria para a Estomatologia do Minist�rio da Sa�de - Governo Federal
				</ul>
			</span>

			<span id=atividade3 style="display:'none'">
				<p class="TextoComum">
				<b>� ENVIE SUGEST�ES DE PAUTAS
				</p>
				<p class="TextoComum">
					Nome<br>
					<input type=text size=50 name=nome class=textbox01><br><br>
					E-mail<br>
					<input type=text size=50 name=Email class=textbox01><br><br>
					Sugest�es de Pautas para o F�rum de Ensino<br>
					<textarea cols=60 rows=8 name=texto class=textarea01></textarea><br><br>
					<input type=button class=botaoPadrao name=enviar value=Enviar onclick="submeter(2,10,<%=id_wk%>,1);">
				</p>
			</span>
		</td>
		<td width=5%>
			&nbsp;
		</td>
		<td class=textoComum width=25% valign=top>
			<table width=100%>
				<tr class="TextoComum">
					<td bgcolor="#ffffff">
						<small>
							&nbsp;<br>
							� <a href="javascript:mostraAtividade(1);">Objetivos</a><hr noshade color="#7A64A5">
							� <a href="javascript:mostraAtividade(2);">Respons�veis</a><hr noshade color="#7A64A5">
							� <a href="javascript:mostraAtividade(3);">Sugest�es de</a><br>&nbsp;&nbsp;<a href="javascript:mostraAtividade(3);">Pauta</a><hr noshade color="#7A64A5">
						</small>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<% 
else

	'cria o objeto para o envio de e-mail 
	Set objCDOSYSMail = Server.CreateObject("CDO.Message")

	'cria o objeto para configura��o do SMTP 
	Set objCDOSYSCon = Server.CreateObject ("CDO.Configuration")

	'SMTP 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp2.locaweb.com.br"

	'porta do SMTP 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25

	'porta do CDO 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2

	'timeout 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 

	objCDOSYSCon.Fields.update 

	'atualiza a configura��o do CDOSYS para o envio do e-mail 
	Set objCDOSYSMail.Configuration = objCDOSYSCon

	'e-mail do remetente 
	objCDOSYSMail.From = trim(request("nome")) & " (por meio do web site)<XIII_CBE@tavares.eti.br>"

	'e-mail do destinat�rio 
	'objCDOSYSMail.To = "marcelo@tavares.eti.br"
	objCDOSYSMail.To = "Cassius Torres-Pereira <cassiust@uol.com.br>"

	'assunto da mensagem 
	objCDOSYSMail.Subject = "Sugest�es para a PAuta do F�rum de Ensino de Estomatologia"

	'conte�do da mensagem 
	str_CrLf = chr(13) & chr(10)
	objCDOSYSMail.TextBody = request("nome") & str_CrLf & str_CrLf & request("Email") & str_CrLf & str_CrLf & request("texto")
	'para envio da mensagem no formato html altere o TextBody para HtmlBody 
	'objCDOSYSMail.HtmlBody = "Teste do componente CDOSYS"

	'objCDOSYSMail.fields.update
	'envia o e-mail 
	objCDOSYSMail.Send 

	'destr�i os objetos 
	Set objCDOSYSMail = Nothing 
	Set objCDOSYSCon = Nothing 
%>
<br><br>
<center>
	<h5>
		A mensagem foi enviada com sucesso. Obrigado por sua participa��o.
	</h5>
</center>
<%
end if
%>