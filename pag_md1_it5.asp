<p class="Titulo" align="center">MENSAGEM DO PRESIDENTE DO CRO-SC</p>

<p class="textoComum">
	<img SRC="images/Foto_Sidnei.jpg" align="right" hspace="5" WIDTH="173" HEIGHT="200">A realiza��o do XIII Congresso Brasileiro de Estomatologia, XXXI Jornada de Estomatologia e II F�rum de Discuss�o em Diagn�stico Bucal da UFSC, confirma Florian�polis e Santa Catarina como p�lo realizador de grandes eventos cient�ficos. O desafio, enfrentado pela Comiss�o Organizadora para realiza��o deste evento pela primeira vez em nosso Estado foram imensos e somente a boa vontade, perseveran�a e muito trabalho ser�o capazes de minimizarem os obst�culos enfrentados.
</p>
<p class="textoComum">
Somos solid�rios � Comiss�o Organizadora e acreditamos que as parcerias s�o indispens�veis nestas ocasi�es. Temos a certeza de que o sucesso do evento j� est� garantido.
</p>
<p class="textoComum">
Parab�ns � Comiss�o Organizadora pelo trabalho que vem realizando, sendo assim, convidamos a todos os colegas Cirurgi�es-Dentistas que acreditam na educa��o continuada, na atualiza��o e na pesquisa cient�fica, para que estejam presentes em Florian�polis nos dias 16, 17, 18, 19 e 20 de julho de 2005, e que com suas participa��es torne o XIII Congresso Brasileiro de Estomatologia um marco na hist�ria da Odontologia brasileira.
</p>
<p class="textoComum">
<b>Sidnei Jos� Garcia, CD</b><br>
Presidente do CRO-SC
</p>
