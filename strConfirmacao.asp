<%
function strConfirmacao(idioma, cd_codigo, de_participante)
	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn = Server.CreateObject("ADODB.Connection") 
	set rs = server.CreateObject("ADODB.Recordset")
	Conn.Open cnpath

	if instr(de_participante, "|") > 0 then
		ar_wk = split(de_participante, "|")
		ar_participante = split(ar_wk(0), "#")
		ar_cartaoVisa = split(ar_wk(1), "#")
	else
		ar_participante = split(de_participante, "#")
		redim ar_cartaoVisa(2)
		ar_cartaoVisa(0) = ""
		ar_cartaoVisa(1) = ""
	end if
	
	set rs = Conn.Execute("select de_estado from TB_Estados where cd_estado = " & ar_participante(9))
	strEstado = rs("de_estado")
	
	set rs = Conn.Execute("select * from TB_Categoria where cd_categoria = " & ar_participante(15))
	strCatPt = rs("de_categoriaPt")
	strCatEn = rs("de_categoriaEn")
	
	set rs = nothing
	set Conn = nothing

	' --------------------------------------------------------------------
	select case ar_participante(21)
		case 1
			if idioma = 1 then
				strTp_pagamento = "Cart�o Visa"
			else
				strTp_pagamento = "Visa"
			end if
		case 2
			if idioma = 1 then
				strTp_pagamento = "Cart�o Mastercard"
			else
				strTp_pagamento = "Mastercard"
			end if
		case 3
			strTp_pagamento = "Cheque Nominal"
		case 4
			strTp_pagamento = "Boleto Banc�rio (envio pelo correio)"
	end select


	if idioma = 1 then
		strAtividades = ""
		if ar_participante(18) then
			strAtividades = strAtividades & "Pr�-Congresso"
		end if
		
		if ar_participante(19) then
			if ar_participante(18) then
				strAtividades = strAtividades & " / "
			end if
			strAtividades = strAtividades & "Congresso"
		end if

		strOdontogeriatria = ""
		if ar_participante(30) then
			strOdontogeriatria = "S�cio da Sociedade Brasileira de Odontogeriatria<br>" 
		end if
		
		valorR = replace(Cstr(ar_participante(28)),".",",")
		reais = int(valorR)
		centavos = Cstr(int ( (valorR - reais) * 100))
		if len(centavos) < 2 then centavos = centavos & "0"
		strPreco = reais & "," & centavos
		
		
		strConfirmacao =		string(80,"-") & "<BR>" _
		&						"Inscri��o N� " & cd_codigo & "<BR>"  _
		&						string(80,"-") & "<BR>"  _
		&						"Nome      : " & ar_participante(0) & "<BR>"  _
		&						"Endere�o  : " & ar_participante(7) & "<BR>"  _
		&						"CEP       : " & ar_participante(11) & " - " & ar_participante(8) & " - " & strEstado & " - " & ar_participante(10) & "<BR>"  _
		&						"Fone      : " & ar_participante(12) & "<BR>"  _
		&						"E-mail    : " & ar_participante(14) & "<BR>"  _
		&						"Fax       : " & ar_participante(13) & "<BR>"  _
		&						string(80,"-")  & "<BR>"  _
		&						"Categoria : " & strCatPt & "<BR>"  _
		&						"Atividade : " & strAtividades & "<BR>" _
		&						strOdontogeriatria _
		&						string(80,"-")  & "<BR>"  _
		&						"Valor Total da Inscri��o: R$ " & strPreco & "<BR>"  _
		&						"Meio de Pagamento : " & strTp_pagamento & "<BR>" _
		&						string(80,"-")& "<BR>"
	else
		strAtividades = ""
		if ar_participante(18) then
			strAtividades = strAtividades & "Pre-Congress"
		end if
		
		if ar_participante(19) then
			if ar_participante(18) then
				strAtividades = strAtividades & " / "
			end if
			strAtividades = strAtividades & "Congress"
		end if
		
		valor = replace(Cstr(ar_participante(27)),".",",")
		dolars =  int(valor)
		cents = Cstr(int ( (valor - dolars) * 100))
		if len(cents) < 2 then cents = cents & "0"
		strPrice = dolars & "." & cents
		
		valorR = replace(Cstr(ar_participante(28)),".",",")
		reais = int(valorR)
		centavos = Cstr(int ( (valorR - reais) * 100))
		if len(centavos) < 2 then centavos = centavos & "0"
		strPreco = reais & "," & centavos
		
		strConfirmacao =	string(80,"-") & "<BR>" _
		&						"Registration N. " & cd_codigo & "<BR>"  _
		&						string(80,"-") & "<BR>"  _
		&						"Name      : " & ar_participante(0) & "<BR>"  _
		&						"Address   : " & ar_participante(7) & "<BR>"  _
		&						"ZIP Code  : " & ar_participante(11) & " - " & ar_participante(8) & " - " & strEstado & " - " & ar_participante(10) & "<BR>"  _
		&						"Phone     : " & ar_participante(12) & "<BR>"  _
		&						"E-mail    : " & ar_participante(14) & "<BR>"  _
		&						"Fax       : " & ar_participante(13) & "<BR>"  _
		&						string(80,"-")  & "<BR>"  _
		&						"Category  : " & strCatEn & "<BR>"  _
		&						"Activity  : " & strAtividades & "<BR>"  _
		&						"Accompanying Persons:" & ar_participante(1) & "<BR>"  _
		&						string(80,"-")  & "<BR>"  _
		&						"Total : US$" & strPrice & " (R$ " & strPreco & ")<BR>"  _
		&						"Credit Card : " & strTp_pagamento & "<BR>" _
		&						string(80,"-")& "<BR>"
	end if
end function
%>