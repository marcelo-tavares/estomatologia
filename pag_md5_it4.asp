<% 

if false then 'liberando para testes em 17/04/2004
if id_wk = 1 then
	%>
	<h5>Inscri��es on-line encerradas. Por favor registre-se no Centro de Conven��es.</h5>
	<%
else
	%>
	<h5>On-line registrations are over. Please register on-site at the Convention Center</h5>
	<%
end if
Response.End
end if

anoAtual = year(now)
mesAtual = month(now)
diaAtual = day(now)

homologado = true

' -------------------------------------------------------------------------------

strFormInscricao_wk = "Formulario de Inscri��o On-line"
strST1_wk = "Dados Pessoais"
strNome_wk = "Nome"
strAcompanhantes_wk = "Acompanhantes"
strAcompanhante_wk = "Acompanhante"
str1Acompanhante_wk = "pessoa"
strmaisde1Acompanhantes_wk = "pessoas"
strSemAcompanhantes_wk = "Nenhum"
strEndereco_wk = "Endere�o"
strCEP_wk = "CEP"
strCidade_wk = "Cidade"
strEstado_wk = "Estado"
strPais_wk = "Pa�s"
strFone_wk = "Fone"
strEmail_wk = "E-mail"
strFax_wk = "Fax"
strST2_wk = "Dados da Inscri��o"
strPC_wk = "Pr�-Congresso"
strC_wk = "Congresso"
strCategoria_wk = "Categoria"
strAtividade_wk = "Atividades"
strCat1_wk = "S�cio da SOBE <sup>*</sup>"
strCat5_wk = "S�cio da ABRO"
strCat6_wk = "S�cio do Col�gio Brasileiro<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de Cirurgia Bucomaxilofacial"
strCat7_wk = "S�cio da Sociedade<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brasileira de Odontogeriatria"
'strCat2_wk = "S�cio ABRO"
strCat2_wk = "N�o S�cio e profissionais de<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;outra �rea da sa�de"
strCat3_wk = "Aluno de P�s-Gradua��o"
strCat4_wk = "Acad�mico de Gradua��o"
strValor_wk = "Total"
strObs1_wk = "Anuidade Prevista para 2005: R$130,00"
strObs2_wk = "Para a confirma��o de suas inscri��es, os acad�micos dever�o apresentar comprovante de sua institui��o de ensino no check in do Congresso"
strST3_wk = "Pagamento"
strFormaPagamento_wk = "Meio de<br>Pagamento"
strCartaoCredito_wk = "Cart�o de Cr�dito"
strObsCartao_wk = "� Pressione o bot�o ""Pr�xima"" para confirmar a inscri��o e informar os dados do cart�o de cr�dito<br>� A Inscri��o somente ser� confirmada ap�s a confirma��o do pagamento da �ltima parcela.<br>� Parcelas pagas n�o ser�o devolvidas."
btnProximo_wk = "Pr�xima"
btnConfirmar_wk = "Confirmar"
lblProximo_wk = btnConfirmar_wk
btnLimpar_wk = "Limpar"
strConfirma = "Deseja confirmar sua inscri��o?"
strImprimeFormulario = "Imprimir o Formul�rio para enviar por correio ou fax."
strVISA="Cart�o VISA"
strMC="MASTERCARD"
strDINERS="DINERS CLUB"
strRecalculo = "Recalcular"
%>

<script>
function refreshFormulario(){
	if (document.Form1.nr_acompanhantes.value > 0) 
		ativaAcompanhantes();
	
	InformouCategoria = 0;	
	for (i=0; i<4; i++)
		if(document.Form1.Categoria[i].checked)
			InformouCategoria = 1;
			
	if (InformouCategoria != 0){
		if (document.Form1.Categoria[0].checked){
			mostraNRMembro(1);
		}
	}
	calculaTotal(1);
}


function  ativaAcompanhantes(){
	nAcomp = document.Form1.nr_acompanhantes.value
	for (i=1; i<=5; i++){
		acompanha = 'tab_acompanha0' + i
		document.getElementById(acompanha).style.display = 'none';
	}

	for (i=1; i<=nAcomp; i++){
		acompanha = 'tab_acompanha0' + i
		document.getElementById(acompanha).style.display = 'inline';
	}
	calculaTotal(1);
}

function calculaTotal(calcularParcelas){
	
	dataAtual = <%=year(now) & string(2-len(cstr(month(now))),"0") & month(now) & string(2-len(cstr(day(now))),"0") & day(now)%>;

	total = 0;
	
	var tarifa_PC = new Array(5)
	tarifa_PC[1] = 80
	tarifa_PC[2] = 50
	tarifa_PC[3] = 50
	tarifa_PC[4] = 40
	tarifa_PC[5] = 20
	
	var tarifa_C_1 = new Array(4)
	tarifa_C_1[1] = 400
	tarifa_C_1[2] = 550
	tarifa_C_1[3] = 300
	tarifa_C_1[4] = 100
	tarifa_C_1[5] = 400
	tarifa_C_1[6] = 400
	tarifa_C_1[7] = 400

	var tarifa_C_2 = new Array(4)
	tarifa_C_2[1] = 420
	tarifa_C_2[2] = 570
	tarifa_C_2[3] = 330
	tarifa_C_2[4] = 120
	tarifa_C_2[5] = 420
	tarifa_C_2[6] = 420
	tarifa_C_2[7] = 420

	var tarifa_C_3 = new Array(4)
	tarifa_C_3[1] = 450
	tarifa_C_3[2] = 600
	tarifa_C_3[3] = 350
	tarifa_C_3[4] = 120
	tarifa_C_3[5] = 450
	tarifa_C_3[6] = 450
	tarifa_C_3[7] = 450

	var tarifa_C = new Array(4)
	var tarifa_PC1
	tarifa_acomp = 50
	tarifa_homologacao = 1
	
	// ------------------------------------------
	// Calculo do Valor do Congresso
	// ------------------------------------------
	
	// Verifica��o dos valores aplicaveis
	if (document.Form1.nr_parcelas.value == 1)
		if (dataAtual <= 20050315)
			for (i=1; i<=7; i++)
				tarifa_C[i] = tarifa_C_1[i];
		else
			if (dataAtual <= 20050708)
				for (i=1; i<=7; i++)
					tarifa_C[i] = tarifa_C_2[i];
			else
				for (i=1; i<=7; i++)
					tarifa_C[i] = tarifa_C_3[i];
	else
		if (dataAtual <= 20050708)
			for (i=1; i<=7; i++)
				tarifa_C[i] = tarifa_C_2[i];
		else
			for (i=1; i<=7; i++)
				tarifa_C[i] = tarifa_C_3[i];
		
	if (document.Form1.cd_categoria.value != 0)
		if (document.Form1.sn_congresso.checked)
			total = total + tarifa_C[document.Form1.cd_categoria.value];
			
	// ------------------------------------------
	// Calculo do Valor do Pr�-Congresso
	// ------------------------------------------

	tarifa_PC1 = 0;
	if(!(document.Form1.cd_categoria.value == 0)){
		if (document.Form1.sn_precongresso.checked) {
			// N�o Congressista
			if (!document.Form1.sn_congresso.checked){
				//alert('N�o Congressista');
				if (!(document.Form1.cd_categoria.value == 7))
					tarifa_PC1 = tarifa_PC[1];
				else
					tarifa_PC1 = tarifa_PC[2];
			}
			else {
				//alert('Congressista - categoria ' + document.Form1.cd_categoria.value);

				// Congressista
				if ((document.Form1.cd_categoria.value == 1)
					||(document.Form1.cd_categoria.value == 2)
					||(document.Form1.cd_categoria.value == 5)
					||(document.Form1.cd_categoria.value == 6)
					||(document.Form1.cd_categoria.value == 7))
					tarifa_PC1 = tarifa_PC[3];
			}
			if (document.Form1.cd_categoria.value == 3)
				tarifa_PC1 = tarifa_PC[4];
			else
				if (document.Form1.cd_categoria.value == 4)
					tarifa_PC1 = tarifa_PC[5];
		}
	}
	
	//alert('tarifa_PC1: ' + tarifa_PC1);
	
	total = total + tarifa_PC1;
	
	//alert('total: ' + total);
	
	document.Form1.vl_inscricaoR.value = total

	totalR = total
	centavos = Math.floor ((totalR - Math.floor(totalR)) * 100)
	if (centavos < 10)
		centavos = centavos + '0';
	reais = Math.floor(totalR)
	document.getElementById("spanTotalR").innerHTML = reais
	document.getElementById("spanTotalC").innerHTML = centavos

	document.Form1.vl_inscricaoR.value = total
	
	if (calcularParcelas == 1)
		calculaParcelas(0);
}

function calculaParcelas(i){
	if (i != 0)
		document.Form1.nr_parcelas.value = i
	calculaTotal(0);
	reais = (document.Form1.vl_inscricaoR.value) / document.Form1.nr_parcelas.value;
	centavos = Math.floor ((reais - Math.floor(reais)) * 100)
	if (centavos < 10)
		centavos = centavos + '0';
	reais = Math.floor(reais)
	document.getElementById("spanValorParcela").innerHTML = (reais + ',' + centavos);
	
	document.Form1.vl_parcela.value = reais + centavos/100
}

function ativaParcelas() {
	document.getElementById("spanParcelas").style.display = 'inline';
}

function desativaParcelas() {
	document.getElementById("spanParcelas").style.display = 'none';
}

function FormaPgto(i){
	switch (i) {
		case 3:
			document.getElementById("spanParcelas").style.display = 'inline';
			document.getElementById("ObsCheques").style.display = 'inline';
			document.getElementById("ObsBoletos").style.display = 'none'; 
			document.Form1.proxima.value = '<%=btnConfirmar_wk%>'
			break;
		case 4:
			document.getElementById("spanParcelas").style.display = 'inline';
			document.getElementById("ObsCheques").style.display = 'none';
			document.getElementById("ObsBoletos").style.display = 'inline';
			document.Form1.proxima.value = '<%=btnConfirmar_wk%>'
			break;
	}
	document.Form1.tp_pagamento.value=i;
}

function categoria(i){
	document.Form1.cd_categoria.value=i;
}

function mostraNRMembro(i){
	switch (i) {
		case 0:
			document.getElementById("nr_membro").style.display = 'none';
			break;
		case 1:
			document.getElementById("nr_membro").style.display = 'inline';
			break;
	}
}

function mostraSocioOdontoGeriatria(i){
	return true;
	if (!document.Form1.sn_precongresso.checked)
		document.getElementById("SocioOdontoGeriatria").style.display = 'none';
	else
		document.getElementById("SocioOdontoGeriatria").style.display = 'inline';
}

function imprimeFormulario(){
	winobj = window.open('imprimeFormulario.htm', 'ImprimeFormulario', '"top=2000,left=2000,height=100,width=100,status=no,toolbar=no,menubar=no,location=no');
}
</script>
<%
	strMsg1 = "Informe o seu Nome!"
	strMsg2 = "Informe o seu Endere�o!"
	strMsg3 = "Informe a sua cidade!"
	strMsg4 = "Informe o seu Estado!"
	strMsg5 = "Informe o seu Pa�s!"
	strMsg6 = "Informe o seu CEP!"
	strMsg7 = "Informe o seu N�mero de Telefone!"
	strMsg8 = "Informe o seu E-mail!"
	strMsg9 = "Escolha uma Categoria!"
	strMsg91 = "Informe seu n�mero de cadastro da SOBE!"
	strMsg95 = "Informe seu n�mero de cadastro da ABRO!"
	strMsg96 = "Informe seu n�mero de cadastro do Colegio Brasileiro de Cirurgia Bucomaxilofacial!"
	strMsg97 = "Informe seu n�mero de cadastro da Sociedade Brasileira de Odontogeriatria!"
	strMsg10 = "Indique pelo menos uma Atividade!"
	strMsg11 = "Informe o nome de todos os acompanhantes"
%>

<script>
function ValidarForm(pr1){
	
	var InformouCategoria = 0;
	
	if (document.Form1.nm_participante.value.length == 0){
		alert('<%=strMsg1%>');
		document.Form1.nm_participante.focus();
		return false;
	}
	
	if (document.Form1.nr_acompanhantes.value >= 1){
		if (document.Form1.nm_acompanhante01.value.length == 0) {
			alert('<%=strMsg11%>');
			document.Form1.nm_acompanhante01.focus();
			return false;
		}

		if (document.Form1.nr_acompanhantes.value >= 2){
			if (document.Form1.nm_acompanhante02.value.length == 0) {
				alert('<%=strMsg11%>');
				document.Form1.nm_acompanhante02.focus();
				return false;
			}
			if (document.Form1.nr_acompanhantes.value >= 3){
				if (document.Form1.nm_acompanhante03.value.length == 0) {
					alert('<%=strMsg11%>');
					document.Form1.nm_acompanhante03.focus();
					return false;
				}
				if (document.Form1.nr_acompanhantes.value >= 4){
					if (document.Form1.nm_acompanhante04.value.length == 0) {
						alert('<%=strMsg11%>');
						document.Form1.nm_acompanhante04.focus();
						return false;
					}
					if (document.Form1.nr_acompanhantes.value == 5){
						if (document.Form1.nm_acompanhante05.value.length == 0) {
							alert('<%=strMsg11%>');
							document.Form1.nm_acompanhante05.focus();
							return false;
						}
					}
				}
			}
		}
	}

	if (document.Form1.de_endereco.value.length == 0){
		alert('<%=strMsg2%>');
		document.Form1.de_endereco.focus();
		return false;
	}

	if (document.Form1.cd_CEP.value.length == 0){
		alert('<%=strMsg6%>');
		document.Form1.cd_CEP.focus();
		return false;
	}

	if (document.Form1.de_cidade.value.length == 0){
		alert('<%=strMsg3%>');
		document.Form1.de_cidade.focus();
		return false;
	}

	if (document.Form1.de_estado.selectedIndex == 0){
		alert('<%=strMsg4%>');
		document.Form1.de_estado.focus();
		return false;
	}

	if (document.Form1.de_pais.value.length == 0){
		alert('<%=strMsg5%>');
		document.Form1.de_pais.focus();
		return false;
	}

	if (document.Form1.nr_fone.value.length == 0){
		alert('<%=strMsg7%>');
		document.Form1.nr_fone.focus();
		return false;
	}

	//if (document.Form1.de_email.value.length == 0){
	//	alert('<%=strMsg8%>');
	//	document.Form1.de_email.focus();
	//	return false;
	//}

	for (i=0; i<7; i++)
		if(document.Form1.Categoria[i].checked)
			InformouCategoria = 1;
			
	if (InformouCategoria == 0){
		alert('<%=strMsg9%>');
		document.Form1.Categoria[0].focus();
		return false;
	}
	else {
		strMsg = '';
		if (document.Form1.Categoria[0].checked)
			strMsg = '<%=strMsg91%>';
		else
			if (document.Form1.Categoria[1].checked)
				strMsg = '<%=strMsg95%>';
			else
				if (document.Form1.Categoria[2].checked)
					strMsg = '<%=strMsg96%>';
			
		if (strMsg != '')
			if (document.Form1.cd_membro.value.length == 0){
				alert(strMsg);
				document.Form1.cd_membro.focus();
				return false;
			}

	}
	
	if ((!document.Form1.sn_congresso.checked) && (!document.Form1.sn_precongresso.checked)) {
		alert('<%=strMsg10%>');
		document.Form1.sn_precongresso.focus();
		return false;
	}

	if (confirm('<%=strConfirma%>'))
		submeterSSL(5,4,<%=id_wk%>,pr1)
}

</script>
<p class="Titulo" align="center">
	<%=strFormInscricao_wk%>
	<% if FALSE then %>
		<img SRC="images/impressorinha.gif" align="right" hspace="10" alt="<%=strImprimeFormulario%>" onclick="imprimeFormulario();" WIDTH="16" HEIGHT="14">
	<% end if %>
</p>

<form name="Form1" method="post">
<table class="TabelaProgramacao" width="100%">
	<tr class="CelulaTitulo">
		<td align="middle" colspan="3">
			<%=strST1_wk%>
		</td>
	<tr>

	<tr class="CelulaCorpo">
		<td colspan="3">
			<%=strNome_wk%><br><input type="textbox" class="textbox01" size="80" name="nm_participante" maxlength=50>
			<input type=hidden name="nr_acompanhantes" value=0>
		</td>
		<% if false then %>
		<td>
			<%=strAcompanhantes_wk%><br>
			<select name="nr_acompanhantes" size="1" class="combo01" onchange="ativaAcompanhantes();">
				<option value="0" selected> <%=strSemAcompanhantes_wk%></option>
				<option value="1"> 01 <%=str1Acompanhante_wk%></option>
				<option value="2"> 02 <%=strmaisde1Acompanhantes_wk%></option>
				<option value="3"> 03 <%=strmaisde1Acompanhantes_wk%></option>
				<option value="4"> 04 <%=strmaisde1Acompanhantes_wk%></option>
				<option value="5"> 05 <%=strmaisde1Acompanhantes_wk%></option>
			</select>
		</td>
		<% end if %>
	<tr>
	
	<tr class="CelulaCorpo">
		<td colspan="3">
			<!-- Tabelas de Acompanhantes -->
			<table id="tab_acompanha01" width="100%" style="DISPLAY: none" cellpadding="0" cellspacing="0">
				<tr class="CelulaCorpo">
					<td>
						<%=strAcompanhante_wk%> 01: <br><input type="textbox" class="textbox01" size="60" name="nm_acompanhante01" maxlength=50>
					</td>
				</tr>
			</table>
			<table id="tab_acompanha02" width="100%" style="DISPLAY: none" cellpadding="0" cellspacing="0">
				<tr class="CelulaCorpo">
					<td>
						<%=strAcompanhante_wk%> 02: <br><input type="textbox" class="textbox01" size="60" name="nm_acompanhante02" maxlength=50>
					</td>
				</tr>
			</table>
			<table id="tab_acompanha03" width="100%" style="DISPLAY: none" cellpadding="0" cellspacing="0">
				<tr class="CelulaCorpo">
					<td>
						<%=strAcompanhante_wk%> 03: <br><input type="textbox" class="textbox01" size="60" name="nm_acompanhante03" maxlength=50>
					</td>
				</tr>
			</table>
			<table id="tab_acompanha04" width="100%" style="DISPLAY: none" cellpadding="0" cellspacing="0">
				<tr class="CelulaCorpo">
					<td>
						<%=strAcompanhante_wk%> 04: <br><input type="textbox" class="textbox01" size="60" name="nm_acompanhante04" maxlength=50>
					</td>
				</tr>
			</table>
			<table id="tab_acompanha05" width="100%" style="DISPLAY: none" cellpadding="0" cellspacing="0">
				<tr class="CelulaCorpo">
					<td>
						<%=strAcompanhante_wk%> 05: <br><input type="textbox" class="textbox01" size="60" name="nm_acompanhante05" maxlength=50>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr class="CelulaCorpo">
		<td colspan="2">
			<%=strEndereco_wk%><br><input type="textbox" class="textbox01" size="60" name="de_endereco" maxlength=60>
		</td>
		<td>
			<%=strCEP_wk%><br><input type="textbox" class="textbox01" size="10" name="cd_CEP" maxlength=10>
		</td>
	</tr>

	<tr class="CelulaCorpo">
		<td>
			<%=strCidade_wk%><br><input type="textbox" class="textbox01" size="30" name="de_cidade" maxlength=50>
		</td>
		<td>
			<script>
				function testaEstado() {
					if ((document.Form1.de_estado.value != 0) && (document.Form1.de_estado.value != 99)) {
						document.Form1.de_pais.value = 'Brasil'; 
						document.Form1.nr_fone.focus();
					}
					else {
						if (document.Form1.de_estado.value == 0) {
							document.Form1.de_pais.value = ''; 
						}
						else {
							document.Form1.de_pais.value = ''; 
							document.Form1.de_pais.focus();
						}
					}
				}	
			</script>
			<%=strEstado_wk%><br>
			<select name="de_estado" size="1" class="combo01" onchange="testaEstado();">
				<option value="0"> --- Selecione ---</option>
				<option value="1">Acre</option>
				<option value="2">Alagoas</option>
				<option value="3">Amazonas</option>
				<option value="4">Amap�</option>
				<option value="5">Bahia</option>
				<option value="6">Cear�</option>
				<option value="7">Distrito Federal</option>
				<option value="8">Esp�rito Santo</option>
				<option value="9">Goi�s</option>
				<option value="10">Maranh�o</option>
				<option value="11">Minas Gerais</option>
				<option value="12">Mato Grosso do Sul</option>
				<option value="13">Mato Grosso</option>
				<option value="14">Par�</option>
				<option value="15">Para�ba</option>
				<option value="16">Pernanbuco</option>
				<option value="17">Piau�</option>
				<option value="18">Paran�</option>
				<option value="19">Rio de Janeiro</option>
				<option value="20">Rio Grande do Norte</option>
				<option value="21">Rond�nia</option>
				<option value="22">Roraima</option>
				<option value="23">Rio Grande do Sul</option>
				<option value="24">Santa Catarina</option>
				<option value="25">Sergipe</option>
				<option value="26">S�o Paulo</option>
				<option value="27">Tocantins</option>
				<option <% if id_wk = 2 then %>selected<% end if %> value="99">Out of Brazil</option>
			</select>
		</td>
		<td>
			<%=strPais_wk%><br><input type="textbox" class="textbox01" size="15" name="de_pais" maxlength=50>
		</td>
	<tr>
	
	<tr class="CelulaCorpo">
		<td>
			<%=strFone_wk%><br><input type="textbox" class="textbox01" size="20" name="nr_fone" maxlength=20>
		</td>
		<td>
			<%=strEmail_wk%><br><input type="textbox" class="textbox01" size="25" name="de_email" maxlength=50>
		</td>
		<td>
			Fax<br><input type="textbox" class="textbox01" size="20" name="nr_fax" maxlength=50>
		</td>
	<tr>
	
	<tr class="CelulaTitulo">
		<td align="middle" colspan="3">
			<%=strST2_wk%>			
		</td>
	<tr>

	<tr class="CelulaCorpo">
		<td valign="top">
			<span class="subtitulo"><%=strCategoria_wk%></span> :<br>
			<input type="hidden" name="cd_categoria" value="0">
			<input type="radio" name="Categoria" onclick="categoria(1);calculaTotal();mostraNRMembro(1);document.Form1.cd_membro.focus();">&nbsp;<%=strCat1_wk%>&nbsp;<br>
			<input type="radio" name="Categoria" onclick="categoria(5);calculaTotal();mostraNRMembro(1);document.Form1.cd_membro.focus();">&nbsp;<%=strCat5_wk%>&nbsp;<br>
			<input type="radio" name="Categoria" onclick="categoria(6);calculaTotal();mostraNRMembro(1);document.Form1.cd_membro.focus();">&nbsp;<%=strCat6_wk%>&nbsp;<br>
			<input type="radio" name="Categoria" onclick="categoria(7);calculaTotal();mostraNRMembro(0);">&nbsp;<%=strCat7_wk%>&nbsp;<br>
				<span id="nr_membro" style="display='none'">
					<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N� <input type="textbox" class="textbox01" size="5" name="cd_membro">
				</span>
				
			<br>
			</span>
			<input type="radio" name="Categoria" onclick="categoria(2);calculaTotal(1);mostraNRMembro(0);">&nbsp;<%=strCat2_wk %><br>
			<input type="radio" name="Categoria" onclick="categoria(3);calculaTotal(1);mostraNRMembro(0);">&nbsp;<%=strCat3_wk %><sup>**</sup><br>
			<input type="radio" name="Categoria" onclick="categoria(4);calculaTotal(1);mostraNRMembro(0);">&nbsp;<%=strCat4_wk %><sup>**</sup>
			<br>&nbsp;
		</td>
		
		<td valign="top">
			<span class="subtitulo"><%=strAtividade_wk%></span> :<br>
			<input type="Checkbox" name="sn_precongresso" onclick="mostraSocioOdontoGeriatria(); calculaTotal(1);" value="ON">&nbsp;<%=strPC_wk%><br>
			&nbsp;&nbsp;<small>
			<span id="SocioOdontoGeriatria"  style="display='none'">
				&nbsp;&nbsp;<small><input type="Checkbox" name="sn_socioOdontoGeriatria" onclick="calculaTotal(1);" value="ON">&nbsp;S�cio da Sociedade<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brasileira de Odontogeriatria</small>
			</span>
			<br>
			<input type="Checkbox" name="sn_congresso" onclick="calculaTotal(1);" value="ON">&nbsp;<%=strC_wk%>
		</td>
		<td valign="top" align="center">
			<span class="subtitulo"><%=strValor_wk%>:</span><br><br>
				<b>R$ </b><span class="Total"><span id="spanTotalR" class="Total">0</span>,<span id="spanTotalC">00</span></span><b></b>
			<input type="hidden" name="vl_inscricaoR" value="0">
		</td>
	<tr>
	
	<tr class="CelulaCorpoCinza">
		<td colspan="3">
			<small>* <%=strObs1_wk%>.</small><br> 
			<small>** <%=strObs2_wk%>.</small> 
		</td>
	<tr>
	
	<tr class="CelulaTitulo">
		<td align="middle" colspan="3">
			<%=strST3_wk%>
		</td>
	</tr>

	<%
	dia = day(now)
	if dia < 10 then dia = "0" & dia
	mes = month(now)
	if mes < 10 then mes = "0" & mes
	ano = year(now)
	agora = ano & mes & dia
	
	if ((agora <= 20050426) or session("modoHomologacao")) then
		parc = 3
	else
		if agora <= 20050526 then
			parc = 2
		else
			parc = 1
		end if
	end if
	%>	
	
	<tr class="CelulaCorpo">
		<td colspan="3">
			<table width="100%" border="0">
				<tr class="CelulaCorpo">
					<td colspan="2">
						<span id="spanParcelas" style="display:<%if id_wk = 1 then%>inline<%else%>none<%end if%>">
						<span class="subtitulo">Parcelas </span>:&nbsp;
						<input type="radio" checked name="Parcela" value="1" onclick="calculaParcelas(1)">&nbsp;1&nbsp;&nbsp;&nbsp;
						<% if parc > 1 then %>
							<input type="radio" name="Parcela" value="2" onclick="calculaParcelas(2)">&nbsp;2&nbsp;&nbsp;&nbsp;
							<% if parc > 2 then %>
								<input type="radio" name="Parcela" value="3" onclick="calculaParcelas(3)">&nbsp;3&nbsp;&nbsp;&nbsp;
							<% end if %>
						<% end if %>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Valor/Parcela = <b>R$ <span id="spanValorParcela" class="Total">0,00</span></b>
						<hr noshade color="#7a64a5">
						</span>
						<input type="hidden" name="nr_parcelas" value="1">
						<input type="hidden" name="vl_parcela" value="0">
					</td>
				</tr>

				<tr class="CelulaCorpo" valign="top">
					<td valign="top" width="110">
						<span class="subtitulo"><%=strFormaPagamento_wk%></span>
					</td>
					<td valign="top">
						<%
						tp_pgto = 3
						%>

						<input type="hidden" name="tp_pagamento" value="<%=tp_pgto%>">
						<input checked type="radio" name="FormaPagamento" onclick="FormaPgto(3);">&nbsp;CHEQUE NOMINAL �<br><b><i>Sociedade Brasileira de Estomatologia</i></b><br>
						<!--
						<hr noshade color="#7a64a5">
						<input checked type="radio" name="FormaPagamento" onclick="FormaPgto(4);">&nbsp;BOLETO BANC�RIO
						<br>&nbsp; -->
					</td>
				</tr>
			</table>
		</td>
	</tr>


	<tr class="CelulaCorpoCinza">
		<td colspan="3">
			<span id="ObsCheques" style="display:'inline'">
				<small>&#149; A Inscri��o somente ser� confirmada ap�s a compensa��o do �ltimo cheque.<br>&#149; Cheques compensados n�o ser�o devolvidos.</small>
			</span>
			<span id="ObsBoletos" style="display:'none'">
				<small>&#149; A Inscri��o somente ser� confirmada ap�s a confirma��o do pagamento do �ltimo boleto.<br>&#149; Boletos pagos n�o ser�o devolvidos.</small>
			</span>
		</td>
	<tr>

	<tr class="CelulaTitulo">
		<td align="middle" colspan="3">
			<input type="button" name="proxima" id="proxima" value="<%=lblProximo_wk%>" class="BotaoPadrao" onmousedown="refreshFormulario();" onclick="ValidarForm(1);">&nbsp;&nbsp;
			<input type="reset" name="reset" value="<%=btnLimpar_wk%>" class="BotaoPadrao" onmouseup="refreshFormulario();">&nbsp;&nbsp;
			<input type="button" name="recalculo" value="<%=strRecalculo%>" class="BotaoPadrao" onmouseup="calculaTotal(1);">
		</td>
	</tr>
</table>
</form>