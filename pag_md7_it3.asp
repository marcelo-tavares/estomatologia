<% 
strTitulo = "FORMUL�RIO PARA INSCRI��O DE TRABALHOS"
strDados = "<b>Informa��es para Contato</b>"
strNome = "Nome completo do respons�vel pela inscri��o"
strAfiliacao = "Afilia��o (Institui��o/Organiza��o)"
strEndereco = "Endere�o completo para Correspond�ncia (incluir cidade, estado e CEP)"
strFone = "Telefone"
strFAX = "Fax"
strEmail = "E-mail"
strCategoria = "<b>Apresenta��o</b>"
strModo = "Modo de apresenta��o desejado:"
strRetirada = "Caso minha op��o de apresenta��o oral n�o seja poss�vel, eu <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gostaria de retirar meu trabalho.</i>"
strEmPainel = "Caso minha op��o de apresenta��o oral n�o seja poss�vel, eu <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;autorizo a Comiss�o Organizadora a alterar o modo de apresenta��o<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;para Painel Cl�nico.</i>"
strEnviar="Enviar"
strOral = "Oral"
strPainel = "Painel"
strConferencia = "Confer�ncia"
strPalavras = "Total de Palavras"
strMaior = "Atingido o numero m�ximo de palavras"
strMsg = "Por favor informe : "
StrErro001 = "Erro ao obter c�digo.  Favor tentar mais tarde."
StrErro002 = "Erro ao gravar inscri��o de trabalho.  Favor tentar mais tarde."
strMensagemEnviada_wk = "Seu Trabalho foi submetido com sucesso.<br>Aguarde contato da comiss�o organizadora do evento."
strDeclaro = "<b>Declaro que li e compreendi as <a href=""javascript:submeter(7,1,1,0)"">Normas para inscri��o de trabalhos</a> apresentadas neste site.</b>"
strTitle = "T�tulo"
strAuthors = "Autores and Afilia��es"
strSummary = "Resumo"

' GERAR UM N�MERO PROVIS�RIO PARA A INSCRI��O
cd_prefixo = buscaPrefixoImagens()

%>
<p class="Titulo" align="center"><%=strTitulo%></p>
<%
if pr1_wk = 0 then
%>
<SCRIPT>
function contaCaracteres(elemento,tamanhoMaximo){
	var frase
		
	frase = elemento.value;
	frase = frase.replace("  ", " ");
	elemento.value = frase;
		
	if (frase.length > tamanhoMaximo){
		alert('Tamanho m�ximo de ' + tamanhoMaximo + ' caracteres atingido!');
		frase = frase.substr(0,tamanhoMaximo)
		elemento.value = frase;				
	}  
}
function contaPalavras(){
	var frase
		
	tamanhoMaximo = document.Form1.tamanhoMaximo.value;
	frase = document.Form1.summary.value;
	frase = frase.replace("  ", " ");
	document.Form1.summary.value = frase;
	palavras = frase.split(" ");
		
	if (palavras.length > tamanhoMaximo){
		alert('<%=strMaior%>!');
		frase='';
		for (i=0; i<(tamanhoMaximo-1); i++)
			frase = frase + palavras[i] + ' ';
		frase = frase + palavras[tamanhoMaximo-1];
		palavras = frase.split(" ");
		document.Form1.summary.value = frase;				
	}  
	nr_palavras = palavras.length;
	if (palavras[nr_palavras-1] == '')
		nr_palavras--; 	
	document.getElementById("tp").innerHTML = nr_palavras;
}
	
function tamMaximo(opcao){
	tamanho = 250;
	if (opcao == 2)
		tamanho = 300;
	if (opcao == 4)
		tamanho = 600;
		
	document.Form1.modoApresentacao.value = opcao;
		
	document.Form1.tamanhoMaximo.value = tamanho;
	document.getElementById("spanTamMaximo").innerHTML = '(M�ximo: ' + tamanho + ' palavras)'
	contaPalavras();
	
	if (opcao == 4) {
		emPainel(1);
		obsReferenciasOral(1);
	}
	else {
		emPainel(0);
		obsReferenciasOral(0);
	}

	if (opcao == 1)
		perguntas(1);
	else
		perguntas(0);

	if ((opcao == 1) || (opcao == 4))
		imagens(1);
	else
		imagens(0);
}

function emPainel(visivel){
	if (visivel == 1)
		document.getElementById("trEmPainel").style.display = 'inline';
	else
		document.getElementById("trEmPainel").style.display = 'none';
}

function imagens(visivel){
	if (visivel == 1)
		document.getElementById("trImagens").style.display = 'inline';
	else
		document.getElementById("trImagens").style.display = 'none';
}

function perguntas(visivel){
	if (visivel == 1)
		document.getElementById("trCasoClinicoConsultivo").style.display = 'inline';
	else
		document.getElementById("trCasoClinicoConsultivo").style.display = 'none';
}

function obsReferenciasOral(visivel){
	if (visivel == 1)
		document.getElementById("spReferenciasOral").style.display = 'inline';
	else
		document.getElementById("spReferenciasOral").style.display = 'none';
}

function ValidarForm(){
	
	var InformouCategoria = 0;
		
	if (document.Form1.nome.value.length == 0){
		alert('<%=strMsg%><%=strNome%>');
		document.Form1.nome.focus();
		return false;
	}
		
	if (document.Form1.endereco.value.length == 0){
		alert('<%=strMsg%><%=strEndereco%>');
		document.Form1.endereco.focus();
		return false;
	}
		
	if (document.Form1.fone.value.length == 0){
		alert('<%=strMsg%><%=strFone%>');
		document.Form1.fone.focus();
		return false;
	}
		
	if (document.Form1.email.value.length == 0){
		alert('<%=strMsg%><%=strEmail%>');
		document.Form1.email.focus();
		return false;
	}
		
	if (document.Form1.inscricaoApresentador.value.length == 0){
		alert('<%=strMsg%>N�mero da Inscri��o do Apresentador do Trabalho');
		document.Form1.inscricaoApresentador.focus();
		return false;
	}
		
	if (document.Form1.title.value.length == 0){
		alert('<%=strMsg%><%=strTitle%>');
		document.Form1.title.focus();
		return false;
	}
		
	if (document.Form1.authors.value.length == 0){
		alert('<%=strMsg%><%=strAuthors%>');
		document.Form1.authors.focus();
		return false;
	}
		
	if (document.Form1.summary.value.length == 0){
		alert('<%=strMsg%><%=strSummary%>');
		document.Form1.summary.focus();
		return false;
	}
	
	if (document.Form1.keywords.value.length == 0){
		alert('<%=strMsg%>Palavras-Chave');
		document.Form1.keywords.focus();
		return false;
	}

	if (document.Form1.references.value.length == 0){
		alert('<%=strMsg%>Refer�ncias Bibliogr�ficas');
		document.Form1.references.focus();
		return false;
	}

	// Espec�fico do Modo 1 de apresenta��o
	if (document.Form1.modoApresentacao.value == 1){
		if (document.Form1.examesImaginologicos.value.length == 0){
			alert('<%=strMsg%>Exames Imaginol�gicos');
			document.Form1.examesImaginologicos.focus();
			return false;
		}

		if (document.Form1.examesAnatomopatologicos.value.length == 0){
			alert('<%=strMsg%>Exames Anatomopatol�gicos');
			document.Form1.examesAnatomopatologicos.focus();
			return false;
		}

		if (document.Form1.examesComplementares.value.length == 0){
			alert('<%=strMsg%>Outros Exames Complementares');
			document.Form1.examesComplementares.focus();
			return false;
		}

		if (document.Form1.infoTratamento.value.length == 0){
			alert('<%=strMsg%>Informa��es sobre o Tratamento');
			document.Form1.infoTratamento.focus();
			return false;
		}

		if (document.Form1.infoSeguimento.value.length == 0){
			alert('<%=strMsg%>Informa��es sobre o Seguimento');
			document.Form1.infoSeguimento.focus();
			return false;
		}

		if (document.Form1.diagnostico.value.length == 0){
			alert('<%=strMsg%>Diagn�stico ou hip�teses diagn�sticas');
			document.Form1.diagnostico.focus();
			return false;
		}

		if (document.Form1.relevancia.value.length == 0){
			alert('<%=strMsg%>Relev�ncia da Apresenta��o do Trabalho no Congresso');
			document.Form1.relevancia.focus();
			return false;
		}

	}
	
	emPainel(1);
	submeter(7,3,<%=id_wk%>,1);
}


function verificaJali(){
	if (document.Form1.jali.checked) {
		document.Form1.enviar.disabled = false;
	}
	else {
		document.Form1.enviar.disabled = true;
	}
}

function popUp(URL,nomeJanela,props){
	window.open(URL,nomeJanela,props);
}

function exemploAutores(){
	url = 'exemploAutores.asp'
	params = 'scrollbars=no,width=600,height=150';
	id = 'exemploAutores';
	popUp(url,id,params);
}

function IncluirImagens(prefixo){
	url = 'incluirImagens.asp?prefixo='+prefixo
	params = 'scrollbars=no,width=600,height=500';
	id = 'incluirImagens';
	popUp(url,id,params);
}
</SCRIPT>
<input type=hidden name=tamanhoMaximo value=250>
<input type=hidden name=prefixoImagens value=<%=cd_prefixo%>>
<table width=100%>
	<tr class=textoComum>
		<td colspan=2>
			&nbsp;<br>
			<%=strDados%><br>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<%=strNome%><br>
			<input type=text size=76 name=nome class=textbox01 maxlength=100>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<%=strEndereco%><br>
			<input type=text size=76 name=endereco class=textbox01 maxlength=250>
		</td>
	</tr>
	<tr class=textoComum>
		<td>
			<%=strFone%><br>
			<input type=text size=30 name=fone class=textbox01 maxlength=20>
		</td>
		<td>
			<%=strFAX%><br>
			<input type=text size=30 name=fax class=textbox01 maxlength=20>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<%=strEmail%><br>
			<input type=text size=76 name=email class=textbox01 maxlength=50>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			N�mero da Inscri��o do Apresentador do Trabalho:&nbsp;
			<input type=text size=5 name=inscricaoApresentador class=textbox01>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<table width=100% border=0>
				<tr class=textoComum>
					<td valign=top width=40%>
						&nbsp;<br>
						<b>Op��es de Inscri��o</b><br>
						<%=strModo%>&nbsp;&nbsp;
					</td>
					<td valign=top width=60%>
						&nbsp<br>
						<input <% if pr4_wk = 1 then %>checked<% end if %> type=radio name=modo value=1 onclick="tamMaximo(1);perguntas(1);">Caso Cl�nico Consultivo<br>
						<input <% if pr4_wk = 2 then %>checked<% end if %> type=radio name=modo value=2 onclick="tamMaximo(2);">Painel de Pesquisa<br>
						<input <% if pr4_wk = 3 then %>checked<% end if %> type=radio name=modo value=3 onclick="tamMaximo(3);">Painel Cl�nico<br>
						<input <% if pr4_wk = 4 then %>checked<% end if %> type=radio name=modo value=4 onclick="tamMaximo(4);">Apresenta��o Oral de Caso Cl�nico
						<input type=hidden name=modoApresentacao value=1>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class=textoComum id=trEmPainel style="display='none'">
		<td colspan=2>
			<input checked type=radio name=naoOral value=1>&nbsp;&nbsp;<%=strRetirada%><br>
			<input type=radio name=naoOral value=2>&nbsp;&nbsp;<%=strEmPainel%>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			&nbsp<br><b><%=strTitle%></b> <i>(M�ximo 250 caracteres)</i><br>
			<textarea cols=76 rows=3 name=title class=textarea01 onkeyup="contaCaracteres(this,250);" onkeydown="contaCaracteres(this,250);"></textarea>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<b><%=strAuthors%></b> <i>(M�ximo 500 caracteres)</i>&nbsp;&nbsp;<a href="javascript:exemploAutores();">ver exemplo de formata��o</a><br>
			<textarea cols=76 rows=3 name=authors class=textarea01 onkeyup="contaCaracteres(this,500);" onkeydown="contaCaracteres(this,500);"></textarea>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<b><%=strSummary%></b> - <i>Palavras:&nbsp;<b><span id=tp>0</span></b>&nbsp;&nbsp;<span id=spanTamMaximo>(M�ximo: 250 palavras)</span></i><br>
			<textarea name=summary rows=10 cols=76 class=textarea01 onkeyup="contaPalavras();" onkeydown="contaPalavras();"></textarea><br>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<b>Palavras-Chave</b> <i>(M�ximo 250 caracteres)</i><br>
			<small>de acordo com o DECs (<a href="http://decs.bvs.br" target=nova>http://decs.bvs.br</a>) ou listas de cabe�alhos de assunto do<br><i>Index to Dental</i> ou <i>Index Medicus</i>. Separados por v�rgula</small>
			<textarea name=keywords rows=2 cols=76 class=textarea01 onkeyup="contaCaracteres(this,250);" onkeydown="contaCaracteres(this,250);"></textarea>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			<b>Refer�ncias Bibliogr�ficas</b><br>
			<small>
				(m�ximo de 10, de acordo com NBR 6023 � ABNT)
				<span id=spReferenciasOral style="display:'none'">
					<br>Para o artigo, n�o h� limite de refer�ncias, devendo ser respeitado apenas o limite de 8 p�ginas dentro da formata��o indicada nas <a href="downloads/modeloArtigoCompleto.doc" target=nova>Normas para Submiss�o do Artigo Completo</a>.	
				</span>
			</small>
			<textarea name=references rows=5 cols=76 class=textarea01></textarea>
		</td>
	</tr>
	
	<tr class=textoComum  id=trCasoClinicoConsultivo style="display='inline'">
		<td colspan=2>
			<br>
			<b>Exames imaginol�gicos? Especificar.</b><br>
			<textarea name=examesImaginologicos rows=3 cols=76 class=textarea01></textarea>
			<br><br>
			<b>Exames anatomopatol�gicos? Especificar.</b><br>
			<textarea name=examesAnatomopatologicos rows=3 cols=76 class=textarea01></textarea>
			<br><br>
			<b>Outros exames complementares? Especificar.</b><br>
			<textarea name=examesComplementares rows=3 cols=76 class=textarea01></textarea>
			<br><br>
			<b>Informa��es sobre o tratamento? Especificar.</b><br>
			<textarea name=infoTratamento rows=3 cols=76 class=textarea01></textarea>
			<br><br>
			<b>Acompanhamento ou Informa��es sobre o seguimento? Especificar.</b><br>
			<textarea name=infoSeguimento rows=3 cols=76 class=textarea01></textarea>
			<br><br>
			<b>A dificuldade do caso est�:</b><br>
			<input checked type=radio name=dificuldade value=1>&nbsp;&nbsp;No diagn�stico<br>
			<input type=radio name=dificuldade value=2>&nbsp;&nbsp;No tratamento<br>
			<input type=radio name=dificuldade value=3>&nbsp;&nbsp;Em ambos
			<br><br>
			<b>Qual o diagn�stico ou hip�teses diagn�sticas? </b><br>
			<textarea name=diagnostico rows=3 cols=76 class=textarea01></textarea>
			<br><br>
			<b>Na sua opini�o qual a relev�ncia da apresenta��o de seu trabalho no congresso? </b><br>
			<textarea name=relevancia rows=3 cols=76 class=textarea01></textarea>
		</td>
	</tr>

	<tr class=textoComum  id=trImagens style="display='inline'">
		<td colspan=2>
			<br>
			<button onclick="javascript:IncluirImagens(<%=cd_prefixo%>);" class=botaoPadrao120>INCLUIR IMAGENS</button>
		</td>
	</tr>

	<tr class=textoComum>
		<td colspan=2>
			&nbsp;
			<br>
			<input type=checkbox name=jali onclick="verificaJali();" value="ON">&nbsp;
			<%=strDeclaro%>
		</td>
	</tr>
	<tr class=textoComum>
		<td colspan=2>
			&nbsp;
			<br>
			<input disabled type=button class=botaoPadrao name=enviar value=<%=strEnviar%> onclick="ValidarForm();">
		</td>
	</tr>
<table>
<% 
else
	' Gravar no banco	
	
	nome_wk = replace(request("nome"),"'", "''")
	endereco_wk = replace(request("endereco"),"'", "''")
	fone_wk = replace(request("fone"),"'", "''")
	fax_wk = replace(request("fax"),"'", "''")
	email_wk = replace(request("email"),"'", "''")
	InscricaoApresentador_wk = request("inscricaoApresentador")
	modo_wk = request("modoApresentacao")
	select case modo_wk
		case 1
			strModo_wk = "Caso Cl�nico Consultivo"
		case 2
			strModo_wk = "Painel de Pesquisa"
		case 3
			strModo_wk = "Painel Consultivo"
		case 4
			strModo_wk = "Apresenta��o Oral de Caso Cl�nico"
	end select
	naoOral_wk = request("naoOral")
	if naoOral_wk = "" then 
		strNaoOral_wk = "Retirar Trabalho"
	else
		strNaoOral_wk = "Alterar para Painel Cl�nico"
	end if
	title_wk = replace(request("title"),"'", "''")
	authors_wk = replace(request("authors"),"'", "''")
	summary_wk = replace(request("summary"),"'", "''")
	keywords_wk = replace(request("keywords"),"'", "''")
	references_wk = replace(request("references"),"'", "''")

	examesImaginologicos_wk = replace(request("examesImaginologicos"),"'", "''")
	examesAnatomopatologicos_wk = replace(request("examesAnatomopatologicos"),"'", "''")
	examesComplementares_wk = replace(request("examesComplementares"),"'", "''")
	infoTratamento_wk = replace(request("infoTratamento"),"'", "''")
	infoSeguimento_wk = replace(request("infoSeguimento"),"'", "''")
	dificuldade_wk = replace(request("dificuldade"),"'", "''")
	select case dificuldade_wk
		case 1
			strDificuldade_wk = "No diagn�stico"
		case 2
			strDificuldade_wk = "No tratamento"
		case 3
			strDificuldade_wk = "Em Ambos (diagn�stico e tratamento)"
	end select
	diagnostico_wk = replace(request("diagnostico"),"'", "''")
	relevancia_wk = replace(request("relevancia"),"'", "''")

	dataInscricao = now()
	
	de_prefixoImagens = Request("prefixoImagens")
	
	cd_obtido = buscaCodigoTrabalho
	
	if cd_obtido > 0 then

		' Gravar Inscri��o
		%><!--#include file=conn.asp --><%
		Set Conn1 = Server.CreateObject("ADODB.Connection") 

		Conn1.Open cnpath
		Conn1.BeginTrans
		
		' 1- Gravar Dados do Trabalho
		strSQL = _
		"INSERT INTO TB_Trabalhos (" _
		& "cd_trabalho, nm_responsavel, de_endereco, nr_fone, nr_fax, " _
		& "de_email, cd_apresentador, cd_modo, cd_naoOral, cd_dificuldade, " _
		& "de_titulo, de_autores, de_sumario, de_palavrasChaves, de_referencias, " _
		& "de_imaginologico, de_anatomopatologico, de_outrosExames, de_infoTratamento, " _
		& "de_infoSeguimento, de_diagnostico, de_relevancia, " _
		& "dt_cadastro, dt_resultado, sn_aceito, de_prefixoImagens) " _
		& "VALUES (" _
		& cd_obtido & ", '" & nome_wk & "', '" & endereco_wk & "', '" _
		& fone_wk & "', '" & fax_wk & "', '" & email_wk & "', " & InscricaoApresentador_wk & ", " _
		& modo_wk & " ," & naoOral_wk & " , " & dificuldade_wk & ", '" & title_wk & "', '" _
		& authors_wk & "', '" & summary_wk & "', '" & keywords_wk & "', '" _
		& references_wk & "', '" & examesImaginologicos_wk & "', '" _
		& examesAnatomopatologicos_wk & "', '" & examesComplementares_wk & "', '" _
		& infoTratamento_wk & "', '" & infoSeguimento_wk & "', '" & diagnostico_wk & "', '" _
		& relevancia_wk & "', '" _
		& dataInscricao & "', '31/12/2029', 0,'" & de_prefixoImagens & "')"
		
		'Response.Write strSQL & "<BR>"
		'Response.end

		on error resume next
		Conn1.Execute strSQL

		if err.number = 0 then
			Conn1.CommitTrans
			set Conn1 = nothing
			on error goto 0

			str_CrLf = chr(13) & chr(10)

			strBody = _
			"Nome do Respons�vel: " & nome_wk & str_CrLf & _
			"Endere�o : " & endereco_wk & str_CrLf & _
			"Fone : " & fone_wk & str_CrLf & _
			"Fax : " & fax_wk & str_CrLf & _
			"E-mail : " & email_wk & str_CrLf & str_CrLf & _
			"Modo : " & strModo_wk & str_CrLf & _
			"T�tulo : " & title_wk & str_CrLf & _
			"Autores : " & authors_wk & str_CrLf & _
			"Data da Inscri��o : " & dataInscricao & str_CrLf & str_CrLf & _
			"Outros Detalhes desta Inscri��o podem ser vistos no site administrativo do congresso " & str_CrLf & str_CrLf & _
			strSQL			
			%><!--#include file=inc_email.asp --><%
			'e-mail do remetente 
			objCDOSYSMail.From = nome_wk & " (Inscri��o de trabalho)<XIII_CBE@tavares.eti.br>"

			'e-mail do destinat�rio 
			'objCDOSYSMail.To = "Comiss�o Organizadora - Trabalhos <sobe2005_trabalhos@tavares.eti.br>"
			'objCDOSYSMail.BCC = "marcelo@tavares.eti.br"
			objCDOSYSMail.To = "marcelo@tavares.eti.br"

			'assunto da mensagem 
			objCDOSYSMail.Subject = "Inscri��o de trabalho (n�mero " & cd_obtido & ")"

			'conte�do da mensagem 
			str_CrLf = chr(13) & chr(10)
			objCDOSYSMail.TextBody = strBody
			'para envio da mensagem no formato html altere o TextBody para HtmlBody 
			'objCDOSYSMail.HtmlBody = "Teste do componente CDOSYS"

			'objCDOSYSMail.fields.update
			'envia o e-mail 
			objCDOSYSMail.Send 

			'destr�i os objetos 
			Set objCDOSYSMail = Nothing 
			Set objCDOSYSCon = Nothing 
			
			Response.Write "<table width=100% ><tr><td align=center><h5>"& strMensagemEnviada_wk & "</h5></td></tr></table>"
		else
			Conn1.RollbackTrans
			set Conn1 = nothing
			Response.Write "<h5>" & strErro002 & "</h5>"

			' DEBUG
			%><!--#include file=inc_email.asp --><%
			'e-mail do remetente 
			objCDOSYSMail.From = "SOBE 2005 <XIII_CBE@tavares.eti.br>"

			'e-mail do destinat�rio 
			objCDOSYSMail.To = "Comiss�o Organizadora - Trabalhos <marcelo@tavares.eti.br>"
			objCDOSYSMail.BCC = "marcelo@tavares.eti.br"
			'objCDOSYSMail.To = "marcelo@tavares.eti.br"

			'assunto da mensagem 
			objCDOSYSMail.Subject = "Erro 002 na Inscri��o de trabalho"

			'conte�do da mensagem 
			objCDOSYSMail.TextBody = err.Description & str_CrLf & str_CrLf & strSQL
			'para envio da mensagem no formato html altere o TextBody para HtmlBody 
			'objCDOSYSMail.HtmlBody = "Teste do componente CDOSYS"

			'objCDOSYSMail.fields.update
			'envia o e-mail 
			objCDOSYSMail.Send 

			'destr�i os objetos 
			Set objCDOSYSMail = Nothing 
			Set objCDOSYSCon = Nothing 
		end if
	else
		Response.Write "<h5>" & strErro001 & "</h5>"

			' DEBUG
			%><!--#include file=inc_email.asp --><%
			'e-mail do remetente 
			objCDOSYSMail.From = "SOBE 2005 <XIII_CBE@tavares.eti.br>"

			'e-mail do destinat�rio 
			'objCDOSYSMail.To = "Comiss�o Organizadora - Trabalhos <sobe2005_trabalhos@tavares.eti.br>"
			'objCDOSYSMail.BCC = "marcelo@tavares.eti.br"
			objCDOSYSMail.To = "marcelo@tavares.eti.br"

			'assunto da mensagem 
			objCDOSYSMail.Subject = "Erro 001 na Inscri��o de trabalho"

			'conte�do da mensagem 
			objCDOSYSMail.TextBody = err.Description
			'para envio da mensagem no formato html altere o TextBody para HtmlBody 
			'objCDOSYSMail.HtmlBody = "Teste do componente CDOSYS"

			'objCDOSYSMail.fields.update
			'envia o e-mail 
			objCDOSYSMail.Send 

			'destr�i os objetos 
			Set objCDOSYSMail = Nothing 
			Set objCDOSYSCon = Nothing 
	end if
end if 

' ===========================================================================
function buscaCodigoTrabalho()
	buscaCodigoTrabalho = ""
	%><!--#include file=conn.asp --><%
	Set objConn = Server.CreateObject("ADODB.Connection") 
	set objRs = server.CreateObject("ADODB.Recordset")
	
	on error resume next
	objConn.Open cnpath

	if err.number <> 0 then
		Response.Write err.number & " - " & err.description & "(00)<br>"
		'Response.Write "<h5>N�o foi poss�vel conectar com o banco de dados. Por favor tente mais tarde.</h5>"
		buscaCodigoTrabalho = -9
		return
	else
		objConn.BeginTrans
	
		with objRs

			.ActiveConnection = objConn
			.CursorType = 2 'adOpenDynamic 
			.LockType = 2 'adLockOptmistic
			.Open "TB_InscricaoTrabalhos"

			.AddNew

			retorno = objRs("cd_inscricao")
			.Update

			if err.number <> 0 then
				.Close
				buscaCodigoTrabalho = -1
				objConn.RollbackTrans
			else
				.Close
				buscaCodigoTrabalho = retorno
				objConn.CommitTrans
			end if
			objConn.Close
		end with
	end if
	
	set objConn = nothing
	set objRs = nothing
end function

function buscaPrefixoImagens()
	buscaPrefixoImagens = ""
	%><!--#include file=conn.asp --><%
	Set objConn = Server.CreateObject("ADODB.Connection") 
	set objRs = server.CreateObject("ADODB.Recordset")
	
	on error resume next
	objConn.Open cnpath

	if err.number <> 0 then
		Response.Write err.number & " - " & err.description & "(00)<br>"
		'Response.Write "<h5>N�o foi poss�vel conectar com o banco de dados. Por favor tente mais tarde.</h5>"
		buscaPrefixoImagens = -9
		return
	else
		objConn.BeginTrans
	
		with objRs

			.ActiveConnection = objConn
			.CursorType = 2 'adOpenDynamic 
			.LockType = 2 'adLockOptmistic
			.Open "TB_PrefixoImagens"

			.AddNew

			retorno = objRs("cd_prefixo")
			.Update

			if err.number <> 0 then
				.Close
				buscaPrefixoImagens = -1
				objConn.RollbackTrans
			else
				.Close
				buscaPrefixoImagens = retorno
				objConn.CommitTrans
			end if
			objConn.Close
		end with
	end if
	
	set objConn = nothing
	set objRs = nothing
end function

%>