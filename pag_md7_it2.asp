<% if id_wk = 1 then %>
	<p class="Titulo" align="center">PREPARANDO O RESUMO DE SEU TRABALHO</p>
	<p class="textoComum">
		Favor observar que todas as informa��es abaixo necessitam ser fornecidas pelo apresentador quando da inscri��o do trabalho, independente do meio utilizado para a inscri��o (website, e-mail, ou inclu�das no disquete/CD enviado para a secretaria do Congresso):
	</p>
	<p class="textoComum">
		<b>� Informa��o para contato</b><br>
		- Sobrenome / Nome<br>
		- Afilia��o (Institui��o/Organiza��o)<br>
		- Endere�o para correspond�ncia<br>
		- Telefone (com c�digo de �rea)<br>
		- Fax (com c�digo de �rea)<br>
		- E-mail
	</p>
	<p class="textoComum">
		<b>� Categorias de apresenta��o</b><br>
		- Modo de apresenta��o desejado: oral (comunica��o cient�fica) / painel / confer�ncia
	</p>
	<p class="textoComum">
		<b>� Op��o por retirada de trabalho</b><br>
		Caso minha op��o de apresenta��o n�o seja poss�vel, eu gostaria de retirar meu trabalho: (sim/n�o)<br>
		(v�lido somente para comunica��es cient�ficas e pain�is)
	</p>
	<p class="textoComum">
		
	</p>
	<p class="textoComum">
		&nbsp;<br><b>INSTRU��ES GERAIS</b>
		<ul class="textoComum">
			<li>Os resumos de trabalhos a serem enviados por e-mail ou pelo correio devem ser digitados em formato Microsoft Word 6.0 ou superior</li>
			<li>Para comunica��es cient�ficas e pain�is, o resumo do trabalho deve conter: Aims, Methods, Results e Conclusions.</li>
			<li>Para confer�ncias, o formul�rio de inscri��o deve conter o resumo da apresenta��o com uma lista dos t�picos a serem discutidos</li>
			<li>O resumo deve ser o mais informativo poss�vel, e deve incluir an�lise estat�stica quando apropriado. N�o use tabelas</li>
			<li>Somente ser�o aceitos trabalhos que demonstrem claramente que os experimentos foram conclu�dos</li>
			<li>Abrevia��es comuns devem ser utilizadas. Escreva o termo por extenso quando mencionado pela primeira vez, seguido da abreviatura em par�nteses</li>
			<li>N�o utilize recuo nos par�grafos novos</li>
		</ul>
	</p>
	<p class="textoComum">
		&nbsp;<br><b>INSTRU��ES ESPEC�FICAS</b><br>(principalmente para os resumos submetidos via e-mail ou correio):
		<ul class="textoComum">
			<li>Configure sua p�gina com margens de 2,5cm. Use carateres do tipo �Times New Roman�, tamanho 12 e espa�amento simples para todo o texto</li>
			<li>Digite o t�tulo, centralizado, diretamente na primeira linha da p�gina. O t�tulo deve ter um m�ximo de tr�s linhas e os caracteres devem estar em negrito e caixa alta (todos mai�sculos).</li>
			<li>Deixe uma linha em branco ap�s o t�tulo</li>
			<li>Digite os autores e respectivas afilia��es em it�lico e centralizado, na forma: Costa E, Abreu M Jr, Universidade Federal de Santa Catarina, Florianopolis, Brasil. Use n�meros sobrescritos (ex.: Costa E<sup>1</sup>, Abreu M<sup>2</sup>) para diferentes afilia��es</li>
			<li>Deixe uma linha em branco ap�s a informa��o sobre os autores</li>
			<li>Digite o resumo de seu trabalho, utilizando texto justificado, em um m�ximo de 250 palavras. Aims, Methods, Results e Conclusions devem, cada qual, iniciar nova linha (sem recuo de par�grafo). N�o deixe linhas em branco entre os par�grafos</li>
			<li>Deixe uma linha em branco ap�s o resumo de seu trabalho</li>
			<li>Liste algumas palavras-chave (Ex.: Keywords: TMJ, diagnosis, MRI), sem rec�o de par�grafo</li>
		</ul>
		
	</p>
	<p class="textoComum">
		&nbsp;<br><b>INSTRU��ES PARA A PREPARA�AO DE  PAIN�IS</b>
		<ul class="textoComum">
			<li>No Congresso, voc� dispor� de um painel vertical com �rea �til de 95cm X 200cm. Os trabalhos ser�o afixados aos pain�is atrav�s de fita adesiva de dupla face fornecida pela Comiss�o Organizadora</li>
			<li>O seu painel ser� numerado. Os congressitas poder�o localizar seu trabalho pelo t�tulo e n�mero constantes no programa oficial do Congresso</li>
			<li>O seu painel deve ser estruturado com os seguintes itens: T�tulo, Autores e Afilia��es, Abstract (incluindo as �keywords�), Aims, Methods, Results, Conclusions e References (quando apropriado)</li>
		</ul>
		<br>&nbsp;
		<br>&nbsp;
		<br>&nbsp;
	</p>
<% else %>
	<p class="Titulo" align="center">PREPARING YOUR ABSTRACT</p>
	<p class="textoComum">
		Please note that all information below need to be provided by the presenting author when submitting the abstract via the Website, e-mail, or included in the floppy/CD mailed to the Secretariat:
	</p>
	<p class="textoComum">
		<b>� Contact information</b><br>
		- Last name / First name<br>
		- Academic/organizational affiliation<br>
		- Correspondence address<br>
		- Telephone number<br>
		- Fax number<br>
		- E-mail address
	</p>
	<p class="textoComum">
		<b>� Presenting category</b><br>
		- Mode of presentation (oral / poster / conference)
	</p>
	<p class="textoComum">
		<b>� Withdraw Option</b><br>
		In case my first preference is not given, I wish to withdraw my presentation: (yes/no)<br>
		(For oral and poster presentations only)
	</p>
	<p class="textoComum">
		
	</p>
	<p class="textoComum">
		&nbsp;<br><b>GENERAL INSTRUCTIONS</b>
		<ul class="textoComum">
			<li>Abstracts to be sent by e-mail or regular mail should be typed in PC format, MS Word 6.0 or above</li>
			<li>For oral and poster presentations, the abstract must include Aims, Methods, Results, Conclusions</li>
			<li>For conferences, the abstract should provide a summary of the presentation with a list of topics to be discussed</li>
			<li>The abstract should be as informative as possible and should include statistical analysis where appropriate. Do not use tables</li>
			<li>Abstracts will be considered only if their results clearly demonstrate that the experiments have been completed</li>
			<li>Standard abbreviations should be used. Give the term in full when first mentioned followed by the abbreviation in parentheses</li>
			<li>Do not indent the first line of paragraphs</li>
		</ul>
	</p>
	<p class="textoComum">
		&nbsp;<br><b>SPECIFIC INSTRUCTIONS </b><br>(mainly for abstracts submitted by e-mail or regular mail):
		<ul class="textoComum">
			<li>Format your page with 2,5cm margins. Use �Times New Roman� font, at 12 point size and single line spacing for the ENTIRE document.</li>
			<li>Type the title directly on the top line. The title should be a maximum of 3 lines, all capitals, bold face, and center justified</li>
			<li>Leave a line in blank after the title</li>
			<li>Type the authors� details in italics and centered, in the format: Costa E, Abreu M Jr, Universidade Federal de Santa Catarina, Florianopolis, Brasil. Use superscript numbers for different affiliations</li>
			<li>Leave a line in blank after the authors� details</li>
			<li>Write the summary of your abstract with a maximum of 250 words. Aims, Methods, Results and Conclusions should each begin a new paragraph (without indentation). Do not leave blank lines between paragraphs.</li>
			<li>Leave a line in blank after the summary</li>
			<li>List a few �keywords� at the last line of the abstract (Ex.: Keywords: TMJ, diagnosis, MRI), without indentation</li>
		</ul>

	</p>
	<p class="textoComum">
		&nbsp;<br><b>INSTRUCTIONS FOR PREPARING POSTERS</b>
		<ul class="textoComum">
			<li>At the Congress, you will be provided with a vertical display board having a viewing area of 95cm X 200cm. Posters will be attached to the display board by means of double-face tape provided by the Organization</li>
			<li>Your board will be numbered. Viewers will find your poster according to the title of your submitted abstract and number as stated in the final program</li>
			<li>Your poster should be structured as follows: Title, Authors� details, Abstract (including the keywords), Aims, Methods, Results, Conclusions and References (when appropriate)</li>
			<li>The heading should list the title of the paper, authors, institution and its location. There will be no title provided.</li>
		</ul>
		<br>&nbsp;
		<br>&nbsp;
		<br>&nbsp;
	</p>
<% end if %>
