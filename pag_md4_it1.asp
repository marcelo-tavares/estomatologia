	<p class="Titulo" align="center">CENTRO DE CULTURA E EVENTOS DA UFSC</p>

	<p class="textoComum" align="left">
		O local que sediar� o XIII Congresso Brasileiro de Estomatologia foi cuidadosamente selecionado de modo a proporcionar todo o conforto para as atividades cient�ficas. O moderno Centro de Cultura e Eventos est� localizado no Campus da Universidade Federal de Santa Catarina, a poucos quilometros do centro de Florian�polis, pr�ximo ao centro geogr�fico da ilha de Santa Catarina. Isto facilitar� o deslocamento e as visitas �s atra��es tur�sticas localizadas nas diferentes partes da ilha e em cidades pr�ximas.
	</p>
	<p class="textoComum" ALIGN="center">
		<img SRC="images/centroEventos.jpg" BORDER="0">
	</p>
	<p class="textoComum" align="left">
		O Centro de Cultura e Eventos � um espa�o com 8.000m� de �rea �til, sendo fruto de um arrojado esfor�o da UFSC para oferecer � comunidade um espa�o dotado de amplo e confort�vel audit�rio com capacidade para 1371 pessoas, e uma infra-estrutura planejada para sediar formaturas, simp�sios semin�rios, congressos, workshops, feiras e demais eventos.
	</p>
	<p class="textoComum" align="left">
		Al�m do Audit�rio Garapuvu, com 1371 acentos, estamos disponibilizando tamb�m 4 salas multifuncionais, com paredes m�veis e capacidade para 75 pessoas cada sala, podendo ser convertida num espa�o �nico para 300 pessoas, para utiliza��o como sal�o, �rea de exposi��es e montagem de estandes. 
	</p>
	<p class="textoComum" align="left">
		Um Hall com 500m�, que possibilitar� o total apoio aos eventos do audit�rio, no que diz respeito a montagem de stands, exposi��es e feiras.
	</p>
	<p class="textoComum" align="left">
		Os usu�rios do Centro de Cultura e Eventos contam com uma ampla infra-estrutura de apoio, com uma pra�a de alimenta��o com mais de 7 op��es na �rea de alimenta��o , ag�ncia de viagens, livraria, banco, laborat�rio fotogr�fico, loja de reprografia entre outros.	 
	</p>
	<br>
