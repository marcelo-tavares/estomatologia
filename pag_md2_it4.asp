<% 
if id_wk = 1 then
	strTitulo = "PALESTRANTES DE DESTAQUE J� CONFIRMADOS"
	strSubTitulo1 = "Clique na foto para ver detalhes"
	strSubTitulo2 = "Clique na foto para retornar"
	strMarcelo = "<b>Marcelo Cavalcanti</b><br>(Brasil)"
	strAllan = "<b>Allan Farman</b><br>(EUA)"
	strNeil = "<b>Neil Frederiksen</b><br>(EUA)"
	strChristina = "<b>Christina Lindh</b><br>(Su�cia)"
	strDale = "<b>Dale Miles</b><br>(EUA)"
	strTohru = "<b>Tohru Kurabayashi</b><br>(Jap�o)"
	strRejane = "<b>Rejane Ribeiro - Rotta</b><br>(Brasil)"
	strIzabel = "<b>Izabel Rubira</b><br>(Brasil)"
	strJaime = "<b>Jaime Valenzuela</b><br>(Chile)"
	strAnn = "<b>Ann Wenzel</b><br>(Dinamarca)"
	strEric = "<b>Eric Whaites</b><br>(Inglaterra)"
	strJackie = "<b>Jackie Brown</b><br>(Inglaterra)"
	strCurso = "Curso"
	strP1_1 = "Participa��o no Simp�sio de Interpreta��o Radiogr�fiaca Avan�ada das Gl�ndulas Salivares"
	strP3_1 = "Participa��o no Simp�sio de Radiologia Digital"
else
	strTitulo = "INVITED SPEAKERS WHO HAVE CONFIRMED THEIR PARTICIPATION"
	strSubTitulo1 = "Click the photo to details"
	strSubTitulo2 = "Click the photo to return"
	strMarcelo = "<b>Marcelo Cavalcanti</b><br>(Brazil)"
	strAllan = "<b>Allan Farman</b><br>(USA)"
	strNeil = "<b>Neil Frederiksen</b><br>(USA)"
	strChristina = "<b>Christina Lindh</b><br>(Sweden)"
	strDale = "<b>Dale Miles</b><br>(USA)"
	strTohru = "<b>Tohru Kurabayashi</b><br>(Japan)"
	strRejane = "<b>Rejane Ribeiro - Rotta</b><br>(Brazil)"
	strIzabel = "<b>Izabel Rubira</b><br>(Brazil)"
	strJaime = "<b>Jaime Valenzuela</b><br>(Chile)"
	strAnn = "<b>Ann Wenzel</b><br>(Denmark)"
	strEric = "<b>Eric Whaites</b><br>(England)"
	strJackie = "<b>Jackie Brown</b><br>(England)"
	strCurso = "Special Lecture"
	strP1_1 = "Presentation in the Symposium on Advanced Imaging Interpretation of the Salivary Glands"
	strP3_1 = "Presentation in the Symposium on Digital Radiology"
end if 

select case pr1_wk
	case 0
	%>
		<p class="Titulo" align="center"><%=strTitulo%></p>
		<p class="TextoComum" align="center">(<%=strSubTitulo1%>)</p>
		<p class="TextoComum"><b>CONGRESSO</b></p>
		<table width="100%">
			<tr>
				<td class="textoComumCentro" width=25% valign=top>
					<img src="images/palestrantes/solSilvermanJr_tn.jpg" vspace="5" onclick="submeter(2,4,<%=id_wk%>,1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="75" HEIGHT="100"><br>
					<b>Sol Silverman Jr.</b><br>(EUA)<br>
					<img src="images/ponto.gif" height="20">
				</td>
				<td class="textoComumCentro" width=25% valign=top>
					&nbsp;
					<img src="images/palestrantes/AbelSilveiraCardoso_tn.jpg" vspace="5" onclick="submeter(2,4,<%=id_wk%>,2);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="75" HEIGHT="100"><br>
					<b>Abel Silveira Cardoso</b><br>(Brasil)<br>
					<img src="images/ponto.gif" height="20">
				</td>
				<td class="textoComumCentro" width=25% valign=top>
					&nbsp;
					<img src="images/palestrantes/Damante_tn.jpg" vspace="5" onclick="submeter(2,4,<%=id_wk%>,3);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="75" HEIGHT="100"><br>
					<b>Jos� Humberto Damante</b><br>(Brasil)<br>
					<img src="images/ponto.gif" height="20">
				</td>
				<td class="textoComumCentro" width=25% valign=top>
					&nbsp;
					<img src="images/palestrantes/imagemNaoDisponivel_tn.jpg" vspace="5" onclick="submeter(2,4,<%=id_wk%>,4);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="75" HEIGHT="100"><br>
					<b>Celso MAssumoto</b><br>(Brasil)<br>
					<img src="images/ponto.gif" height="20">
				</td>
			</tr>
		</table>
		<p class="TextoComum"><b>PR�-CONGRESSO</b></p>
		<table width="100%">
			<tr>
				<td class="textoComumCentro" width=33%>
					<img src="images/palestrantes/GMachuca_tn.jpg" vspace="5" onclick="submeter(2,4,<%=id_wk%>,11);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="75" HEIGHT="100"><br>
					<b>Guillermo Machuca Portillo</b><br>(Espanha)<br>
					<img src="images/ponto.gif" height="20">
				</td>
				<td class="textoComumCentro" width=34%>
					&nbsp;
					<!-- inserir aqui a imagem do palestrante --><br>
					<%'aqui o nome do palestrante%><br>
					<img src="images/ponto.gif" height="20">
				</td>
				<td class="textoComumCentro" width=33%>
					&nbsp;
					<!-- inserir aqui a imagem do palestrante --><br>
					<%'aqui o nome do palestrante%><br>
					<img src="images/ponto.gif" height="20">
				</td>
			</tr>
		</table>
	<%
	case 1
	%>
		<p class="Titulo" align="center">Sol Silverman Jr.</p>
		<p class="TextoComum" align="center">(Clique na foto para retornar)</p>
		<table class="TabelaProgramacao" width="100%">
			<tr valign="top">
				<td class="TextoComum">
					<b>Curriculum Vitae</b><br>
					<ul>
						<li>Professor de Medicina Oral da Universidade da Calif�rnia, S�o Francisco � EUA 
						<li>Forma��o:
							<ul>
								<li>Universidade da Calif�rnia, Berkeley, BA, 1950, Zoology 
								<li>Universidade da Calif�rnia, Berkeley, MA, 1951, Physiology 
								<li>Universidade da Calif�rnia San Francisco, DDS, 1954, Dentistry 
								<li>Universidade da Calif�rnia San Francisco, Certificate, 1955, Oral Medicine 
							</ul>
						<li>Honrarias e Pr�mios
							<ul>
								<li>American Dental Association Norton Ross Award for Excellence in Clinical Research, 1996 
								<li>Doctor of Science, Honoris Causa, McGill University, Montreal, 1998 
								<li>Keynote speaker, 6th International Oral Cancer Congress, Delhi, India, 1999 
								<li>Diamond Pin (highest award), American Academy of Oral Medicine, 1999 
								<li>Samuel Harvey Lecturer, Annual Meeting, American Association Cancer Education, 2001 
							</ul>
					</ul>
				</td>
				<td align="right">
					<img src="images/palestrantes/solSilvermanJr.jpg" vspace="5" hspace="5" onclick="submeter(2,<%=pr3_wk%>,<%=id_wk%>,0);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="225" HEIGHT="300"><br>
				</td>
			</tr>
			<tr>
				<td class="TextoComum" colspan="2">
					<p class="TextoComum" >
						Diplomado pela American Board of Oral Medicine e ex-presidente desta associa��o e da American Academy of Oral Medicine. Publicou mais de 300 artigos cient�ficos, cap�tulos em livros, e monografias. � autor de textos como "Cancer bucal", "Manifesta��es bucais da AIDS", e "Bases da Medicina Bucal". 
					</p>
					<p class="TextoComum" >
						Atual consultor  cient�fico do Conselho Odontol�gico da Associa��o Americana e porta-voz nacional para a ADA (American Dental Association). Entre muitas honras recebeu a "Medalha de Honra" da UCSF em 1981; o grande pr�mio "Norton Ross" pela ADA  em 1996, pela excel�ncia em pesquisa cl�nica; em 1998 recebeu t�tulo Honor�rio em "Doctor of Science Degree" da Universidade de McGill em Montreal, Canad�; Medalha "Margaret Hay Edwards" , em 2002,  da American Association for Cancer Education,  por suas destac�veis contribui��es na educa��o sobre o C�ncer e em 2005,  recebeu o t�tulo de Conferencista de Pesquisa para a Faculdade de Odontologia da Universidade da Calif�rnia, S�o Francisco. 
					</p>
					<p class="TextoComum" >
						Dr. SILVERMAN DE SOL � revisor e consultor editorial de diversas revistas cient�ficas e atua ativamente em pesquisas cl�nicas, no atendimento di�rio de pacientes e no ensino da Estomatologia.
					</p>
					<p class="TextoComum" >
						P�gina pessoal: <a href="http://cc.ucsf.edu/people/silverman_sol.html" target=_nova>http://cc.ucsf.edu/people/silverman_sol.html</a>
					</p>
					<b>Palestrante do Curso Internacional sobre C�ncer Bucal</b>
				</td>
			</tr>
		</table>
	<%
	case 11
	%>
		<p class="Titulo" align="center">Guillermo Machuca Portillo</p>
		<p class="TextoComum" align="center">(<%=strSubTitulo2%>)</p>
		<table class="TabelaProgramacao" width="100%">
			<tr valign="top">
				<td class="TextoComum">
					<b>Curriculum Vitae</b><br>
					<ul>
						<li>Licenciado em Medicina e Cirurgia. Faculdade de Medicina. Universidade de Sevilla. 1985.
						<li>Doutor em Medicina e Cirurgia. Faculdade de Medicina. Universidade de Sevilla. 1987.
						<li>Especialista em Estomatolog�a. Universidade de Sevilla. 1989.
						<li>Professor Titular. Docencia em Cl�nica Odontol�gica Integrada de Pacientes Especiais. Universidade de Sevilla.
						<li>Diretor de Estudos de Posgradua��o. Universidade de Sevilla.�Tratamento Odontol�gico de Pacientes Especiais�.
						<li>Acad�mico da �Real Academia de Medicina de Sevilla�, desde 1988.
						<li>Editor dos livros: �La atenci�n odontol�gica en pacientes m�dicamente comprometidos (1� e 2� Edi��es)� e �Bases farmacol�gicas de la terap�utica odontol�gica�.
						<li>Autor de 40 cap�tulos de livros odontol�gicos.
						<li>Autor de mais de 100 publica��es em revistas nacionais e internacionais, em Medicina Bucal (Estomatologia), Periodontia e Pacientes Especiais.
						<li>Sessenta cursos e confer�ncias ministrados.
					</ul>				</td>
				<td align="right">
					<img src="images/palestrantes/GMachuca.jpg" vspace="5" hspace="5" onclick="submeter(2,<%=pr3_wk%>,<%=id_wk%>,0);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="225" HEIGHT="300"><br>
				</td>
			</tr>
			<tr>
				<td class="TextoComum" colspan="2">
					<b>Palestrante do Pr�-Congresso</b>
				</td>
			</tr>
		</table>
	<%
	case 2
	%>
		<p class="Titulo" align="center">Abel Silveira Cardoso</p>
		<p class="TextoComum" align="center">(<%=strSubTitulo2%>)</p>
		<table class="TabelaProgramacao" width="100%">
			<tr valign="top">
				<td class="TextoComum">
					<b>Curriculum Vitae</b><br><br>
					<ul>
						<li>Professor Titular (aposentado) do Departamento de Patologia e Diagn�stico Oral - Universidade Federal do Rio de Janeiro - Rio de Janeiro - Brasil.
						<li>Master of Science in Dentistry em Diagn�stico e Medicina Oral pela Universidade de Indiana.
						<li>S�cio Fundador e Ex Presidente (duas gest�es) da Sociedade Brasileira de Estomatologia.
						<li>Fellow do International College of Dentists
					</ul>
				</td>
				<td align="right">
					<img src="images/palestrantes/AbelSilveiraCardoso.jpg" vspace="5" hspace="5" onclick="submeter(2,<%=pr3_wk%>,<%=id_wk%>,0);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="225" HEIGHT="300"><br>
				</td>
			</tr>
			<tr>
				<td class="TextoComum" colspan="2">
					&nbsp;<br><b>T�tulo do Curso</b>:<br><br>
					<br><b>DIAGN�STICO DIFERENCIAL E CONDUTA CL�NICA FACE A LES�ES BRANCO-AVERMELHADAS DE BOCA</b>
					<br>
					<br>
					TEMAS A SEREM ABORDADOS: Os in�meros quadros cl�nicos que se expressam por les�es brancas ou branco-avermelhadas em boca constituem um diagn�stico diferencial dos mais importantes em estomatologia. Entre estas les�es, encontramos exemplos de praticamente todos os cap�tulos da Patologia Bucal, inclusive algumas com potencial de transforma��o maligna. H� tamb�m diverg�ncias de opini�o quanto ao manejo cl�nico de algumas delas. Por esta raz�o, o objetivo deste curso � rever o diagn�stico diferencial destas les�es bem como a conduta cl�nica (incluindo o processo diagn�stico e a terap�utica) frente �s mesmas. �nfase ser� dada � discuss�o das leucoplasias e les�es leuco-eritropl�sicas face �s controv�rsias existentes.
				</td>
			</tr>
		</table>
	<%
	case 3
	%>
		<p class="Titulo" align="center">Jos� Humberto Damante</p>
		<p class="TextoComum" align="center">(<%=strSubTitulo2%>)</p>
		<table class="TabelaProgramacao" width="100%">
			<tr valign="top">
				<td class="TextoComum">
					<b>Curriculum Vitae</b><br><br>
					<ul>
						<li>Professor Titular (Livre Docente) do Departamento de Estomatologia da Faculdade de Odontologia de Baur� (SP) - Brasil.
						<li>Mestre e Doutor em Estomatologia.
						<li>Coordenador do Programa de P�s-Gradua��o em Estomatologia da Faculdade de Odontologia de Baur�.
						<li>Ex-Presidente da SOBE
					</ul>
				</td>
				<td align="right">
					<img src="images/palestrantes/Damante.jpg" vspace="5" hspace="5" onclick="submeter(2,<%=pr3_wk%>,<%=id_wk%>,0);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="225" HEIGHT="300"><br>
				</td>
			</tr>
			<tr>
				<td class="TextoComum" colspan="2">
					&nbsp;<br><b>T�tulo do Curso</b>:<br><br>
					<br><b>EXAMINANDO O PACIENTE E INTERPRETANDO IMAGENS : UM GRANDE PASSO PARA O DIAGN�STICO.</b>
					<br>
					<br>
					TEMAS A SEREM ABORDADOS: Em torno desse tema o autor pretende apresentar e discutir alguns casos cl�nicos, enriquecendo a exposi��o com dados cient�ficos sobre a doen�a ,ap�s estabelecido o diagn�stico final. �s vezes, o tratamento tamb�m ser� contemplado.
				</td>
			</tr>
		</table>
	<%
	case 4
	%>
		<p class="Titulo" align="center">Celso Massumoto</p>
		<p class="TextoComum" align="center">(<%=strSubTitulo2%>)</p>
		<table class="TabelaProgramacao" width="100%">
			<tr valign="top">
				<td class="TextoComum">
					<b>Curriculum Vitae</b><br><br>
					<ul>
						<li>Professor Doutor da Universidade de S�o Paulo.
						<li>Ex-Clinical fellow do Fred Hutchinson Cancer Center, Seattle, Washington
						<li>Membro do Centro de Oncologia do Hospital S�rio Liban�s.
					</ul>
				</td>
				<td align="right">
					<img src="images/palestrantes/imagemNaoDisponivel.jpg" vspace="5" hspace="5" onclick="submeter(2,<%=pr3_wk%>,<%=id_wk%>,0);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="225" HEIGHT="300"><br>
				</td>
			</tr>
			<tr>
				<td class="TextoComum" colspan="2">
					&nbsp;<br><b>T�tulo do Curso</b>:<br><br>
					<br><b>ASPECTOS QUIMIOTER�PICOS DE INTERESSE ODONTOL�GICO. CUIDADOS ORAIS NOS PACIENTES ONCOL�GICOS.</b>
					<br>
					<br>
					TEMAS A SEREM ABORDADOS: Ser�o abordados os aspectos quimioter�picos,principalmente mecanismo de a��o e preven��o de toxicidades dos agentes oncol�gicos. Ser� dada �nfase aos aspectos da mucosite secund�ria a quimio/radioterapia e � experi�ncia com novos agentes que reduzem a mucosite em pacientes oncol�gicos.
				</td>
			</tr>
		</table>
	<%
end select
%>