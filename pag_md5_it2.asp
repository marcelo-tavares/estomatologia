<% 
strValores_wk = "Valores de Ades�o"
strCategoria_wk = "CATEGORIA"
strPC_wk = "PR�-CONGRESSO"
strC_wk = "CONGRESSO<br>� Vista Antecipado<br>(at� 15/03/2004)"
strC1_wk = "CONGRESSO<br><br>at� 15/03/2004"
strSOBE_wk = "S�cio da SOBE<br><small>(anuidade prevista para 2005: R$130,00)</small>"
strNaoSocio_wk = "N�o-s�cio e outros profissionais da �rea da sa�de"
strAcadPG_wk = "Aluno de P�s-gradua��o (especializa��o, mestrado e doutorado)"
strAcadG_wk = "Acad�mico (gradua��o)"
strAcompanhante_wk = "Acompanhante"
strObs_wk = "A partir de 01/01/2003, os valores acima sofrer�o acr�scimo de 10%"
%>


<p class="Titulo" align="center"><%=strValores_wk%></p>

<p class="TituloNegro" align="left"><b>CONGRESSO</b></p>

<table width=100% class=TabelaProgramacao>
	<tr class=CelulaTitulo>
		<td><b>CATEGORIA</b></td>
		<td align=center><b>A Vista Antecipado<br>at� 15/03/2005</b></td>
		<td align=center><b>Parcelado em at� 3 vezes ou<br>A vista at� 08/07/2005</b></td>
		<td align=center><b>A Vista no evento</b></td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td>
			S�cio da SOBE <sup>*</sup><br>
			S�cio da ABRO<br>
			S�cio da SBOG<br>
			S�cio do Col�gio Brasileiro de Cirurgia Bucomaxilofacial
		</td>
		<td align=center>R$ 400.00</td>
		<td align=center>R$ 420.00</td>
		<td align=center>R$ 450.00</td>
	</tr>
	<tr class=CelulaCorpo>
		<td>N�o-s�cio e outros profissionais da �rea da sa�de</td>
		<td align=center>R$ 550.00</td>
		<td align=center>R$ 570.00</td>
		<td align=center>R$ 600.00</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td>Aluno de P�s-gradua��o<br>(especializa��o, mestrado e doutorado)</td>
		<td align=center>R$ 300.00</td>
		<td align=center>R$ 330.00</td>
		<td align=center>R$ 350.00</td>
	</tr>
	<tr class=CelulaCorpo>
		<td>Acad�mico<br>(gradua��o)</td>
		<td align=center>R$ 100.00</td>
		<td align=center>R$ 120.00</td>
		<td align=center>R$ 120.00</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td colspan=4></td>
	</tr>
	<% if false then %>
	<tr class=CelulaCorpoCinza>
		<td colspan=2><%=strAcompanhante_wk%></td>
		<td align=center>US$ 49.50</td>
	</tr>
	<% end if %>
</table>

<p class=TextoComum>
	<small>* anuidade da SOBE prevista para 2005: R$130,00</small><br>
</p>

<br>

<p class="TituloNegro" align="left"><b>PR�-CONGRESSO</b></p>


<table width=100% class=TabelaProgramacao>
	<tr class=CelulaTitulo>
		<td><b>CATEGORIA</b></td>
		<td align=center><b>Valor</b></td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td>N�o Congressista</td>
		<td align=center>R$ 80.00</td>
	</tr>
	<tr class=CelulaCorpo>
		<td>N�o Congressista S�cio da Sociedade Brasileira de Odontogeriatria</td>
		<td align=center>R$ 50.00</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td>Congressista</td>
		<td align=center>R$ 50.00</td>
	</tr>
	<tr class=CelulaCorpo>
		<td>Aluno de P�s-Gradua��o</td>
		<td align=center>R$ 40.00</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td>Acad�mico de Gradua��o</td>
		<td align=center>R$ 20.00</td>
	</tr>
</table>