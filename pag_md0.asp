<%
if id_wk = 1 then
	strApoio = "Apoio:<br>"
	strvarig = "Transportadora Oficial:"
	strAmplestur = "Ag�ncia Oficial de Turismo:"
	strAssociar = "Como se Associar?"
	strProgramacao = "Conhe�a a Programa��o Preliminar do Congresso"
	strProgramaTit = "PROGRAMA��O"
	strBemVindos = "Bem Vindos ao XIII Congresso Brasileiro de Estomatologia"
	strPalestrantes = "PALESTRANTES"
	strPalestrantesTxt = "Conhe�a os palestrantes que confirmaram presen�a"
	strMais = "Mais"
	strChamadaTit = "CHAMADA DE TRABALHOS"
	strChamadaTxt = "Inscreva seu trabalho e compartilhe sua experi�ncia cient�fica! "
	strChamadaTxt1 = "Prazo de inscricao<br>de trabalhos �<br>prorrogado para<br>28 de fevereiro de 2003."
	strSaiba = "Saiba como"
	strInscricoes = "INSCRI��ES"
	strInscricoesTxt1 = ""
	strInscricoesTxt1 = "Fa�a a sua inscri��o utilizando Boleto Banc�rio ou Cheque Nominal"
	strInscricoesTxt2 = "<br><br><b>Valor das Inscri��es</b><br>"
	strInscricoesTxt3 = "Em 01/01/2003 os valores das inscri��es sofrer�o acr�scimo de 10%. Antecipe sua inscri��o!"	
	strInscricoesTxt4 = "Inscreva-se agora!"
	strFloripaTxt = "Conhe�a a cidade que vai receb�-lo para o Congresso da SOBE"
	strMSIE = "Desenvolvido para <br>Internet Explorer 5.0 ou superior<br>&nbsp"
	strDD = "Web Design & Desenvolvimento"
	strFimInscricoesOnLine = "Data limite para pagamento em 2 parcelas: 26/05."
end if

select case it_wk
	case 1
		%>
		<table width="100%">
			<tr>
				<td width="100%">
					<table border="0" width="100%" cellSpacing="5" cellPadding="10">
						<% if true then %>
						<tr class="textoComum" align="center">
							<td valign="middle" colspan="3" class="TituloGeral">
								<%=strBemVindos%>
							</td>
						</tr>
						<% end if %>
						<tr class="textoComum">
							<td valign="top" width="230" align="left" rowspan="2">
								<table width="100%">
									<tr>
										<td class="CelulaTitulo1">
											:: CHEGANDO EM FLORIPA
										</td>
									</tr>
									<tr>
										<td class="textoComum">
											<b>Deslocamento</b>
											<br>
											<a href="javascript:submeter(4,5,<%=id_wk%>,0);">Acesse</a> as orienta��es sobre o transporte coletivo de Florian�polis e sobre a contrata��o de servi�os de deslocamento para as atividades cient�ficas e sociais.
											<br><br>
											<b>Hospedagem, Viagens e  Passeios</b>
											<br>
											Veja <a href="javascript:submeter(4,3,<%=id_wk%>,0);">aqui</a> onde obter informa��es sobre como chegar, onde se hospedar e sobre passeios em Floripa e Santa Catarina, al�m do <b>mapa</b> geral da �rea central de Floripa com a localiza��o dos principais hot�is.
										</td>
									</tr>
								</table>
							</td>
							<td valign="top" width="190" align="left" rowspan="2">
								<table width="100%">
									<tr>
										<td class="CelulaTitulo1">
											:: <%=strInscricoes%>
										</td>
									</tr>
									<tr>
										<td class="textoComum">
										<% if prazoParticipanteEncerrado = false then %>
											<b>Aten��o: �ltimo dia para inscri��es on-line: 08/07/2005</b>
											<br><br>
											<%=strInscricoesTxt1%>
											<br>
											<a href="javascript:submeter(5,4,<%=id_wk%>,0);"><%=strInscricoesTxt4%></a>
										<% else %>
											<b>Aten��o: Inscri��es on-line encerradas. Inscreva-se a partir de agora na secretaria do evento.</b>
										<% end if %>
										</td>
									</tr>
								</table>
								<br>
								<table width="100%">
									<tr>
										<td class="CelulaTitulo1">
											:: TRABALHOS
										</td>
									</tr>
									<tr>
										<td class="textoComum">
											<b>Cronograma de Apresenta��o</b>
											<br>
											<a href="javascript:submeter(7,6,<%=id_wk%>,0);">Confira</a> o cronograma de apresenta��o dos trabalhos aprovados.
											<br><br>
											<b>Lista de Trabalhos Aprovados</b>
											<br>
											Comiss�o Cient�fica libera a lista de trabalhos aprovados. <a href="javascript:submeter(7,4,<%=id_wk%>,0);">Acesse aqui.</a>
										</td>
									</tr>
								</table>
							</td>
							<td valign="top" width="160" height="60" align="center">
								<table width="100%">
									<tr>
										<td class="CelulaTitulo1">
											:: PR�-CONGRESSO
										</td>
									</tr>
									<tr>
										<td class="textoComum">
											O Curso do pr�-congresso, com o Prof. Guillermo Machuca Portillo (Sevilla, Espanha) teve sua programa��o ampliada (7 horas).<br><a href="javascript:submeter(2,8,1,2)">Veja aqui os temas a serem apresentados.</a> 
											<br><br>
											
										</td>
									</tr>
								</table>
								<br><br>
								<table width="100%">
									<tr>
										<td class="CelulaTitulo1">
											:: FLORIAN�POLIS
										</td>
									</tr>
									<tr>
										<td class="textoComum">
											<a href="javascript:submeter(4,2,<%=id_wk%>,0);">Conhe�a a cidade</a> que vai receb�-lo para o Congresso da SOBE
											<br>
										</td>
									</tr>
								</table>
								<!--
								<center>&nbsp;<br>
									<a href="javascript:submeter(4,2,<%=id_wk%>,0);">
										<img src="images/imagensChamadaFloripa2.jpg" border="0" WIDTH="116" HEIGHT="80">
									</a>
								</center>
								-->
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table border="0" width="100%" cellSpacing="3" cellPadding="0">
						<tr class="textoComum" align="center">
							<td valign="top" colspan="2">
								<hr>
							</td>
						</tr>
						<tr class="textoComum" align="center">
							<td valign="top" width="100%" colspan="2" align="center">
								<table width="100%">
									<tr valign="middle">
										<td class="textoComum" align="center" width="30%">
											<a href="http://www.estomatologia.com.br" target="_nova">
												<img name="SOBE" border="0" src="images/logoSOBE100x33.jpg" alt="SOBE" WIDTH="100" HEIGHT="33">
												<br>Sociedade<br>Brasileira de<br>Estomatologia
											</a>
										</td>
										<td class="textoComum" align="center" width="30%">
											<a href="http://www.sbog.org.br" target="_nova">
												<img name="SOBE" border="0" src="images/logo_sbog.jpg" ALT="SBOG" WIDTH="197" HEIGHT="86">
											</a>
										</td>
										<td class="textoComum" align="center" width="30%">
											<strong>
												<img src="images/ufsc_logo.gif" border="0" WIDTH="100" HEIGHT="74">
												<br>
												<big>UFSC</big>
												<br><br>
												<a href="http://www.ccs.ufsc.br/patologia" target="_topo">
													Departamento de Patologia
												</a>
												<br><br>
												<a href="javascript:submeter(0,2,<%=id_wk%>,0);">
													Ambulat�rio de Estomatologia<br>Hospital Universit�rio
												</a>
											</strong>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr class="textoComum" align="center">
							<td valign="top" colspan="2">
								<hr>
							</td>
						</tr>
						<tr class="textoComum" align="center">
							<td valign="top" width="50%" colspan="2" align="center">
									<h5>ORGANIZA��O:</h5>
									<a href="http://www.acorianaeventos.com.br" target="_nova">
										<img src="images/acorianaeventos.jpg" vspace="5" border="0" WIDTH="241" HEIGHT="80">
									</a>
									<hr>	
							</td>
						</tr>
						<tr class="textoComum" align="center">
							<td valign="top" width="50%" colspan="2" align="center">
									<h5><%=strAmplestur%></h5>
									<a href="http://www.amplestur.com.br" target="_nova">
										<img src="images/amplestur.jpg" vspace="5" border="0" WIDTH="200" HEIGHT="24">
									</a>	
							</td>
						</tr>
					</table>
					&nbsp;
					<table border="0" width="100%" cellSpacing="3" cellPadding="0">
						<tr class="textoComum" align="center">
							<td valign="top" colspan="4">
								<hr>
							</td>
						</tr>
				<% if false then %>
						<tr class="textoComum" align="center">
							<td valign="middle" colspan="4">
								<h5><%=strApoio%></h5>
							</td>
						</tr>
						<tr class="textoComum" align="center">
							<td valign="top">
								<strong>
									<a href="http://www.ufsc.br/" target="_topo">
									<img src="images/ufsc_logo.gif" border="0" WIDTH="100" HEIGHT="74"></a>
									<br>
									<big>UFSC</big><br>Depto de Patologia</big>
								</strong>
							</td>
							<td valign="top">
								<strong>
									<a href="http://www.santur.sc.gov.br/" target="_topo">
										<img src="images/santur.gif" border="0" WIDTH="100" HEIGHT="100">
									</a>
								</strong>
							</td>
							<td valign="top">
								<strong>
									<big>SETUR</big>
									</b>
									<br>Secretaria
									<br>Municipal de
									<br>Turismo,
									<br>Cultura e
									<br>Esportes
								</strong>
							</td>
							<td valign="top">
								<strong>
									<img src="images/fcvb.jpg" WIDTH="100" HEIGHT="44">
									<br>Florian�polis
									<br>Convention
									<br>&amp; Visitors
									<br>Bureau
								</strong>
							</td>
						</tr>
				<% end if %>
						<tr class="textoComum" align="center">
							<td valign="top" colspan="4">
								<br>
								<%=strDD%><br><a href="mailto:marcelo@tavares.eti.br">Marcelo Tavares</a>
								<br>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<%
		if false then
		%>
			<table width="100%">
				<tr>
					<td align="middle" valign="top">
						<h5>
						<% 
						if id_wk = 1 then
						%>
							Fa�a sua inscri��o<br>com seu Cart�o VISA!
						<%
						else
						%>
							Now register using your VISA Credit Card !!!
						<%
						end if
						%>
						</h5>
						<br>
						<img src="images/icdmfr_ponte1.gif" vspace="25" WIDTH="400" HEIGHT="246">
					</td>
					<td width="100">
						<table cellSpacing="3" cellPadding="0" class="tabelaProgramacao">
						  <tr class="textoComum">
						    <td width="100" align="middle">
								<strong>
									<%=strvarig%>
									<a href="javascript:submeter(0,2,<%=id_wk%>,0);">
										<img src="images/varig_logo.gif" vspace="2" border="0" WIDTH="100" HEIGHT="66">
									</a>
									<hr>
									<%=strAplestur%>
									<a href="www.amplestur.com.br" target="_nova">
										<img src="images/amplestur.jpg" vspace="5" border="0" WIDTH="200" HEIGHT="24">
									<\a>	
									<hr>
									<%=strApoio%>
									<a href="http://www.ufsc.br/" target="_topo">
										<img src="images/ufsc_logo.gif" border="0" WIDTH="100" HEIGHT="74">
									</a>
									<big>UFSC</big>
									<br>
									<a href="http://www.santur.sc.gov.br/" target="_topo">
										<img src="images/santur.gif" border="0" WIDTH="100" HEIGHT="100">
									</a>
									<br>
									<b>
										<big>SETUR</big>
									</b>
									<br>Secretaria
									<br>Municipal de
									<br>Turismo,
									<br>Cultura e
									<br>Esportes
									<br><br>
									<img src="images/fcvb.jpg" WIDTH="100" HEIGHT="44">
									<br>Florian�polis
									<br>Convention
									<br>&amp; Visitors
									<br>Bureau
								</strong>
							</td>
						  </tr>
						</table>
					</td>
				</tr>
			</table>
			<%
		end if 
	case 2 
	%>
	<table width="100%">
		<tr class="textoComum">
			<td height="320" align="left" class="textoComum" valign="top">
				<p class="Titulo" align="center">
				<img SRC="images/ambEstomHU.jpg" BORDER="0" hspace="5" align="left" WIDTH="123" HEIGHT="123">Ambulat�rio de Estomatologia do Hospital Universit�rio - UFSC
				</p>
				<p>
					O ambulat�rio de Estomatologia do Hospital Universit�rio da UFSC realiza atividades voltadas para o diagn�stico, tratamento e preven��o das doen�as da boca, com especial �nfase �s altera��es em tecidos moles. Tamb�m se encarrega da realiza��o de bi�psias com finalidade de diagn�stico, da excis�o cir�rgica de les�es benignas dos tecidos moles da cavidade bucal e do tratamento cl�nico de in�meras altera��es estomatol�gicas. Juntamente com a equipe multidisciplinar oncol�gica oferece suporte ao paciente com C�ncer de Boca, auxiliando no diagn�stico de les�es canceriz�veis, realizando o preparo de boca e a adequa��o do meio bucal de pacientes que ser�o submetidos oa tratamento antineopl�sico, bem como o diagn�stico e tratamento das manifesta��es estomatol�gicas decorrentes da terapia do c�ncer.
				</p>
				<p>
					A equipe de atendimento � composta por professores de Patologia Geral, Patologia Bucal e Estomatologia, contando com a participa��o de residentes do curso de Cirurgia e Traumatologia Bucomaxilofacial, cirurgi�es-dentistas volunt�rios e acad�micos do curso de Odontologia. 
				</p>
				<p>
					O servi�o � desenvolvido no Ambulat�rio da �rea C do HU/UFSC, �s ter�as-feiras, a partir das 16:00hs. O telefone de contato �: (48) 331-9137.
				</p>
			</td>
		</tr>
	</table>
	<%
	case 3
	%>
		<!--#include file="modoHomologacao.asp"-->
	<%		
	case 4
		Response.Redirect ("adm/admin.asp")
	case 5
		Response.Redirect ("avaliacao/default.asp")
	case 6
		Response.Redirect ("banca/default.asp")
end select 
%>