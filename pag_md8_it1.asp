<% if id_wk = 1 then %>
	<p class="Titulo" align="center">PR�MIO IADMFR DE PESQUISA EM IMAGIOLOGIA MAXILOFACIAL</p>
	<p class="textoComum">
		O 2� IADMFR Maxillofacial Imaging Research Award (2o Pr�mio de Pesquisa da IADMFR) ser� outorgado em Florian�polis durante o 14th ICDMFR, desde que inscri��es compat�veis sejam recebidas. O prop�sito do pr�mio � encorajar a pesquisa em Radiologia Maxilo-facial e estimular o interesse na IADMFR. Pr�mios de U$7.500 e U$2.500, respectivamente, ser�o outorgados aos dois melhores trabalhos cient�ficos inscritos. O candidato necessita ser membro da IADMFR e estudante de P�s-Gradua��o ou ter concl��do sua P�s-Gradua��o nos �ltimos 5 anos em um programa oficial de Radiologia Maxilo-facial. Os candidatos precisam estar preparados para apresentar seus trabalhos no 14th ICDMFR.
	</p>
	<p class="textoComum">
		Informa��es adicionais podem ser obtidas no site da IADMFR (<a href="http://www.iadmfr.org" target="_topo">www.iadmfr.org</a>), com o Secret�rio Geral da IADMFR, Dr. Gerard Sanderink (<a href="mailto:G.Sanderink@acta.nl">G.Sanderink@acta.nl</A>), ou com o editor da revista Dentomaxillofacial Radiology, Dr. Douglas Benn (<a href="mailto:benn@dental.ufl.edu">benn@dental.ufl.edu</a>).
	</p>
	<p class="textoComum">
		<b>Obs.:</b> O fato de inscrever seu trabalho ao Pr�mio de Pesquisa da IADMFR (prazo para inscri��es: 01/01/2003) n�o dispensa a inscri��o do trabalho no 14th ICDMFR.
	</p>
<% else %>
	<p class="Titulo" align="center">IADMFR MAXILLOFACIAL IMAGING RESEARCH AWARD </p>
	<p class="textoComum">
		The 2nd IADMFR Maxillofacial Imaging Research Award will be granted in Florian�polis during the 14th ICDMFR, subject to suitable applications being received. The purpose of the award is to encourage research in maxillofacial imaging and to stimulate interest in the IADMFR. The prizes of $7,500 and $2,500, respectively, will be granted for the two best research papers submitted. The applicant must be a member of the IADMFR and a postgraduate student or junior faculty in a recognized maxillofacial radiology program within 5 years of graduation. Applicants must be prepared to present their papers at the 14th ICDMFR.
	</p>
	<p class="textoComum">
		Further information can be obtained on the IADMFR website (<a href="http://www.iadmfr.org" target="_topo">www.iadmfr.org</a>), with the Secretary General of IADMFR, Dr. Gerard Sanderink (<a href="mailto:G.Sanderink@acta.nl">G.Sanderink@acta.nl</A>), or with the editor of Dentomaxillofacial Radiology Journal, Dr. Douglas Benn (<a href="mailto:benn@dental.ufl.edu">benn@dental.ufl.edu</a>).
	</p>
	<p class="textoComum">
		<b>P.S.:</b> Submitting the research paper to the IADMFR award (deadline: January 1, 2003) does not exempt the applicant from submitting the abstract to the 14th ICDMFR (deadline: January 15, 2003).
	</p>
<% end if %>

 
