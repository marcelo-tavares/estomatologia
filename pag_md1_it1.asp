<p class="Titulo" align="center">Mensagem do Magn�fico Reitor da UFSC<br>Presidente de Honra do COngresso</p>
<p class=textoComum>
	<img SRC="images/Foto_Lucio.jpg" align="right" hspace="5" WIDTH="173" HEIGHT="200">A Estomatologia est�, sem d�vida nenhuma, entre as �reas que mais avan�aram nos �ltimos 50 anos. O car�ter multidisciplinar adquirido, seja por sua articula��o com outros espa�os do saber, como por exemplo, a descoberta e uso de novos materiais.
</p>
<p class=textoComum>
	Associado a isso, uma gama enorme de novos m�todos diagn�sticos, com maior precocidade e, principalmente, com os conhecimentos sobre causalidade, seja geral ou espec�fica, impulsionaram para a preven��o, a cura e a reabilita��o de maneira muito veloz.
</p>
<p class=textoComum>
	Mais do que est�tica, a Estomatologia ganhou contornos �ticos e se aproxima cada vez mais da popula��o que h� poucos anos s� conhecia o profissional para extra��o e pr�tese.
</p>
<p class=textoComum>
	Portanto, a n�s, Universidade e Profissionais, cabe este compromisso constante com acompanhar e desenvolver, atualizar e predizer t�cnicas e m�todos, e isto faz sentir a import�ncia do XIII Congresso Brasileiro de Estomatologia, XXXI Jornada Brasileira de Estomatologia e III F�rum de Discuss�o em Diagn�stico Bucal da Universidade Federal de Santa Catarina.
</p>
<p class="textoComum">
<b>Prof. Lucio Jos� Botelho</b><br>
Reitor da UFSC
</p>
