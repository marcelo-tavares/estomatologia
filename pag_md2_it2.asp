<input type="hidden" name="pr3" value="2">
<script>
function mostraAtividade(atividade){
	for (i=1; i<=5; i++) {
		document.getElementById('atividade'+i).style.display = 'none';
		if (atividade == i){
			document.getElementById('atividade'+i).style.display = 'inline';
		}
	}
}

</script>
<p class="Titulo" align="center">Informa��es sobre as atividades</p>
<table width="100%" border="0">
	<tr>
		<td width="70%">
			<span id="atividade1" style="display:'inline'">
				<p class="TextoComum">
				<b>&#149; CURSOS</b><br><br>
				CURSOS INTERNACIONAIS:<br><br>
				<b>Enfermidades sist�micas cr�nicas mais freq�entes nos idosos</b>: repercuss�es na cavidade bucal e implica��es no procedimento odontol�gico<br>
<b><a href="javascript:submeter(2,4,<%=id_wk%>,11);">Prof. Guillermo Machuca Portillo</a></b><br>Universidade de Sevilla, Espanha<br>Pr�-Congresso (7 horas)<br><br>
				<b>C�ncer Bucal</b><br>
				<b><a href="javascript:submeter(2,4,<%=id_wk%>,1);">Prof. Sol Silverman, Jr.</a></b><br>Universidade da California, San Francisco, EUA<br>(8 horas)<br><br>
				<br>CURSOS NACIONAIS:<br> <br>
				<b>Examinando o paciente e interpretando imagens : um grande passo para o diagn�stico<br>
				<a href="javascript:submeter(2,4,<%=id_wk%>,3);">Prof. Jos� Humberto Damante</a></b><br><br> 
				<b>Aspectos quimioter�picos de interesse odontol�gico. Cuidados orais nos pacientes Oncol�gicos.<br>
				<a href="javascript:submeter(2,4,<%=id_wk%>,4);">Prof. Celso Massumoto</a></b><br><br> 
				<b>Diagn�stico diferencial e conduta cl�nica face a les�es branco-avermelhadas de boca</b><br>
				<b><a href="javascript:submeter(2,4,<%=id_wk%>,2);">Prof. Abel Silveira Cardoso</a></b><br><br><br>
				</p>
			</span>

			<span id="atividade2" style="display:'none'">
				<p class="TextoComum">
				<b>&#149; SEMIN�RIO DE HISTOPATOLOGIA</b><br><br>
				O semin�rio de histopatologia tem como objetivo a discuss�o de casos interessantes, raros ou de dif�cil diagn�stico histopatol�gico, sendo o p�blico-alvo principalmente patologistas e alunos de p�s-gradua��o que atuam na �rea de diagn�stico bucal, assim como outros profissionais que tenham interesse nessa �rea.
				</p>
				<p class="TextoComum">
				As l�minas dos casos ficar�o dispon�veis durante todo o evento em uma sala reservada para tal atividade. Os congressistas interessados em assistir ao semin�rio dever�o se inscrever na secretaria do evento sem custos adicionais.
				</p>
				<p class="TextoComum">
				A din�mica do Semin�rio ser� a seguinte, para cada caso em estudo:<br><br>
				&nbsp;&nbsp;&nbsp;a) Apresenta��o, pelo relator;<br>
				&nbsp;&nbsp;&nbsp;b) Discuss�o, pelo plen�rio;<br>
				&nbsp;&nbsp;&nbsp;c) Conclus�o e considera��es finais, pelas professoras organizadoras.<br><br>
				</p>
				<p class="TextoComum">
				ORGANIZA��O: Adriana Etges, Ana Paula Neutzling Gomes, Lenita Maria Aver de Ara�jo e Sandra Beatriz Chaves Tarqu�nio, do Centro de Diagn�stico das Doen�as da Boca da Faculdade de Odontologia da Universidade Federal de Pelotas/RS, com a colabora��o de v�rios Centros de Refer�ncias do Brasil.
				</p>
				<p class="TextoComum">
				COORDENA��O: Elena Riet Correa Rivero (UFSC) e Christine Kalvelage Philippi (UNIVALI)
				</p>
				<p class="TextoComum">
				PATROC�NIO:<br><a href="http://www.zeiss.de/micro" target="_nova"><img src="images/zeiss_logo.jpg" border="0" align="center" WIDTH="200" HEIGHT="200"></a>
				</p>
			</span>

			<span id="atividade3" style="display:'none'">
				<p class="TextoComum">
					<b>&#149; CASOS CL�NICOS CONSULTIVOS</b>
				</p>
				<p class="TextoComum">
				Esta atividade tem por finalidade a discuss�o de casos cl�nicos de dif�cil diagn�stico ou tratamento. O apresentador ter� a oportunidade de consultar profissionais experientes, al�m de expor d�vidas e trocar conhecimentos e informa��es com os demais congressistas. O objetivo � auxiliar na elucida��o ou andamento do caso apresentado. A sala para esta atividade estar� equipada com 01 (um) projetor multim�dia (datashow).
				</p>
				<p class="TextoComum">
					<a href="javascript:submeter(7,1,1,0)">Conhe�a as Normas para Inscri��o de Trabalhos</a>
				</p>
			</span>

			<span id="atividade4" style="display:'none'">
				<p class="TextoComum">
					<b>&#149; APRESENTA��O ORAL DE CASOS CL�NICOS</b>
				</p>
				<p class="TextoComum">
					A apresenta��o oral de casos cl�nicos � uma oportunidade para os congressistas compartilharem sua experi�ncia cl�nica no diagn�stico e tratamento de doen�as da boca. Uma banca composta por profissionais de reconhecida compet�ncia ser� respons�vel pela discuss�o do caso, bem como pela avalia��o do mesmo. A sala para esta atividade estar� equipada com 01 (um) projetor multim�dia (datashow).
				</p>
				<p class="TextoComum">
					<a href="javascript:submeter(7,1,1,0)">Conhe�a as Normas para Inscri��o de Trabalhos</a>
				</p>
			</span>

			<span id="atividade5" style="display:'none'">
				<p class="TextoComum">
				<b>&#149; APRESENTA��O DE PAIN�IS</b>
				</p>
				<p class="TextoComum">
					<b>Pain�is de Pesquisa</b><br>
					Apresenta��o de trabalhos de pesquisa originais, demonstrando metodologia
					cient�fica e incluindo an�lise estat�stica apropriada.
				</p>
				<p class="TextoComum">
					<b>Pain�is Cl�nicos</b><br>
					Apresenta��o de casos cl�nicos completos (com diagn�stico final).
				</p>
				<p class="TextoComum">
					<a href="javascript:submeter(7,1,1,0)">Conhe�a as Normas para Inscri��o de Trabalhos</a>
				</p>
			</span>
		</td>
		<td width="5%">
			&nbsp;
		</td>
		<td class="textoComum" width="25%" valign="top">
			<table width="100%">
				<tr class="TextoComum">
					<td bgcolor="#ffffff">
						<small>
							&nbsp;<br>
							&#149; <a href="javascript:mostraAtividade(1);">CURSOS</a><hr noshade color="#7A64A5">
							&#149; <a href="javascript:mostraAtividade(2);">Semin�rio de</a><br>&nbsp;&nbsp;<a href="javascript:mostraAtividade(2);">Histopatologia</a><hr noshade color="#7A64A5">
							&#149; <a href="javascript:mostraAtividade(3);">Casos Cl�nicos</a><br>&nbsp;&nbsp;<a href="javascript:mostraAtividade(3);">Consultivos</a><hr noshade color="#7A64A5">
							&#149; <a href="javascript:mostraAtividade(4);">Apresenta��o Oral</a><br>&nbsp;&nbsp;<a href="javascript:mostraAtividade(4);">de Casos Cl�nicos</a><hr noshade color="#7A64A5">
							&#149; <a href="javascript:mostraAtividade(5);">Pain�is</a><hr noshade color="#7A64A5">
						</small>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

