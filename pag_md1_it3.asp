<p class="Titulo" align="center">MENSAGEM DO PRESIDENTE DA SBOG</p>

<p class="TextoComum">
	<img SRC="images/foto_Oleinisk.jpg" align="right" hspace="5" WIDTH="173" HEIGHT="200">O envelhecimento da popula��o mundial constitui a transforma��o demogr�fica mais importante da sociedade atual. Estamos come�ando a viver a �era dos idosos�. Estima-se que no Brasil, no ano 2020, deveremos ter 32 milh�es de idosos, a 6� popula��o do mundo nesta faixa et�ria.
</p>
<p class="TextoComum">
O processo fisiol�gico de envelhecimento � um fator predisponente a enfermidades sist�micas cr�nico-degenerativas que, por sua vez, geram limita��es na capacidade funcional do indiv�duo, um aumento substancial do consumo de f�rmacos, e repercuss�es na cavidade bucal. Esta cascata de transtornos leva � necessidade de maiores conhecimentos cient�ficos sobre patologias mais freq�entes, varia��es f�sico-org�nicas, psicol�gicas e sociais da popula��o idosa, ou seja, avaliar a sa�de do indiv�duo como um todo, implicando em diferen�as no manejo e tratamento odontol�gico. Desta forma, os idosos requerem uma aten��o odontol�gica especial e interdisciplinar, que � a raz�o da ODONTOGERIATRIA, utilizando procedimentos cl�nicos e t�cnicas peculiares, objetivando a qualidade de vida deste coletivo.
</p>
<p class="TextoComum">
Vale destacar que este campo ainda est� pouco explorado e existe uma demanda que n�o pode mais esperar...
</p>
<p class="textoComum">
<b>Prof. Jos� Carlos Oleiniski</b><br>
Presidente da Sociedade Brasileira de Odontogeriatria
</p>
