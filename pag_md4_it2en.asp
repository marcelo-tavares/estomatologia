<p class="Titulo" align="center">FLORIAN�POLIS</p>

<script>
	function ativaDefinicao(elemento){
		elemento.style.cursor='hand';
		elemento.style.color='#FFD359';
		elemento.style.fontWeight = 700;
	}

	function ativaTabela(id){
		for  (i=1; i<=7; i++) {
			tabela='tab' + i
			document.getElementById(tabela).style.display = "none";
		}
		
		tabela='tab' + id
		document.getElementById(tabela).style.display = "inline";

		document.getElementById("hr1").style.display = "inline";
		document.getElementById("imgPonte").style.display = "none";
	}

	function desativaDefinicao(elemento){
		elemento.style.cursor='default'; 
		elemento.style.color='#000000';
		elemento.style.fontWeight = 700;
	}

	function desativaTabela(id){
		tabela='tab' + id
		document.getElementById(tabela).style.display = "none";
	}

	function abrirJanelaNews(URL,nomeJanela,props){
		window.open(URL,nomeJanela,props);
	}
		 
	function ampliarFoto(nm_foto, de_foto, cd_idioma){
		url = 'ampliaFoto.asp?nm_foto='+nm_foto+'&de_foto='+de_foto+'&cd_idioma='+cd_idioma
		id = 'Imagem';
		params = 'scrollbars=no,width=546,height=396';
		abrirJanelaNews(url,id,params);
	}
</script>

<table class="Visivel" border="0" cellpadding="5" cellspacing="0" WIDTH="100%">
	<tr>
		<td align="middle" class="textocomum">
		  <b>
			<span id="sp1" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(1);">FLORIAN�POLIS</span>&nbsp;&#149;&nbsp;

			<span id="sp2" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(2);">SANTA CATARINA ISLAND</span>&nbsp;&#149;&nbsp;

			<span id="sp3" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(3);">HISTORY</span><br><br>
			
			<span id="sp4" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(4);">BEACHES</span>&nbsp;&#149;&nbsp;

			<span id="sp5" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(5);">THE MAINLAND</span>&nbsp;&#149;&nbsp;

			<span id="sp6" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(6);">FORTS</span>&nbsp;&#149;&nbsp;

			<span id="sp7" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="ativaTabela(7);">CUISINE</span>
		  </b>
		</td>
	</tr>
</table>

<p align="center">
	<img src="images/PontesVistaAerea.jpg" id="imgPonte" WIDTH="408" HEIGHT="227">
</p>

<hr id="hr1" style="DISPLAY: none">

<table style="DISPLAY: none" border="0" id="tab1">
	<tr>
		<td class="textoComum">
			<p>
				<b>FLORIAN�POLIS IS SUN, SEA, TRADITION E HISTORY</b>
				<br>
			</p>
			<p>
				Its first inhabitants were the Tupi Guarani Indians, who gave it its first names: Miembipe and Jurer� Mirim. The first to land here was the Spanish explorer Cabeza de Vaca in 1541. The bandeirantes pioneers came a century later with Francisco Dias Velho, who founded the population of Nossa Senhora do Desterro. To maintain the settlement, from 1748 to 1756 the Portuguese government sent nearly 5 thousand settlers from the Azorean Islands and Madeira. 
			</p>
			<p>
				In 1894, in homage to Marechal Floriano Peixoto, the city received its current name - Florian�polis. It is now the muse of thousands of tourists attracted not only by history, but also by the natural attributes of Santa Catarina Island, where the largest portion of the city is located. With 370 inhabitants, Florian�polis is the only capital of a Brazilian state where the population is exceeded by a city in the interior, in this case Joinville, which has some 440 thousand inhabitants.
			</p>
			<p>
				When the subject is beaches, wealth and abundance are distinctions of Florian�polis. With 42 beaches, some of them are wild, others of uncommon beauty and prepared for sophisticated international tourism, some have warm and calm waters, others have rough and chilly waters.
			</p>
			<p>
				Among the best known are Jurer�, which is the most internationalized, preferred by Argentines and
			</p>
			<p>
				Uruguayans; Ribeir�o da Ilha, where the Azorean heritage is seen in the homes, churches and faces; Lagoa da Concei��o, which has one of the most revered views; Joaquina, the stage for surf in central southern Brazil; and the much commented Canasvieiras, notable for nautical sports and night life.
			</p>
			<p>
				<b>
					CAPITAL OF DIVERSITY
				</b>
				<br>
			</p>
			<p>
				The visitor who arrives at Florian�polis at any season of the year faces an intriguing problem: the excess of diversity. For this reason, a smart tourist should set some priorities. In addition to the plentiful supply of beaches, there is also history, with various forts that surround Santa Catarina Island. No one passes without noticing them. Some are restored, others are in ruins, they are unique spectacles which represent the most significant heritage of the Brazilian Navy on the entire Atlantic Coast.
			</p>
			<p>
				To defend Florian�polis from invasions, Portugal built various forts in strategic locations to control access to the village of Desterro from the north and south. From 1738 to 1761, five forts were built. That of S�o Jos� de Ponta Grossa was the first. The most imposing is Anhatomirim, in which US$ 1 million was invested for restoration.
			</p>
			<p>
				Various other monuments are incorporated to the history of Florian�polis. One of the principal ones is the Hercilio Luz Bridge. For decades it was considered the most important landmark of the city. It is now closed to traffic because its structure is damaged. With 819 meters in length, it was inaugurated in 1926, with two towers 75 meters high. It is a rare example of a suspension bridge.
			</p>
			<p>
				Another place that deserves a visit is the Metropolitan Cathedral, located at the most central portion of the city and in the same location where in 1675 Francisco Dias Velho built a chapel in homage of Nossa Senhora do Desterro. He was killed there in a pirate attack. It was later substituted by a small church that was transformed into the cathedral. A few meters away is the Cruz e Sousa Palace, built in the eighteenth century and initially used as a residence and workplace of the Provincial President, it was home to the governors of Santa Catarina until the first half of this century. Today it houses the Santa Catarina Historic Museum.
			</p>
			<p>
				The Alf�ndega or Customs House building is another fine example of Florian�polis history. It was built between 1875 and 1876, with neo-classical architecture, and is rich in details. Nearby Alf�ndega is the most traditional commercial center of the city - the Public Market, with 140 stands housing establishments that sell typical local products (fish, meat, seafood, bamboo furniture and clay dishes). Ten thousand people pass through the Market each day. The building was inaugurated in 1898. There are also various monuments of historic and architectural importance at the interior of the island, especially at Ribeir�o da Ilha and Lagoa da Concei��o.
			</p>
			<p>
				It is especially in the summer that the visitor to Florian�polis can see some of the most significant folkloric displays of the city, all with a strong Azorean and Spanish influence, such as the boi-de-mam�o dramatic dance.
			</p>
			<br><br>&nbsp;
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab2">
	<tr>
		<td class="textoComum">
			<p>
				<b>SANTA CATARINA ISLAND: A WORD TO BE DISCOVERED</b>
				<br>
			</p>
			<p>
				Among its 1,045 km2, Santa Catarina Island has a lot to be explored. By foot there are trails through dense forest, by horse, or boat one reaches isolated communities. Or one can tour through the sky, by hang-glider or &quot;para-glider&quot;. The never monotonous landscape reveals a diversity that ennobles the territory with the 42 most enchanting beaches of southern Brazil. There are plains, dunes, lagoons, mangrove swamps, mountains and native Atlantic Forest. Nearby there are 30 other small paradise islands.
			</p>
			<p>
				Florian�polis, which occupies all of Santa Catarina plus part of the mainland, still preserves traces of the eighteenth century colonizers. In the architecture, the fishing, the agriculture, the cooking and even in the manner of the people. It is a land designed for explorers with the most different interests. 
			</p>
			<p>
				The most wild trails are on the southern portion of the island. The three that lead to Saquinho beach, leaving from Solid�o beach, from Caieira da Barra do Sul or from Naufragados; are steep and demand stamina, but compensate the effort by revealing an immaculate portion of Atlantic forest - or for those who leave from Solid�o beach, a magnificent coast decorated by seagulls. Saquinho beach, which is shaped like a conch is inhabited by just a house or two.
			</p>
			<p>
				One of the most spectacular deserted beaches on the island, which the &quot;regulars&quot; make a point of keeping anonymous, is Lagoinha do Leste. Its waters have good waves for surfing. The sand is fine and soft. It is a bit more than a kilometer long and circled by a mountain. There is a lagoon close to the beach and rich vegetation. Campers mount their tents along the margin. Lagoinha do Leste can be reached from P�ntano do Sul, after a walk of a just over an hour, or by the trail that leaves from Arma��o Beach walking along what the locals call the &quot;cost�o &quot;, the jagged coast formed by large rocks. 
			</p>
			<p>
				Between Arma��o and Lagoinha there is the tiny beach of Matadeiro, only 200 meters long, which has better waves than the more crowded Joaquina, according to many surfers. But the native trails reveal not only beaches. A trip departing from just south of the center of Ribeir�o da Ilha, passing by Lagoa do Peri and ending at Arma��o beach, hides on one of the slopes the last working ox-driven mill of Florian�polis. Over a route sprinkled by half a dozen gates one reaches the Engenho do Chico [Chico's Mill], in a region curiously known as Sert�o do Ribeir�o. There are lots of streams, an abundance of peach trees and the pasture is so green it stings the eyes. In sum, the region is nothing like the true desert-like &quot;Sert�o&quot; region of the Brazilian northeast. There is an ox-driven flour mill, which according to the old-timers grinds during the months that do not have the letter &quot;r&quot; from May to June. And there is a still which makes 500 liters of the island's best cacha�a (sugar-cane whiskey) per week. 
			</p>
			<p>
				Following the same trail, the 8 km from the Engenho do Chico, [Chico's Mill] there is the Peri Lagoon Municipal Park, the largest fresh water reserve on Santa Catarina Island, 5 km2 in size. It is an excellent place for fishing and hiking. To walk on the old trails demands care and attention. Despite the effort, the sweat and pleasure of adventure is guaranteed.
			</p>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab3">
	<tr>
		<td class="textoComum">

			<p>
				<b>HUMAN OCCUPATION SANTA CATARINA ISLAND</b>
				<br>
			</p>
			<p>
				Evidence of human presence on Santa Catarina Island can be considered recent when compared to ancient Amerindian Civilization which has remains dating back some 30 thousand years. As along the entire coast of Santa Catarina state, the oldest human vestiges found and cataloged on the island date back 5 thousand years of occupation always directly linked to the sambaqui culture.
			</p>
			<p>
				Also known as casqueiro, concheiro and berbigueiro, or by other names, this is an archeological site that in its Guarini origin means a mountain of shells. For hundreds or thousands of years the primitive inhabitants who were naturally dependent on the collection of sea food, would accumulate in appropriate locations the remains and shells of mollusks. These mounds grew so much with each generation that they came to be a locationte suitable for residences. In addition to being near the sea, the sambaquis were dry and safe places.
			</p>
			<p>
				By 1989, 120 sambaquis had been registered on Santa Catarina island and archeological research conducted on 20% of the sites. Many of the shell mounds were destroyed by later occupation, often transformed into raw material for making lime. It is suspected that in the highest portions of the sambaquis another new human group was present, the Itarar�s, who had habits different from the first residents of the sambaquis. The ceramic remains indicate that they probably had an agricultural culture. With this second group that occupied the island, there was a noticeable decrease in the consumption of mollusks. It is also believed that these peoples did not have contact with each other, inhabiting the island at different moments, one after the other.
			</p>
			<p>
				The third indigenous group that migrated to the island in the fourteenth century were the Carij�s, from the traditional Tupi-Guarani linguistic family from Brazil's southern coast. They settled approximately two centuries before the arrival of the first Europeans. Their villages varied from 30 to 80 residences, which left a larger and denser occupation of Santa Catarina Island. The first contacts with the Carij�s and foreigners occurred around 1556 when expeditions left on the island people who were shipwrecked or deserters. From the beginning, this co-existence was peaceful. On one hand, the friendly Carij�s supplied the Spanish visitors foods and sound information about the routes that led to rivers such as the Igua�� and Itapoc�, to reach Paraguay. On the other hand, the strangers regaled the Indians with many presents. Nevertheless, this coexistence lasted less than one century because it became damaging to the Carij�s culture, which migrated to the interior of the mainland.
			</p>
			<p>
				It is believed that by 1600 there were no more tribes on Santa Catarina Island. The flight of the Carij�s from the place where they lived, and called Meiembipe, which means &quot;mountain along the canal&quot; in Tupi, did not imply a break with the Portuguese, but simply a defensive action. The natives came to feel vulnerable to the degree in which the needs of the increasing number of visitors required greater food production which deeply changed the Carij�s' habits, believes and even their physical liberty. By the middle of the eighteenth century, although the Tordesilhas Treaty of 1494 legally guaranteed the possession of Santa Catarina Island to the Portuguese crown, Portugal had not taken any effective measure towards settlement. Only Spanish activity was noticed. After a few unstable occupations and various disputes over land possession, it would be left up to Francisco Dias Velho, a noble from S�o Vicente, to establish himself on the island. His settlement dates from about 1675, given that three years later Dias Velho requested from the governor of the Paulista captaincy, two square leagues on the island, explaining that a church had been established to worship Our Lady of Exile, and improvement and planting had begun in the region.
			</p>
			<p>
				The strategic importance of Santa Catarina Island grew, requiring that it be better defended and occupied more substantially. In this way, in 1726, Desterro was raised to the category of a village and military projects and fortifications were begun on the Island, establishing definitive colonization by Azoreans,
			</p>
			<p>
				On January 6, 1748, 461 people arrived on Santa Catarina Island from the Azores. By 1756, nearly six thousand Azoreans and Madeirenses were settled on the Island, if precariously. Seeds, weapons, tools, and horses and bulls for plowing were supplied to the settlers. Unfortunately their adaptation to the island was made more difficult because their traditional wheat culture was not suitable to the region, which required the Azoreans to produce crops inherited from the Indians.
			</p>
			<p>
				Thus, cassava became the basis of the diet and in a short period of about 30 years, there were about 300 small mills, some for sugar cane. Many settlers abandoned their lands and dedicated themselves to urban tasks and fishing. Others were recruited for military training, which in a certain way restrained and limited development of the small commercial production of the promising Desterro. At the turn of the nineteenth century, the beginning of a rural exodus was perceptible, motivated by military enlistment, fishing and urban jobs, which in addition to increasing the center of the city, allowed the formation of the first large estates.
			</p>
			<p>
				<b>FROM DESTERRO TO FLORIAN�POLIS</b>
				<br>
			</p>
			<p>
				In an homage from then governor Herc�lio Pedro da Luz to Marechal Floriano Peixoto, and a decision now widely contested by many Catarinenses and those aware of Peixoto's history, Desterro had its name changed, on October 10, 1894 to Florian�polis. Strengthened by an emerging commercial bourgeoisie, the administration of the city passed from the hands of the military to the hands of merchants, who accumulated capital by handling local exports and the flow of products at the principal port of Santa Catarina, which at that time was still on the Island.
			</p>
			<p>
				Due to the notable and growing concentration of population encouraged by commerce, the Santa Catarina capital entered the twentieth century with features very unique to large urban centers.
			</p>
			<p>
				The urban district suffered greatly, and its problems increased. In 1926 the first of three bridges that link the island to the mainland was inaugurated, and called the Herc�lio Luz bridge. This represented a considerable mark for economic activity, because it offered a privileged position to the city as a regional pole in relation to the interior of the State, and triggered a true modernization process.
			</p>
			<p>
				The Revolution of 1930 caused another boost to trade, now conducted by highway, and activity at the old port of Desterro declined. Nevertheless, the main urban transformations in the city of Florian�polis came to occur with greater intensity after the second half of this century. At the end of the 1950's the Federal University at Santa Catarina was established and in association with the creation of the Eletrosul utility and other state companies, the city attracted a large migratory flow, not only from the interior but also from other States. The sub-divisions and residential neighborhoods multiplied, as well as apartment buildings, companies and commerce.
			</p>
			<p>
				In parallel to the growth of the middle class (which took place thanks to the growth of the public sector due to the injection of federal and state resources), there was also an invasion of a large number of poor immigrants from the countryside and other cities, who were attracted by promising opportunities for employment. At the same time the number of favelas and precarious occupations of the urban periphery grew in Florian�polis. 
			</p>
			<p>
				Circled by crystal waters, beaches with clear waters and a pleasant climate, Florian�polis is about one kilometer from the continent at the closest point of contact. With its favorable geographic and climatic conditions, it is natural that its inhabitants dedicate themselves to nautical activities, such as sport fishing, underwater fishing, sailing, surfing and rowing. With many events year round, in addition to the Fenaostra (National Oyster Festival) and the Festa da Tainha, which offers different fish dishes, there is also a mussel festival and Carnival, with costumed floats and much joy.
			</p>
			<p>
				Beaches for all tastes are found on the island, from the busiest, with international hotels, good infrastructure and night life, to virgin beaches, where the environment is totally preserved.
			</p>
			<p>
				Called by the Guaraini Indians &quot;Jurer� Mirim&quot; (small &quot;mouth of water&quot;) because of the strait that separates it from the mainland) Santa Catarina Island has been known since the sixteenth century, discovered by sailors seeking a route to the Indies. When the Portuguese decided to colonize the southern Brazilian coast, various villas were established, among them, at that time, Nossa Senhora do Desterro [Our Lady of Exile[, which is now Florian�polis. After various attacks by pirates, the small villages nearly disappeared.
			</p>
			<p>
				At this time, the Portuguese government decided to facilitate the arrival of families from the Azorean Islands to populate the coast and consolidate the colonization process. To guarantee possession of the island, the Portuguese built various forts. Among these stand out Anhatomirim, the largest and best preserved. It was built on a small island, 5 miles from the city, in 1744.
			</p>
			<p>
				It is now part of the historic heritage of Santa Catarina. There is a marine research center at the site, a museum and a maritime aquarium.
			</p>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab4">
	<tr>
		<td class="textoComum" valign="top">
			<p>
				<b>BEACHES</b>
				<br>
			</p>
			<p>
				The municipalities that border Santa Catarina Island - Governador Celso Ramos, to the north; Palho�a, to the south; and S�o Jos� and Bigua��, to the west - have in common the Azorean colonization and the fact that they are home to companies of the metropolitan region. When it comes to beaches, the big competitors of Florian�polis are Palho�a and Governador Celso Ramos. 
			</p>
			<p>
				In the municipality of Palho�a, Praia de Fora, Enseada do Brito, Praia do Sonho and Pinheira are fishing communities that are popular with tourists. Pedras Altas is the official naturalist beach in the municipality, and Guarda do Emba� is recognized as one of the best for surfing. Governador Celso Ramos is better known for the polemical farra do boi &#150; or the running of oxes through the streets - than for its beautiful coast, with coves, bays and peninsulas. The municipality is also the largest producer of cultivated mussels in Brazil. In addition to the beaches of Palmas, Praia Grande, Arma��o, and Costeira, Anhatomirim Island and the Ba�a dos Golfinhos (Dolphin Bay) is part of the municipality. Still little explored by tourists and without many leisure options other than the beaches, Governador Celso Ramos is a trip worth taking to see the well-preserved nature. 
			</p>
			<p>
				At the center of attraction is Santa Catarina Island, with its mix of plateaus, dunes lagoons, mangrove swamps and mountains with Atlantic Forest that is displayed in various forms. Santa Catarina Island is surrounded by 30 small islands, among them those that house the forts of Anhatomirim and Ratones Grande, in addition to Ratones Pequena, Franc�s, Guar�s Grande, Diamante and Noivos, all in the North Bay. In the South Bay are Vinhas, Laranjeiras, Maria Francisca, do Largo, do Andrade and Carlos Islands. To the north stand out Deserta and Arvoredo Islands, which are part of a Marine Biology Reserve. To the east are the islands of Moleques do Norte, Mata-Fome, Badejo, das Aranhas, do Xavier and Campeche. On the latter, exuberant nature and the remains of an ancient pre-historic civilization are preserved. Sixteen hundred meters long, the island is a trip not to be missed. With help from a good guide one can see a lot in one day. There are prehistoric rock carvings and a cave full of bats, which cuts 30 meters into the island. One can hike and dive, or enjoy the beautiful beach. 
			</p>
			<p>
				To the south there are the Tr�s Irm�s, Moleques do Sul, do Coral, Ara�atuba and Papagaios Islands. On the north coast of Santa Catarina Island are the beaches with greater tourist infrastructure, calm, warm waters, ideal for families with children, except for Praia Brava.
			</p>
			<p>
				<b>THE NORTH COAST</b>
				<br>
			</p>
			<p>
				<b>Santinho</b> - On the open ocean and also good for surfing, it is 40 km from downtown and 2.5 km from the beach. One of its attractions is the rock carvings along the coast. There are inscriptions made in the rocks more than 5 thousands years ago by the Carij�s Indians. The strip of light sand is soft and wide. With its dunes, it forms an ecosystem of rare beauty.
			</p>
			<p>
				<b>Ingleses</b> - This beach, 36 kilometers from downtown, is the farthest north of Santa Catarina Island. Its name comes from an English ship that was stranded there in 1700. It is one of the beaches with the most complete tourist infrastructure. Although its waters are not as warm, it offers fine moments to bathers who seek leisure. It is approximately 5 kilometers long and one of the most sought after in summer. Brava - The rough cold sea, usually with strong waves, is great for surfing and doesn't impede a great dip in the sea. Located 38 kilometers from downtown, Brava is behind a hill from which there is a magnificent view on top, before reaching the beach. The 1.7 km strip of soft sand slopes deeply and the water is very deep right at the edge. The commercial options at this beach, which was recently developed with the construction of an imposing group of Mediterranean style buildings, have been increasing each year.
			</p>
			<p>
				<b>Lagoinha</b> - With a calm sea and cold waters, Lagoinha is located 37 kilometers from downtown. Its strip of sand is 900 meters long and has received increasingly more people in recent years. The access road is found just before that for Pontas das Canas. It is one of the most pleasant spots on Santa Catarina Island. It is considered a fine option for families with children.
			</p>
			<p>
				<b>Ponta das Canas and Canasvieiras</b> - Following the historic natural tendency on the north of the island, due to the need for defense from Spanish attacks, the region was settled and urbanized on a much greater scale than the south. Located 34 kilometers from the center of the city it has calm, usually warm waters. It is 1.9 kilometers long with fine, light colored sand. It is a good place for water sports, and one of the beaches most sought by Argentine and Uruguayan tourists. Cachoeira do Bom Jesus - Located between Ponta das Canas and Canasvieiras, this beach with calm seas has nearly 3 kilometers of white sand, and is located 27 kilometers from the city center. It is one of the beaches where the most beautiful houses of the city are found and it has a good set of commercial services. Canasvieiras - The principal spot for Argentineans in Florian�polis, Canasvieiras is the most cosmopolitan location on Santa Catarina Island. So much so that &quot;portunhol&quot; (a mixture of Spanish and Portuguese) is the official language during the summer months. It is one of the busiest points of the island, by day and by night. There are bars, restaurants, hotels, clubs, game rooms, camp grounds - all the facilities necessary for a comfortable stay with plenty of options. Equipment is also offered for various sports and there are many good tennis courts at the hotels, clubs, and posadas. Three kilometers long, the waters at Canasvieiras beach are extremely pleasant, warm and calm. One can rent a boat that makes the trip to Anhatomirim Island in 40 minutes. Even closer is Franc�s Island, where nearly everything is destined to the use of the only landowner. Other options at this beach, which is 27 kilometers from downtown, are the schooner trips and paddle boats for rent.
			</p>
			<p>
				<b>Daniela</b> - Three kilometers long and 26 kilometers from downtown, this beach is occupied mostly by summer houses. Its has clear waters on a warm sea, it is much sought after by families with children. There are not many services.
			</p>
			<p>
				<b>THE WEST COAST</b>
				<br>
			</p>
			<p>
				The west coast of Santa Catarina Island, known as Mar de Dentro (The Inner Sea), faces the mainland, and is divided between the north and south bays. In the north bay are found the beaches of Saco Grande, Santo Ant�nio de Lisboa, and Sambaqui, formed by communities located on bays with sheltered waters that conserve characteristics of Azorean immigrants. 
			</p>
			<p>
				<b>Sambaqui</b> - Part of the district of Santo Ant�nio de Lisboa, Sambaqui is an option for those who seek tranquility in one of the most pleasant areas for recreation and rest on the island. Seventeen kilometers from downtown, its beach is 1.1 km long. The name Sambaqui originates from the shell mounds in the area, calling the attention of researchers, and which from time to time have revealed fossilized animals or vegetable matter, in addition to the relics of the indigenous communities that inhabited the region. Its clear waters are calm, nearly still. The beach line is formed by rocky coasts. This is one place on the island where the folkloric boi-de-mam�o dance is still found. Sambaqui has various bars and good restaurants on the beach.
			</p>
			<p>
				<b>Santo Ant�nio de Lisboa</b> - The oldest neighborhood on the Island, Santo Ant�nio is an important center of Azorean culture. Its beach has warm, calm waters, It is 13 kilometers from downtown, noted for its extreme beauty and peacefulness, with coarse sand beaches. To substitute fishing, one of the activities that has been increasing is the cultivation of oysters and mussels. Another ideal place to seek peace is neighboring Cacup�. It has a beach 900 meters long. In addition to various restaurants, it is a fine option for a picnic or family outing. 
			</p>
			<p>
				<b>THE SOUTHERN PART OF THE ISLAND</b>
				<br>
			</p>
			<p>
				<b>Tapera</b> - Located on the southern coast of the Island, near the airport, it has clear, still waters. The beach is 1.5 km long and has firm, white, thick sand. There are a number of bars and it is a good place for an outing.
			</p>
			<p>
				<b>Ribeir�o da Ilha</b> - Located 36 km from downtown, Ribeir�o da Ilha is one of the oldest settlements of Florian�polis. Records indicate that in 1526 Spanish explorer Sebasti�o Caboto landed here and some of his sailors joined others already on the island because they were shipwrecked from an earlier expedition of Dias de Solis, in 1515. Ribeir�o is composed of various small beaches with calm waters with thick sand. It is one of the locations with the most traditional architecture, such as the Azorean style houses, the Nossa Senhora da Lapa do Ribeir�o Church and the Ethnology Museum, with a collection of rare artifacts related to Azorean colonization.
			</p>
			<p>
				<b>EAST COAST</b>
				<br>
			</p>
			<p>
				On the East Coast are located the beaches that face the Atlantic Ocean, settled above sandy banks. They are good for surfing and have clean, cold waters.
			</p>
			<p>
				<b>Joaquina</b> - World famous for its excellent surfing conditions and for the infinite number of surf competitions held there, the sand dunes of Joaquina attract a new type of sport - sandboarding. The beach's name comes from the legend of an old lacemaker named Joaquina, who was on the sand of the beach making lace when she was washed away by an enormous wave. Its sea are choppy. Seventeen km from downtown, it is called &quot;Joaca&quot; by the surfers. The beach is 3.5 km long and has one of the best facilities for visitors including hotels, restaurants, bars, a tourist terminal with a police and lifeguard post, public showers and bathrooms. Swimming is not recommended because of the large waves and current. Joaquina is invariably full of surfers and bodyboarders with their colorful clothes. There are always people playing soccer and volleyball in the sand. There is a large paid parking lot, as well as ice cream stands and crafts stores. An efficient lighting system allows surfing and other sports at night.
			</p>
			<p>
				<b>Mole</b> - A fine option for surfing, Praia Mole is not recommended for children. There are deep spots right at the edge. Located 15 kilometers from downtown between Lagoa da Concei��o and Barra da Lagoa, its name comes from its soft sands. It also has public parking, showers, bathrooms and a life guard post. There are bars with seafood and snacks. With its regularly cold waters, it is the hot spot of the city. With a predominantly young crowd, it is busy on summer nights with its bars with live music. It is also one of the favorite spots for those who play frescobol (or beach paddleball).
			</p>
			<p>
				<b>Lagoa da Concei��o</b> - Lagoa da Concei��o, along with the Herc�lio Luz Bridge, are the most famous symbols of the city. It can be said that there are different worlds that inhabit Florian�polis &#150; the cosmopolitan and the rural - they both mix in Lagoa. Although there are no waves, its is where the majority of surfers live. Although it has no libraries, theaters or cinemas, it is a center for artists and intellectuals. Although there is no large stage for shows, distinct rhythms sway the bars at night. Those who seek fun and art cannot miss visiting Lagoa. Nearby are some of the best beaches for surfing and other extreme sports. From the surrounding mountains rise hang-gliders and paragliders that color the sky. In the sand-dunes, between Avenida das Rendeiras - where bobbin lace is still made - to Joaquina, sandboarding is practiced. The lagoon waters are dominated by motorboats, jet-skis, windsurfers and sailboats of all types and sizes. For many years Lagoa was linked to a number of neighboring villages. Among these are Barra da Lagoa, Costa da Lagoa and Canto da Lagoa, communities of fishermen and their families which preserve many of the cultural characteristics. Canto da Lagoa has a sambaqui (Indigenous cemetery), where semi-polished ax heads, ceramic ware and other archeological remains were found.
			</p>
			<p>
				<b>Costa da Lagoa</b> is a fishing village. Its origin is linked to the settlement of the village of Lagoa da Concei��o. In the eighteenth and nineteenth centuries, it achieved prestige for the important exchange of goods that were produced there and taken to the Lagoa and other centers. Vestiges of the large quantity of crops and the many mills that existed there can still be noticed. There are still some traditional mills, large houses and colonial homes along the century-old stone route that links the villages of Costa da Lagoa with that of Lagoa da Concei��o. The local fishermen and lace-makers still conduct their traditional crafts, as in generations past. A municipal decree has preserved Costa da Lagoa as a historical-cultural landmark. 
			</p>
			<p>
				<b>Galheta</b> - Galheta beach, alongside Mole beach, is another example of a wild beach that can only be reached by foot. To get there one must follow a 300-meter trail located along the rocky coast on the northern side of Mole Beach. Galheta is a paradise with no buildings. The City Council recently declared the location to be a place for natural bathing. In addition to the beauty of the landscape, the environment is also good for surfing.
			</p>
			<p>
				<b>Barra da Lagoa</b> - The largest fishing center on the Island, Barra da Lagoa is also one of the most popular beaches in Florian�polis with its 2.50 km of sand. Twenty kilometers from downtown, Barra is well served by bars and restaurants. Its waters are not rough, although on some days it is possible to surf, principally near the rocky coast on the south side of the beach. It also has various types of posadas and houses for rent, in addition to camp grounds. In July the location becomes lively with the Tainha Festival, a large traditional event organized by the local community and which attracts visitors from near and far.
			</p>
			<p>
				<b>Mo�ambique</b> - Nearly deserted, it is the largest beach on the island, with 12.5 km of soft sands. The nearly nonexistent occupation is due to the fact that this beach is near preserved regions, such as the dunes of Santinho and the Rio Vermelho Forest Park, a reserve of 400 mil m2 established in 1970. Another good option for surfing is Mo�ambique or Praia Grande, which has cold waters. Its name comes from the fact that it has many mo�ambiques, a type of mollusk similar to an oyster. Located 35 km from downtown, it has very limited commerce.
			</p>
			<p>
				<b>SOUTH COAST</b>
				<br>
			</p>
			<p>
				On the southern coast are found beaches with less tourist facilities and which have traditional communities in addition to a number of rough trails that lead to nearly deserted open ocean beaches.
			</p>
			<p>
				<b>Campeche</b> - Located 20 km from the center of the city, it is a rather large beach, circled by small dunes and scrub vegetation. It is one of the best options for surfing on the island. It is 11.4 km long and the road that leads to the sea is well served by bars and restaurants. In front of the beach is Campeche Island, with exuberant native vegetation. From 1926 to 1939, the French writer and aviator Saint-Exup�ry (author of &quot;The Little Prince&quot;) landed at the natural air strip in the village. 
			</p>
			<p>
				<b>Naufragados</b> - To arrive at this beach one travels 28 km from downtown to Ribeir�o da Ilha and another 12 km by dirt road to Caieira da Barra do Sul. It is then necessary to walk 3 km to reach the final destination, which is the southern most point on the island. Despite the effort, the trip is pleasant. The name of the beach comes from a shipwreck (naufragados means shipwrecked in Portuguese) which occurred in 1715 with a boat of Azorean immigrants. With 1,500 meters of light colored sands, it is an open ocean beach with generally cold waters. It is much sought by campers and surfers. For those who prefer greater comfort, there is sea access from Ribeir�o da Ilha. The boat trip is one of rare beauty, for the trajectory runs along rocky coasts where the Island's vegetation is well preserved.
			</p>
			<p>
				<b>Morro das Pedras</b> - Also with rough seas, where one must swim with care, this beach that is 3,200 meters long and 20 km from downtown, connects Campeche and Arma��o. It is parallel to the Peri Lagoon. From the access road one can see the waves crashing against the rocky coast. On top of the hill is a retreat for Jesuit priests built from rocks taken from the site. There is a beautiful view of Arma��o and the Peri Lagoon Park.
			</p>
			<p>
				<b>Arma��o</b> - This beach of 2.25 km was a whaling center. Even with good waves, it is safe for swimming. There are popular restaurants along the plaza and a century-old Azorean chapel. Located 25 km from downtown, it is one of the most traditional fishing villages on the island. It also has one of the most important archeological sites in the state.
			</p>
			<p>
				<b>Matadeiro</b> - Separated from Arma��o beach by a small river, this beach is only 200 meters long and can only be reached by foot. In addition to surfing, it is excellent for volleyball, frescobol and soccer. There are small bars that have various snacks.
			</p>
			<p>
				<b>Lagoinha do Leste</b> - Another wild beach which can only be reached by foot, Lagoinha do Leste (The little lagoon of the east) is worth a visit. It is accessible after a walk of about an hour on a trail over the hill. There are no bars or restaurants, but it is a natural paradise, with soft sands and a rough seas. There is also a lagoon on the north side where campers set up tents on the margins. Its 1.25 km are a true sanctuary of nature.
			</p>
			<p>
				<b>Saquinho</b> - There are three trails that lead to Saquinho beach, leaving from Solid�o, from Caieira da Barra do Sul or from Naufragados. They are located in steep areas and require stamina, but are worth the effort because one finds a portion of well-preserved Atlantic Forest, or for those who leave Solid�o, a coast that is magnificently carved with a distinctive presence of seagulls. This beach is in the shape of a conch and is only 200 meters long. It is popular with surfers.
			</p>
			<p>
				<b>P�ntano do Sul</b> - This beach is one of the richest fishing villages on the island. It has good options for restaurants, and a relatively rough sea, but the waves are not big and it is safe for swimming. It is 31 km from downtown and has an important archeological site, in addition to a swamp, the ecosystem from where it took its name (P�ntano is swamp in Portuguese). The beach has a wide strip of damp sand. 
			 </p>
		</td>
		<td class="textoComum" width="120" valign="top" align="right">
			<%
			redim nm_figuras(28)
			nm_figuras( 1) = "Armacao"
			nm_figuras( 2) = "Barra da Lagoa"
			nm_figuras( 3) = "Canasvieiras1"
			nm_figuras( 4) = "Canasvieiras2"
			nm_figuras( 5) = "Canasvieiras3"
			nm_figuras( 6) = "Costao da Ilha da Armacao"
			nm_figuras( 7) = "Costao do Santinho"
			nm_figuras( 8) = "Daniela"
			nm_figuras( 9) = "Hotel da Praia Brava"
			nm_figuras(10) = "Ilha da Armacao"
			nm_figuras(11) = "Ilha da Fortaleza da Aracatuba"
			nm_figuras(12) = "Ilha do Campeche"
			nm_figuras(13) = "Joaquina 1"
			nm_figuras(14) = "Joaquina"
			nm_figuras(15) = "Pantano do sul"
			nm_figuras(16) = "Piscina Natural na Praia Mole"
			nm_figuras(17) = "Ponta das Canas"
			nm_figuras(18) = "Ponta de Naufragados"
			nm_figuras(19) = "Ponta do Sambaqui"
			nm_figuras(20) = "Praia Brava 1"
			nm_figuras(21) = "Praia Brava 2"
			nm_figuras(22) = "Praia Deserta"
			nm_figuras(23) = "Praia do Matadeiro"
			nm_figuras(24) = "Praia do Ribeirao da Ilha"
			nm_figuras(25) = "Praia Mole 1"
			nm_figuras(26) = "Ribeirao da Ilha"

			redim alt_figuras(28)
			alt_figuras( 1) = "Arma��o Beach"
			alt_figuras( 2) = "Barra da Lagoa Beach"
			alt_figuras( 3) = "Praia de Canasveiras Beach"
			alt_figuras( 4) = "Praia de Canasveiras Beach"
			alt_figuras( 5) = "Praia de Canasveiras Beach"
			alt_figuras( 6) = "Coast Arma��o Island"
			alt_figuras( 7) = "Coast of Santinho"
			alt_figuras( 8) = "Daniela Beach"
			alt_figuras( 9) = "Hotel at Praia Brava"
			alt_figuras(10) = "Arma��o Island"
			alt_figuras(11) = "Ara�atuba Fort Island"
			alt_figuras(12) = "Campeche Island"
			alt_figuras(13) = "Joaquina Beach"
			alt_figuras(14) = "Joaquina Beach"
			alt_figuras(15) = "P�ntano do Sul Beach"
			alt_figuras(16) = "Natural Swimming pool at Mole Beach"
			alt_figuras(17) = "Ponta das Canas Beach"
			alt_figuras(18) = "Ponta de Naufragados Beach"
			alt_figuras(19) = "Ponta do Sambaqui Beach"
			alt_figuras(20) = "Brava Beach"
			alt_figuras(21) = "Brava Beach"
			alt_figuras(22) = "Deserta Beach"
			alt_figuras(23) = "Matadeiro Beach"
			alt_figuras(24) = "Ribeir�o da Ilha Beach"
			alt_figuras(25) = "Mole Beach"
			alt_figuras(26) = "Ribeirao da Ilha"
			
			for i=1 to 26
				%>
				<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - Click to enlarge" vspace="50" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>);">
				<br>
				<%
			next
			%>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab5">
	<tr>
		<td class="textoComum">
			<p>
				<b>THE MAINLAND</b>
				<br>
			</p>
				On the mainland in front of the island are beautiful beaches with calm seas. They give name to the neighborhoods of the city and were very popular in the 1950s and 1960s. The Beaches in the neighborhoods of Coqueiros and Estreito are not regularly used for swimming. In compensation, in the case of Coqueiros, they offer beautiful views, such as that from the beaches of Saudade, do Sorriso, do Meio, Itagua�� and das Palmeiras, as well as that of Bom Abrigo, located in the neighborhood of the same name. In Estreito is found Balne�rio beach. It is not suitable for swimming, but is good for walking and sporting activities.
			</p>
			<p>
				In a transition region between the coast and the mountains, with a pleasant and temperate climate, Florian�polis has two neighboring cities famous for their mineral springs. �guas Mornas and Santo Amaro da Imperatriz, are 35 kilometers from the center of the state capital. The rural landscape that combines with the mountains, protects the rustic nature of a time when these places were routes for mule troops between the mountain region and the coast. 
			</p>
			<p>
				Of German settlement, �guas Mornas was declared a municipality in 1961. Its typically rural villages and settlements created by German immigrants still have strong marks of that culture which are easily noticed in the architecture, language, habits and customs of the residents. 
			</p>
			<p>
				In addition to the options offered by the hotels in the region, the visitor has various alternatives such as trips through the Serra do Tabuleiro mountains State Park, a location where the flora and fauna are preserved, highlighted by a small zoo with species threatened with extinction. There are also beaches nearby such as Pinheira and Guarda do Emba�, in the municipality of Palho�a, some 39 kilometers from Florian�polis.
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab6">
	<tr>
		<td class="textoComum">
			<p>
				<b>FORTS</b>
				<br>
			</p>
			<p>
				Centuries old, the forts surround Santa Catarina Island, distributed along its 172-kilometer coastline, they were recently revitalized and open to public visits. They are monumental fortresses that until recently constituted the principal project in the field of cultural heritage in Brazil. The complete restoration of the largest, that of Santa Cruz de Anhatomirim, has consolidated these historic monuments as the main tourist attractions in Florian�polis. They have thousands of visitors from throughout the country and abroad, principally from neighboring countries. The average number of visitors in the months of January and February (the height of the summer season) has been 400,000 people.
			</p>
			<p>
				These monuments constitute a set of six units that date back to the seventeenth century when the Spanish and Portuguese entered into conflict in the region of the Plata River and the Portuguese government decided to maintain a single command on the entire southern Brazilian coast as far as the Col�nia de Sacramento. Santa Catarina Island, then called Nossa Senhora do Desterro, was the strategic point in the South Atlantic.
			</p>
			<p>
				In 1738, Portuguese King Dom Jo�o V determined that Brigadeiro Jos� da Silva Paes should go to Desterro to build a system of forts around the island. A military engineer, Silva Paes planned four: Santo Ant�nio de Lisboa, on Ratones Grande Island; Santa Cruz, on Anhatomirim Island; S�o Jos�, on the northern portion of Santa Catarina Island and Nossa Senhora da Concei��o, to the south, on the small island of Ara�atuba.
			</p>
			<p>
				Of the group, the largest and that which suffered less damage over the centuries was that of Santa Cruz, which together with that of Santo Ant�nio and S�o Jos� formed the triangular defensive system that protected the northern entrance of Santa Catarina Island. This basis of the defense was a system of cross-fire. Each cannon would be able to reach half the distance between the other two forts in order to block the access of enemies.
			</p>
			<p>
				According to historians, this fortress was not effectively utilized from a military point of view, not even during the Spanish invasion of 1777. After this episode the system entered into discredit and Santa Cruz came was progressively abandoned. In 1894, during the Federalist Revolution, it served as a prison and the place where those who revolted against the government of Floriano Peixoto were sent to the firing squad. In 1907, it was turned over to the Naval Ministry and went back to being utilized as a prison at the end of the Constitutionalist Revolution of 1932. It functioned as a fort until the end of World War II, when the appearance of new military technologies made it completely obsolete as a military unit. It was then disactivated.
			</p>
			<p>
				Although it was declared a landmark in 1938, only in 1970 were the first steps at restoration taken by the Federal University at Santa Catarina, which officially re-opened the fort to public visits in 1984. The complete restoration was terminated in 1993.
			</p>
			<p>
				As the other eighteenth century buildings in Brazil, Santa Cruz has renaissance traces in its architecture. On the Island circled by rocky coast, its buildings are distributed in a sparse manner on different levels, contained by thick walls, on top of which were sheltered the prominent circular lookout posts. Among the most significant buildings of the fort stand out the portal, which is composed of a corridor formed by a brick dome over which rise two stepped masses of masonry; the commandants house, a type of chamber and jail; and a two-story house that was the first government headquarters of Santa Catarina, where Silva Paes resided. The troop quarters is an important highlight, representing the peak of the majesty of the work of Silva Paes. The classic construction style is marked by straight lines, colonial roofs and 12 ground-level arcades built with such care that they rarely fail to be mentioned by European travelers in their diaries. In addition to these buildings, there is the powder house, the flour house and the beach warehouse.
			</p>
			<p>
				Other buildings were constructed later, such as the electrical plant and the telegraph house. Visitor's also have the natural side of the island to be appreciated. The greater part of the surroundings of Anhatomirim Island is formed by rocky coasts that protect a very special marine fauna. Three small sandy beaches complete the coast. The terrain is modest and the highest point is 31 meters above sea level. Between the island and the mainland, which are separated by two miles, the maximum depth is 5 meters.
			</p>
			<p>
				In the surroundings, there is a bay, which is home to some 500 dolphins. The Santo Ant�nio fort, on Ratones Grande Island, is the second largest of the region. Its construction began in 1740, and concluded four years later. It remained in ruins until 1990, when its restoration was initiated. It also had renaissance traces, the principle buildings are built in a row, protected by the coast and overlook the sea. The powder storage, located on the most prominent point of land is the only construction of two stories. The buildings, as well as the walls that protect them, were built with a lime base. Among the most significant architectural elements of the fort are the portada, the water well and the aqueduct which joins the commanders house to the troop quarters. It has an original and interesting method of retrieving, transporting and using rain water from the roofs of the main buildings.
			</p>
			<p>
				The S�o Jos� da Ponta Grossa fort is 25 kilometers from the center of Florian�polis on Santa Catarina Island. It is strategically located on top of a hill, framed by the beauty of the rocky coasts and the white sand of the Forte Beach, it constitutes the third flank of the triangular defense system. The largest of its buildings had their restoration completed in the second half of the 1990's. The most significant building is the commander's house which is curiously constructed with a common wall to the powder storage, forming the only two-floor structure at the location. The strength and sobriety of the composition generated by the principal patio, framed by the colonial house and the austere chapel, are a good reflection of the importance of the relationship of the powers of Church and State in the eighteenth century.
			</p>
			<p>
				In addition to the Federal University at Santa Catarina, which organizes excursions with students and scholars, private companies offer tourist trips to the island forts (Anhatomirim and Santo Ant�nio). The excursions are conducted on schooners and the principal company in the business is Escuna Sul. Even outside of the summer season, there are daily trips at different hours and with on-board service. In addition to the services, various whaling boats leave at any hour if there are passengers, from Lagoa da Concei��o, to the principal points of Santa Catarina Island and its surroundings, including the forts.
			</p>
		</td>
	</tr>
</table>
<table style="DISPLAY: none" border="0" id="tab7">
	<tr>
		<td class="textoComum">
			<p>
				<b>CULIN�RIA DA ILHA DE SANTA CATARINA PRIVILEGIA FRUTOS DO MAR</b>
				<br>
			</p>
			<p>
				The cooking on Santa Catarina Island gives special emphasis to seafood. Fish, shrimp, and mussels are some of the dishes served in a now numerous group of specialized restaurants, located both in the center of the city as well as at most of the most popular beaches. Fish is prepared in many ways, depending on the species. Mullet, catfish and flounder for example can be served in fillets or steaks. Mussels are served stewed, whether they are fresh or dried.
			</p>
			<p>
				The most common dishes are fish broth, served only with salt and aromatic herbs (parsley, scallions and bay leaves) and eaten with cassava flour puree cooked in the fish broth; and fried fish, cut in steaks and dipped in cassava or wheat flour before frying. The fat of the fish is used for frying. Normally fried fish is eaten with cassava flour puree. Another common dish is fish - it is cleaned but the skin is left on.  Fish is also grilled over charcoal, and maybe rolled in banana leaves. When it is ready, the skin easily flakes off the meat. The dish is eaten with cassava flour puree with beans. Mullet row is another favorite dish, fried and served with lemon. A typical menu also has shrimp, both fried as well as stewed or cooked in batter, accompanied by cassava flour puree, cooked in fish and rice. The shrimp broth, enjoyed with a soup of vegetables and greens, is widely consumed, when not served as a base for cassava flour puree.
			</p>
		</td>
	</tr>
</table>
