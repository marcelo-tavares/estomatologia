<% 
if id_wk = 1 then
	strTitulo="Planta Baixa da Exposi��o"
	strMouse = "Passe o mouse pelos estandes para ver o nome das empresas"
else
	strTitulo="Exhibition Area Plan"
	strMouse = "Move the mouse into a stand to see the companie name"
end if 
%>
<script>
	function acs(stand){
		eid = 'st' + stand
		document.getElementById(eid).style.backgroundColor = "#b7ddc8"
	}

	function aps(stand){
		eid = 'st' + stand
		document.getElementById(eid).style.backgroundColor = "#ffffff"
	}
	
	function acendeEmpresa(empresa){
		id_emp = 'e' + empresa
		switch (empresa){

		case 1:
			acs(23);
			document.getElementById("nmEmpresa").innerText = 'TROPHY';
			break;
		case 31:
			acs(24);
			document.getElementById("nmEmpresa").innerText = 'N. MARTINS';
			break;
		case 2:
			acs(71);
			document.getElementById("nmEmpresa").innerText = 'ASTEX';
			break;
		case 3:
			acs(51);	acs(52);	acs(54);	acs(55);
			document.getElementById("nmEmpresa").innerText = 'C.A.S.';
			break;
		case 4:
			acs(44);	acs(45);	acs(46);	acs(47);	acs(48);	acs(49);
			document.getElementById("nmEmpresa").innerText = 'CDROM';
			break;
		case 5:
			acs(56);	acs(57);	acs(58);
			document.getElementById("nmEmpresa").innerText = 'CDT';
			break;
		case 6:
			acs(73);
			document.getElementById("nmEmpresa").innerText = 'CIRRUS';
			break;
		case 7:
			acs(59);	acs(60);	acs(61);
			document.getElementById("nmEmpresa").innerText = 'DABI ATLANTI';
			break;
		case 8:
			acs(29);	acs(30);
			document.getElementById("nmEmpresa").innerText = 'DEL GRANDI';
			break;
		case 9:
			acs(27);	acs(28);
			document.getElementById("nmEmpresa").innerText = 'FABINJECT GENDEX';
			break;
		case 10:
			acs(4);
			document.getElementById("nmEmpresa").innerText = 'FLODONTOMED';
			break;
		case 11:
			acs(13);	acs(14);	acs(15);	acs(16);	acs(17);
			document.getElementById("nmEmpresa").innerText = 'GNATUS';
			break;
		case 12:
			acs(7);	acs(8);
			document.getElementById("nmEmpresa").innerText = 'HEREAUS KULZER';
			break;
		case 13:
			acs(25);	acs(26);
			document.getElementById("nmEmpresa").innerText = 'IMAGING SCIENCE';
			break;
		case 14:
			acs(40);	acs(41);	acs(42);	acs(43);
			document.getElementById("nmEmpresa").innerText = 'INSTRUMENTARIUM';
			break;
		case 15:
			acs(36);	acs(37);	acs(38);	acs(39);
			document.getElementById("nmEmpresa").innerText = 'J. MORITA DO BRASIL';
			break;
		case 16:
			acs(11);	acs(12);
			document.getElementById("nmEmpresa").innerText = 'KODAK BRASILEIRA';
			break;
		case 17:
			acs(20);
			document.getElementById("nmEmpresa").innerText = 'LOSKA';
			break;
		case 18:
			acs(2);	acs(3);
			document.getElementById("nmEmpresa").innerText = 'JO SUAREZ';
			break;
		case 19:
			acs(70);
			document.getElementById("nmEmpresa").innerText = 'ODONTO ATUAL';
			break;
		case 20:
			acs(9);	acs(10);
			document.getElementById("nmEmpresa").innerText = 'ODONTOMED';
			break;
		case 21:
			acs(18);
			document.getElementById("nmEmpresa").innerText = 'ORTHO GESSO';
			break;
		case 22:
			acs(34);
			document.getElementById("nmEmpresa").innerText = 'PANATHENA';
			break;
		case 23:
			acs(50);	acs(53);
			document.getElementById("nmEmpresa").innerText = 'PLANMECA';
			break;
		case 24:
			acs(19);	acs(21);	acs(22);
			document.getElementById("nmEmpresa").innerText = '';
			break;
		case 25:
			acs(62);	acs(63);	acs(64);	acs(65);	acs(66);	acs(67);
			document.getElementById("nmEmpresa").innerText = 'RADIOMEMORY';
			break;
		case 26:
			acs(68);
			document.getElementById("nmEmpresa").innerText = 'TEMPL�S';
			break;
		case 27:
			acs(35);
			document.getElementById("nmEmpresa").innerText = 'TOL TOTAL';
			break;
		case 28:
			acs(72);
			document.getElementById("nmEmpresa").innerText = 'TROLLPLAST DO BRASIL';
			break;
		case 29:
			acs(33);
			document.getElementById("nmEmpresa").innerText = 'N.D.T.';
			break;
		case 30:
			acs(69);
			document.getElementById("nmEmpresa").innerText = 'X-TEC';
			break;



		}
		document.getElementById("nmEmpresa").className = 'nomeEmpresa';
	}
	
	function apagaEmpresa(empresa){
		switch (empresa){
		case 1:
			aps(23);
			break;
		case 31:
			aps(24);
			break;
		case 2:
			aps(71);
			break;
		case 3:
			aps(51);	aps(52);	aps(54);	aps(55);
			break;
		case 4:
			aps(44);	aps(45);	aps(46);	aps(47);	aps(48);	aps(49);
			break;
		case 5:
			aps(56);	aps(57);	aps(58);
			break;
		case 6:
			aps(73);
			break;
		case 7:
			aps(59);	aps(60);	aps(61);
			break;
		case 8:
			aps(29);	aps(30);
			break;
		case 9:
			aps(27);	aps(28);
			break;
		case 10:
			aps(4);
			break;
		case 11:
			aps(13);	aps(14);	aps(15);	aps(16);	aps(17);
			break;
		case 12:
			aps(7);	aps(8);
			break;
		case 13:
			aps(25);	aps(26);
			break;
		case 14:
			aps(40);	aps(41);	aps(42);	aps(43);
			break;
		case 15:
			aps(36);	aps(37);	aps(38);	aps(39);
			break;
		case 16:
			aps(11);	aps(12);
			break;
		case 17:
			aps(20);
			break;
		case 18:
			aps(2);	aps(3);
			break;
		case 19:
			aps(70);
			break;
		case 20:
			aps(9);	aps(10);
			break;
		case 21:
			aps(18);
			break;
		case 22:
			aps(34);
			break;
		case 23:
			aps(50);	aps(53);
			break;
		case 24:
			aps(19);	aps(21);	aps(22);
			break;
		case 25:
			aps(62);	aps(63);	aps(64);	aps(65);	aps(66);	aps(67);
			break;
		case 26:
			aps(68);
			break;
		case 27:
			aps(35);
			break;
		case 28:
			aps(72);
			break;
		case 29:
			aps(33);
			break;
		case 30:
			aps(69);
			break;
		}
		document.getElementById("nmEmpresa").innerText = '<%=strMouse%>'
		document.getElementById("nmEmpresa").className = 'passeMouse';
	}
</script>
<p class="Titulo" align="center"><%=strTitulo%></p>

<TABLE class=tabelaComBorda width=560 border=0 bgcolor="#f8f8f8">

	<TR>
		<TD colspan=6 height=30>
			<center>
				<span id=nmEmpresa class=passeMouse><%=strMouse%></span>
			</center> 
		</TD>
	</TR>

	<% 'Primeira Linha %>
	
	<%
	function eventosMouse(empresa)
		eventosMouse = "onmouseover=""acendeEmpresa(" & empresa & ");"" onmouseout=""apagaEmpresa(" & empresa & ");"""
	end function
	%>
	
	<TR>
		<TD colspan=5>
			<table align=center>
				<TR>
					<TD id=st13 class=celulaExposicaoA <%=eventosMouse(11)%>>
						13
					</TD>
					<TD id=st14 class=celulaExposicaoA <%=eventosMouse(11)%>>
						14
					</TD>
					<TD id=st15 class=celulaExposicaoA <%=eventosMouse(11)%>>
						15
					</TD>
					<TD id=st16 class=celulaExposicaoA <%=eventosMouse(11)%>>
						16
					</TD>
					<TD id=st17 class=celulaExposicaoA <%=eventosMouse(11)%>>
						17
					</TD>
					<TD id=st18 class=celulaExposicaoB <%=eventosMouse(21)%>>
						18
					</TD>
					<TD id=st20 class=celulaExposicaoB <%=eventosMouse(17)%>>
						20
					</TD>
					<TD id=st19 class=celulaExposicaoA <%=eventosMouse(24)%>>
						19
					</TD>
					<TD id=st21 class=celulaExposicaoA <%=eventosMouse(24)%>>
						21
					</TD>
					<TD id=st22 class=celulaExposicaoA <%=eventosMouse(24)%>>
						22
					</TD>
					<TD id=st23 class=celulaExposicaoA <%=eventosMouse(1)%>>
						23
					</TD>
					<TD id=st24 class=celulaExposicaoA <%=eventosMouse(31)%>>
						24
					</TD>
				</TR>
			</table>
		</TD>
		<TD rowspan=6>
			&nbsp;
		</TD>
	</TR>

	<% 'Separa��o %>
	<TR>
		<TD colspan=5>
			&nbsp;
		</TD>
	</TR>

	<% 'Segunda Linha %>
	<TR>
		<TD>
			<table align=center>
				<TR>
					<TD id=st12 class=celulaExposicaoA <%=eventosMouse(16)%>>
						12
					</TD>
				</TR>
				<TR>
					<TD id=st11 class=celulaExposicaoA <%=eventosMouse(16)%>>
						11
					</TD>
				</TR>
			</table>
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st44 class=celulaExposicaoC <%=eventosMouse(4)%>>
						&nbsp;44&nbsp;
					</TD>
					<TD id=st45 class=celulaExposicaoC <%=eventosMouse(4)%>>
						&nbsp;45&nbsp;
					</TD>
					<TD id=st46 class=celulaExposicaoC <%=eventosMouse(4)%>>
						&nbsp;46&nbsp;
					</TD>
				</TR>
				<TR>
					<TD id=st47 class=celulaExposicaoC <%=eventosMouse(4)%>>
						&nbsp;47&nbsp;
					</TD>
					<TD id=st48 class=celulaExposicaoC <%=eventosMouse(4)%>>
						&nbsp;48&nbsp;
					</TD>
					<TD id=st49 class=celulaExposicaoC <%=eventosMouse(4)%>>
						&nbsp;49&nbsp;
					</TD>
				</TR>
			</table>
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st36 class=celulaExposicaoC <%=eventosMouse(15)%>>
						&nbsp;36&nbsp;
					</TD>
					<TD id=st37 class=celulaExposicaoC <%=eventosMouse(15)%>>
						&nbsp;37&nbsp;
					</TD>
				</TR>
				<TR>
					<TD id=st38 class=celulaExposicaoC <%=eventosMouse(15)%>>
						&nbsp;38&nbsp;
					</TD>
					<TD id=st39 class=celulaExposicaoC <%=eventosMouse(15)%>>
						&nbsp;39&nbsp;
					</TD>
				</TR>
			</table>
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st56 class=celulaExposicaoC <%=eventosMouse(5)%>>
						&nbsp;56&nbsp;
					</TD>
					<TD id=st57 class=celulaExposicaoC <%=eventosMouse(5)%>>
						&nbsp;57&nbsp;
					</TD>
					<TD id=st58 class=celulaExposicaoC <%=eventosMouse(5)%>>
						&nbsp;58&nbsp;
					</TD>
				</TR>
				<TR>
					<TD id=st59 class=celulaExposicaoC <%=eventosMouse(7)%>>
						&nbsp;59&nbsp;
					</TD>
					<TD id=st60 class=celulaExposicaoC <%=eventosMouse(7)%>>
						&nbsp;60&nbsp;
					</TD>
					<TD id=st61 class=celulaExposicaoC <%=eventosMouse(7)%>>
						&nbsp;61&nbsp;
					</TD>
				</TR>
			</table>
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st25 class=celulaExposicaoA <%=eventosMouse(13)%>>
						25
					</TD>
				</TR>
				<TR>
					<TD id=st26 class=celulaExposicaoA <%=eventosMouse(13)%>>
						26
					</TD>
				</TR>
			</table>
		</TD>
	</TR>

	<% 'Separa��o %>
	<TR>
		<TD colspan=5>
			&nbsp;
		</TD>
	</TR>

	<% 'Terceira Linha %>
	<TR>
		<TD rowspan=2>
			<table align=center>
				<TR>
					<TD id=st10 class=celulaExposicaoA <%=eventosMouse(20)%>>
						10
					</TD>
				</TR>
				<TR>
					<TD id=st9 class=celulaExposicaoA <%=eventosMouse(20)%>>
						09
					</TD>
				</TR>
				<TR>
					<TD id=st8 class=celulaExposicaoA <%=eventosMouse(12)%>>
						08
					</TD>
				</TR>
				<TR>
					<TD id=st7 class=celulaExposicaoA <%=eventosMouse(12)%>>
						07
					</TD>
				</TR>
			</table>
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st50 class=celulaExposicaoC <%=eventosMouse(23)%>>
						&nbsp;50&nbsp;
					</TD>
					<TD id=st51 class=celulaExposicaoC <%=eventosMouse(3)%>>
						&nbsp;51&nbsp;
					</TD>
					<TD id=st52 class=celulaExposicaoC <%=eventosMouse(3)%>>
						&nbsp;52&nbsp;
					</TD>
				</TR>
				<TR>
					<TD id=st53 class=celulaExposicaoC <%=eventosMouse(23)%>>
						&nbsp;53&nbsp;
					</TD>
					<TD id=st54 class=celulaExposicaoC <%=eventosMouse(3)%>>
						&nbsp;54&nbsp;
					</TD>
					<TD id=st55 class=celulaExposicaoC <%=eventosMouse(3)%>>
						&nbsp;55&nbsp;
					</TD>
				</TR>
			</table>
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st40 class=celulaExposicaoC <%=eventosMouse(14)%>>
						&nbsp;40&nbsp;
					</TD>
					<TD id=st41 class=celulaExposicaoC <%=eventosMouse(14)%>>
						&nbsp;41&nbsp;
					</TD>
				</TR>
				<TR>
					<TD id=st42 class=celulaExposicaoC <%=eventosMouse(14)%>>
						&nbsp;42&nbsp;
					</TD>
					<TD id=st43 class=celulaExposicaoC <%=eventosMouse(14)%>>
						&nbsp;43&nbsp;
					</TD>
				</TR>
			</table>
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st62 class=celulaExposicaoC <%=eventosMouse(25)%>>
						&nbsp;62&nbsp;
					</TD>
					<TD id=st63 class=celulaExposicaoC <%=eventosMouse(25)%>>
						&nbsp;63&nbsp;
					</TD>
					<TD id=st64 class=celulaExposicaoC <%=eventosMouse(25)%>>
						&nbsp;64&nbsp;
					</TD>
				</TR>
				<TR>
					<TD id=st65 class=celulaExposicaoC <%=eventosMouse(25)%>>
						&nbsp;65&nbsp;
					</TD>
					<TD id=st66 class=celulaExposicaoC <%=eventosMouse(25)%>>
						&nbsp;66&nbsp;
					</TD>
					<TD id=st67 class=celulaExposicaoC <%=eventosMouse(25)%>>
						&nbsp;67&nbsp;
					</TD>
				</TR>
			</table>
		</TD>
		<TD rowspan=2>
			<table align=center>
				<TR>
					<TD id=st27 class=celulaExposicaoA <%=eventosMouse(9)%>>
						27
					</TD>
				</TR>
				<TR>
					<TD id=st28 class=celulaExposicaoA <%=eventosMouse(9)%>>
						28
					</TD>
				</TR>
				<TR>
					<TD id=st29 class=celulaExposicaoA <%=eventosMouse(8)%>>
						29
					</TD>
				</TR>
				<TR>
					<TD id=st30 class=celulaExposicaoA <%=eventosMouse(8)%>>
						30
					</TD>
				</TR>
			</table>
		</TD>
	</TR>

	<% 'Separa��o %>
	<TR>
		<TD colspan=3>
			&nbsp;
		</TD>
	</TR>


	<TR>
		<TD>
			&nbsp;
		</TD>
		<TD colspan=3>
			<table width=100%>
				<tr>
					<td>
						<table align=left>
							<TR>
								<TD id=st4 class=celulaExposicaoD <%=eventosMouse(10)%>>
									04
								</TD>
								<TD id=st3 class=celulaExposicaoB <%=eventosMouse(18)%>>
									03
								</TD>
								<TD id=st2 class=celulaExposicaoB <%=eventosMouse(18)%>>
									02
								</TD>
							</TR>
						</table>
					</td>
					<td>
						<table align=right>
							<tr>
								<td align=right>
									<table align=left>
										<TR>
											<TD id=st35 class=celulaExposicaoD <%=eventosMouse(27)%>>
												35
											</TD>
											<TD id=st34 class=celulaExposicaoB <%=eventosMouse(22)%>>
												34
											</TD>
											<TD id=st33 class=celulaExposicaoB <%=eventosMouse(29)%>>
												33
											</TD>
										</TR>
									</table>
								</td>
							</tr>
							<tr>
								<td align=right>
									<table align=left>
										<TR>
											<TD id=st68 class=celulaExposicaoF <%=eventosMouse(26)%>>
												68
											</TD>
											<TD id=st69 class=celulaExposicaoF <%=eventosMouse(30)%>>
												69
											</TD>
											<TD id=st70 class=celulaExposicaoF <%=eventosMouse(19)%>>
												70
											</TD>
											<TD id=st71 class=celulaExposicaoF <%=eventosMouse(2)%>>
												71
											</TD>
											<TD id=st72 class=celulaExposicaoF <%=eventosMouse(28)%>>
												72
											</TD>
										</TR>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</TD>
		<TD>
			&nbsp;
		</TD>
		<TD>
			<table align=center>
				<TR>
					<TD id=st73 class=celulaExposicaoE <%=eventosMouse(6)%>>
						&nbsp;73&nbsp;
					</TD>
				</TR>
			</table>
		</TD>
	</TR>
</TABLE>
