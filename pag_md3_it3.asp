<p class="Titulo" align="center">MANUAL DO EXPOSITOR <% if id_wk=2 then %>(in portuguese)<% end if %></p>
 
<p class="textoComum">
<STRONG>Prezado Expositor</STRONG><br><br>Neste Manual voc� encontra as normas da Exposi��o Comercial do 14<sup>th</sup> International Congress of Dentomaxillofacial Radiology. Desde j� agradecemos a sua presen�a e solicitamos a leitura minuciosa destas instru��es.
</p>
<p class="textoComum">
<b>
	I - DISPONIBILIZA��O DOS ESTANDES E MONTAGEM DA EXPOSI��O COMERCIAL
</b>
</p>
<p class="textoComum">
<ul class="textoComum">
	<li>As Empresas que necessitarem modifica��es nos estandes dever�o providenci�-las, junto � Empresa Apoio (Montadora Oficial do Evento, fones +55 (48) 335 6155 ou 335 6226, e-mail: apoiom@terra.com.br), impreterivelmente, at� o dia 22 de abril de 2003. 
	<li><STRONG>ATEN��O</STRONG>: A voltagem no local do Evento � de 220 volts, portanto, transformadores para suprir equipamentos com outra voltagem, ficar�o sob a responsabilidade dos Expositores.
	<li><STRONG>IMPORTANTE</STRONG>: Nos estandes haver� uma l�mpada de 100 watts a cada 3m2, e uma tomada tripolar a cada 9 m2 (rede monof�sica) com capacidade para equipamentos com consumo de at� 300 watts (instala��o el�trica b�sica com consumo gratuito). Em hip�tese alguma a rede el�trica poder� ser sobrecarregada, seja por equipamentos com consumo acima do especificado (300 watts por tomada), ou acoplamento de v�rios equipamentos em uma mesma tomada. Portanto, solicitamos que seja enviada � Secretaria do Evento (endere�o: Universidade Federal de Santa Catarina - CCS/STM, Disciplina de Radiologia - Campus Universit�rio - Trindade - CEP 88040-900 - Florian�polis - SC - Brasil; fones: +55 (48) 331 9630, fax +55 (48) 331 9542; e-mail: 14icdmfr@web.ufsc.br ), impreterivelmente, at� o dia 22 de abril de 2003, a rela��o dos equipamentos el�tricos, com o respectivo consumo de energia (formul�rio anexo), a serem utilizados no estande de sua empresa, para que seja providenciado o c�lculo do consumo total e verifica��o se o mesmo est� de acordo com a capacidade m�xima estabelecida (300 watts por tomada). Na hip�tese de necessidade de instala��o extra, o custo ser� de R$ 40,00 (quarenta reais) por KW, n�o inclu�dos neste os honor�rios da Montadora Oficial. 
	<li>Linhas telef�nicas poder�o ser solicitadas e or�adas diretamente no Centro de Conven��es (fone +55 (48)251 4011, c/ Cl�udia, e-mail: claudia@centrodeconvencoes.com.br ), at� o dia 22 de abril de 2003. 
	<li><STRONG>ATEN��O</STRONG>: N�o ser� fornecida, em hip�tese alguma, instala��o hidr�ulica nos estandes (�gua/esgoto).
	<li>N�o ser� permitida a instala��o de aparelhos de ar condicionado nos estandes, pois o piso superior do Centro de Conven��es possui ar condicionado central.
	<li><STRONG>ATEN��O</STRONG>: As portas internas e de acesso � Exposi��o Comercial t�m 2,15m de altura X 2,15m de largura, e o local da Exposi��o 4,20m de p� direito, portanto, verifique as medidas de seus equipamentos.
	<li><STRONG>ATEN��O</STRONG>: � expressamente proibido exceder o limite de carga de 500 (quinhentos) kg/m2, no piso superior do Cento de Conven��es, local onde ser� instalada a Exposi��o Comercial do Evento.
	<li><STRONG>ATEN��O</STRONG>: N�o ser� permitida, em hip�tese alguma, a utiliza��o e armazenagem de materiais inflam�veis, inclusive g�s natural ou GLP, explosivos ou qualquer outro produto que ponha em risco a seguran�a dos participantes e das instala��es.
	<li>Ser� obrigat�ria a coloca��o de extintores de inc�ndio em cada estande.
	<li>As Empresas dever�o informar � Comiss�o Organizadora do Evento (e-mail: 14icdmfr@web.ufsc.br), at� o dia 22 de abril de 2003, a rela��o com os nomes das pessoas que trabalhar�o no respectivo estande, para que sejam providenciados os crach�s. ATEN��O: Apenas as pessoas devidamente credenciadas poder�o participar como Expositores.
	<li>Inicio da montagem da Exposi��o Comercial: Os estandes estar�o dispon�veis aos Expositores, para montagem de materiais e equipamentos, no dia 18 de maio de 2003, das 08:00 �s 22:00 horas.
	<li>A entrada e sa�da de materiais e equipamentos para a Exposi��o Comercial dever�o ser realizadas pela rampa sul do Centro de Conven��es (carga e descarga), n�o sendo permitido o estacionamento de ve�culos naquele local. 
	<li>Os Expositores que desejarem adquirir passe de estacionamento, com desconto no per�odo do Evento, dever�o solicitar ao Setor Operacional do Centro de Conven��es, com um dia de anteced�ncia do in�cio do Evento (fone +55 (48) 251 4011, c/ Cl�udia, e-mail: claudia@centrodeconvencoes.com.br ).
	<li>T�rmino da montagem da Exposi��o Comercial: A montagem dos estandes dever� ser finalizada, impreterivelmente, at� as 08:30 horas do dia 19 de maio de 2003, quando ser� inaugurada a Exposi��o Comercial do Evento.
	<li>Os estandes s�o fornecidos em regime de loca��o, n�o sendo permitidas, portanto, quaisquer modifica��es, inclusive a fixa��o de pregos, parafusos, adesivos e pintura em suas paredes, sem pr�via autoriza��o da Montadora Oficial e da Organiza��o do Evento. 
</ul>
</p>
<p class="textoComum">
	<b>
		II - IN�CIO E FUNCIONAMENTO DA EXPOSI��O COMERCIAL:
	</b>
</p>
<p class="textoComum">
<ul class="textoComum">
	<li>A exposi��o comercial ser� inaugurada no dia 19 de maio de 2003 �s 08:30 horas e funcionar� das 08:30 �s 18:30 horas.
	<li>� terminantemente proibida a sub-loca��o dos estandes.
	<li>Apenas as pessoas devidamente credenciadas, portando os respectivos crach�s, poder�o participar como Expositores. A substitui��o de pessoal dever� ser comunicada � Organiza��o do Evento, para que seja providenciada a troca do(s) respectivo(s) crach�(s), sendo expressamente vedado o fornecimento de crach�s a pessoas n�o credenciadas. 
	<li>A Comiss�o Organizadora do Evento (Promotora) se reserva o direito de rejeitar ou proibir a exposi��o de material e/ou equipamento que, a seu ju�zo, n�o esteja de acordo com o car�ter geral da Exposi��o, restri��o esta que abrange inclusive a perman�ncia de pessoas cuja conduta atente contra a moral e os bons costumes e a ordem no local. Poder� tamb�m proibir a exposi��o de materiais ou produtos perigosos e de publica��o que atentem contra a �tica e a imagem da Classe Odontol�gica. 
	<li>N�o � permitido fumar na �rea da Exposi��o Comercial. 
	<li>A Organiza��o do Evento (Promotora) se exime de toda e qualquer responsabilidade relacionada a tributos incidentes sobre a importa��o, exporta��o, transporte, e comercializa��o de bens, produtos e servi�os, antes durante e ap�s a Exposi��o Comercial do 14th ICDMFR.  
	<li>A Organiza��o do Evento (Promotora) se exime de toda e qualquer responsabilidade relacionada a encargos trabalhistas e previdenci�rios dos Expositores ou de pessoas que estejam a seu servi�o. 
	<li>� permitida a distribui��o de brindes, amostras, folhetos, cat�logos, etc., desde que realizada na parte interna dos estandes, reservando-se � Comiss�o Organizadora (Promotora) o direito de fazer cessar a distribui��o, sempre que esta ocasione dist�rbios ou aglomera��es, ou esteja sendo realizada fora dos limites do estande. 
	<li>A proje��o de qualquer elemento, decora��o ou propaganda, inclusive faixas e cartazes, dever� ser, obrigatoriamente, limitada � �rea do estande.  
	<li>Toda sonoriza��o nos estandes n�o poder� exceder o volume normal da voz, sendo vedado o uso de m�sica ou avisos, atrav�s de audiovisuais, megafones ou similares, que venham perturbar o p�blico e os demais Expositores.
	<li>Os Expositores dever�o comunicar previamente � Comiss�o Organizadora do Evento a realiza��o de eventuais sorteios, que, quando for o caso, dever�o ser realizados entre 18:00 e 18:30 horas. 
	<li>N�o ser� permitida a perman�ncia de pessoas ap�s o hor�rio estabelecido para o funcionamento da Exposi��o Comercial (18:30 horas).
	<li>Todos os aparelhos el�tricos dever�o ser desligados ao t�rmino do expediente (18:30 horas).
	<li>A Seguran�a das �reas comuns da Exposi��o Comercial ficar� sob a responsabilidade da Organiza��o do Evento, por�m esta n�o se responsabiliza por qualquer dano, avarias, preju�zos, furtos e roubos de material ou equipamento de propriedade dos Expositores, que, quando julgarem necess�rio, poder�o contratar seguran�a para a �rea interna dos estandes, em todos os per�odos, condicionada � aprova��o pela Comiss�o Organizadora (Promotora).
	<li>A reposi��o e a sa�da de materiais e equipamentos das Empresas Expositoras poder� ser realizada diariamente, no hor�rio das 08:00 �s 08:30 horas, sempre pela rampa sul.
	<li>A limpeza das �reas comuns da Exposi��o Comercial ficar� sob a responsabilidade da Organiza��o do Evento, enquanto a limpeza interna dos estandes a cargo dos Expositores, que, poder�o contratar pessoal para essa tarefa. O lixo di�rio dever� ser colocado em sacos pl�sticos e em local a ser determinado pela Organiza��o do Evento. 
</ul>
</p>

<p class="textoComum">
	<b>
		III - T�RMINO DA EXPOSI��O COMERCIAL E DESMONTAGEM DOS ESTANDES
	</b>
</p>
<p class="textoComum">
<ul class="textoComum">
	<li>A Exposi��o Comercial ser� encerrada no dia 23 de maio de 2003 �s 12:30 horas, devendo os Expositores entregar os estandes desocupados at� as 18:00 horas do mesmo dia.
</ul>
</p>


