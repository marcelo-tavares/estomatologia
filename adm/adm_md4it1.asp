<% 
strTitulo = "FORMUL�RIO PARA INSCRI��O DE TRABALHOS"
strDados = "<b>Informa��es para Contato</b>"
strNome = "Nome completo do respons�vel pela inscri��o"
strAfiliacao = "Afilia��o (Institui��o/Organiza��o)"
strEndereco = "Endere�o completo para Correspond�ncia (incluir cidade, estado e CEP)"
strFone = "Telefone"
strFAX = "Fax"
strEmail = "E-mail"
strCategoria = "<b>Apresenta��o</b>"
strModo = "Modo de apresenta��o desejado:"
strRetirada = "Caso minha op��o de apresenta��o oral n�o seja poss�vel, eu <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gostaria de retirar meu trabalho.</i>"
strEmPainel = "Caso minha op��o de apresenta��o oral n�o seja poss�vel, eu <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;autorizo a Comiss�o Organizadora a alterar o modo de apresenta��o<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;para Painel Cl�nico.</i>"
strEnviar="Enviar"
strOral = "Oral"
strPainel = "Painel"
strConferencia = "Confer�ncia"
strPalavras = "Total de Palavras"
strMaior = "Atingido o numero m�ximo de palavras"
strMsg = "Por favor informe : "
StrErro001 = "Erro ao obter c�digo.  Favor tentar mais tarde."
StrErro002 = "Erro ao gravar inscri��o de trabalho.  Favor tentar mais tarde."
strMensagemEnviada_wk = "Seu Trabalho foi submetido com sucesso.<br>Aguarde contato da comiss�o organizadora do evento."
strDeclaro = "<b>Declaro que li e compreendi as <a href=""javascript:submeter(7,1,1,0)"">Normas para inscri��o de trabalhos</a> apresentadas neste site.</b>"
strTitle = "T�tulo"
strAuthors = "Autores and Afilia��es"
strSummary = "Resumo"
%>

<script>
	function ordenar(pr1, ordem) {
		document.Form1.h_ordem.value = ordem;
		submeter(4,1,0);
	}
			
	function visualizar(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,1);
	}

	function editar(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,2);
	}

	function enviar(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,2);
	}
	
	function avaliacao(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,4);
	}
	
	function remover(cd_trabalho) {
		if (confirm('Confirma remo��o do Trabalho ' + cd_trabalho + ' ?')){
			document.Form1.hcd_trabalho.value = cd_trabalho;
			submeter(4,1,3);
		}
	}
	
	function incluirAvaliador(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,5);
	}

	function executaInclusao(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,6);
	}

	function removerAvaliadorTrabalho(cd_trabalho, cd_avaliador) {
		if (confirm('Confirma remo��o deste avaliador?')){
			document.Form1.hcd_trabalho.value = cd_trabalho;
			document.Form1.hcd_avaliador.value = cd_avaliador;
			submeter(4,1,7);
		}
	}

	function cadastroImagens(cd_trabalho){
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,9);
	}
	
	function editarDadosImagem(cd_trabalho, prefixo, sequencial) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		document.Form1.de_prefixoImagens.value = prefixo;
		document.Form1.nu_seqImagem.value = sequencial;
		submeter(4,1,10);
	}

	function GravarDadosImagem(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(4,1,11);
	}

	function paginar(codPag){
		document.Form1.paginacao.value = codPag;
		submeter(4,1,0);
	}
function contaCaracteres(elemento,tamanhoMaximo){
	var frase
		
	frase = elemento.value;
	frase = frase.replace("  ", " ");
	elemento.value = frase;
		
	if (frase.length > tamanhoMaximo){
		alert('Tamanho m�ximo de ' + tamanhoMaximo + ' caracteres atingido!');
		frase = frase.substr(0,tamanhoMaximo)
		elemento.value = frase;				
	}  
}
function contaPalavras(){
	var frase
		
	tamanhoMaximo = document.Form1.tamanhoMaximo.value;
	frase = document.Form1.summary.value;
	frase = frase.replace("  ", " ");
	document.Form1.summary.value = frase;
	palavras = frase.split(" ");
		
	if (palavras.length > tamanhoMaximo){
		alert('<%=strMaior%>!');
		frase='';
		for (i=0; i<(tamanhoMaximo-1); i++)
			frase = frase + palavras[i] + ' ';
		frase = frase + palavras[tamanhoMaximo-1];
		palavras = frase.split(" ");
		document.Form1.summary.value = frase;				
	}  
	nr_palavras = palavras.length;
	if (palavras[nr_palavras-1] == '')
		nr_palavras--; 	
	document.getElementById("tp").innerHTML = nr_palavras;
}
	
function tamMaximo(opcao){
	tamanho = 250;
	if (opcao == 2)
		tamanho = 300;
	if ((opcao == 4)||(opcao == 3))
		tamanho = 600;
		
	document.Form1.modoApresentacao.value = opcao;
		
	document.Form1.tamanhoMaximo.value = tamanho;
	document.getElementById("spanTamMaximo").innerHTML = '(M�ximo: ' + tamanho + ' palavras)'
	contaPalavras();
	
	if (opcao == 4) {
		emPainel(1);
		obsReferenciasOral(1);
	}
	else {
		emPainel(0);
		obsReferenciasOral(0);
	}

	if (opcao == 1)
		perguntas(1);
	else
		perguntas(0);

	if ((opcao == 1) || (opcao == 4))
		imagens(1);
	else
		imagens(0);
}

function emPainel(visivel){
	if (visivel == 1)
		document.getElementById("trEmPainel").style.display = 'inline';
	else
		document.getElementById("trEmPainel").style.display = 'none';
}

function imagens(visivel){
	if (visivel == 1)
		document.getElementById("trImagens").style.display = 'inline';
	else
		document.getElementById("trImagens").style.display = 'none';
}

function perguntas(visivel){
	if (visivel == 1)
		document.getElementById("trCasoClinicoConsultivo").style.display = 'inline';
	else
		document.getElementById("trCasoClinicoConsultivo").style.display = 'none';
}

function obsReferenciasOral(visivel){
	if (visivel == 1)
		document.getElementById("spReferenciasOral").style.display = 'inline';
	else
		document.getElementById("spReferenciasOral").style.display = 'none';
}

function ValidarFormEdicaoTrabalho(){
	
	var InformouCategoria = 0;
		
	if (document.Form1.nome.value.length == 0){
		alert('<%=strMsg%><%=strNome%>');
		document.Form1.nome.focus();
		return false;
	}
		
	if (document.Form1.endereco.value.length == 0){
		alert('<%=strMsg%><%=strEndereco%>');
		document.Form1.endereco.focus();
		return false;
	}
		
	if (document.Form1.fone.value.length == 0){
		alert('<%=strMsg%><%=strFone%>');
		document.Form1.fone.focus();
		return false;
	}
		
	if (document.Form1.email.value.length == 0){
		alert('<%=strMsg%><%=strEmail%>');
		document.Form1.email.focus();
		return false;
	}
		
	if (document.Form1.inscricaoApresentador.value.length == 0){
		alert('<%=strMsg%>N�mero da Inscri��o do Apresentador do Trabalho');
		document.Form1.inscricaoApresentador.focus();
		return false;
	}
		
	if (document.Form1.title.value.length == 0){
		alert('<%=strMsg%><%=strTitle%>');
		document.Form1.title.focus();
		return false;
	}
		
	if (document.Form1.authors.value.length == 0){
		alert('<%=strMsg%><%=strAuthors%>');
		document.Form1.authors.focus();
		return false;
	}
		
	if (document.Form1.summary.value.length == 0){
		alert('<%=strMsg%><%=strSummary%>');
		document.Form1.summary.focus();
		return false;
	}
	
	if (document.Form1.keywords.value.length == 0){
		alert('<%=strMsg%>Palavras-Chave');
		document.Form1.keywords.focus();
		return false;
	}

	if (document.Form1.references.value.length == 0){
		alert('<%=strMsg%>Refer�ncias Bibliogr�ficas');
		document.Form1.references.focus();
		return false;
	}

	// Espec�fico do Modo 1 de apresenta��o
	if (document.Form1.modoApresentacao.value == 1){
		if (document.Form1.examesImaginologicos.value.length == 0){
			alert('<%=strMsg%>Exames Imaginol�gicos');
			document.Form1.examesImaginologicos.focus();
			return false;
		}

		if (document.Form1.examesAnatomopatologicos.value.length == 0){
			alert('<%=strMsg%>Exames Anatomopatol�gicos');
			document.Form1.examesAnatomopatologicos.focus();
			return false;
		}

		if (document.Form1.examesComplementares.value.length == 0){
			alert('<%=strMsg%>Outros Exames Complementares');
			document.Form1.examesComplementares.focus();
			return false;
		}

		if (document.Form1.infoTratamento.value.length == 0){
			alert('<%=strMsg%>Informa��es sobre o Tratamento');
			document.Form1.infoTratamento.focus();
			return false;
		}

		if (document.Form1.infoSeguimento.value.length == 0){
			alert('<%=strMsg%>Informa��es sobre o Seguimento');
			document.Form1.infoSeguimento.focus();
			return false;
		}

		if (document.Form1.diagnostico.value.length == 0){
			alert('<%=strMsg%>Diagn�stico ou hip�teses diagn�sticas');
			document.Form1.diagnostico.focus();
			return false;
		}

		if (document.Form1.relevancia.value.length == 0){
			alert('<%=strMsg%>Relev�ncia da Apresenta��o do Trabalho no Congresso');
			document.Form1.relevancia.focus();
			return false;
		}

	}
	
	emPainel(1);
	submeter(4,1,8);
}

function popUp(URL,nomeJanela,props){
	window.open(URL,nomeJanela,props);
}

function IncluirImagens(prefixo){
	url = '../incluirImagens.asp?prefixo='+prefixo
	params = 'scrollbars=no,width=600,height=500';
	id = 'incluirImagens';
	popUp(url,id,params);
}

function aceitacao(cd_trabalho){
	url = 'aceitaRejeita.asp?cd_trabalho='+cd_trabalho
	params = 'scrollbars=no,width=50,height=50,top=2000,left=2000';
	id = 'aceitaRejeita';
	popUp(url,id,params);
}

function alteraImagemAceitacao(cd_trabalho){
	if (document.getElementById('imgAceitacao'+cd_trabalho).aceito == 0){
		document.getElementById('imgAceitacao'+cd_trabalho).aceito = 1
		document.getElementById('imgAceitacao'+cd_trabalho).src = '../images/ico_aceita.gif';
		document.getElementById('imgAceitacao'+cd_trabalho).alt = 'Alterar para rejeitado';
	}
	else {
		document.getElementById('imgAceitacao'+cd_trabalho).aceito = 0
		document.getElementById('imgAceitacao'+cd_trabalho).src = '../images/ico_rejeita.gif';
		document.getElementById('imgAceitacao'+cd_trabalho).alt = 'Alterar para aceito';
	}
}
</script>
<p class="Titulo" align="center">TRABALHOS SUBMETIDOS</p>
<form name="FormI" method="Post">
	<input type="hidden" name="h_ordem" value="<%=ordenacao_wk%>">
	<input type="hidden" name="paginacao">
	<input type="hidden" name="hcd_trabalho">
	<input type="hidden" name="hcd_avaliador">
	<%
	select case pr1_wk
		case 0		
		
		' Conectar com o banco
		%>
		<!--#include file=conn.asp -->
		<%
		Set Conn = Server.CreateObject("ADODB.Connection") 
		Conn.Open cnpath

		set rsD = server.CreateObject("ADODB.Recordset")
		set rsD = Conn.Execute("Select vl_descricao from TB_Config where cd_codigo = 5")
		tm_pagina_wk = cdbl(rsD("vl_descricao"))
		%>
		
			<table BORDER="0" CELLSPACING="0" CELLPADDING="0" class="TabelaProgramacao">
				<tr>
					<td>
					<%
					' Recuperar vari�veis de formul�rio
					ordenacao_wk = request("h_ordem")
					if ordenacao_wk = "" then ordenacao_wk = 1
							
					filtroNome_wk = request("filtroNome")
					filtroSobrenome_wk = request("filtroSobrenome")
					filtroTitle_wk = request("filtroTitle")
					filtroModo_wk = request("filtroModo")
					filtroAceito_wk = request("filtroAceito")
					if filtroAceito_wk = "" then filtroAceito_wk = -1
					select case pr1_wk
						case 0
							' Tratamento de Paginacao
							paginacao_wk = request("paginacao")
							if paginacao_wk = "" then paginacao_wk = 0
								
							select case paginacao_wk
								case 0
									session("paginaAtual")=1
								case 1
									session("paginaAtual")=1
								case 2
									if session("paginaAtual") > 1 then
										session("paginaAtual") = session("paginaAtual") - 1
									end if
								case 3
									if session("paginaAtual") < session("tp") then
										session("paginaAtual") = session("paginaAtual") + 1
									end if			
								case 4
									session("paginaAtual") = session("tp")
							end select
							
							' Criar o recordSet
							set rsT = server.CreateObject("ADODB.RecordSet")	
								
							sqlStr = "SELECT t.*, i.qt_imagens FROM (TB_Trabalhos t left outer join " _
							& "(SELECT it.cd_trabalho, Count(it.cd_trabalho) AS qt_imagens " _
							& " FROM TB_ImagensTrabalhos it GROUP BY it.cd_trabalho) i " _
							& " on t.de_prefixoImagens = i.cd_trabalho)"
							jaWhere = false
							
							if filtroNome_wk <> "" then
								sqlStr = sqlStr & " WHERE nm_responsavel like '" & filtroNome_wk & "%'"
								jaWhere = true
							end if 

							if filtroModo_wk <> 0 then
								if jaWhere then
									sqlStr = sqlStr & " AND"
								else
									sqlStr = sqlStr & " WHERE"
								end if
								sqlStr = sqlStr & " cd_modo = " & filtroModo_wk
								jaWhere = true
							end if 

							if filtroTitle_wk <> "" then
								if jaWhere then
									sqlStr = sqlStr & " AND"
								else
									sqlStr = sqlStr & " WHERE"
								end if
								sqlStr = sqlStr & " de_titulo like '" & filtroTitle_wk & "%'"
							end if 

							sqlStr = sqlStr & " ORDER BY " & ordenacao_wk
							
							sqlStr = sqlStr & ";"
							
							'Response.Write  sqlStr
							'Response.End
							
							' pagina��o
							rsT.PageSize = tm_pagina_wk
							rsT.CacheSize = 5
							rsT.Open sqlStr, Conn, adOpenKeyset
							totalRegistros_wk = rsT.recordcount
							totalPaginas_wk = CInt(rsT.PageCount)
							tamanhoPagina_wk = rsT.PageSize
							session("tp") = totalPaginas_wk
							reg=0	
								
							%>
							<table width="564" border="0">
								<tr class="CelulaTitulo">
									<b>
										<td width="564" colspan="6">&nbsp;</td>
									</b>
								</tr>
							<% if rsT.RecordCount = 0 then %>
								<tr><td><h5>Nenhum Trabalho atende aos crit�rios de pesquisa.</h5></td><tr>
							<% else %>
								<% ' linha de t�tulo %>
								<tr class="CelulaCorpo">
									<b>
									<td width="15"></td>
									<td width="30"><a href="javascript:ordenar(0,1)">C�d.</a></td>
									<td width="200"><a href="javascript:ordenar(0,2)">Nome</a></td>
									<td width="204">&nbsp;</td>
									<td width="45"><a href="javascript:ordenar(0,23)">Data</td>
									<td width="70" align="left">A��es</td>
									</b>
								</tr>
								<% ' linhas de detalhes %>
								<%
								flagLinha_wk = 1
								rsT.AbsolutePage = Cint(session("paginaAtual"))			
								while ((not rsT.EOF) and (reg < tamanhoPagina_wk))
									if flagLinha_wk then 
										nomeClasse = "txtTabelaLida"
									else	
										nomeClasse = "txtTabela"
									end if
									flagLinha_wk = abs(flagLinha_wk - 1)
									select case rsT("cd_modo")
										case 1
											strImg = "CCC"
											strCat = "Caso Cl�nico Consultivo"
										case 2
											strImg = "PP "
											strCat = "Painel de Pesquisa"
										case 3
											strImg = "PC"
											strCat = "Painel Consultivo"
										case 4
											strImg = "O  "
											strCat = "Apresenta��o Oral de Caso Cl�nico"
									end select

									strClasse = ""
									strCarinha = "parc_am.gif"
									strAceito = "Aguardando Avalia��o."
									sn_avaliacao = false
									if rsT("dt_resultado") < now() then
										if rsT("sn_aceito") then
											strCarinha = "parc_vd.gif"
											strClasse = "Negrito"
											strAceito = "Aprovado!"
										else
											strCarinha = "parc_vm.gif"
											strClasse= "Cinzento"
											strAceito = "Reprovado..."
										end if
									else
										sn_avaliacao = true
									end if
									%>
									<tr class="CelulaCorpo">
										<td width="564" colspan="6"><hr></td>
									</tr>
									<tr class="CelulaCorpo">
										<td width="15"><strong><%=strImg%></strong></td>
										<td width="30" align="center" class="<%=strClasse%>"><%=rsT("cd_trabalho")%></td>
										<td width="404" colspan="2" class="<%=strClasse%>"><%=ucase(rsT("nm_responsavel"))%></td>
										<td width="45" class="<%=strClasse%>"><%=day(rsT("dt_cadastro")) & "/" & month(rsT("dt_cadastro"))%></td>
										<td width="70" align="center">
											<img src="../images/ico_lupa.gif" border="0" onclick="visualizar(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Visualizar Trabalho" WIDTH="22" HEIGHT="15">
											<% if session("cientifico") then%>
												<img src="../images/ico_editar.gif" border="0" onclick="editar(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Editar Trabalho" WIDTH="19" HEIGHT="15">
												<img src="../images/ico_remover.gif" border="0" onclick="remover(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Remover Trabalho" WIDTH="15" HEIGHT="15">
											<% end if %>
										</td>
								</tr>
									<tr class="CelulaCorpo">
										<td width="15">&nbsp;</td>
										<td width="30" valign="top"><a href="javascript:ordenar(0,11)">T�tulo</a> :</td>
										<td width="449" valign="top" colspan="3" class="<%=strClasse%>">
											<%=Ucase(rsT("de_titulo"))%>
										</td>
										<td width="70" align="center">
											<% if session("cientifico") then %>
												<img src="../images/ico_avaliacao.jpg" border="0" onclick="avaliacao(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Detalhes sobre a Avalia��o" WIDTH="15" HEIGHT="13">
												<% 
												if rsT("qt_imagens") <> "" then 
													%>
													<img src="../images/ico_imagens.gif" border="0" onclick="cadastroImagens(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Imagens" WIDTH="15" HEIGHT="15">
													<% 
												end if 
	
												if Cbool(rsT("sn_aceito")) then
													%>
													<img id="imgAceitacao<%=rsT("cd_trabalho")%>" aceito=1 src="../images/ico_aceita.gif" border="0" onclick="aceitacao(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Alterar para rejeitado" WIDTH="13" HEIGHT="15">
													<%
												else
													%>
													<img id="imgAceitacao<%=rsT("cd_trabalho")%>" aceito=0 src="../images/ico_rejeita.gif" border="0" onclick="aceitacao(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Alterar para aceito" WIDTH="13" HEIGHT="15">
													<%
												end if
												%>
											<% end if %>
										</td>
								</tr>
								<%
									rsT.MoveNext
									reg = reg + 1
								wend
								%>
							<% end if %>
							</table>

							<%
							' Barra de Pagina��o	%>
							<table width="564">
								<tr><td colspan="3"><hr></td></tr>
								<tr class="CelulaCorpo" valign="middle">
									<td width="200">
										P�gina <%=session("paginaAtual")%> de <%=totalPaginas_wk%>
									</td>
									<td width="100">
										Registros : <%=rsT.RecordCount%>
									</td>
									<td width="264" align="right">
										<% 
										if session("paginaAtual") > 1 then %>
											<a href="javascript:paginar(1);"><img src="../images/Ico_PrimeiraOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(2);"><img src="../images/Ico_AnteriorOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_PrimeiraOff.gif" border="0" WIDTH="12" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_AnteriorOff.gif" border="0" WIDTH="8" HEIGHT="15">
										<% 
										end if %>
										&nbsp;
										<% 
										if session("paginaAtual") < session("tp") then %>
											<a href="javascript:paginar(3);"><img src="../images/Ico_ProximaOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(4);"><img src="../images/Ico_UltimaOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_ProximaOff.gif" border="0" WIDTH="8" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_Ultimaoff.gif" border="0" WIDTH="12" HEIGHT="15">
										<% 
										end if %>
									</td>
								</tr>
							</table>
							<% ' Barra de Filtros	%>
							<table width="100%" border="0">
								<tr class="CelulaCorpo">
									<td colspan="5"><hr></td>
								</tr>
								<tr class="CelulaCorpo" valign="middle">
									<td width="100%" colspan="5">
										<b>Filtros:</b>
									</td>
								<tr>
								<tr class="CelulaCorpo" valign="middle">
									<%
									' buscar categorias no banco de dados
									' Criar o record Set
									set rsModo = server.CreateObject("ADODB.RecordSet")	
									sqlStr = "SELECT * FROM TB_ModoApresentacao ORDER BY de_modo;"
									rsModo.Open sqlStr, Conn, adOpenKeyset
									%>
									<td width="40%" align="left" colspan="2">
										Modo<br>
										<select class="Combo01" name="filtroModo" onchange="paginar(0);">
											<option value="0">Todos</option>
											<%
											while not rsModo.EOF
												%>
												<option <% if CInt(filtroModo_wk) = CInt(rsModo("cd_modo")) then %> selected <% end if %> value="<%=rsModo("cd_modo")%>"><%=rsModo("de_modo")%></option>
												<%
												rsModo.MoveNext
											wend
											%>
										</select>
									</td>
									<td width="20%" align="left">
										Nome<br>
										<input class="textbox01" name="filtroNome" size="10" <%
											if filtroNome_wk <> "" then
											%> value="<%=filtroNome_wk%>" <%
					 						end if
											%>>									</td>
									<td width="40%" align="left" colspan="2">
										T�tulo<br>
										<input class="textbox01" name="filtroTitle" size="25" <%
											if filtroTitle_wk <> "" then
											%> value="<%=filtroTitle_wk%>" <%
					 						end if
											%>>&nbsp;
											<img src="../images/Ico_ProximaOn.gif" border="0" onclick="paginar(0);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="8" HEIGHT="15">
									</td>
								</tr>
								<tr class="CelulaTitulo">
									<td width="100%" colspan="5">&nbsp;</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			<%
			end select
			set rsT = nothing
			set rsD = nothing
			set rsCat = nothing
			set Conn = nothing
		case 1

			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
			
			strSQL = "select t.*, p.nm_participante from TB_Trabalhos t" _
			& " left outer join TB_Participantes p on p.cd_participante = t.cd_apresentador" _
			& " where cd_trabalho = " & request("hcd_trabalho")

			set rsT = server.CreateObject("ADODB.Recordset")
			set rsT = Conn.Execute(strSQL)
			%>
			<table class="TabelaProgramacao" width="564">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						DADOS DO TRABALHO <%=rsT("cd_trabalho")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="514" colspan="2" align="left">
						<b>Nome do respons�vel:&nbsp;</b><br>
						<%=rsT("nm_responsavel")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" align="left" colspan="2">
						<b>Endere�o :&nbsp;</b><br>
						<%=rsT("de_endereco")%>
					</td>
				<tr>
				
				<tr class="CelulaCorpo">
					<td width="564" colspan="2">
						<table width="564">
							<tr class="CelulaCorpo">
								<td width="188" align="left">
									<b>Fone :&nbsp;</b><br>
									<%=rsT("nr_fone")%>
								</td>
								<td width="188" align="left">
									<b>Fax :&nbsp;</b><br>
									<%=rsT("nr_fax")%>
								</td>
								<td width="188" align="left">
									<b>E-mail :&nbsp;</b><br>
									<%=rsT("de_email")%>
								</td>
							<tr>
						</table>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<hr>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" colspan="2">
						<b>Apresentador :&nbsp;</b><br>
						<%
						if len(rsT("nm_participante")) <> 0 then
							Response.write rsT("cd_apresentador") & " - " & rsT("nm_participante")
						else
							Response.write "<b>" & rsT("cd_apresentador") & " - Apresentador n�o inscrito!<b>"
						end if
						%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<hr>
					</td>
				<tr>

				<%
				strSQL = "select a.*, e.sg_estado from (TB_Avaliadores a" _
				& " inner join TB_TrabalhoAvaliadores ta" _
				& " on a.cd_avaliador = ta.cd_avaliador)" _
				& " inner join TB_Estados e on a.cd_estado = e.cd_estado" _
				& " where ta.cd_trabalho = " & request("hcd_trabalho")

				set rsA = server.CreateObject("ADODB.Recordset")
				set rsA = Conn.Execute(strSQL)
				%>
				<tr class="CelulaCorpo">
					<td width="564" colspan="2">
						<b>Avaliadores :&nbsp;</b>
						<%
						if rsA.EOF then
							Response.Write "<br>Nenhum avaliador atribu�do para este trabalho."
						else
							while not rsA.EOF
								Response.Write "<br>� " & rsA("nm_avaliador") & " (" & rsA("de_cidade") & "-" & rsA("sg_estado") & ")"
								rsA.MoveNext
							wend
						end if
						%>
					</td>
				<tr>


				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<hr>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" align="left" colspan="2">
						<b>T�tulo :&nbsp;</b><br>
						<%=replace(rsT("de_titulo"),chr(10),"<br>")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" align="left" colspan="2">
						<b>Autores :&nbsp;</b><br>
						<%=rsT("de_autores")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" align="left" colspan="2">
						<b>Resumo :&nbsp;</b><br>
						<%=rsT("de_sumario")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" align="left" colspan="2">
						<b>Palavras Chaves :&nbsp;</b><br>
						<%=rsT("de_palavrasChaves")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" align="left" colspan="2">
						<b>Refer�ncias Bibliogr�ficas :&nbsp;</b><br>
						<%=rsT("de_referencias")%>
					</td>
				<tr>

				<% 
				if rsT("cd_modo") = 1 then
					%>
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Exames imaginol�gicos :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Exames anatomopatol�gicos :&nbsp;</b><br>
							<%=rsT("de_anatomopatologico")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Outros exames complementares :&nbsp;</b><br>
							<%=rsT("de_outrosExames")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Informa��es sobre o tratamento :&nbsp;</b><br>
							<%=rsT("de_infoTratamento")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Informa��es sobre seguimento :&nbsp;</b><br>
							<%=rsT("de_infoSeguimento")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Diagn�stico ou hip�teses diagn�sticas :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					<%
					select case cint(rsT("cd_dificuldade"))
						case 1
							strDificuldade_wk = "No diagn�stico"
						case 2
							strDificuldade_wk = "No tratamento"
						case 3
							strDificuldade_wk = "Em Ambos (diagn�stico e tratamento)"
					end select
					%>
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Dificuldade :&nbsp;</b><br>
							<%=strDificuldade_wk%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="564" align="left" colspan="2">
							<b>Relev�ncia da apresenta��o do trabalho no congresso :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					
					<%
				end if
				%>


				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<hr>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="564" colspan="2">
						<table width="564">
							<tr class="CelulaCorpo">
								<td width="80" align="center">
									<b>Modo</b><br>
									<%
									select case rsT("cd_modo")
										case 1
											%>Caso Cl�nico Consultivo<%
										case 2
											%>Painel de Pesquisa<%
										case 3
											%>Painel Cl�nico<%
										case 4
											%>Apresenta��o Oral Caso Cl�nico<%
									end select
									%>
								</td>
								<td width="112" align="center">
									<% 
									if rsT("cd_modo") = 4 then 
										%>
										<b>Retirada</b><br>
										<%
										if rsT("cd_naoOral") = 1 then
											%>Sim<%
										else
											%>N�o - Inscrever<br>Painel Consultivo<%
										end if
									end if
									%>
								</td>
								<td width="112" align="center">
									<b>Data Inscri��o</b><br>
									<%
										Response.Write day(rsT("dt_cadastro")) & "/" & month(rsT("dt_cadastro")) & "/" & year(rsT("dt_cadastro"))
									%>
								</td>
								<td width="112" align="center">
									<b>Resultado</b><br>
									<%
									if rsT("dt_resultado") < Cdate("31/12/2029") then
										if rsT("sn_aceito") then
											Response.Write "Aceito"
										else
											Response.Write "N�o Aceito"
										end if
									else
										Response.Write "N�o Avaliado"
									end if
									%>
								</td>
								<td width="116" align="center">
									<b>Data Resultado</b><br>
									<%
									if rsT("dt_resultado") < Cdate("31/12/2029") then
										Response.Write day(rsT("dt_resultado")) & "/" & month(rsT("dt_resultado")) & "/" & year(rsT("dt_resultado"))
									else
										Response.Write "-"
									end if
									%>
								</td>
							<tr>
						</table>
					</td>
				<tr>
				<% if ((rsT("cd_modo") = 1) or (rsT("cd_modo") = 4))  then  %>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<hr><b>Imagens</b><br><br>
						<%
						strSQL = "select de_prefixoImagens from TB_Trabalhos where cd_trabalho = " & request("hcd_trabalho")
						set rs = Conn.Execute(strSQL)
							
						strSQL = "select * from TB_ImagensTrabalhos where cd_trabalho = " & rs("de_prefixoImagens") & " order by nu_seqImagem"
						
						set rs = Conn.Execute(strSQL)
						
						if not rs.eof then
							while not rs.EOF
								%>
								<img src="http://www.tavares.eti.br/upload_admin/estomatologia/<%=rs("cd_trabalho")%>_<%=rs("de_nomeArquivo")%>">
								<br>
								<%=rs("de_legenda")%>
								<hr>
								<%
								rs.MoveNext
							wend
						else
							Response.Write "<br>Nenhuma Imagem encontrada para este trabalho."
						end if
						%>
					</td>
				<tr>
				<% end if %>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
					</td>
				<tr>
			<%
		case 2
			' Editar dados do Trabalho
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
			
			strSQL = "select t.*, p.nm_participante from TB_Trabalhos t" _
			& " left outer join TB_Participantes p on p.cd_participante = t.cd_apresentador" _
			& " where cd_trabalho = " & request("hcd_trabalho")

			set rsT = server.CreateObject("ADODB.Recordset")
			set rsT = Conn.Execute(strSQL)
			
			cd_prefixo = rsT("de_prefixoImagens")
			
			strDisplayPerguntas = "none"
			select case rsT("cd_modo")
				case 1
					tamanhoMaximo = 250
					strDisplayPerguntas = "inline"
				case 2
					tamanhoMaximo = 300
				case 3
					tamanhoMaximo = 600
				case 4
					tamanhoMaximo = 600
			end select
			%>
			<input type=hidden name=tamanhoMaximo value=<%=tamanhoMaximo%>>
			<input type=hidden name=prefixoImagens value=<%=cd_prefixo%>>
			<input type=hidden name=cd_trabalho value=<%=rsT("cd_trabalho")%>>
			
			<table width=600 BORDER=0>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="2">
						EDI��O DOS DADOS DO TRABALHO <%=rsT("cd_trabalho")%>
					</td>
				<tr>

				<tr class=textoComum>
					<td colspan=2>
						&nbsp;<br>
						<b>Informa��es para contato</b><br>
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						Nome completo do respons�vel pela inscri��o<br>
						<input type=text size=76 name=nome class=textbox01 maxlength=100 value="<%=rsT("nm_responsavel")%>">
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						Endere�o completo para Correspond�ncia (incluir cidade, estado e CEP)<br>
						<input type=text size=76 name=endereco class=textbox01 maxlength=250 value="<%=rsT("de_endereco")%>">
					</td>
				</tr>
				<tr class=textoComum>
					<td>
						Telefone<br>
						<input type=text size=30 name=fone class=textbox01 maxlength=20 value="<%=rsT("nr_fone")%>">
					</td>
					<td>
						Fax<br>
						<input type=text size=30 name=fax class=textbox01 maxlength=20 value="<%=rsT("nr_fax")%>">
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						E-mail<br>
						<input type=text size=76 name=email class=textbox01 maxlength=50 value="<%=rsT("de_email")%>">
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						N�mero da Inscri��o do Apresentador do Trabalho:&nbsp;
						<input type=text size=5 name=inscricaoApresentador class=textbox01 value="<%=rsT("cd_apresentador")%>">
					</td>
				</tr>
				
				<tr class=textoComum>
					<td colspan=2 width=100%>
						<table width=100% border=0>
							<tr class=textoComum>
								<td valign=top width=40%>
									&nbsp;<br>
									<b>Op��es de Inscri��o</b><br>
									Modo de apresenta��o desejado:&nbsp;&nbsp;
								</td>
								<td valign=top width=60%>
									&nbsp<br>
									<input <% if rsT("cd_modo") = 1 then %>checked<% end if %> type=radio name=modo value=1 onclick="tamMaximo(1);perguntas(1);">Caso Cl�nico Consultivo<br>
									<input <% if rsT("cd_modo") = 2 then %>checked<% end if %> type=radio name=modo value=2 onclick="tamMaximo(2);">Painel de Pesquisa<br>
									<input <% if rsT("cd_modo") = 3 then %>checked<% end if %> type=radio name=modo value=3 onclick="tamMaximo(3);">Painel Cl�nico<br>
									<input <% if rsT("cd_modo") = 4 then %>checked<% end if %> type=radio name=modo value=4 onclick="tamMaximo(4);">Apresenta��o Oral de Caso Cl�nico
									<input type=hidden name=modoApresentacao value=<%=rsT("cd_modo")%>>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<% 
				strDisplay = "none"
				if rsT("cd_modo") = 4 then 
					strDisplay = "inline"
				end if
				%>
				<tr class=textoComum id=trEmPainel style="display='<%=strDisplay%>'">
					<td colspan=2>
						<input <% if rsT("cd_naoOral") = 1 then %>checked<% end if %> type=radio name=naoOral value=1>&nbsp;&nbsp;<%=strRetirada%><br>
						<input <% if rsT("cd_naoOral") = 2 then %>checked<% end if %> type=radio name=naoOral value=2>&nbsp;&nbsp;<%=strEmPainel%>
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						&nbsp<br><b><%=strTitle%></b> <i>(M�ximo 250 caracteres)</i><br>
						<textarea cols=76 rows=3 name=title class=textarea01 onkeyup="contaCaracteres(this,250);" onkeydown="contaCaracteres(this,250);"><%=rsT("de_titulo")%></textarea>
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						<b><%=strAuthors%></b> <i>(M�ximo 250 caracteres)</i>&nbsp;&nbsp;<a href="javascript:exemploAutores();">ver exemplo de formata��o</a><br>
						<textarea cols=76 rows=3 name=authors class=textarea01 onkeyup="contaCaracteres(this,250);" onkeydown="contaCaracteres(this,250);"><%=rsT("de_autores")%></textarea>
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						<b><%=strSummary%></b> - <i>Palavras:&nbsp;<b><span id=tp>0</span></b>&nbsp;&nbsp;<span id=spanTamMaximo>(M�ximo: <%=tamanhoMaximo%> palavras)</span></i><br>
						<textarea name=summary rows=10 cols=76 class=textarea01 onkeyup="contaPalavras();" onkeydown="contaPalavras();"><%=rsT("de_sumario")%></textarea><br>
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						<b>Palavras-Chave</b> <i>(M�ximo 250 caracteres)</i><br>
						<small>de acordo com o DECs (<a href="http://decs.bvs.br" target=nova>http://decs.bvs.br</a>) ou listas de cabe�alhos de assunto do<br><i>Index to Dental</i> ou <i>Index Medicus</i>. Separados por v�rgula</small>
						<textarea name=keywords rows=2 cols=76 class=textarea01 onkeyup="contaCaracteres(this,250);" onkeydown="contaCaracteres(this,250);"><%=rsT("de_palavrasChaves")%></textarea>
					</td>
				</tr>
				<tr class=textoComum>
					<td colspan=2>
						<b>Refer�ncias Bibliogr�ficas</b><br>
						<small>
							(m�ximo de 10, de acordo com NBR 6023 � ABNT)
							<span id=spReferenciasOral style="display:'none'">
								<br>Para o artigo, n�o h� limite de refer�ncias, devendo ser respeitado apenas o limite de 8 p�ginas dentro da formata��o indicada nas <a href="downloads/modeloArtigoCompleto.doc" target=nova>Normas para Submiss�o do Artigo Completo</a>.	
							</span>
						</small>
						<textarea name=references rows=5 cols=76 class=textarea01><%=rsT("de_referencias")%></textarea>
					</td>
				</tr>
	
				<tr class=textoComum  id=trCasoClinicoConsultivo style="display='<%=strDisplayPerguntas%>'">
					<td colspan=2>
						<br>
						<b>Exames imaginol�gicos? Especificar.</b><br>
						<textarea name=examesImaginologicos rows=3 cols=76 class=textarea01><%=rsT("de_imaginologico")%></textarea>
						<br><br>
						<b>Exames anatomopatol�gicos? Especificar.</b><br>
						<textarea name=examesAnatomopatologicos rows=3 cols=76 class=textarea01><%=rsT("de_anatomopatologico")%></textarea>
						<br><br>
						<b>Outros exames complementares? Especificar.</b><br>
						<textarea name=examesComplementares rows=3 cols=76 class=textarea01><%=rsT("de_outrosExames")%></textarea>
						<br><br>
						<b>Informa��es sobre o tratamento? Especificar.</b><br>
						<textarea name=infoTratamento rows=3 cols=76 class=textarea01><%=rsT("de_infoTratamento")%></textarea>
						<br><br>
						<b>Acompanhamento ou Informa��es sobre o seguimento? Especificar.</b><br>
						<textarea name=infoSeguimento rows=3 cols=76 class=textarea01><%=rsT("de_infoSeguimento")%></textarea>
						<br><br>
						<b>A dificuldade do caso est�:</b><br>
						<input <% if rsT("cd_dificuldade") = 1 then %>checked<% end if %> type=radio name=dificuldade value=1>&nbsp;&nbsp;No diagn�stico<br>
						<input <% if rsT("cd_dificuldade") = 2 then %>checked<% end if %> type=radio name=dificuldade value=2>&nbsp;&nbsp;No tratamento<br>
						<input <% if rsT("cd_dificuldade") = 3 then %>checked<% end if %> type=radio name=dificuldade value=3>&nbsp;&nbsp;Em ambos
						<br><br>
						<b>Qual o diagn�stico ou hip�teses diagn�sticas? </b><br>
						<textarea name=diagnostico rows=3 cols=76 class=textarea01><%=rsT("de_diagnostico")%></textarea>
						<br><br>
						<b>Na sua opini�o qual a relev�ncia da apresenta��o de seu trabalho no congresso? </b><br>
						<textarea name=relevancia rows=3 cols=76 class=textarea01><%=rsT("de_relevancia")%></textarea>
					</td>
				</tr>

				<tr class=textoComum  id=trImagens style="display='inline'">
					<td colspan=2>
						<br>
						<button onclick="javascript:IncluirImagens(<%=cd_prefixo%>);" class=botaoPadrao120 id=button1 name=button1>INCLUIR IMAGENS</button>
					</td>
				</tr>

				<tr class=textoComum>
					<td colspan=2>
						&nbsp;
						<br>
						<input type=button class=botaoPadrao name=enviar value=<%=strEnviar%> onclick="ValidarFormEdicaoTrabalho();">
						&nbsp;
						<input type=button class=botaoPadrao name=voltar value=Voltar onclick="history.go(-1);">
						
					</td>
				</tr>
			</table>			
			<%
		case 8
			cd_trabalho_wk = request("cd_trabalho")
			nome_wk = replace(request("nome"),"'", "''")
			endereco_wk = replace(request("endereco"),"'", "''")
			fone_wk = replace(request("fone"),"'", "''")
			fax_wk = replace(request("fax"),"'", "''")
			email_wk = replace(request("email"),"'", "''")
			InscricaoApresentador_wk = request("inscricaoApresentador")
			modo_wk = request("modoApresentacao")
			select case modo_wk
				case 1
					strModo_wk = "Caso Cl�nico Consultivo"
				case 2
					strModo_wk = "Painel de Pesquisa"
				case 3
					strModo_wk = "Painel Consultivo"
				case 4
					strModo_wk = "Apresenta��o Oral de Caso Cl�nico"
			end select
			naoOral_wk = request("naoOral")
			if naoOral_wk = "" then 
				strNaoOral_wk = "Retirar Trabalho"
			else
				strNaoOral_wk = "Alterar para Painel Cl�nico"
			end if
			title_wk = replace(request("title"),"'", "''")
			authors_wk = replace(request("authors"),"'", "''")
			summary_wk = replace(request("summary"),"'", "''")
			keywords_wk = replace(request("keywords"),"'", "''")
			references_wk = replace(request("references"),"'", "''")

			examesImaginologicos_wk = replace(request("examesImaginologicos"),"'", "''")
			examesAnatomopatologicos_wk = replace(request("examesAnatomopatologicos"),"'", "''")
			examesComplementares_wk = replace(request("examesComplementares"),"'", "''")
			infoTratamento_wk = replace(request("infoTratamento"),"'", "''")
			infoSeguimento_wk = replace(request("infoSeguimento"),"'", "''")
			dificuldade_wk = replace(request("dificuldade"),"'", "''")
			select case dificuldade_wk
				case 1
					strDificuldade_wk = "No diagn�stico"
				case 2
					strDificuldade_wk = "No tratamento"
				case 3
					strDificuldade_wk = "Em Ambos (diagn�stico e tratamento)"
			end select
			diagnostico_wk = replace(request("diagnostico"),"'", "''")
			relevancia_wk = replace(request("relevancia"),"'", "''")

			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn1 = Server.CreateObject("ADODB.Connection") 

			Conn1.Open cnpath
			Conn1.BeginTrans
		
			' 1- Gravar Dados do Trabalho
			strSQL = _
			"UPDATE TB_Trabalhos set " _
			& "nm_responsavel = '" & nome_wk & "', " _
			& "de_endereco = '" & endereco_wk & "', " _
			& "nr_fone = '" & fone_wk & "', " _
			& "nr_fax = '" & fax_wk & "', " _
			& "de_email = '" & email_wk & "', " _
			& "cd_apresentador = " & InscricaoApresentador_wk & ", " _
			& "cd_modo = " & modo_wk & ", " _
			& "cd_naoOral = " & naoOral_wk & ", " _
			& "cd_dificuldade = " & dificuldade_wk & ", " _
			& "de_titulo = '" & title_wk & "', " _
			& "de_autores = '" & authors_wk & "', " _
			& "de_sumario = '" & summary_wk & "', " _
			& "de_palavrasChaves = '" & keywords_wk & "', " _
			& "de_referencias = '" & references_wk & "', " _
			& "de_imaginologico = '" & examesImaginologicos_wk & "', " _
			& "de_anatomopatologico = '" & examesAnatomopatologicos_wk & "', " _
			& "de_outrosExames = '" & examesComplementares_wk & "', " _
			& "de_infoTratamento = '" & infoTratamento_wk & "', " _
			& "de_infoSeguimento = '" & infoSeguimento_wk & "', " _
			& "de_relevancia = '" & diagnostico_wk & "' " _
			& "where cd_trabalho = " & cd_trabalho_wk
		
			'Response.Write strSQL & "<BR>"
			'Response.end

			on error resume next
			Conn1.Execute strSQL

			if err.number = 0 then
				Conn1.CommitTrans
				Response.Write "<h5>Altera��o executada com sucesso!</h5>"
				set Conn1 = nothing
				on error goto 0
			else
				Conn1.RollbackTrans
				set Conn1 = nothing
				Response.Write "<h5>Erro ao gravar inscri��o de trabalho. (" & err.Description & ")</h5>"
			end if

		case 3
			' Remover Trabalhos
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			on error resume next
		
			set rsD = server.CreateObject("ADODB.Recordset")
			strSQL = "Delete from TB_Trabalhos where cd_trabalho = " & request("hcd_trabalho")
			set rsD = Conn.Execute(strSQL )
			on error goto 0
			%>
			<table class="TabelaProgramacao" width="564">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						REMO��O DO TRABALHO N� <%=request("hcd_trabalho")%>
					</td>
				<tr>
			<%
			if err.Number = 0 then
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Removido com sucesso !</h5>
					</td>
				<tr>
				<%
			else
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Problema na remo��o !</h5>
					Imprima esta p�gina e reporte ao suporte t�cnico.<br>			
					<%
					Response.Write "Erro N� " & err.number & "<br>"
					Response.Write err.description
					%>
					</td>
				<tr>
				<%
			end if
			%>
			<tr class="CelulaTitulo">
				<td align="middle" colspan="3">
					<input type="button" name="Voltar" value="Voltar" class="BotaoVerde" onclick="paginar(9);">
				</td>
			<tr>
			<%
		case 4
		
			cd_trabalho_wk = request("hcd_trabalho")
		
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			'Response.Write strSQL
			'Response.End
			
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			'on error resume next
		
			strSQL = "select a.*, ta.vl_media, ta.dt_avaliacao, e.sg_estado" _
			& " from (TB_Avaliadores a" _
			& " inner join TB_TrabalhoAvaliadores ta" _
			& " on a.cd_avaliador = ta.cd_avaliador)" _
			& " inner join TB_Estados e on a.cd_estado = e.cd_estado" _
			& " where ta.cd_trabalho = " & cd_trabalho_wk
			set rsA = server.CreateObject("ADODB.Recordset")
			set rsA = Conn.Execute(strSQL)

			'on error goto 0
			%>
			<table class="TabelaProgramacao" width="100%" border="0">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="4">
						Detalhes da Avalia��o do Trabalho <%=cd_trabalho_wk%>
					</td>
				<tr>

				<%
				qt_avaliadores = 0
				SomaNotas = 0
				if rsA.EOF then
					%>
					<tr class="CelulaCorpo">
						<td width="100%" colspan="4" align="center">
							<h5>Nenhum avaliador atribu�do para este trabalho.</h5>
						</td>
					</tr>
					<%
				else
					%>
					<tr class="CelulaCorpo">
						<td width="65%">
							<b>Nome do Avaliador</b>
						</td>
						<td align="center" width="25%">
							<b>Data da Avalia��o</b>
						</td>
						<td align="center" width="5%">
							<b>Nota</b>
						</td>
						<td align="center" width="5%">
							<b>A��es</b>
						</td>
					<tr>
					<%
					while not rsA.EOF
						if rsA("dt_avaliacao") <> "" then
							qt_avaliadores = qt_avaliadores + 1
							SomaNotas = SomaNotas + cdbl(rsA("vl_media"))
						end if
						%>
						<tr class="CelulaCorpo">
							<td width="65%">
								<%="<br>� " & rsA("nm_avaliador") & " (" & rsA("sg_estado") & ")"%>
							</td>
							<td align="center" width="25%">
								<%
								if rsA("dt_avaliacao") <> "" then
									Response.write day(rsA("dt_avaliacao")) & "/" & month(rsA("dt_avaliacao")) & "/" & year(rsA("dt_avaliacao"))
								else
									Response.Write "-"
								end if
								%>
							</td>
							<td align="center" width="5%">
								<%=rsA("vl_media")%>
							</td>
							<td align="center" width="5%">
								<img SRC="../images/ico_remover.gif" onclick="removerAvaliadorTrabalho(<%=cd_trabalho_wk%>,<%=rsA("cd_avaliador")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Remover Avaliador deste Trabalho" WIDTH="15" HEIGHT="15">
							</td>
						<tr>
						<%
						rsA.MoveNext
					wend
					%>

					<tr class="CelulaCorpo">
						<td width="100%" colspan="4">
							<hr>
						</td>
					</tr>

					<tr class="CelulaCorpo">
						<td align="right" width="70%">
							<b>Avalia��es efetuadas:</b>
						</td>
						<td width="30%" colspan="3">
							<%=qt_avaliadores%>
						</td>
					<tr>

					<tr class="CelulaCorpo">
						<td align="right" width="70%">
							<b>M�dia:</b>
						</td>
						<td width="30%" colspan="2">
							<%
							if SomaNotas = 0 then
								%>
								-
								<%
							else
								Response.Write int((somaNotas/qt_avaliadores)*1000)/1000
							end if 
							%>
						</td>
					<tr>
					<%
				end if
				%>
								
				<tr class="CelulaTitulo">
					<td align="middle" colspan="4">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
						&nbsp;&nbsp;
						<input type="button" name="Incluir Avaliador" value="Incluir Avaliador" class="BotaoPadrao120" onclick="incluirAvaliador(<%=request("hcd_trabalho")%>);">
					</td>
				<tr>
			</table>
			<%
			set rsA = nothing
			set conn = nothing
		case 5
			%>
			<!--#include file=conn.asp -->
			<%
		
			cd_trabalho_wk = request("hcd_trabalho")
		
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			'on error resume next
		
			strSQL = "select cd_avaliador from TB_TrabalhoAvaliadores" _
			& " where cd_trabalho = " & cd_trabalho_wk
			set rsA = server.CreateObject("ADODB.Recordset")
			set rsA = Conn.Execute(strSQL)
			notin = ""
			while not rsA.EOF
				notin = notin & rsA("cd_avaliador")
				rsA.MoveNext
				if not rsA.EOF then
					notin = notin & ", "
				end if
			wend
			
			strSQL = "select * from TB_Avaliadores"
			if len(notin) <> 0 then
				strSQL = strSQL & " where cd_avaliador not in (" & notin & ")"
			end if
			
			set rsA = Conn.Execute(strSQL)
			
			%>
			<table class="TabelaProgramacao" width="100%" border="0">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						Inclus�o de Avaliador para o trabalho <%=cd_trabalho_wk%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" colspan="3" align="center">
						<select name="avaliadores" class="combo01" size="8" MULTIPLE>
							<%
							while not rsA.EOF
								%>
								<option value="<%=rsA("cd_avaliador")%>"><%=rsA("cd_avaliador") & " - " & rsA("nm_avaliador")%></option>
								<%
								rsA.MoveNext
							wend
							%>
						</select>
					</td>
				</tr>

				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="avaliacao(<%=cd_trabalho_wk%>);">
						&nbsp;&nbsp;
						<input type="button" name="Incluir" value="Incluir" class="BotaoPadrao" onclick="executaInclusao(<%=cd_trabalho_wk%>);">
					</td>
				<tr>
			<%
			set rsA = nothing
			set conn = nothing
		case 6
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
		
			cd_trabalho_wk = request("hcd_trabalho")
			str_avaliadores = request("avaliadores")
			
			arr_avaliadores = split(str_avaliadores,",")
			
			set rsA = server.CreateObject("ADODB.Recordset")
			for i = 0 to ubound(arr_avaliadores)
				strSQL = "insert into TB_TrabalhoAvaliadores (cd_trabalho, cd_avaliador) values (" _
				& cd_trabalho_wk & ", " & arr_avaliadores(i) & ")" 
				set rsA = Conn.Execute(strSQL )
			next
			
			on error goto 0
			%>
			<table class="TabelaProgramacao" width="564">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						Inclus�o de Avaliador para o trabalho <%=cd_trabalho_wk%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Inclus�o conclu�da</h5>
					</td>
				<tr>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="avaliacao(<%=cd_trabalho_wk%>);">
					</td>
				<tr>
			</table>
			<%
		case 7
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
		
			cd_trabalho_wk = request("hcd_trabalho")
			cd_avaliador_wk = request("hcd_avaliador")

			strSQL = "delete from TB_TrabalhoAvaliadores where cd_trabalho = " & cd_trabalho_wk _
			& " and cd_avaliador = " & cd_avaliador_wk

			'Response.Write strSQL
			set rsA = Conn.Execute(strSQL )
			%>
			<table class="TabelaProgramacao" width="564">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						Exclus�o de Avaliador para o trabalho <%=cd_trabalho_wk%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Exclus�o conclu�da</h5>
					</td>
				<tr>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="avaliacao(<%=cd_trabalho_wk%>);">
					</td>
				<tr>
			</table>
			<%
			case 9:
			' Listar Imagens do Trabalho
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
			
			strSQL = "SELECT i.* FROM TB_ImagensTrabalhos i INNER JOIN TB_Trabalhos t " _
			& "ON i.cd_trabalho = t.de_prefixoImagens " _
			& "WHERE (((t.cd_trabalho) = " & request("hcd_trabalho") & "));"

			set rsI = server.CreateObject("ADODB.Recordset")
			set rsI = Conn.Execute(strSQL)
			%>
			<input type=hidden name=de_prefixoImagens>
			<input type=hidden name=nu_seqImagem>
			<table width=564 BORDER=0>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="4">
						LISTA DE IMAGENS DO TRABALHO <%=request("hcd_trabalho")%>
					</td>
				<tr>
				<%
				While not rsI.EOF
					%>
					<tr class=textoComum valign=top>
						<td width=10>
							<%=rsI("nu_seqImagem")%>
						</td>
						<td width=200>
							<%=rsI("de_nomeArquivo")%>
						</td>
						<td width=334>
							<%=rsI("de_Legenda")%>
						</td>
						<td width=20>
							<img src="../images/ico_editar.gif" border="0" onclick="editarDadosImagem(<%=request("hcd_trabalho")%>, <%=rsI("cd_trabalho")%>, <%=rsI("nu_seqImagem")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Editar dados de Imagem de Trabalho" WIDTH="19" HEIGHT="15">
						</td>
					</tr>
					<tr class=textoComum valign=top>
						<td width=564 colspan=4>
							<hr>
						</td>
					</tr>
					<%
					rsI.MoveNext
				wend 
				%>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="4">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
					</td>
				<tr>
			</table>
			<%
			case 10
			' Listar Imagens do Trabalho
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
			
			strSQL = "SELECT * FROM TB_ImagensTrabalhos " _
			& " WHERE cd_trabalho = " & request("de_prefixoImagens") _
			& " AND nu_seqImagem = " & request("nu_seqImagem")

			set rsI = server.CreateObject("ADODB.Recordset")
			set rsI = Conn.Execute(strSQL)
			%>
			<input type=hidden name=de_prefixoImagens value=<%=request("de_prefixoImagens")%>>
			<input type=hidden name=nu_seqImagem value=<%=request("nu_seqImagem")%>>
			<table width=564 BORDER=0>
				<tr class="CelulaTitulo">
					<td align="middle">
						EDI��O DE DADOS DA IMAGEM <%=request("nu_seqImagem")%> DO TRABALHO <%=request("hcd_trabalho")%>
					</td>
				<tr>
				<tr class=textoComum>
					<td>
						Nome do arquivo<br>
						<input type=text size=76 name=nome class=textbox01 maxlength=100 value="<%=rsI("de_nomeArquivo")%>">
					</td>
				</tr>
				<tr class=textoComum>
					<td>
						Legenda da imagem<br>
						<textarea name=legenda rows=3 cols=76 class=textarea01><%=rsI("de_legenda")%></textarea>
					</td>
				</tr>
				<tr class="CelulaTitulo">
					<td align="middle">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="cadastroImagens(<%=request("hcd_trabalho")%>);">
						&nbsp;&nbsp;
						<input type="button" name="Incluir" value="Alterar" class="BotaoPadrao" onclick="GravarDadosImagem(<%=request("hcd_trabalho")%>);">
					</td>
				<tr>
			</table>
			<%
			case 11
			' Listar Imagens do Trabalho
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
			
			strSQL = "UPDATE TB_ImagensTrabalhos SET " _
			& " de_nomeArquivo = '" & request("nome") & "', " _
			& " de_legenda = '" & request("legenda") & "'" _
			& " WHERE cd_trabalho = " & request("de_prefixoImagens") _
			& " AND nu_seqImagem = " & request("nu_seqImagem")

			'Response.Write strSQL
			set rsI = Conn.Execute(strSQL)
			%>
			<script>
				cadastroImagens(<%=request("hcd_trabalho")%>);
			</script>
			<%
		end select
		%>
	</table>
</form>