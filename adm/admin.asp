<%@ LANGUAGE=vbscript enablesessionstate=true LCID=1046 %>
<!--#include file="adovbs.inc"-->
<%

Response.Expires = -1
Response.Buffer = true

desenvolvimento = false
if Request.ServerVariables("SERVER_NAME") = "marcelo" then desenvolvimento = true

md_wk = request("md")

if md_wk = "" then 
	md_wk = 0
end if

if session("administracao") = "" then session("administracao") = False
if session("cobranca") = "" then session("cobranca") = False

if (not session("administracao") and (md_wk <> 0 ))then Response.Redirect("admin.asp?md=0") 

it_wk = request("it")
if it_wk = "" then 
	it_wk = 1
end if

pr1_wk = request("pr1")
if pr1_wk = "" then 
	pr1_wk = 0
end if

' -------------- Qual o Navegador ? ---------------
navegador = 0

'response.write Request.ServerVariables("HTTP_USER_AGENT") & "<BR>"

strNetscape = instr(Request.ServerVariables("HTTP_USER_AGENT"), "Netscape")
if strNetscape > 0 then
	'Response.Write "NETSCAPE" & " Vers�o " & mid(Request.ServerVariables("HTTP_USER_AGENT"),Cint(strNetscape)+9,1)
	navegador=2
else
	strMSIE = instr(Request.ServerVariables("HTTP_USER_AGENT"), "MSIE")
	if strMSIE > 0 then
		strOpera = instr(Request.ServerVariables("HTTP_USER_AGENT"), "Opera")
			if strOpera > 0 then
				'Response.Write "Opera" & " Vers�o " & mid(Request.ServerVariables("HTTP_USER_AGENT"), Cint(strOpera)+6,1)
				navegador=3
			else
				'Response.Write "INTERNET EXPLORER" & " Vers�o " & mid(Request.ServerVariables("HTTP_USER_AGENT"), Cint(strMSIE)+5,1)
				navegador=1
			end if
	end if
end if
' -----------------------------------------------------
%>

<html>
<head>
<link REL="stylesheet" HREF="../estilos.css">
<script>
	function submeter(md, it, pr1){
		<% if desenvolvimento then %>
		document.Form1.action = 'admin.asp';
		<% else %>
		document.Form1.action = 'admin.asp';
		<% end if %>
		document.Form1.md.value = md;
		document.Form1.it.value = it;
		document.Form1.pr1.value = pr1;
		document.Form1.submit();
	}

	function acende(elemento){
		elemento.style.cursor='hand';
		elemento.style.color='#ff0000';
	}

	function apaga(elemento){
		elemento.style.cursor='default';
		if (elemento.ativado == 0) 
			elemento.style.color='#000000'
		else
			elemento.style.color='#ff0000'
	}
	
	function acendeSM(elemento){
		elemento.style.cursor='hand';
		elemento.style.fontWeight = 700;
	}

	function apagaSM(elemento){
		elemento.style.cursor='default';
		elemento.style.fontWeight = 400;

	}
	
	function AtivaDesativaSubmenu(sm){
		submenu = document.getElementById('submenu'+sm);
		menu = document.getElementById('menu'+sm);
		
		if (submenu.ativado == 0){
		
			for (i=1; i<=5; i++){
				if ((i!=3)<%if not session("cobranca") then %> && (i!=1)<% end if %>) {
					sm_wk = document.getElementById('submenu'+i);
					m_wk = document.getElementById('menu'+i);
					sm_wk.style.display = 'none';
					sm_wk.ativado = 0;
					if (i != sm){
						m_wk.style.color = '#000000';
					}
					else {
						m_wk.style.color = '#FF0000';
					}
					m_wk.ativado = 0;
				}
			}		
		
			submenu.ativado = 1;
			menu.ativado = 1;
			submenu.style.display='inline';
			menu.style.color = "#ff0000"
		}
		else {
			submenu.ativado = 0;
			menu.ativado = 0;
			submenu.style.display='none';
			menu.style.color = "#FFFFFF";
		}
	}

	function fechaSubmenu(sm){
		document.getElementById('submenu'+sm).className = 'submenu';
	}
	
	
	function load(md){
		if (md == 6) md = 1;
		if (md != 0){
			// Abrir o Submenu
			submenu = 'submenu' + md
			menu = 'menu' + md
			document.getElementById(submenu).style.display = 'inline';
			document.getElementById(menu).style.color='#ff0000';
		}
	}

	function attrAtivado(){
		for (i=1; i<=5; i++) {
			nm_menu = 'menu' + i;
			nm_submenu = 'submenu' + i;
			nm_submenuitem1 = sm + i + 1;
			nm_submenuitem2 = sm + i + 2;
			menu = document.getElementById(nm_menu)
			submenu = document.getElementById(nm_submenu)
			sm1 = document.getElementById(nm_submenuitem1)
			sm2 = document.getElementById(nm_submenuitem2)
			if ((menu.ativado != 0) &&  (menu.ativado != 1)) {
				menu.ativado=0;
				submenu.ativado=0;
				sm1.ativado=0;
				sm2.ativado=0;
			}
		}
		
	}

</script>

</head>
<body leftmargin="0" topmargin="0" onload="<%if navegador <> 1 then %>attrAtivado();<% end if %>load(<%=md_wk%>);"  vlink="#ff0000" alink="#ff0000" link="#ff0000">
	<form name="Form1" method="POST">
	<input name="md" type="hidden" value=<%=md_wk%>>
	<input name="it" type="hidden" value=<%=it_wk%>>
	<input name="pr1" type="hidden" value=<%=pr1_wk%>>
	<table WIDTH="780" ALIGN="left" BORDER="0" CELLSPACING="0" CELLPADDING="0">
		<tr bgcolor=#e8e8e8>
			<td width="780" colspan="2">
				<img height="70" src="../images/ponto.gif" width="10" align="right" border="0">
				<br><center><span class="titulo">XIII Congresso Brasileiro de Estomatologia<br>M�dulo de Administra��o</span></center>
			</td>
		</tr>
		<tr>
			<td width="180" valign="top">
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">
					<tr>
						<td valign="top" height="4000" bgcolor=#EDF0FF>
							<table BORDER="0" CELLSPACING="0" CELLPADDING="0" width="170">
								<tr>
									<td>
										<img height="10" src="../images/ponto.gif" width="1" align="left" border="0">
									</td>
								</tr>
								<% if session("administracao") then %>

								<tr>
									<td class="textoMenu" height="30">
										<span id="menu2" ativado="0" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="AtivaDesativaSubmenu(2);">
											INSCRI��ES
										</span>
										<span id="submenu2" class="submenu" ativado="0">
											<br>
											<span id="sm21" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(2,1,0);">
												Participantes
											</span>
											<br>
											<span id="sm22" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(2,2,0);">
												Part. / P�gina
											</span>
											<br>											
											<span id="sm23" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(2,3,0);">
												Confirma��es
											</span>											
											<br>
											<span id="sm24" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(2,4,0);">
												Lista de E-mails
											</span>											
											<br>
										</span>
									</td>
								</tr>
								
								<% if session("cobranca") then %>
								<tr>
									<td class="textoMenu" height="30">
										<span id="menu1" ativado="0" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="AtivaDesativaSubmenu(1);">
											COBRAN�A
										</span>
										<span id="submenu1" class="submenu" ativado="0">
											<br>
											<span id="sm11" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(6,1,0);">
												Enviar Novo Lote
											</span>
											<br>
											<span id="sm15" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(6,5,0);">
												Re-enviar Lote
											</span>
											<br>
											<span id="sm13" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(6,2,0);">
												Processar Retorno
											</span>
											<br>
											<span id="sm14" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(6,4,0);">
												Confer�ncia
											</span>
										</span>
									</td>
								</tr>
								<% end if %>
								
								<% if false then %>
								<tr>
									<td class="textoMenu" height="30">
										<span id="menu3" ativado="0" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="AtivaDesativaSubmenu(3);">
											BOLETOS
										</span>
										<span id="submenu3" class="submenu" ativado="0">
											<br>
											<span id="sm31" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(3,1,0);">
												1� Vencimento
											</span>
											<br>
											<span id="sm32" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(3,2,0);">
												Imprimir
											</span>
										</span>
									</td>
								</tr>
								
								<% end if %>
								
								<tr>
									<td class="textoMenu" height="30">
										<span id="menu4" ativado="0" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="AtivaDesativaSubmenu(4);">
												TRABALHOS
										</span>
										<span id="submenu4" class="submenu" ativado="0">
											<br>
											<span id="sm41" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(4,1,0);">
												Resumos
											</span>
											<br>
											<span id="sm42" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(4,2,0);">
												Avaliadores
											</span>
											<br>
											<span id="sm43" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(4,3,0);">
												Resumos / P�gina
											</span>
											<br>
											<span id="sm44" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(4,4,0);">
												Avaliadores / P�gina
											</span>
										</span>
									</td>
								</tr>
								
								<% if true then %>
								
								<tr>
									<td class="textoMenu" height="30">
										<span id="menu5" ativado="0" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="AtivaDesativaSubmenu(5);">
												CONSULTAS
										</span>
										<span id="submenu5" class="submenu" ativado="0">
											<br>
											<span id="sm51" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(5,1,0);">
												Total Inscri��es (R$)
											</span>
											<br>
											<span id="sm52" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(5,2,0);">
												Total Recebido (R$)
											</span>
											<br>
											<span id="sm57" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(5,7,0);">
												Inscri��es / Atividades
											</span>
											<br>
											<span id="sm53" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(5,3,0);">
												Trabalhos/Apresentador
											</span>
											<br>
											<span id="sm54" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(5,4,0);">
												Situa��o Avaliadores
											</span>
											<br>
											<span id="sm55" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(5,5,0);">
												Situa��o Trabalhos
											</span>
											<br>
											<span id="sm56" ativado="0" onmouseover="acendeSM(this);" onmouseout="apagaSM(this)" onclick="submeter(5,6,0);">
												Quadro Sin�ptico
											</span>
										</span>
									</td>
								</tr>
								<% 
									end if 
								end if 
								%>
								<tr>
									<td class="textoMenu" height="30">
										<span id="menu9" ativado="0" onmouseover="acende(this);" onmouseout="apaga(this)" onclick="submeter(0,99,0);">
												VOLTAR
										</span>
										<br><br>
										<%if desenvolvimento then Response.Write md_wk & " - " & it_wk & " - " & pr1_wk%>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" height="600" rowspan="2" align=center>
				<!--#include file="strConfirmacao.asp"-->
				<%
				select case md_wk
					case 0
						%><!--#include file="adm_md0.asp"--><%
					case 1
						select case it_wk
							case 1
								%><!--#include file="adm_md1it1.asp"--><%
							case 2
								%><!--#include file="adm_md1it2.asp"--><%
						end select
					case 2
						select case it_wk
							case 1
								%><!--#include file="adm_md2it1.asp"--><%
							case 2
								%><!--#include file="adm_md2it2.asp"--><%
							case 3
								%><!--#include file="adm_md2it3.asp"--><%
							case 4
								%><!--#include file="adm_md2it4.asp"--><%
						end select
					case 3
						select case it_wk
							case 1
								%><!--#include file="adm_md3it1.asp"--><%
							case 2
								%><!--#include file="adm_md3it2.asp"--><%
						end select
					case 4
						select case it_wk
							case 1
								%><!--#include file="adm_md4it1.asp"--><%
							case 2
								%><!--#include file="adm_md4it2.asp"--><%
							case 3
								%><!--#include file="adm_md4it3.asp"--><%
							case 4
								%><!--#include file="adm_md4it4.asp"--><%
						end select
					case 5
						select case it_wk
							case 1
								%><!--#include file="adm_md5it1.asp"--><%
							case 2
								%><!--#include file="adm_md5it2.asp"--><%
							case 3
								%><!--#include file="adm_md5it3.asp"--><%
							case 4
								%><!--#include file="adm_md5it4.asp"--><%
							case 5
								%><!--#include file="adm_md5it5.asp"--><%
							case 6
								%><!--#include file="adm_md5it6.asp"--><%
							case 7
								%><!--#include file="adm_md5it7.asp"--><%
						end select
					case 6
						select case it_wk
							case 1
								%><!--#include file="adm_md6it1.asp"--><%
							case 2
								%><!--#include file="adm_md6it2.asp"--><%
							case 3
								%><!--#include file="adm_md6it3.asp"--><%
							case 4
								%><!--#include file="adm_md6it4.asp"--><%
							case 5
								%><!--#include file="adm_md6it5.asp"--><%
						end select
				end select
				%>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	</form>
</body>
</html>