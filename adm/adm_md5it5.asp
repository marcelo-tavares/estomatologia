<% 
function trabalhosAvaliados(ta)
	if isnull(ta) then
		trabalhosAvaliados = 0
	else
		trabalhosAvaliados = ta
	end if
end function
'response.write "..."
'response.flush
%>

<HTML>
<HEAD>
</HEAD>
<BODY>

<% 
	strSQL= "select s.cd_trabalho, s.qt_total, s.qt_avaliados from (" _
	& "SELECT a.cd_trabalho as cd_trabalho, a.qt_avaliados as qt_avaliados, " _
	& "b.qt_total from ((SELECT ta1.cd_trabalho, Count(ta1.cd_trabalho) AS qt_avaliados " _
	& "FROM TB_TrabalhoAvaliadores ta1 WHERE dt_avaliacao is not null " _
	& "GROUP BY ta1.cd_trabalho) a " _
	& "inner join (SELECT ta2.cd_trabalho, Count(ta2.cd_trabalho) AS qt_total " _
	& "FROM TB_TrabalhoAvaliadores ta2 GROUP BY ta2.cd_trabalho) b " _
	& "on a.cd_trabalho = b.cd_trabalho and a.qt_avaliados =  b.qt_total )) s  " _
	& "where cd_trabalho not in (347, 519,520)"
	
	'Response.Write strSQL
	'Response.Flush
	
	' Conectar com o banco
	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn = Server.CreateObject("ADODB.Connection") 
	Conn.Open cnpath

	set rsP = server.CreateObject("ADODB.Recordset")
	set rsX = server.CreateObject("ADODB.Recordset")
	
	set rsP = Conn.Execute(strSQL)
	
	strTrabalhosAvaliacaoConcluida = ""

	on error resume next

	strIniciados = "("
	i = 0
	while not rsP.EOF
		strTrabalhosAvaliacaoConcluida  = strTrabalhosAvaliacaoConcluida & "� " & rsP("cd_trabalho") & " (" & rsP("qt_avaliados") & "/" & rsP("qt_total") & ") - "
		
		strSQL = "select nm_participante from TB_Participantes p inner join TB_Trabalhos t on p.cd_participante = t.cd_apresentador where t.cd_trabalho = " & rsP("cd_trabalho")
		
		set rsX = Conn.Execute(strSQL)
		
		strTrabalhosAvaliacaoConcluida  = strTrabalhosAvaliacaoConcluida & rsX("nm_participante") & " - "
		
		strSQL = "select * from TB_Parcelas p inner join TB_Trabalhos t on p.cd_participante = t.cd_apresentador where t.cd_trabalho = " & rsP("cd_trabalho")
		set rsX = Conn.Execute(strSQL)
		
		while not rsX.EOF
			if rsX("sn_pago") then
				strIMG = "parc_vd.gif"
			else
				if CDate(rsX("dt_parcela")) > now() then
					strIMG = "parc_am.gif"
				else
					strIMG = "parc_vm.gif"
				end if 
			end if
			strTrabalhosAvaliacaoConcluida  = strTrabalhosAvaliacaoConcluida _
			& "<img src=""../images/" & strIMG & """>"
			rsX.MoveNext
		wend
		strTrabalhosAvaliacaoConcluida  = strTrabalhosAvaliacaoConcluida & "<BR>"

		strIniciados = strIniciados & rsP("cd_trabalho")  & ","
		i = i + 1
		rsP.movenext
	wend
	
	strTrabalhosAvaliacaoNaoConcluida = ""
	
	strSQL= "select s.cd_trabalho, s.qt_total, s.qt_avaliados from (" _
	& "SELECT a.cd_trabalho as cd_trabalho, a.qt_avaliados as qt_avaliados, " _
	& "b.qt_total from ((SELECT ta1.cd_trabalho, Count(ta1.cd_trabalho) AS qt_avaliados " _
	& "FROM TB_TrabalhoAvaliadores ta1 WHERE dt_avaliacao is not null " _
	& "GROUP BY ta1.cd_trabalho) a " _
	& "inner join (SELECT ta2.cd_trabalho, Count(ta2.cd_trabalho) AS qt_total " _
	& "FROM TB_TrabalhoAvaliadores ta2 GROUP BY ta2.cd_trabalho) b " _
	& "on a.cd_trabalho = b.cd_trabalho and a.qt_avaliados <  b.qt_total )) s  " _
	& "where cd_trabalho not in (347, 519,520)"

	set rsP = Conn.Execute(strSQL)
	
	j=0
	while not rsP.EOF
		strTrabalhosAvaliacaoNaoConcluida  = strTrabalhosAvaliacaoNaoConcluida & "� " & rsP("cd_trabalho") & " (" & rsP("qt_avaliados") & "/" & rsP("qt_total") & ")<BR>"
		strIniciados = strIniciados & rsP("cd_trabalho")  & ","
		j = j + 1
		rsP.movenext
	wend
	
	strIniciados = left(strIniciados,len(strIniciados)-1) & ")"

	'Response.Write strIniciados
	
	strTrabalhosAvaliacaoNaoIniciada = ""
	
	strSQL= "select cd_trabalho from TB_Trabalhos where cd_trabalho not in " & strIniciados

	'Response.Write strSQL
	'Response.Flush
	
	set rsP = Conn.Execute(strSQL)
	
	k=0
	while not rsP.EOF
		strTrabalhosAvaliacaoNaoIniciada  = strTrabalhosAvaliacaoNaoIniciada & "� " & rsP("cd_trabalho") & "<BR>"
		k=k+1
		rsP.movenext
	wend
%>

	<table width="550" align=left>
		<tr class="textoComum">
			<td align="center" valign="middle">
				<p class="Titulo" align="center">ADMINISTRA��O DO CONGRESSO</p>
			</td>
		</tr>
		<tr class="textoComum">
			<td align="center" valign="middle">
				<br><b>Situa��o dos Trabalhos (Avalia��o)</b><br><br>
			</td>
		</tr>
		
		<tr class="textoComum">
			<td align="left" valign="top">
				<b>AVALIA��O CONCLU�DA (<%=i%> Trabalhos)</b><br>
				<%=strTrabalhosAvaliacaoConcluida%>
				<br><br>
				<b>AVALIA��O INICIADA MAS N�O CONCLU�DA (<%=j%> Trabalhos)</b><br>
				<%=strTrabalhosAvaliacaoNaoConcluida%>
				<br><br>
				<b>AVALIA��O N�O INICIADA (<%=k%> Trabalhos)</b><br>
				<%=strTrabalhosAvaliacaoNaoIniciada%>
			</td>
		</tr>
	</table>
</BODY>
</HTML>
<%
set rsP = nothing
set conn = nothing
%>