<%
function strConfirmacaoPorInscricao(cd_codigo)

	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn = Server.CreateObject("ADODB.Connection") 
	set rs = server.CreateObject("ADODB.Recordset")
	set rs2 = server.CreateObject("ADODB.Recordset")
	Conn.Open cnpath

	strSQL = "SELECT p.*, e.de_estado, c.de_categoriaPt, c.de_categoriaEn " _
		& "FROM TB_Categoria c, TB_Participantes p, TB_Estados e " _
		& "WHERE p.cd_participante = " & cd_codigo & " " _ 
		& "AND p.cd_estado = e.cd_estado " _
		& "AND c.cd_categoria = p.cd_categoria;"

	set rs = Conn.Execute(strSQL)
	
	if not rs.eof then

		strSQL = "select count(*) as nr_acompanhantes from tb_acompanhantes where cd_participante = " & cd_codigo
		set rs2 = Conn.Execute(strSQL)

		idioma = 1
		if (rs("cd_estado") = 99) then idioma=2
		
		select case rs("tp_pagamento")
			case 1
				if idioma = 1 then
					strTp_pagamento = "Cart�o Visa"
				else
					strTp_pagamento = "Visa"
				end if
			case 2
				if idioma = 1 then
					strTp_pagamento = "Cart�o Mastercard"
				else
					strTp_pagamento = "Mastercard"
				end if
			case 3
				strTp_pagamento = "Cheque Nominal"
			case 4
				strTp_pagamento = "Boleto Banc�rio (envio pelo correio)"
		end select


		if idioma = 1 then
			strAtividades = ""
			if rs("sn_precongresso") then
				strAtividades = strAtividades & "Pr�-Congresso"
			end if
			
			if rs("sn_congresso") then
				if rs("sn_precongresso") then
					strAtividades = strAtividades & " / "
				end if
				strAtividades = strAtividades & "Congresso"
			end if
			
		strOdontogeriatria = ""
		if rs("sn_socioOdontogeriatria") then
			strOdontogeriatria = "S�cio da Sociedade Brasileira de Odontogeriatria<br>" 
		end if
		
			valorR = replace(Cstr(rs("vl_inscricaoR")),".",",")
			reais = int(valorR)
			centavos = Cstr(int ( (valorR - reais) * 100))
			if len(centavos) < 2 then centavos = centavos & "0"
			strPreco = reais & "," & centavos
			
			
			strConfirmacaoPorInscricao =	string(80,"-") & "<BR>" _
			&						"Inscri��o N� " & cd_codigo & "<BR>"  _
			&						string(80,"-") & "<BR>"  _
			&						"Nome      : " & rs("nm_participante") & "<BR>"  _
			&						"Endere�o  : " & rs("de_endereco") & "<BR>"  _
			&						"CEP       : " & rs("cd_cep") & " - " & rs("de_cidade") & " - " & rs("de_estado") & " - " & rs("de_pais") & "<BR>"  _
			&						"Fone      : " & rs("nr_fone") & "<BR>"  _
			&						"E-mail    : " & rs("de_email") & "<BR>"  _
			&						"Fax       : " & rs("nr_fax") & "<BR>"  _
			&						string(80,"-")  & "<BR>"  _
			&						"Categoria : " & rs("de_categoriaPt") & "<BR>"  _
			&						"Atividade : " & strAtividades & "<BR>"  _
			&						strOdontogeriatria  _
			&						string(80,"-")  & "<BR>"  _
			&						"Valor Total da Inscri��o: R$ " & strPreco & "<BR>"  _
			&						"Meio de Pagamento : " & strTp_pagamento & "<BR>" _
			&						string(80,"-")& "<BR>"
		else
			strAtividades = ""
			if rs("sn_precongresso") then
				strAtividades = strAtividades & "Pre-Congress"
			end if
			
			if rs("sn_congresso") then
				if rs("sn_precongresso") then
					strAtividades = strAtividades & " / "
				end if
				strAtividades = strAtividades & "Congress"
			end if
			
			valor = replace(Cstr(rs("vl_inscricao")),".",",")
			dolars =  int(valor)
			cents = Cstr(int ( (valor - dolars) * 100))
			if len(cents) < 2 then cents = cents & "0"
			strPrice = dolars & "." & cents
			
			valorR = replace(Cstr(rs("vl_inscricaoR")),".",",")
			reais = int(valorR)
			centavos = Cstr(int ( (valorR - reais) * 100))
			if len(centavos) < 2 then centavos = centavos & "0"
			strPreco = reais & "," & centavos
			
			strConfirmacaoPorInscricao =	string(80,"-") & "<BR>" _
			&						"Registration N. " & cd_codigo & "<BR>"  _
			&						string(80,"-") & "<BR>"  _
			&						"Name      : " & rs("nm_participante") & "<BR>"  _
			&						"Address   : " & rs("de_endereco") & "<BR>"  _
			&						"ZIP Code  : " & rs("cd_cep") & " - " & rs("de_cidade") & " - " & rs("de_estado") & " - " & rs("de_pais") & "<BR>"  _
			&						"Phone     : " & rs("nr_fone") & "<BR>"  _
			&						"E-mail    : " & rs("de_email") & "<BR>"  _
			&						"Fax       : " & rs("nr_fax") & "<BR>"  _
			&						string(80,"-")  & "<BR>"  _
			&						"Category  : " & rs("de_categoriaEn") & "<BR>"  _
			&						"Activity  : " & strAtividades & "<BR>"  _
			&						"Accompanying Persons:" & rs2("nr_acompanhantes") & "<BR>"  _
			&						string(80,"-")  & "<BR>"  _
			&						"Total : US$" & strPrice & " (R$ " & strPreco & ")<BR>"  _
			&						"Credit Card : " & strTp_pagamento & "<BR>" _
			&						string(80,"-")& "<BR>"
		end if
	else
		strConfirmacaoPorInscricao = "Inscri��o n�o encontrada (Registration not found)."
	end if
	set rs = nothing
	set rs2 = nothing
	set Conn = nothing
end function
%>