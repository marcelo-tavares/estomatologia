<!--#include file=conn.asp -->
<%
server.ScriptTimeout = 300
set conn = server.CreateObject("ADODB.Connection")
set rs = server.CreateObject("ADODB.Recordset")

conn.Open cnpath
	
inicial = 1	
final = 180

strSQL = "SELECT * FROM TB_Avaliadores " _
& "where cd_avaliador in (3,5,7,9,12,14,18,20,21,23,25,28,33,39,40,41,44,46,47,50,52,53,54,55,57,58,59,60,61,63,64,65,67,68,71,72,73,78,80,81,84,85,86,90,91,92,96,97,100,101,102,105,107,108,109,110,111,112,113,115,117,119,125,127,128,130,132,133,136,142,143,144,145,146,147,152,153,155,157,158)" _
& " order by 1"


'& "where cd_avaliador >= " & inicial _
'& " and cd_avaliador <= " & final _

'Response.Write strSQL
'Response.End
set rs = conn.Execute(strSQL)
%>
<!--#include file=../inc_email.asp -->
<%
'e-mail do remetente 
objCDOSYSMail.From = "Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia <sobe2005_ccientifica@tavares.eti.br>"

i=0
j=0
while not rs.EOF
	str_CrLf = chr(13) & chr(10)
	objCDOSYSMail.TextBody = rs("de_email") & str_CrLf _
	& "Prezado(a) colega " & ucase(rs("nm_avaliador")) _
	& str_CrLf & str_CrLf _
	& "A Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia vem solicitar a sua colabora��o para a avalia��o dos trabalhos submetidos para apresenta��o no evento." _
	& str_CrLf & str_CrLf _
	& "O processo de avalia��o funcionar� da seguinte forma: cada trabalho ser� enviado a tr�s avaliadores, que dever�o preencher uma ficha de avalia��o, pontuando separadamente cada item. A m�dia de pontua��o das tr�s avalia��es ser� utilizada como par�metro para a aprova��o, sendo que as maiores m�dias ocupar�o, de forma decrescente, os espa�os dispon�veis para apresenta��o."  _
	& str_CrLf & str_CrLf _
	& "Todo o processo de avalia��o ser� online, a partir da p�gina do evento." _
	& str_CrLf & str_CrLf _
 	& "Os trabalhos enviados para avalia��o podem pertencer a 4 categorias:" & str_CrLf _
	& "- APRESENTA��O ORAL DE CASOS CL�NICOS: esta atividade permite ao grupo apresentador compartilhar sua experi�ncia cl�nica no diagn�stico e tratamento de doen�as da boca. Os casos apresentados oralmente precisam ter relev�ncia a ponto de interessar profissionais com experi�ncia no diagn�stico de les�es bucais." & str_CrLf _
	& "- CASOS CL�NICOS CONSULTIVOS: aqui a finalidade � a discuss�o de casos cl�nicos de dif�cil diagn�stico ou tratamento. O objetivo � auxiliar na elucida��o ou no andamento do caso apresentado." & str_CrLf _ 
	& "- PAINEL DE PESQUISA: apresenta��o de trabalhos de pesquisa originais, demonstrando metodologia cient�fica e incluindo an�lise estat�stica apropriada." & str_CrLf _ 
	& "- PAINEL CL�NICO: casos cl�nicos completos (com diagn�stico final)." _
	& str_CrLf & str_CrLf _
	& "Seguem as instru��es espec�ficas para uso do sistema de avalia��o:" & str_CrLf _
	& "1) Acessar o site do congresso (www.estomatologia.com.br/congresso2005)." & str_CrLf _
	& "2) Clicar no menu ""TRABALHOS"" e selecionar o submenu ""Avalia��o (Restrito)""." & str_CrLf _
	& "3) No campo ""Senha de Acesso"" informar a senha " & rs("de_senha") & " e pressionar o bot�o ""Enviar""." & str_CrLf _
	& "4) Caso seja seu primeiro acesso ao Sistema de Avalia��o, este lhe indagar� se aceita avaliar os trabalhos que lhe foram atribuidos." & str_CrLf _
   	& "    - Clique no bot�o ""Aceito"" para concordar em avaliar os trabalhos ou em ""N�o aceito"" para recusar." & str_CrLf _
   	& "    - Caso tenha aceitado, pressione o bot�o ""Entrar"" para ver o Menu. Caso contr�rio pressione ""Voltar"" para retornar ao site do Congresso" & str_CrLf _
   	& "    - Obs.: Nos acessos subsequentes, o Sistema de Avalia��o verificar� se voc� aceitou anteriormente avaliar os trabalhos e lhe dar� a permiss�o de entrada." & str_CrLf _
	& "5) No Menu ""Trabalhos"" do Sistema de Avalia��o, clique em ""RESUMOS"" para visualizar a lista dos trabalhos designados para voc� avaliar." & str_CrLf _
	& "6) Para visualizar um trabalho clique no �cone LUPA (Visualizar Trabalho) do trabalho desejado. Os detalhes (como t�tulo, resumo, etc., al�m de imagens, quando for o caso) lhe ser�o apresentados." & str_CrLf _
	& "7) Para efetuar a avalia��o, clique no link ""Abrir Formul�rio de Avalia��o"" que aparece na barra superior do trabalho, acima do T�tulo. O formul�rio possui duas partes: uma rela��o de itens a serem pontuados de 1 a 10 e uma �rea onde voc� dever� informar sua recomenda��o, justificativas e observa��es/sugest�es. Ap�s preencher o formul�rio, pressione o bot�o ""Registrar Avalia��o""."  _
	& str_CrLf & str_CrLf _
	& "Para viabilizar a confec��o dos Anais, o per�odo de avalia��o encerra-se no dia 12 de maio de 2005, e at� esta data voc� poder� atualizar/modificar suas avalia��es, acessando novamente o resumo. Solicitamos a gentileza de acessar o site nos primeiros dias, informando se aceita ou n�o ser avaliador, pois caso n�o aceite, os trabalhos precisam ser redirecionados a outros avaliadores."  _
	& str_CrLf & str_CrLf _
	& "Agradecemos antecipadamente a sua colabora��o, de extrema import�ncia para o nosso evento." _
	& str_CrLf & str_CrLf _
	& "Atenciosamente," & str_CrLf _
	& "Profa. Maria In�s Meurer " & str_CrLf _
	& "Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia"
	'& str_CrLf & str_CrLf _
	'& rs("de_email")
	
	'assunto da mensagem 
	objCDOSYSMail.Subject = "Convite para avalia��o de trabalhos do XIII Congresso Brasileiro de Estomatologia"

	'e-mail do destinat�rio 
	'strTo = rs("de_email")
	'strTo = """" & rs("nm_avaliador") & """ <" & rs("de_email") & ">"
	strTo = """" & rs("nm_avaliador") & """ <emana@brturbo.com.br>"
	objCDOSYSMail.To = strTo

	strBcc = """" & rs("nm_avaliador") & """ <marcelo@tavares.eti.br>"
	objCDOSYSMail.Bcc = strBcc
	
	'objCDOSYSMail.fields.update
	'envia o e-mail 
	
	on error resume next
	objCDOSYSMail.Send
	if err.number = 0 then 
		Response.Write "Mensagem enviada para (" & rs("cd_avaliador") & ") " & rs("nm_avaliador") & "<BR>"
		Response.Flush
		i = i + 1
	else
		Response.Write "<hr>ERRO enviando Mensagem para (" & rs("cd_avaliador") & ") " & rs("nm_avaliador") & " - " & rs("de_email") & "<BR>"
		Response.Write err.Description & "<hr>"
		Response.Flush
		j=j+1
	end if
	on error goto 0
	rs.MoveNext
wend 

Response.Write "Enviadas " & i & " mensagens.<br>"
Response.Write "Erro em " & j & " mensagens."
'destr�i os objetos 
Set objCDOSYSMail = Nothing 
Set objCDOSYSCon = Nothing 
%>