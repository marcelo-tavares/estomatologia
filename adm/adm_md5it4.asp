<% 
function trabalhosAvaliados(ta)
	if isnull(ta) then
		trabalhosAvaliados = 0
	else
		trabalhosAvaliados = ta
	end if
end function
%>

<HTML>
<HEAD>
</HEAD>
<BODY>

<% 

	strSQL= "SELECT a.cd_avaliador, a.nm_avaliador, a.de_cidade, a.cd_aceitouAvaliar, " _
	& "e.de_estado, e.sg_estado, qtt.qt_trabalhos, qta.qt_trabalhosAvaliados " _
	& "FROM (((TB_Avaliadores a " _
	& "INNER JOIN TB_Estados e ON a.cd_estado = e.cd_estado) " _
	& "LEFT OUTER JOIN " _
	& "(SELECT cd_avaliador, Count(cd_trabalho) AS qt_trabalhos FROM TB_TrabalhoAvaliadores GROUP BY cd_avaliador) qtt " _
	& "ON qtt.cd_avaliador = a.cd_avaliador) " _
	& "LEFT OUTER JOIN " _
	& "(SELECT cd_avaliador, Count(cd_trabalho) AS qt_trabalhosAvaliados FROM TB_TrabalhoAvaliadores WHERE dt_avaliacao is not null GROUP BY cd_avaliador) qta " _
	& "ON qta.cd_avaliador = a.cd_avaliador) " _
	& " where a.cd_aceitouAvaliar = 1 " _ 
	& " order by 2"
	
	' Conectar com o banco
	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn = Server.CreateObject("ADODB.Connection") 
	Conn.Open cnpath

	set rsP = server.CreateObject("ADODB.Recordset")
	set rsP = Conn.Execute(strSQL)
	
	strAvaliadoresConcluidos = ""
	strAvaliadoresNaoConcluidos = ""

	i = 0
	j = 0
	while not rsP.EOF
		if ( rsP("qt_trabalhos") = trabalhosAvaliados(rsP("qt_trabalhosAvaliados"))) then
			strAvaliadoresConcluidos  = strAvaliadoresConcluidos & "� " & ucase(rsP("nm_avaliador")) & "<BR>"
			i = i + 1
		else
			strAvaliadoresNaoConcluidos = strAvaliadoresNaoConcluidos & "� " & ucase(rsP("nm_avaliador")) & "<BR>"
			j = j + 1
		end if
		rsP.movenext
	wend
	
	strSQL = "select * from TB_Avaliadores where cd_aceitouAvaliar = 0"
	set rsP = Conn.Execute(strSQL)
	
	strAvaliadoresNaoEntraram = ""

	k = 0
	while not rsP.EOF
		strAvaliadoresNaoEntraram  = strAvaliadoresNaoEntraram & "� " & ucase(rsP("nm_avaliador")) & "<BR>"
		k = k + 1
		rsP.movenext
	wend

	strSQL = "select * from TB_Avaliadores where cd_aceitouAvaliar = 2"
	set rsP = Conn.Execute(strSQL)
	
	strAvaliadoresNaoAceitaram = ""

	m = 0
	while not rsP.EOF
		strAvaliadoresNaoAceitaram  = strAvaliadoresNaoAceitaram & "� " & ucase(rsP("nm_avaliador")) & "<BR>"
		m = m + 1
		rsP.movenext
	wend

%>

	<table width="550" align=left>
		<tr class="textoComum">
			<td align="center" valign="middle">
				<p class="Titulo" align="center">ADMINISTRA��O DO CONGRESSO</p>
			</td>
		</tr>
		<tr class="textoComum">
			<td align="center" valign="middle">
				<br><b>Situa��o dos Avaliadores</b><br><br>
			</td>
		</tr>
		
		<tr class="textoComum">
			<td align="left" valign="top">
				<b>Avalia��o Conclu�da (<%=i%> avaliadores)</b><br>
				<%=strAvaliadoresConcluidos%>
				<br><br>
				<b>Avalia��o N�o Conclu�da (<%=j%> avaliadores)</b><br>
				<%=strAvaliadoresNaoConcluidos%>
				<br><br>
				<b>N�o informaram se aceitam avaliar (<%=k%> avaliadores)</b><br>
				<%=strAvaliadoresNaoEntraram%>
				<br><br>
				<b>N�o Aceitaram (<%=m%> avaliadores)</b><br>
				<%=strAvaliadoresNaoAceitaram  %>
			</td>
		</tr>
	</table>
</BODY>
</HTML>
<%
set rsP = nothing
set conn = nothing
%>