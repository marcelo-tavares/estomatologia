<!--#include file=conn.asp -->
<%
Function dvModuloOnze(campo)

	varLen = Len(campo)
	soma = 0

	peso = 1
	for i=1 to varLen
		peso = peso + 1
		if peso = 10 then
			peso = 2
		end if
	next

	for i=1 to varLen
		varInt = Cint(Mid(campo,i,1))
		varInt = varInt * peso
		soma = soma + varInt
		peso = peso - 1
		if peso = 1 then
			peso = 9
		end if
	next

	resto = soma - Int(soma/11)*11
	dv = 11 - resto

	if (dv = 11) then
		dv = 1
	end if

	dvModuloOnze = dv
End Function

set conn = server.CreateObject("ADODB.Connection")
Conn.Open cnpath
set rs = server.CreateObject("ADODB.RecordSet")
set rs = conn.Execute("UltimoLote")
nr_lote = rs("ultimoLote") + 1
strnr_lote = string(7-len(Cstr(nr_lote)),"0") & Cstr(nr_lote)
set rs = conn.Execute("ParcelasParaEnvioCobranca")


' Cria��o do Registro 01 - HEADER
cd_registro = "01" '2
cd_cedente = "02916"
nu_cnpjCedente = "46367215000146"
cd_carteira = "18" '2
strBrancos6 = "      " '6
nm_cedente = "Sociedade Brasileira de Estomatologia        " '45
sg_cedente = "029165-A  " '10
tp_impressao = "02" '2
de_enderecoDev = "Rua Deputado Ant�nio Edu Vieira, 1000 - Pantanal            " '60
nu_cepDev = "88000000" '8
de_pracaUFDev = "Florianopolis - SC  " '20
nu_seqRemessa = string(7-len(Cstr(nr_lote)),"0") & Cstr(nr_lote) '7
de_ene = "N" '1
strBrancos68 = string(68," ")

strRegistro01 = cd_registro & cd_cedente & nu_cnpjCedente & cd_carteira _
	& strBrancos6 & nm_cedente & sg_cedente & tp_impressao _
	& de_enderecoDev & nu_cepDev & de_pracaUFDev _
	& nu_seqRemessa & de_ene & strBrancos68

' Cria��o do Registro 02 - Instru��es Fixas Para Ficha de Compensa��o
cd_registro = "02" '2
tp_fonte01 = "1" '1
tp_fonte02 = "1" '1
tp_fonte03 = "1" '1
tp_fonte04 = "1" '1
de_instrucao01 = "N�o cobrar multa e juros de mora                            "
de_instrucao01 = "Em caso de atraso cobrar multa de 2% e juros de 0,33% a.d.  "
de_instrucao02 = string(60," ")
de_instrucao03 = string(60," ")
de_instrucao04 = string(60," ")
strBrancos4 = "    "

strRegistro02 = cd_registro & tp_fonte01 & tp_fonte02 & tp_fonte03 & tp_fonte04 _
	& de_instrucao01 & de_instrucao02 & de_instrucao03 & de_instrucao04 & strBrancos4

' Cria��o do Registro 03 - Instru��es Fixas Para Recibo do Sacado
'cd_registro = "03" '2
'tp_fonte01 = "1" '1
'tp_fonte02 = "1" '1
'tp_fonte03 = "1" '1
'de_instrucao01 = "XIII Congresso Brasileiro de Estomatologia" & string(38," ")
'de_instrucao02 = string(80," ")
'de_instrucao03 = string(80," ")
'strBrancos5 = "     "

'strRegistro03 = cd_registro & tp_fonte01 & tp_fonte02 & tp_fonte03 _
'	& de_instrucao01 & de_instrucao02 & de_instrucao03 & strBrancos5

strLote = strRegistro01 & chr(13) & chr(10) & strRegistro02 & chr(13) & chr(10) 
contaRegistros = 2
existemRegistros = 0
while not rs.EOF
	existemRegistros = 1
	dataInscricao = "Inscri��o efetuada em " & day(rs("dt_inscricao")) & "/" & month(rs("dt_inscricao")) & "/" & year(rs("dt_inscricao"))
	
	parcela = "Parcela " & rs("cd_parcela") & "/" & rs("parcelas")
	
	categoria = "Categoria: "
	select case rs("cd_categoria")
		case 1
			categoria = categoria & "S�CIO DA SOBE"
		case 2
			categoria = categoria & "N�O S�CIO"
		case 3
			categoria = categoria & "ESTUDANTE DE P�S-GRADUA��O"
		case 4
			categoria = categoria & "ACAD�MICO DE GRADUA��O"
		case 5
			categoria = categoria & "S�CIO da ABRO"
		case 6
			categoria = categoria & "S�CIO DO CBCB"
		case 7
			categoria = categoria & "S�CIO DA SBOG"
	end select

	atividades = "Atividades da Inscri��o:"
	if rs("sn_precongresso") then atividades = atividades & " Pr�-Congresso" 
	if rs("sn_congresso") then atividades = atividades & " Congresso" 
	
	' Cria��o do primeiro Registro 05 - Instru��es Espec�ficas do Recibo do Sacado
	cd_registro = "05" '2
	tp_fonte01 = "1" '1
	tp_fonte02 = "1" '1
	tp_fonte03 = "1" '1
	tp_fonte04 = "1" '1
	de_instrucao01 = dataInscricao & string(60-len(dataInscricao)," ")
	de_instrucao02 = parcela & string(60-len(parcela)," ")
	de_instrucao03 = categoria & string(60-len(categoria)," ")
	de_instrucao04 = atividades & string(60-len(atividades)," ")
	strBrancos5 = "    "
	strRegistro05 = cd_registro & tp_fonte01 & tp_fonte02 & tp_fonte03 & tp_fonte04 _
		& de_instrucao01 & de_instrucao02 & de_instrucao03 & de_instrucao04 & strBrancos5
	strLote = strLote & strRegistro05 & chr(13) & chr(10)
	contaRegistros = contaRegistros + 1
		
	' Cria��o do Registro 11 - Dados do T�tulo
	cd_registro = "11" '2
	tp_docSacado = "3"
	nr_docSacado = "000000000000000"
	nm_sacado = rs("nm_participante") & string(60-len(rs("nm_participante"))," ")
	de_endereco = rs("de_endereco") & string(60-len(rs("de_endereco"))," ")
	
	cd_CEP = replace(replace(rs("cd_CEP"),".",""),"-","")
	if len(cd_CEP) > 8 then
		cd_CEP = left(cd_CEP,5) & right(cd_CEP,3)
	else
		if len(cd_CEP) < 8 then
			cd_CEP = cd_cep & string(8-len(cd_CEP),"0")
		end if
	end if
	
	de_pracaUF = left(trim(rs("de_cidade")),18)
	de_pracaUF = de_pracaUF & string(18-len(de_pracaUF)," ")
	de_pracaUF = de_pracaUF & rs("sg_estado") 
	
	dt_DiaEmissao = day(now)
	dt_DiaEmissao = string(2-len(dt_DiaEmissao),"0") & dt_DiaEmissao
	dt_MesEmissao = month(now)
	dt_MesEmissao = string(2-len(dt_MesEmissao),"0") & dt_MesEmissao
	dt_AnoEmissao = right(year(now),2)
	dt_emissao = dt_DiaEmissao & dt_MesEmissao & dt_AnoEmissao
	
	dt_DiaVencimento = day(rs("dt_parcela"))
	dt_DiaVencimento = string(2-len(dt_DiaVencimento),"0") & dt_DiaVencimento
	dt_MesVencimento = month(rs("dt_parcela"))
	dt_MesVencimento = string(2-len(dt_MesVencimento),"0") & dt_MesVencimento
	dt_AnoVencimento = right(year(rs("dt_parcela")),2)
	dt_Vencimento = dt_DiaVencimento & dt_MesVencimento & dt_AnoVencimento
	
	de_ene = "N"
	de_especieTitulo = "DS"
	cd_cedente = "02916"
	nu_seqCedente = rs("cd_participante") & rs("cd_parcela")
	nu_seqCedente = string(12-len(nu_seqCedente),"0") & nu_seqCedente
	nr_nossoNumero = cd_cedente & nu_seqCedente
	
	nr_tituloCedente = string(10,"0")
	de_zeronove = "09"
	de_zeros = string(15,"0")
	vl_titulo = Cstr(int(rs("vl_parcela") * 100))
	vl_titulo = string(15-len(vl_titulo),"0") & vl_titulo
	strBrancos10 = string(10," ")
	
	strRegistro11 = cd_registro & tp_docSacado & nr_docSacado & nm_sacado & de_endereco _
		& cd_CEP & de_pracaUF & dt_emissao & dt_Vencimento & de_ene _
		& de_especieTitulo & nr_nossoNumero & nr_tituloCedente _
		& de_zeronove & de_zeros & vl_titulo & strBrancos10
	strLote = strLote & strRegistro11 & chr(13) & chr(10)
	contaRegistros = contaRegistros + 1
		
	rs.MoveNext
wend

' Cria��o do Registro 99- Trailer
cd_registro = "99" '2
qt_registros = string(15-len(Cstr(contaRegistros)),"0") & Cstr(contaRegistros)
strBrancos233 = string(233," ")
strRegistro99= cd_registro & qt_registros & strBrancos233 & chr(13) & chr(10)
strLote = strLote & strRegistro99

if existemRegistros = 1 then

	'cria o objeto para o envio de e-mail 
	Set objCDOSYSMail = Server.CreateObject("CDO.Message")

	'cria o objeto para configura��o do SMTP 
	Set objCDOSYSCon = Server.CreateObject ("CDO.Configuration")

	'SMTP 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp2.locaweb.com.br"

	'porta do SMTP 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25

	'porta do CDO 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2

	'timeout 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 

	objCDOSYSCon.Fields.update 

	'atualiza a configura��o do CDOSYS para o envio do e-mail 
	Set objCDOSYSMail.Configuration = objCDOSYSCon

	'e-mail do remetente 
	objCDOSYSMail.From = "XIII_CBE@tavares.eti.br"

	'e-mail do destinat�rio 
	objCDOSYSMail.To = "marcelo@tavares.eti.br"
	'objCDOSYSMail.To = "sobefloripa2005@ccs.ufsc.br"

	'assunto da mensagem 
	objCDOSYSMail.Subject = "SOBE - Lote de Cobran�a n� " & strnr_lote

	'conte�do da mensagem 
	objCDOSYSMail.TextBody = strLote
	'para envio da mensagem no formato html altere o TextBody para HtmlBody 
	'objCDOSYSMail.HtmlBody = "Teste do componente CDOSYS"

	'objCDOSYSMail.fields.update
	'envia o e-mail 
	objCDOSYSMail.Send 

	'destr�i os objetos 
	Set objCDOSYSMail = Nothing 
	Set objCDOSYSCon = Nothing 

	response.write "<br><br><h5>Lote de Cobran�a n� " & strnr_lote & " Enviado para a Secretaria por e-mail.</h5>"

	strSQL = "update TB_Parcelas set nr_loteCobranca = " & nr_lote & " where nr_loteCobranca = 0"

	conn.Execute(strSQL)
else
	%>
	<br><br>
	<h5>Todos os t�tulos j� foram remetidos.</h5>
	<%
end if

set rs = nothing
set conn = nothing
%>