<!--#include file=conn.asp -->
<%
set conn = server.CreateObject("ADODB.Connection")
set rs = server.CreateObject("ADODB.Recordset")

conn.Open cnpath

strSQL = "select nm_participante, de_email from TB_Participantes where cd_participante in (57,196,201,202,204,205,206,207,208,209,210,211,212,213,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,231,232,233,234,235,236,237,238,240,241)"

set rs = conn.Execute(strSQL)
%>
<!--#include file=../inc_email.asp -->
<%
'e-mail do remetente 
objCDOSYSMail.From = "SOBE 2005<XIII_CBE@tavares.eti.br>"

'assunto da mensagem 
objCDOSYSMail.Subject = "SOBE 2005 - Inscri��o de Trabalhos"

'conte�do da mensagem 
str_CrLf = chr(13) & chr(10)
objCDOSYSMail.TextBody = "Prezados Congressistas" _
& str_CrLf & str_CrLf _
& "O sistema de inscri��o de trabalhos no XIII Congresso Brasileiro de Estomatologia enfrentou alguns problemas t�cnicos no dia 30/03, havendo a possibilidade da n�o efetiva��o da inscri��o em alguns casos. A Comiss�o Cient�fica confirmou, atrav�s de mail, todos os trabalhos inscritos. Assim, se voc� inscreveu um trabalho e n�o recebeu confirma��o, por favor entre em contato com a Comiss�o Cient�fica (sobe2005_ccientifica@tavares.eti.br), para que possamos verificar a situa��o." _
& str_CrLf & str_CrLf _
& "Atenciosamente," & str_CrLf _
& "Profa. Maria In�s Meurer " & str_CrLf _
& "Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia"
'para envio da mensagem no formato html altere o TextBody para HtmlBody 
'objCDOSYSMail.HtmlBody = "Teste do componente CDOSYS"
i=0
while not rs.EOF
	if rs("de_email") <> "" then
		'e-mail do destinat�rio 
		strTo = rs("nm_participante") & "<" & rs("de_email") & ">"
		objCDOSYSMail.To = strTo

		'objCDOSYSMail.fields.update
		'envia o e-mail 
		objCDOSYSMail.Send 
		
		Response.Write "Mensagem enviada para " & rs("nm_participante") & "<BR>"
		Response.Flush
		i = i + 1
	end if
	rs.MoveNext
wend

Response.Write "Enviadas " & i & " mensagens."
'destr�i os objetos 
Set objCDOSYSMail = Nothing 
Set objCDOSYSCon = Nothing 
%>