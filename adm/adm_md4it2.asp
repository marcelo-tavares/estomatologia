<% 
function trabalhosAvaliados(ta)
	if isnull(ta) then
		trabalhosAvaliados = 0
	else
		trabalhosAvaliados = ta
	end if
end function
%>

<script>
	function ordenar(sentido, ordem) {
		document.Form1.h_ordem.value = ordem;
		document.Form1.h_sentido.value = sentido;
		submeter(4,2,0);
	}
			
	function editar(cd_avaliador) {
		document.Form1.hcd_avaliador.value = cd_avaliador;
		submeter(4,2,1);
	}

	function enviar(cd_avaliador) {
		document.Form1.hcd_avaliador.value = cd_avaliador;
		submeter(4,2,2);
	}
	
	function remover(cd_avaliador) {
		if (confirm('Confirma remo��o do avaliador ' + cd_avaliador + ' ?')){
			document.Form1.hcd_avaliador.value = cd_avaliador;
			submeter(4,2,3);
		}
	}
	
	function incluirNovo() {
			submeter(4,2,5);
	}

	function enviarNovo() {
		submeter(4,2,6);
	}

	function listaTrabalhosAvaliador(cd_avaliador){
		document.Form1.hcd_avaliador.value = cd_avaliador;
		submeter(4,2,7);
	}
		
	function paginar(codPag){
		document.Form1.paginacao.value = codPag;
		submeter(4,2,0);
	}
</script>
<p class="Titulo" align="center">AVALIADORES</p>
<form name="Form1" method="Post">
	<input type="hidden" name="h_ordem" value="<%=ordenacao_wk%>">
	<input type="hidden" name="h_sentido">
	<input type="hidden" name="paginacao">
	<input type="hidden" name="hcd_avaliador">
	<%
	select case pr1_wk
		case 0		
		
		' Conectar com o banco
	%>
	<!--#include file=conn.asp -->
	<%
		Set Conn = Server.CreateObject("ADODB.Connection") 
		Conn.Open cnpath

		set rsD = server.CreateObject("ADODB.Recordset")
		set rsD = Conn.Execute("Select vl_descricao from TB_Config where cd_codigo = 6")
		tm_pagina_wk = cdbl(rsD("vl_descricao"))
		%>
		
			<table BORDER="0" CELLSPACING="0" CELLPADDING="0" class="TabelaProgramacao" width="100%">
				<tr>
					<td>
					<%
					' Recuperar vari�veis de formul�rio
					ordenacao_wk = request("h_ordem")
					if ordenacao_wk = "" then ordenacao_wk = 1
							
					strSentido = " ASC"		
					sentido_wk = request("h_sentido")
					if sentido_wk = "1" then strSentido = " DESC"
							
					filtroNome_wk = request("filtroNome")
					filtroInscricao_wk = request("filtroInscricao")
					filtroCat_wk = request("filtroCategoria")
					filtroMp_wk = request("filtroMeioPagamento")
					filtroSiglaEstado_wk = request("filtroSiglaEstado")
					select case pr1_wk
						case 0
							' Tratamento de Paginacao
							paginacao_wk = request("paginacao")
							if paginacao_wk = "" then paginacao_wk = 0
								
							select case paginacao_wk
								case 0
									session("paginaAtual")=1
								case 1
									session("paginaAtual")=1
								case 2
									if session("paginaAtual") > 1 then
										session("paginaAtual") = session("paginaAtual") - 1
									end if
								case 3
									if session("paginaAtual") < session("tp") then
										session("paginaAtual") = session("paginaAtual") + 1
									end if			
								case 4
									session("paginaAtual") = session("tp")
							end select
							
							' Criar o recordSet
							set rsP = server.CreateObject("ADODB.RecordSet")	
								
							sqlStr = "SELECT a.cd_avaliador, a.nm_avaliador, a.de_cidade, a.cd_aceitouAvaliar, " _
							& "e.de_estado, e.sg_estado, qtt.qt_trabalhos, qta.qt_trabalhosAvaliados " _
							& "FROM (((TB_Avaliadores a " _
							& "INNER JOIN TB_Estados e ON a.cd_estado = e.cd_estado) " _
							& "LEFT OUTER JOIN " _
							& "(SELECT cd_avaliador, Count(cd_trabalho) AS qt_trabalhos FROM TB_TrabalhoAvaliadores GROUP BY cd_avaliador) qtt " _
							& "ON qtt.cd_avaliador = a.cd_avaliador) " _
							& "LEFT OUTER JOIN " _
							& "(SELECT cd_avaliador, Count(cd_trabalho) AS qt_trabalhosAvaliados FROM TB_TrabalhoAvaliadores WHERE dt_avaliacao is not null GROUP BY cd_avaliador) qta " _
							& "ON qta.cd_avaliador = a.cd_avaliador) " 
							
							jaWhere = false
							if filtroNome_wk <> "" then
								sqlStr = sqlStr & " WHERE a.nm_avaliador like '" & filtroNome_wk & "%'"
								jaWhere = true
							end if 

							if filtroSiglaEstado_wk <> "" then
								if jaWhere then
									sqlStr = sqlStr & " AND"
								else
									sqlStr = sqlStr & " WHERE"
								end if
								sqlStr = sqlStr & " e.sg_estado = '" & filtroSiglaEstado_wk & "'"
							end if 

							sqlStr = sqlStr & " ORDER BY " & ordenacao_wk & strSentido
							
							sqlStr = sqlStr & ";"
							
							
							'Response.Write sqlStr
							'Response.Flush
							
							' pagina��o
							rsP.PageSize = tm_pagina_wk
							rsP.CacheSize = 5
							rsP.Open sqlStr, Conn, adOpenKeyset
							totalRegistros_wk = rsP.recordcount
							totalPaginas_wk = CInt(rsP.PageCount)
							tamanhoPagina_wk = rsP.PageSize
							session("tp") = totalPaginas_wk
							reg=0	
								
							%>
							<table width="100%" border="0">
								<tr class="CelulaCorpo">
									<b>
										<td width="100%" colspan="6" align="right">
											<button class="BotaoPadrao120" onclick="incluirNovo();">Novo Avaliador</button>
											<br>&nbsp;
										</td>
									</b>
								</tr>
								<tr class="CelulaTitulo">
									<b>
										<td width="100%" colspan="6" align="right">
											&nbsp;
										</td>
									</b>
								</tr>
							<% if rsP.RecordCount = 0 then %>
								<tr><td><h5>Nenhum Avaliador atende aos crit�rios de pesquisa.</h5></td><tr>
							<% else %>
								<% ' linha de t�tulo %>
								<tr class="CelulaCorpo">
									<b>
									<td width="6%"><a href="javascript:ordenar(0,1)">C�d.</a></td>
									<td width="40%"><a href="javascript:ordenar(0,2)">Nome</a></td>
									<td width="30%"><a href="javascript:ordenar(0,3)">Cidade</a></td>
									<td width="6%"><a href="javascript:ordenar(0,4)">Estado</a></td>
									<td width="7%" align="center"><a href="javascript:ordenar(1,6)">Aval.</a></td>
									<td width="11%" align="left">A��es</td>
									</b>
								</tr>
								<% ' linha de t�tulo %>
								<tr class="CelulaCorpoAdm">
									<td colspan="6"><hr></td>
								</tr>
								<% ' linhas de detalhes %>
								<%
								flagLinha_wk = 1
								rsP.AbsolutePage = Cint(session("paginaAtual"))			
								while ((not rsP.EOF) and (reg < tamanhoPagina_wk))
									if flagLinha_wk then 
										nomeClasse = "txtTabelaLida"
									else	
										nomeClasse = "txtTabela"
									end if
									flagLinha_wk = abs(flagLinha_wk - 1)
									%>
									<tr class="CelulaCorpoAdm">
										<td width="6%" class="txtTabela1"><%=rsP("cd_avaliador")%></td>
										<td width="40%" class="txtTabela1"><%=ucase(rsP("nm_avaliador"))%></td>
										<td width="30%" class="txtTabela1"><%=ucase(rsP("de_cidade"))%></td>
										<td width="6%" class="txtTabela1" align="center"><%=ucase(rsP("sg_estado"))%></td>
										<td width="7%" class="txtTabela1" align="center">
											<% 
											if len(rsP("qt_trabalhos")) >= 1 then
												%>
												<a href="javascript:listaTrabalhosAvaliador(<%=rsP("cd_avaliador")%>)"><%=rsP("qt_trabalhos")%> (<%=trabalhosAvaliados(rsP("qt_trabalhosAvaliados"))%>)</a>
												<%
											else
												%>
												-
												<%
											end if
											%>
										</td>
										<td width="11%" align="left">
											<img src="../images/ico_editar.gif" border="0" onclick="editar(<%=rsP("cd_avaliador")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Editar Avaliador" WIDTH="19" HEIGHT="15">
											<% if session("cientifico") then %>
												<img src="../images/ico_remover.gif" border="0" onclick="remover(<%=rsP("cd_avaliador")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Remover Avaliador" WIDTH="15" HEIGHT="15">
											<% end if %>
											<%
											select case rsP("cd_aceitouAvaliar")
												case 0
													%>
													<img SRC="../images/parc_am.gif" border="0" WIDTH="15" HEIGHT="15">
													<%
												case 1
													if rsP("qt_trabalhos") = trabalhosAvaliados(rsP("qt_trabalhosAvaliados")) then
														%>
														<img SRC="../images/parc_vd2.gif" border="0" WIDTH="15" HEIGHT="15">
														<%
													else
														%>
														<img SRC="../images/parc_vd.gif" border="0" WIDTH="15" HEIGHT="15">
														<%
													end if
												case 2
													%>
													<img SRC="../images/parc_vm.gif" border="0" WIDTH="15" HEIGHT="15">
													<%
											end select
											%>
										</td>
								</tr>
								<%
									rsP.MoveNext
									reg = reg + 1
								wend
								%>
							<% end if %>
							</table>

							<%
							' Barra de Pagina��o	%>
							<table width="100%">
								<tr><td colspan="3"><hr></td></tr>
								<tr class="CelulaCorpo" valign="middle">
									<td width="30%">
										P�gina <%=session("paginaAtual")%> de <%=totalPaginas_wk%>
									</td>
									<td width="15%">
										Registros : <%=rsP.RecordCount%>
									</td>
									<td width="55%" align="right">
										<% 
										if session("paginaAtual") > 1 then %>
											<a href="javascript:paginar(1);"><img src="../images/Ico_PrimeiraOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(2);"><img src="../images/Ico_AnteriorOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_PrimeiraOff.gif" border="0" WIDTH="12" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_AnteriorOff.gif" border="0" WIDTH="8" HEIGHT="15">
										<% 
										end if %>
										&nbsp;
										<% 
										if session("paginaAtual") < session("tp") then %>
											<a href="javascript:paginar(3);"><img src="../images/Ico_ProximaOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(4);"><img src="../images/Ico_UltimaOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_ProximaOff.gif" border="0" WIDTH="8" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_Ultimaoff.gif" border="0" WIDTH="12" HEIGHT="15">
										<% 
										end if %>
									</td>
								</tr>
							</table>

							<% ' Barra de Filtros	%>
							<table width="100%" border="0">
								<tr class="CelulaCorpo">
									<td colspan="5"><hr></td>
								</tr>
								<tr class="CelulaCorpo" valign="middle">
									<td colspan="5">
										<b>Filtros:</b>
									</td>
								<tr>
								<tr class="CelulaCorpo" valign="middle">
									<td width="20%" align="left">
										Nome<br>
										<input class="textbox01" name="filtroNome" size="16" <%
											if filtroNome_wk <> "" then
												%> value="<%=filtroNome_wk%>" <%
					 						end if
											%>>&nbsp;
									</td>
									<td width="80%" align="left">
										Estado<br>
										<input class="textbox01" name="filtroSiglaEstado" size="6" <%
										if filtroSiglaEstado_wk <> "" then
										%> value="<%=filtroSiglaEstado_wk%>" <%
					 					end if
										%>>&nbsp;
										<img src="../images/Ico_ProximaOn.gif" border="0" onclick="paginar(0);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" WIDTH="8" HEIGHT="15">
									</td>
								</tr>
								<tr class="CelulaTitulo">
									<td width="100%" colspan="5">&nbsp;</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			<%
			end select
			set rsP = nothing
			set Conn = nothing
		case 1
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			set rsP = server.CreateObject("ADODB.Recordset")
			set rsP = Conn.Execute("select * from TB_Avaliadores where cd_avaliador = " & request("hcd_avaliador"))

			%>
			<table class="TabelaProgramacao" width="100%">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						EDI��O DOS DADOS DE AVALIADOR
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td align="left" colspan="2">
						<b>Nome :&nbsp;</b><br>
						<input name="nm_avaliador" type="textbox" class="textbox01" size="64" value="<%=rsP("nm_avaliador")%>" maxlength="50">
					</td>
					<td align="left">
						<b>Senha:&nbsp;</b><br>
						<input name="de_senha" type="textbox" class="textbox01" size="10" value="<%=rsP("de_senha")%>" maxlength="6">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="33%" align="left">
						<b>Cidade :&nbsp;</b><br>
						<input name="de_cidade" type="textbox" class="textbox01" size="20" value="<%=rsP("de_cidade")%>" maxlength="50">
					</td>
					<td width="33%" align="left">
						<b>Estado :&nbsp;</b><br>
						<select name="cd_estado" size="1" class="combo01">
							<option <% if rsP("cd_estado") = 1 then %> selected <% end if %>value="1">Acre</option>
							<option <% if rsP("cd_estado") = 2 then %> selected <% end if %>value="2">Alagoas</option>
							<option <% if rsP("cd_estado") = 3 then %> selected <% end if %>value="3">Amazonas</option>
							<option <% if rsP("cd_estado") = 4 then %> selected <% end if %>value="4">Amap�</option>
							<option <% if rsP("cd_estado") = 5 then %> selected <% end if %>value="5">Bahia</option>
							<option <% if rsP("cd_estado") = 6 then %> selected <% end if %>value="6">Cear�</option>
							<option <% if rsP("cd_estado") = 7 then %> selected <% end if %>value="7">Distrito Federal</option>
							<option <% if rsP("cd_estado") = 8 then %> selected <% end if %>value="8">Esp�rito Santo</option>
							<option <% if rsP("cd_estado") = 9 then %> selected <% end if %>value="9">Goi�s</option>
							<option <% if rsP("cd_estado") = 10 then %> selected <% end if %>value="10">Maranh�o</option>
							<option <% if rsP("cd_estado") = 11 then %> selected <% end if %>value="11">Minas Gerais</option>
							<option <% if rsP("cd_estado") = 12 then %> selected <% end if %>value="12">Mato Grosso do Sul</option>
							<option <% if rsP("cd_estado") = 13 then %> selected <% end if %>value="13">Mato Grosso</option>
							<option <% if rsP("cd_estado") = 14 then %> selected <% end if %>value="14">Par�</option>
							<option <% if rsP("cd_estado") = 15 then %> selected <% end if %>value="15">Para�ba</option>
							<option <% if rsP("cd_estado") = 16 then %> selected <% end if %>value="16">Pernanbuco</option>
							<option <% if rsP("cd_estado") = 17 then %> selected <% end if %>value="17">Piau�</option>
							<option <% if rsP("cd_estado") = 18 then %> selected <% end if %>value="18">Paran�</option>
							<option <% if rsP("cd_estado") = 19 then %> selected <% end if %>value="19">Rio de Janeiro</option>
							<option <% if rsP("cd_estado") = 20 then %> selected <% end if %>value="20">Rio Grande do Norte</option>
							<option <% if rsP("cd_estado") = 21 then %> selected <% end if %>value="21">Rond�nia</option>
							<option <% if rsP("cd_estado") = 22 then %> selected <% end if %>value="22">Roraima</option>
							<option <% if rsP("cd_estado") = 23 then %> selected <% end if %>value="23">Rio Grande do Sul</option>
							<option <% if rsP("cd_estado") = 24 then %> selected <% end if %>value="24">Santa Catarina</option>
							<option <% if rsP("cd_estado") = 25 then %> selected <% end if %>value="25">Sergipe</option>
							<option <% if rsP("cd_estado") = 26 then %> selected <% end if %>value="26">S�o Paulo</option>
							<option <% if rsP("cd_estado") = 27 then %> selected <% end if %>value="27">Tocantins</option>
							<option <% if rsP("cd_estado") = 99 then %> selected <% end if %>value="99">Fora do Brasil</option>
						</select>
					</td>
					<td width="33%" align="left">
						<b>E-mail :&nbsp;</b><br>
						<input name="de_email" type="textbox" class="textbox01" size="25" value="<%= rsP("de_email")%>" maxlength="100">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>Titula��o :&nbsp;</b><br>
						<input name="de_titulacao" type="textbox" class="textbox01" size="75" value="<%=rsP("de_titulacao")%>" maxlength="255">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>Institui��o :&nbsp;</b><br>
						<input name="de_instituicao" type="textbox" class="textbox01" size="75" value="<%=rsP("de_instituicao")%>" maxlength="255">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>Disciplinas :&nbsp;</b><br>
						<input name="de_disciplinas" type="textbox" class="textbox01" size="75" value="<%=rsP("de_disciplinas")%>" maxlength="255">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>�reas de Pesquisa :&nbsp;</b><br>
						<input name="de_areasPesquisa" type="textbox" class="textbox01" size="75" value="<%=rsP("de_areasPesquisa")%>" maxlength="255">
					</td>
				<tr>


				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<hr>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="33%" align="left">
						<b>Trabalhos em Avalia��o :&nbsp;</b><br>
					</td>
					<td width="33%" align="left">
						&nbsp;
					</td>
					<td width="33%" align="left">
						&nbsp;
					</td>
				<tr>
				

				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<hr>
					</td>
				<tr>
				
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						<% if session("cientifico") then %>
							<input type="button" name="Alterar" id="Alterar" value="Alterar" class="BotaoPadrao" onclick="enviar(<%=rsP("cd_avaliador")%>);">&nbsp;&nbsp;
						<% end if %>
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
					</td>
				<tr>
		<%
		case 2
		
			cd_avaliador_wk = request("hcd_avaliador")
			nm_avaliador_wk = request("nm_avaliador")
			de_cidade_wk = request("de_cidade")
			cd_estado_wk = request("cd_estado")
			de_email_wk = request("de_email")
			de_titulacao_wk = request("de_titulacao")
			de_instituicao_wk = request("de_instituicao")
			de_disciplinas_wk = request("de_disciplinas")
			de_areasPesquisa_wk = request("de_areasPesquisa")
			de_senha_wk = request("de_senha")
		
			' Conectar com o banco
			%>
			<!--#include file="conn.asp"-->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			'on error resume next
		
			set rsD = server.CreateObject("ADODB.Recordset")
			strSQL = "update TB_Avaliadores set nm_avaliador = '" & nm_avaliador_wk _
			& "', de_cidade = '" & de_cidade_wk _
			& "', cd_estado = " & cd_estado_wk _
			& ", de_email = '" & de_email_wk _
			& "', de_titulacao = '" & de_titulacao_wk _
			& "', de_instituicao = '" & de_instituicao_wk _
			& "', de_disciplinas = '" & de_disciplinas_wk _
			& "', de_areasPesquisa = '" & de_areasPesquisa_wk _
			& "', de_senha = '" & de_senha_wk _
			& "' where cd_avaliador = " & cd_avaliador_wk
			
			'Response.Write strSQL
			'Response.End
			
			set rsD = Conn.Execute(strSQL )
			on error goto 0
			%>
			<table class="TabelaProgramacao" width="100%">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						ATUALIZA��O DE DADOS DO AVALIADOR N� <%=request("hcd_participante")%>
					</td>
				<tr>
			<%
			if err.Number = 0 then
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Atualizado com sucesso !</h5>
					</td>
				<tr>
				<%
			else
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Problema na Atualiza��o !</h5>
					Imprima esta p�gina e reporte ao suporte t�cnico.<br>			
					<%
					Response.Write "Erro N� " & err.number & "<br>"
					Response.Write err.description
					%>
					</td>
				<tr>
				<%
			end if
			%>
			<tr class="CelulaTitulo">
				<td align="middle" colspan="3">
					<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
				</td>
			<tr>
			<%
		case 3
			' Remover Participante
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			on error resume next
		
			set rsD = server.CreateObject("ADODB.Recordset")
			strSQL = "Delete from TB_Avaliadores where cd_avaliador = " & request("hcd_avaliador")
			set rsD = Conn.Execute(strSQL )
			on error goto 0
			%>
			<table class="TabelaProgramacao" width="564">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						REMO��O DO AVALIADOR N� <%=request("hcd_participante")%>
					</td>
				<tr>
			<%
			if err.Number = 0 then
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Removido com sucesso !</h5>
					</td>
				<tr>
				<%
			else
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Problema na remo��o !</h5>
					Imprima esta p�gina e reporte ao suporte t�cnico.<br>			
					<%
					Response.Write "Erro N� " & err.number & "<br>"
					Response.Write err.description
					%>
					</td>
				<tr>
				<%
			end if
			%>
			<tr class="CelulaTitulo">
				<td align="middle" colspan="3">
					<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
				</td>
			<tr>
			<%
		case 4
		
			cd_participante_wk = request("hcd_participante")
			cd_parcela_wk = request("hcd_parcela")
		
			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			'on error resume next
		
			set rsD = server.CreateObject("ADODB.Recordset")
			strSQL = "update TB_Parcelas set sn_pago = true " _
			& "where cd_participante = " & cd_participante_wk _
			& " and cd_parcela = " & cd_parcela_wk
			
			'Response.Write strSQL
			'Response.End
			
			set rsD = Conn.Execute(strSQL )
			on error goto 0
			%>
			<table class="TabelaProgramacao" width="564">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						Baixa da Parcela <%=cd_parcela_wk%> da Inscri��o N� <%=cd_participante_wk%>
					</td>
				<tr>
			<%
			if err.Number = 0 then
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Baixada com sucesso !</h5>
					</td>
				<tr>
				<%
				'Verificar se pagou todas e confirmar a inscri��o
				set rsD = Conn.Execute ("Select cd_parcela from TB_Parcelas where sn_pago = false and cd_participante = " & cd_participante_wk )
				if rsD.EOF then
					Conn.execute("update TB_Participantes set dt_confirmacao = '" & now & "' where cd_participante = " & cd_participante_wk)
				end if
			else
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Problema na Baixa da Parcela !</h5>
					Imprima esta p�gina e reporte ao suporte t�cnico.<br>			
					<%
					Response.Write "Erro N� " & err.number & "<br>"
					Response.Write err.description
					%>
					</td>
				<tr>
				<%
			end if
			%>
			<tr class="CelulaTitulo">
				<td align="middle" colspan="3">
					<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
				</td>
			<tr>
			<%
		case 5
			%>
			<table class="TabelaProgramacao" width="100%">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						INCLUS�O DE NOVO AVALIADOR
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td align="left" colspan="3">
						<b>Nome :&nbsp;</b><br>
						<input name="nm_avaliador" type="textbox" class="textbox01" size="84" maxlength="50">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="33%" align="left">
						<b>Cidade :&nbsp;</b><br>
						<input name="de_cidade" type="textbox" class="textbox01" size="20" maxlength="50">
					</td>
					<td width="33%" align="left">
						<b>Estado :&nbsp;</b><br>
						<select name="cd_estado" size="1" class="combo01">
							<option selected value="0">Escolha um Estado</option>
							<option value="1">Acre</option>
							<option value="2">Alagoas</option>
							<option value="3">Amazonas</option>
							<option value="4">Amap�</option>
							<option value="5">Bahia</option>
							<option value="6">Cear�</option>
							<option value="7">Distrito Federal</option>
							<option value="8">Esp�rito Santo</option>
							<option value="9">Goi�s</option>
							<option value="10">Maranh�o</option>
							<option value="11">Minas Gerais</option>
							<option value="12">Mato Grosso do Sul</option>
							<option value="13">Mato Grosso</option>
							<option value="14">Par�</option>
							<option value="15">Para�ba</option>
							<option value="16">Pernanbuco</option>
							<option value="17">Piau�</option>
							<option value="18">Paran�</option>
							<option value="19">Rio de Janeiro</option>
							<option value="20">Rio Grande do Norte</option>
							<option value="21">Rond�nia</option>
							<option value="22">Roraima</option>
							<option value="23">Rio Grande do Sul</option>
							<option value="24">Santa Catarina</option>
							<option value="25">Sergipe</option>
							<option value="26">S�o Paulo</option>
							<option value="27">Tocantins</option>
							<option value="99">Fora do Brasil</option>
						</select>
					</td>
					<td width="33%" align="left">
						<b>E-mail :&nbsp;</b><br>
						<input name="de_email" type="textbox" class="textbox01" size="25" maxlength="100">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>Titula��o :&nbsp;</b><br>
						<input name="de_titulacao" type="textbox" class="textbox01" size="75" maxlength="255">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>Institui��o :&nbsp;</b><br>
						<input name="de_instituicao" type="textbox" class="textbox01" size="75" maxlength="255">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>Disciplinas :&nbsp;</b><br>
						<input name="de_disciplinas" type="textbox" class="textbox01" size="75" maxlength="255">
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="5">
						<b>�reas de Pesquisa :&nbsp;</b><br>
						<input name="de_areasPesquisa" type="textbox" class="textbox01" size="75" maxlength="255">
					</td>
				<tr>
				
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						<input type="button" name="Incluir" id="Incluir" value="Incluir" class="BotaoPadrao" onclick="enviarNovo();">&nbsp;&nbsp;
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
					</td>
				<tr>
		<%
		case 6
			nm_avaliador_wk = request("nm_avaliador")
			de_cidade_wk = request("de_cidade")
			cd_estado_wk = request("cd_estado")
			de_email_wk = request("de_email")
			de_titulacao_wk = request("de_titulacao")
			de_instituicao_wk = request("de_instituicao")
			de_disciplinas_wk = request("de_disciplinas")
			de_areasPesquisa_wk = request("de_areasPesquisa")
		
			' Conectar com o banco
			%>
			<!--#include file="conn.asp"-->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

		
			strSQL = "INSERT INTO TB_Avaliadores (nm_avaliador, de_cidade, cd_estado " _
			& ", de_email, de_titulacao, de_instituicao, de_disciplinas, de_areasPesquisa" _
			& ") VALUES (" _
			& "'" & nm_avaliador_wk & "', " _
			& "'" & de_cidade_wk & "', " _
			& "" & cd_estado_wk & ", " _
			& "'" & de_email_wk & "', " _
			& "'" & de_titulacao_wk & "', " _
			& "'" & de_instituicao_wk & "', " _
			& "'" & de_disciplinas_wk & "', " _
			& "'" & de_areasPesquisa_wk & "')"
			
			'Response.Write strSQL
			'Response.End
			
			on error resume next
			Conn.Execute(strSQL )
			%>
			<table class="TabelaProgramacao" width="100%">
				<tr class="CelulaTitulo">
					<td align="middle" colspan="3">
						INCLUS�O DE NOVO AVALIADOR
					</td>
				<tr>
			<%
			if err.Number = 0 then
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>incluido com sucesso !</h5>
					</td>
				<tr>
				<%
			else
				%>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="3">
						<h5>Problema na Inclus�o !</h5>
					Imprima esta p�gina e reporte ao suporte t�cnico.<br>			
					<%
					Response.Write "Erro N� " & err.number & "<br>"
					Response.Write err.description
					%>
					</td>
				<tr>
				<%
			end if
			on error goto 0
			%>
			<tr class="CelulaTitulo">
				<td align="middle" colspan="3">
					<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
				</td>
			<tr>
			<%
		case 7
			%>
			<!--#include file="conn.asp"-->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath

			set rsT = server.CreateObject("ADODB.Recordset")
			strSQL = "select nm_avaliador from TB_Avaliadores where cd_avaliador = " & request("hcd_avaliador")
			set rsT = Conn.Execute(strSQL)
			nm_avaliador_wk = rsT("nm_avaliador")

			strSQL = "select ta.*, t.de_titulo, t.cd_modo from (TB_TrabalhoAvaliadores ta inner join TB_Trabalhos t " _
			& "on ta.cd_trabalho = t.cd_trabalho) " _
			& "where ta.cd_avaliador = " & request("hcd_avaliador")
			
			'Response.Write strSQL
			'Response.Flush
			
			set rsT = Conn.Execute(strSQL)
			%>
			<table class="TabelaProgramacao" width="100%">
				<tr class="CelulaTitulo">
					<td align="center" colspan="5">
						Trabalhos em avalia��o por:<br><b><%=nm_avaliador_wk%></b>
					</td>
				<tr>

				<tr class="CelulaCorpo" valign="top">
					<td width="5%" align="center">
						<b>Cod.</b>
					</td>
					<td width="5%" align="center">
						<b>Modo</b>
					</td>
					<td width="65%">
						<b>T�tulo</b>
					</td>
					<td width="15%" align="center">
						<b>Data da Avalia��o</b>
					</td>
					<td width="10%" align="center">
						<b>M�dia</b>
					</td>
				<tr>
				<%
				while not rsT.EOF
					%>
					<tr class="CelulaCorpo" valign="top">
						<td width="5%" align="center">
							<%=rsT("cd_trabalho")%>
						</td>
						<td width="5%" align="center">
							<%
							select case rsT("cd_modo")
								case 1
									%>CCC<%
								case 2
									%>PP<%
								case 3
									%>PC<%
								case 4
									%>O<%
							end select
							%>
						</td>
						<td width="65%">
							<%=ucase(rsT("de_titulo"))%>
						</td>
						<td width="15%" align="center">
							<%=rsT("dt_avaliacao")%>
						</td>
						<td width="10%" align="center">
							<%=rsT("vl_media")%>&nbsp;
							<% 
							select case rsT("cd_aprovacao")
								case 1
									%>
									<img SRC="../images/ico_aceita.gif" WIDTH="13" HEIGHT="15">
									<%
								case 2
									%>
									<img SRC="../images/ico_troca.jpg" WIDTH="13" HEIGHT="15">
									<%
								case 3
									%>
									<img SRC="../images/ico_rejeita.gif" WIDTH="13" HEIGHT="15">
									<% 
								case Else
									%>&nbsp;<%
							end select
							%>
						</td>
					<tr>
					<%
					rsT.movenext
					if not rsT.EOF then
						%>
						<tr><td colspan="5"><hr></td></tr>
						<%
					end if
				wend
				%>
				<tr class="CelulaTitulo">
					<td align="center" colspan="5">
					<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
					</td>
				<tr>
			</table>	
			<%
		end select
		%>
	</table>
</form>