<% 
' Conectar com o banco
%>
<!--#include file=conn.asp -->
<%
Set Conn = Server.CreateObject("ADODB.Connection") 
Conn.Open cnpath

function trabalhosAvaliados(ta)
	if isnull(ta) then
		trabalhosAvaliados = 0
	else
		trabalhosAvaliados = ta
	end if
end function

ordem = pr1_wk
if ordem = 0 then ordem=1

filtroModo_wk = request("filtroModo")
if filtroModo_wk = "" then filtroModo_wk = 0

filtroAceitacao_wk = request("filtroAceitacao")
if filtroAceitacao_wk = "" then filtroAceitacao_wk = 0

%>
<script>
function ordenar(ordem) {
	submeter(5,6,ordem);	
}

function filtrar(ordem) {
	submeter(5,6,ordem);	
}

function popUp(URL,nomeJanela,props){
	window.open(URL,nomeJanela,props);
}

function aceitacao(cd_trabalho){
	<%
	if not cbool(session("cientifico")) then
		%>
		alert('Voc� n�o tem autoriza��o para esta opera��o');
		<%
	else
		%>
		url = 'aceitaRejeita.asp?cd_trabalho='+cd_trabalho
		params = 'scrollbars=no,width=50,height=50,top=2000,left=2000';
		id = 'aceitaRejeita';
		popUp(url,id,params);
		<%
	end if
	%>
}

function alteraImagemAceitacao(cd_trabalho){
	if (document.getElementById('imgAceitacao'+cd_trabalho).aceito == 0){
		document.getElementById('imgAceitacao'+cd_trabalho).aceito = 1
		document.getElementById('imgAceitacao'+cd_trabalho).src = '../images/ico_aceita.gif';
		document.getElementById('imgAceitacao'+cd_trabalho).alt = 'Alterar para rejeitado';
	}
	else {
		document.getElementById('imgAceitacao'+cd_trabalho).aceito = 0
		document.getElementById('imgAceitacao'+cd_trabalho).src = '../images/ico_rejeita.gif';
		document.getElementById('imgAceitacao'+cd_trabalho).alt = 'Alterar para aceito';
	}
}

</script>

<input type="hidden" name="ordenacaoSinopse" value="<%=ordem%>">

<table width="600" border="0">
	<tr class="textoComum">
		<td colspan="10" align="center" valign="middle">
			<p class="Titulo" align="center">ADMINISTRA��O DO CONGRESSO</p>
		</td>
	</tr>
	<tr colspan="10" class="textoComum">
		<td align="center" valign="middle">
			<br><b>Sinopse da Avalia��o de Trabalhos</b>
		</td>
	</tr>
</table>
<br>
<table width="580" border="0" class="TabelaSinoptica">
	<tr class="CelulaCorpo" valign="middle">
		<%
		' buscar categorias no banco de dados
		' Criar o record Set
		set rsCat = server.CreateObject("ADODB.RecordSet")	
		sqlStr = "SELECT * FROM TB_ModoApresentacao ORDER BY cd_modo;"
		rsCat.Open sqlStr, Conn, adOpenKeyset
		%>
		<td align="left" colspan=6>
			&nbsp;<br>Modo de Apresenta��o<br>
			<select class="Combo01" name="filtroModo" onchange="filtrar(<%=ordem%>);">
				<option value="0">Todas</option>
					<%
					while not rsCat.EOF
						%>
						<option <% if CInt(filtroModo_wk) = CInt(rsCat("cd_modo")) then %> selected <% end if %> value="<%=rsCat("cd_modo")%>"><%=rsCat("de_modo")%></option>
						<%
						rsCat.MoveNext
					wend
					%>
			</select>
			<br>&nbsp;
		</td>
		<td align="left" colspan=2>
			&nbsp;<br>Aceita��o<br>
			<select class="Combo01" name="filtroAceitacao" onchange="filtrar(<%=ordem%>);">
				<option <%if filtroAceitacao_wk = 0 then%>selected<%end if%> value="0">Todas</option>
				<option <%if filtroAceitacao_wk = 1 then%>selected<%end if%> value="1">Aceitos</option>
				<option <%if filtroAceitacao_wk = 2 then%>selected<%end if%> value="2">Rejeitados</option>
			</select>
			<br>&nbsp;
		</td>
		<td align="left" colspan=2>
			<%
			if ordem = 1 then
				%>Ordem:<br><b>Pelo N�mero</b> (Crescente)<%
			else
				%>Ordem:<br><b>Pela M�dia</b> (Decrescente)<%
			end if
			%>
		</td>
	</tr>
	<tr class="textoComum" bgcolor="#e6e6fa">
		<td class="textoComumSIN" width="20" align="center"><b><a href="javascript:ordenar(1)">Cd</a></b></td>
		<td class="textoComumSIN" width="20" align="center"><b>Md</b></td>
		<td class="textoComumSIN" width="20" align="center"><b>Qa</b></td>
		<td class="textoComumSIN" width="20" align="center"><b>Aok</b></td>
		<td class="textoComumSIN" width="95" align="center"><b>M1</b></td>
		<td class="textoComumSIN" width="95" align="center"><b>M2</b></td>
		<td class="textoComumSIN" width="95" align="center"><b>M3</b></td>
		<td class="textoComumSIN" width="50" align="center"><b>Aceite</b></td>
		<td class="textoComumSIN" width="110" align="center"><b>Pgto</b></td>
		<td class="textoComumSIN" width="55" align="center"><b><a href="javascript:ordenar(6)">MF</a></b></td>
	</tr>
	<% 
	ordemSentido = "SomaTotal/avaliacoes_feitas DESC"
	if ordem = 1 then ordemSentido = "1 ASC"
	strSQL= "select * from [SC05 - Consolidacao] where 1=1"
	
	if filtroModo_wk <> 0 then
		strSQL = strSQL & "and cd_modo = " & filtroModo_wk 
	end if
	
	select case filtroAceitacao_wk
		case 1
			strSQL = strSQL & "and sn_aceito = true"
		case 2
			strSQL = strSQL & "and sn_aceito = false" 
	end select
	
	strSQL = strSQL & " order by " & ordemSentido 	

	'response.write strSQL
	'response.end

	set rsP = server.CreateObject("ADODB.Recordset")
	set rsA = server.CreateObject("ADODB.Recordset")
	set rsX = server.CreateObject("ADODB.Recordset")
	Response.Flush
	set rsP = Conn.Execute(strSQL)
		t = 0
		while not rsP.EOF
			if isnull(rsP("avaliacoes_feitas")) then
				af = 0
				media = 0
				st = 0
			else
				af = rsP("avaliacoes_feitas")
				st = rsP("SomaTotal")
				media = st/af
			end if
			%>
			<tr>
				<td class="textoComumSIN" align="center"><%=rsP("cd_trabalho")%></td>
				<td class="textoComumSIN" align="center">
					<%
					select case rsP("cd_modo")
						case 1
							strImg = "CCC"
						case 2
							strImg = "PP"
						case 3
							strImg = "PC"
						case 4
							if rsP("cd_naoOral") = 1 then
								strImg = "O"
							else
								strImg = "O*"
							end if
					end select
					%>
					<%=strImg%>
				</td>
				<td class="textoComumSIN" align="center"><%=rsP("total_avaliadores")%></td>
				<td class="textoComumSIN" align="center"><%=af%></td>
				<%
				strSQL = "select *, a.nm_avaliador from TB_trabalhoAvaliadores ta " _
				& "inner join tb_avaliadores a on ta.cd_avaliador = a.cd_avaliador " _
				& "where ta.cd_trabalho = " & rsP("cd_trabalho") & " order by dt_avaliacao desc"
				
				set rsX = Conn.Execute(strSQL)
		
				strSQL = "select sn_aceito from TB_Trabalhos where cd_trabalho = " & rsP("cd_trabalho")
				
				set rsA = Conn.Execute(strSQL)
		
				i=0
				while not rsX.EOF
					i = i + 1
					%>
					<td class="textoComumSIN" align="center">
						<%
						if isnull(rsX("vl_media")) or rsX("vl_media") = 0 then
							if isnull(rsX("dt_avaliacao")) then
								%>-<%
							else
								%>0.00<%
							end if
						else
							Response.Write formatnumber(rsX("vl_media"),2)
						end if
						%>
						<img SRC="../images/ico_avaliador.gif" alt="<%=ucase(rsX("nm_avaliador"))%>" WIDTH="12" HEIGHT="15">
						<%
						strAlt = ""
						select case rsX("cd_aprovacao")
								case 1
									if len(rsX("de_observacoes")) > 0 then
										strAlt = strAlt & "| Obs./Sug.: " & rsX("de_observacoes") & " | "
									end if
									%>
									<img SRC="../images/ico_aceita.gif" alt="<%=replace(strAlt,"""","'")%>" WIDTH="13" HEIGHT="15">
									<%
								case 2
									if len(rsX("de_justificativa")) > 0 then
										strAlt = strAlt & "| Justificativa: " & rsX("de_justificativa") & " | "
									end if
									if len(rsX("de_observacoes")) > 0 then
										strAlt = strAlt & "| Obs./Sug.: " & rsX("de_observacoes") & " | "
									end if
									%>
									<img SRC="../images/ico_troca.jpg" alt="<%=replace(strAlt,"""","'")%>" WIDTH="13" HEIGHT="15">
									<%
								case 3
									if len(rsX("de_justificativa")) > 0 then
										strAlt = strAlt & "| Justificativa: " & rsX("de_justificativa") & " | "
									end if
									if len(rsX("de_observacoes")) > 0 then
										strAlt = strAlt & "| Obs./Sug.: " & rsX("de_observacoes") & " | "
									end if
									%>
									<img SRC="../images/ico_rejeita.gif" alt="<%=replace(strAlt,"""","'")%>" WIDTH="13" HEIGHT="15">
									<% 
								case Else
									%>&nbsp;<%
						end select
						%>
					</td>
					<%
					rsX.MoveNext
				wend
				%>
				<td class="textoComumSIN" align="center">
					<%
					if Cbool(rsA("sn_aceito")) then
						%>
						<img id="imgAceitacao<%=rsP("cd_trabalho")%>" aceito=1 src="../images/ico_aceita.gif" border="0" onclick="aceitacao(<%=rsP("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Alterar para rejeitado" WIDTH="13" HEIGHT="15">
						<%
					else
						%>
						<img id="imgAceitacao<%=rsP("cd_trabalho")%>" aceito=0 src="../images/ico_rejeita.gif" border="0" onclick="aceitacao(<%=rsP("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Alterar para aceito" WIDTH="13" HEIGHT="15">
						<%
					end if
					%>
				</td>

				<td class="textoComumSIN" align="center">
					<%
					set rsX = server.CreateObject("ADODB.Recordset")
					strSQL = "select * from TB_Parcelas p inner join TB_Trabalhos t on p.cd_participante = t.cd_apresentador where t.cd_trabalho = " & rsP("cd_trabalho")
					set rsX = Conn.Execute(strSQL)
		
					while not rsX.EOF
						if rsX("sn_pago") then
							strIMG = "parc_vd.gif"
						else
							if CDate(rsX("dt_parcela")) > now() then
								strIMG = "parc_am.gif"
							else
								strIMG = "parc_vm.gif"
							end if 
						end if
						%>
						<img src="../images/<%=strIMG%>">
						<%
						rsX.MoveNext
					wend
					%>
				</td>

				<td class="textoComumSIN" align="center">
					<%
					if not isnull(media) then
						Response.write formatnumber(media,2)
					else
						%>
						&nbsp;
						<%
					end if%>
				</td>
			</tr>
			<%
			t = t + 1
			rsP.movenext
		wend
	
%>
	</table>
<p class=TextoComum>
	* - Pode ser passado para Painel Cl�nico<br>
	<b>Total de Trabalhos : <%=t%></b>
</p>
<%
set rsP = nothing
set rsA = nothing
set rsX = nothing
set conn = nothing
%>