<!--#include file=conn.asp -->
<%
server.ScriptTimeout = 300
set conn = server.CreateObject("ADODB.Connection")
set rs = server.CreateObject("ADODB.Recordset")
set rsA = server.CreateObject("ADODB.Recordset")

conn.Open cnpath

inicial = 1
final = 600

strSQL = "SELECT [SomaTotal]/[avaliacoes_feitas], t.* " _
& "AS vl_media FROM [SC05 - Consolidacao] c " _
& "inner join TB_Trabalhos t on t.cd_trabalho = c.cd_trabalho " _
& "where t.sn_aceito = false order by 1 desc"
'& "and t.cd_modo = 1 "

set rs = conn.Execute(strSQL)
%>
<!--#include file=../inc_email.asp -->
<%
'e-mail do remetente 
objCDOSYSMail.From = "SOBE 2005 <XIII_CBE@tavares.eti.br>"

i=0
while not rs.EOF

	strSQL = "select * from TB_TrabalhoAvaliadores where cd_trabalho = " & rs("cd_trabalho")
	set rsA = conn.Execute(strSQL)
	
	strObs = ""
	strJust = ""
	while not rsA.EOF
		if len(rsA("de_observacoes")) > 1 then
			strObs = strObs & rsA("de_observacoes") & str_CrLf
		end if
		if len(rsA("de_justificativa")) > 1 then
			strObs = strObs & rsA("de_justificativa") & str_CrLf
		end if
		rsA.MoveNext
	wend
	
	str_CrLf = chr(13) & chr(10)
	texto = rs("de_email") _
	& str_CrLf & str_CrLf _
	& ucase(rs("nm_responsavel")) _
	& str_CrLf & str_CrLf _
	& "Comunicamos que o seu trabalho N�" & rs("cd_trabalho") & ", intitulado """ & ucase(trim(rs("de_titulo"))) & """, n�o foi aprovado para apresenta��o no XIII Congresso Brasileiro de Estomatologia, na categoria " & categoria
	
	select case rs("cd_modo")
		case 1
			strModo = "CASO CL�NICO CONSULTIVO"
		case 2
			strModo = "PAINEL DE PESQUISA"
		case 3
			strModo = "PAINEL CL�NICO"
		case 4
			strModo = "APRESENTA��O ORAL DE CASO CL�NICO"
	end select
	
	texto = texto & strModo
	if len(strObs) > 0 or len(strJust) > 0 then
		texto = texto  & str_CrLf & str_CrLf _
		& "O trabalho foi avaliado por 3 avaliadores que forneceram as seguintes justificativas e observa��es/sugest�es: " _
		& str_CrLf & str_CrLf _
		& "Observa��es:" & str_CrLf _
		& strObs _
		& str_CrLf & str_CrLf _
		& "Justificativas:" & str_CrLf _
		& strJust
	end if
	
	texto = texto & str_CrLf & str_CrLf _
	& "Atenciosamente," & str_CrLf _
	& "Profa. Maria In�s Meurer " & str_CrLf _
	& "Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia"
	'& str_CrLf & str_CrLf _
	'& rs("de_email")
	
	objCDOSYSMail.TextBody = texto
	
	'assunto da mensagem 
	textoAssunto = "Trabalho N� " & rs("cd_trabalho") & " na categoria " & strModo 
	objCDOSYSMail.Subject = textoAssunto

	'e-mail do destinat�rio 
	'strTo = """" & rs("nm_responsavel") & """ <" & rs("de_email") & ">"
	'strTo = """" & rs("nm_responsavel") & """ <marcelo@tavares.eti.br>"
	strTo = """" & rs("nm_responsavel") & """ <emana@brturbo.com.br>"
	objCDOSYSMail.To = strTo

	strBcc = """" & rs("nm_responsavel") & """ <marcelo@tavares.eti.br>"
	objCDOSYSMail.Bcc = strBcc
	
	'objCDOSYSMail.fields.update
	'envia o e-mail 
	
	on error resume next
	objCDOSYSMail.Send
	if err.number = 0 then 
		Response.Write "Mensagem enviada para (" & rs("cd_trabalho") & ") " & rs("nm_responsavel") & "<BR>"
		Response.Flush
		i = i + 1
	else
		Response.Write "<hr>ERRO enviando Mensagem para (" & rs("cd_trabalho") & ") " & rs("nm_responsavel") & "<BR>"
		Response.Write err.Description & "<hr>"
		Response.Flush
	end if
	'Response.Write textoAssunto & "<br>"
	'Response.Write texto & "<hr>"
	on error goto 0
	rs.MoveNext
wend 

Response.Write "Enviadas " & i & " mensagens."
'destr�i os objetos 
Set objCDOSYSMail = Nothing 
Set objCDOSYSCon = Nothing 
set rs = nothing
set rsA = nothing
set Conn = nothing
%>