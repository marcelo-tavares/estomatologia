<%
function valida_email(email)
	valida_email = true
	if instr(1,email,"@") = 0 then valida_email = false

	if valida_email then
		arroba = instr(1,email,"@")
		if  instr(arroba+1,email,"@") <> 0 then 
			valida_email = false	
		end if
	end if

	if instr(1,email,".") = 0 then valida_email = false
end function
%>
	<table width="100%">
		<tr class="textoComum">
			<td align="center" valign="middle">
				<p class="Titulo" align="center">ADMINISTRAÇÃO DO CONGRESSO</p>
			</td>
		</tr>
		<tr class="textoComum">
			<td align="center" valign="middle">
				<br><b>GERAÇÃO DE LISTA DE E-MAILS DE PARTICIPANTES</b>
			</td>
		</tr>
		<tr class="textoComum">
			<td align="left">
	<!--#include file=conn.asp -->
	<%
		Set Conn = Server.CreateObject("ADODB.Connection") 
		Conn.Open cnpath

		set rs = server.CreateObject("ADODB.Recordset")
		set rs = Conn.Execute("Select cd_participante,nm_participante, de_email from TB_Participantes")
		
		lista = 1
		emails_lista = 0
		invalidos = ""
		
		Response.Write "<b>Lista " & lista & "</b><br>"
		while not rs.EOF		
			if not valida_email(rs("de_email")) then
				invalidos = invalidos & rs("cd_participante") & " - " & rs("nm_participante") & " (" & rs("de_email") & ")<br>"
				rs.MoveNext
			else
				Response.Write rs("de_email")
				emails_lista = emails_lista + 1
				rs.MoveNext
				if emails_lista = 99 then
					lista = lista + 1
					Response.Write "<br><BR><b>Lista " & lista & "</b>"
					emails_lista = 0
				else
					if not rs.EOF then Response.Write ";"
				end if
				Response.Write "<BR>"
			end if
		wend
		
		if len(invalidos) > 0 then
			Response.Write "<br><br><b>E-mails inválidos:</b><br><br>" & invalidos
		end if
	%>
			</td>
		</tr>
	</table>

<%
set rs=nothing
set conn = nothing
%>
