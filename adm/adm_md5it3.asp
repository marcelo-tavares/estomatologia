<HTML>
<HEAD>
</HEAD>
<BODY>

<% 

	strSQL = "SELECT p.nm_participante, Count(t.cd_apresentador) AS qt_trabalhos " _
	& "FROM TB_Trabalhos t LEFT JOIN TB_Participantes p " _
	& "ON t.cd_apresentador = p.cd_participante GROUP BY p.nm_participante " _
	& "ORDER BY count(t.cd_apresentador) DESC, nm_participante ASC"

	' Conectar com o banco
	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn = Server.CreateObject("ADODB.Connection") 
	Conn.Open cnpath

	set rsP = server.CreateObject("ADODB.Recordset")
	set rsP = Conn.Execute(strSQL)
		
%>

	<table width="100%">
		<tr class="textoComum">
			<td align="center" valign="middle">
				<p class="Titulo" align="center">ADMINISTRA��O DO CONGRESSO</p>
			</td>
		</tr>
		<tr class="textoComum">
			<td align="center" valign="middle">
				<br><b>Rela��o de Trabalhos por Apresentador:</b><br><br>
			</td>
		</tr>
		
		<tr class="textoComum">
			<td align="center" valign="middle">
				<table width=100%>
					<tr class="textoComum">
						<td width=70%><b>Apresentador</b></td>
						<td width=30%><b>Trabalhos</b></td>
					</tr>
					<%
					while not rsP.EOF
						if rsP("nm_participante") <> "" then 
							%>
							<tr class="textoComum">
								<td width=70%><%=rsP("nm_participante")%></td>
								<td width=30%><%=rsP("qt_trabalhos")%></td>
							</tr>
							<%
						end if
						rsP.MoveNext
					wend
					%>
				</table>
			</td>
		</tr>
		<%
		strSQL = "SELECT p.cd_participante, t.cd_trabalho, nm_responsavel " _
		& "FROM TB_Trabalhos t LEFT JOIN TB_Participantes p " _
		& "ON t.cd_apresentador = p.cd_participante " _
		& "WHERE (((p.cd_participante) Is Null));"
		set rsP = Conn.Execute(strSQL)
		
		if not rsP.EOF then
			%>
			<tr class="textoComum">
				<td align="center" valign="middle">
					<hr><br><b>Rela��o de Trabalhos sem Apresentador V�lido:</b><br><br>
				</td>
			</tr>

			<tr class="textoComum">
				<td align="center" valign="middle">
					<table width=100%>
						<tr class="textoComum" align=center>
							<td width=100%>C�digo do Trabalho e Nome do Respons�vel</td>
						</tr>
						<%
						while not rsP.EOF
							if rsP("cd_trabalho") <> "" then 
								%>
								<tr class="textoComum" align=center>
									<td width=100%><%=rsP("cd_trabalho") & " - " & rsP("nm_responsavel")%></td>
								</tr>
								<%
							end if
							rsP.MoveNext
						wend
						set rsP = nothing
						set Conn = nothing
						%>
					</table>
				</td>
			</tr>
			<%
		end if
		%>
	</table>



</BODY>
</HTML>
