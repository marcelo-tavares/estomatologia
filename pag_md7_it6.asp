<script>
function mostraAtividade(atividade){
	for (i=1; i<=4; i++) {
		document.getElementById('atividade'+i).style.display = 'none';
		if (atividade == i){
			document.getElementById('atividade'+i).style.display = 'inline';
		}
	}
}

</script>
<p class="Titulo" align="center">CRONOGRAMA DE APRESENTA��O</p>

<table width=100%>
	<tr class="TextoComum">
		<td bgcolor="#ffffff">
			<small>
				<a href="javascript:mostraAtividade(1);">Casos Consultivos</a> | 
				<a href="javascript:mostraAtividade(2);">Pain�is de Pesquisa</a> | 
				<a href="javascript:mostraAtividade(3);">Pain�is Cl�nicos</a> | 
				<a href="javascript:mostraAtividade(4);">Casos Cl�nicos</a>
			</small>
		</td>
	</tr>
</table>
<br>
<table width=100% border=0>
	<tr valign=top>
		<td width=100%>
			<span id=atividade1 style="display:'inline'" class="TextoComum">
				<b>CASOS CL�NICOS CONSULTIVOS</b><br>
				<br>
				<table WIDTH=100%>
					<tr class=textoComumSmall>
						<td WIDTH=100% colspan=3>
							<b>Quarta, 20 de julho</b>
						</td>
					</tr>
					<tr bgcolor=#e8e8e8 class=textoComumSmall>
						<td WIDTH=20%>
							HOR�RIO
						</td>
						<td WIDTH=10% align=center>
							No.
						</td>
						<td WIDTH=70%>
							T�TULO
						</td>
					</tr>
					<tr valign=top class=textoComumSmall>
						<td>13:30 � 14:00</td><td align=center>542</td><td>M�ltiplos dentes permanentes inclusos em paciente jovem</td>
					</tr>
					<tr valign=top class=textoComumSmall>
						<td>14:00 � 14:30</td><td align=center>483</td><td>Neurofibroma plexiforme extenso em bochecha de crian�a de 1,6 anos: remover ou proservar?</td>
					</tr>
					<tr valign=top class=textoComumSmall>
						<td>14:30 � 15:00</td><td align=center>405</td><td>Papiloma invertido de l�bio?</td>
					</tr>
					<tr valign=top class=textoComumSmall>
						<td>15:00 � 15:30</td><td align=center>053</td><td>S�ndrome da sobreposi��o de l�pus eritematoso/l�quen plano associada a condiloma acuminado: um desafio cl�nico</td>
					</tr>
					<tr valign=top class=textoComumSmall bgcolor=#e8e8e8 >
						<td colspan=3>Coffee-break</td>
					</tr>
					<tr valign=top class=textoComumSmall>
						<td>16:00 � 16:30</td><td align=center>514</td><td>Neoplasia mesenquimal de mand�bula</td>
					</tr>
					<tr valign=top class=textoComumSmall>
						<td>16:30 � 17:00</td><td align=center>416</td><td>Seq�elas bucais de radioterapia: mucosite em dorso de l�ngua refrat�ria a tratamento</td>
					</tr>
					<tr valign=top class=textoComumSmall>
						<td>17:00 � 17:30</td><td align=center>009</td><td>Penfig�ide benigno de mucosas : les�es gengivais extensas de dif�cil manejo terap�utico</td>
					</tr>
				</table>
			</span>

			<span id=atividade2 style="display:'none'" class="TextoComum">
				<b>PAIN�IS DE PESQUISA</b><br><br>
				<table WIDTH=100%>
					<tr class=textoComumSmall>
						<td WIDTH=100% colspan=3>
							<b>17 e 18 de julho</b>
						</td>
					</tr>
					<tr bgcolor=#e8e8e8 class=textoComumSmall>
						<td WIDTH=10% align=center>
							No.
						</td>
						<td WIDTH=90%>
							T�TULO
						</td>
					</tr>
					<tr valign=top class=textoComumSmall><td align=center>003</td><td>Estudo epidemiol�gico das fissuras l�bio-palatais </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>004</td><td>S�ndrome do carcinoma nev�ide basocelular (Gorlin) - caracter�sticas biol�gicas e gen�ticas de uma fam�lia portadora da condi��o gen�tica </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>012</td><td>Express�o imuno-histoqu�mica da prote�na DAP quinase em carcinoma epiderm�ide oral </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>014</td><td>Unidade de sa�de e assistencia domiciliar: as limita��es dos idosos e abrang�ncia dos servi�os </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>015</td><td>Higiene oral pobre como fator de risco para pneumonia em pacientes idosos hospitalizados </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>017</td><td>Compara��o entre radiografias digitais diretas e indiretas na detec��o de perdas minerais na regi�o posterior da mand�bula - estudo in vitro </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>020</td><td>Efeito dos clareadores dentais sobre �lceras induzidas com naoh na mucosa bucal de ratos � an�lise cl�nica e morfol�gica </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>026</td><td>Diagn�stico histopatol�gico de les�es bucais por meio da bi�psia por agulha cortante </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>027</td><td>Estudo epidemiol�gico das les�es bucais em pacientes portadores do HIV/AIDS </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>036</td><td>Adenocarcinoma polimorfo de baixo grau de malignidade: estudo retrospectivo de 10 casos. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>042</td><td>An�lise do n�vel de informa��o da popula��o sobre o c�ncer de boca </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>055</td><td>Preval�ncia das les�es diagnosticadas em um servi�o de CTBMF no per�odo entre maio e dezembro de 2004. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>057</td><td>Avalia��o do conhecimento dos pacientes atendidos no ambulat�rio m�dico do hospital universit�rio sobre o c�ncer bucal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>058</td><td>Expres�o imuno-histoqu�mica da laminina na membrana basal de carcinomas escamocelulares de boca </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>073</td><td>Levantamento epidemiol�gico do carcinoma espinocelular: retrospectiva dos �ltimos 11 anos. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>114</td><td>Avalia��o do conhecimento sobre c�ncer bucal entre universit�rios e pacientes odontol�gicos. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>129</td><td>Fatores progn�sticos cl�nicos, histopatol�gicos e imunohistoqu�micos em carcinoma aden�ide c�stico de cabe�a e pesco�o. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>148</td><td>Avalia��o cl�nico-citol�gica da mucosa oral em pacientes portadores de leishmaniose visceral </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>149</td><td>Protocolo para a preven��o e tratamento de complica��es estomatol�gicas em crian�as com leucemia linfobl�stica aguda (LLA)</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>172</td><td>Cisto linfoepitelial oral: levantamento epidemiol�gico de 7 anos do servi�o de anatomia patol�gica </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>182</td><td>�ndice panor�mico mandibular na detec��o precoce de osteoporose </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>184</td><td>Alcooliza��o cr�nica experimental em camundongos: estudo morfologico e da express�o imunoistoqu�mica da metalotione�na em epit�lio lingual </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>303</td><td>Preval�ncia de les�es orais em pacientes pedi�tricos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>313</td><td>Avalia��o do polimorfismo do gene da IL-10 em pacientes com l�quen plano bucal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>326</td><td>Altera��es do ph e do fluxo salivar induzidas pelo consumo de vitamina C. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>328</td><td>Preven��o e diagn�stico precoce de tumores de boca em pacientes idosos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>341</td><td>Estudo das condi��es bucais e sist�micas de um grupo de pacientes, portadores da s�ndrome de Sj�gren, assistido no ambulat�rio de reumatologia do hospital universit�rio </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>348</td><td>Estudo da preval�ncia de altera��es labiais em pescadores</td></tr>
					<tr class=textoComumSmall>
						<td WIDTH=100% colspan=3>
							<br><br><b>19 e 20 de julho</b>
						</td>
					</tr>
					<tr bgcolor=#e8e8e8 class=textoComumSmall>
						<td WIDTH=10% align=center>
							No.
						</td>
						<td WIDTH=90%>
							T�TULO
						</td>
					</tr>
					<tr valign=top class=textoComumSmall><td align=center>351</td><td>Neoplasias de gl�ndulas salivares menores: estudo cl�nico-patol�gico de 548 casos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>356</td><td>Cuidados com a sa�de bucal de idosos: apresenta��o de um protocolo para pacientes de um hospital geri�trico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>358</td><td>Estudo da presen�a do DNA do citomegalov�rus na mucosa bucal de pacientes submetidos ao transplante de c�lulas tronco-hematopoi�ticas, atrav�s da t�cnica de nested-PCR </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>363</td><td>An�lise da densidade �ssea em regi�o parassinfis�ria de mand�bulas humanas: compara��o entre n�veis de cinza em radiografias digitais (sistema digora) e unidades hounsfield. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>364</td><td>Fraturas mandibulares: an�lise retrospectiva de 161 pacientes </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>368</td><td>Estudo retrospectivo da incid�ncia e fatores de risco de pacientes diagnosticados com carcinoma espinocelular oral em um hospital universit�rio </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>371</td><td>Estudo imunoistoqu�mico comparativo entre o carcinoma espinocelular oral e a mucosa n�o neopl�sica adjacente </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>385</td><td>Estudo da freq��ncia do herpesv�rus humano tipo 8 na cavidade bucal de crian�as infectadas pelo HIV </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>386</td><td>Estudo das manifesta��es cl�nicas, estomatol�gicas e radiogr�ficas em pacientes portadores de anemia de fanconi. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>390</td><td>Concord�ncia entre os terceiros molares do mesmo paciente em rela��o a sua condi��o de impacta��o �ssea e suas caracter�sticas de posi��o </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>394</td><td>Caracteriza��o microsc�pica das p�rolas de esmalte em molares permanentes humanos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>401</td><td>Candid�ase oral em portadores de diabetes mellitus � estudo cl�nico e citopatol�gico. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>402</td><td>Diagn�stico ultrasonogr�fico da doen�a das gl�ndulas salivares associada ao HIV </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>404</td><td>Visitas domiciliares na preven��o e diagn�stico do c�ncer bucal: integra��o do cognitivo com o social em benef�cio da comunidade e do ensino. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>410</td><td>An�lise cl�nica de altera��es na mucosa da cavidade oral dos pacientes submetidos ao transplante de f�gado </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>412</td><td>Atividade antimicrobiana in vitro do extrato hidroalco�lico de rosmarinus officinalis linn sobre bact�rias orais planct�nicas e organizadas em biofilme </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>414</td><td>An�lise comparativa da express�o da prote�na p53 dos clones DO-7 e PAB-240 em carcinomas de c�lulas escamosas bucais </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>419</td><td>Adenoma pleom�rfico: um estudo de 68 casos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>420</td><td>Ceratocisto odontog�nico: um estudo retrospectivo de 57 casos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>432</td><td>Estudo epidemiol�gico de 2.147 casos de les�es da cabe�a e pesco�o diagnosticadas no laborat�rio de patologia bucal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>453</td><td>Levantamento de exames histopatol�gicos de les�es bucais em indiv�duos idosos, provenientes do laborat�rio de patologia bucal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>471</td><td>Estudo do comportamento da press�o arterial e freq��ncia card�aca em pacientes normotensos submetidos a exodontias simples </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>492</td><td>Altera��o do ph e do fluxo salivar induzida pelo consumo de vitamina c efervescente � estudo �in vivo� </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>515</td><td>Incid�ncia de neoplasias de gl�ndulas salivares em disciplina de gradua��o em estomatologia entre os anos de 1998 a 2004. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>523</td><td>Incid�ncia de les�es bucais examinadas atrav�s de bi�psias em uma disciplina de gradua��o. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>524</td><td>Cistos odontog�nicos: estudo epidemiol�gico de 809 casos. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>529</td><td>An�lise quantitativa da presen�a de neutr�filos ap�s a aplica��o de tr�s diferentes t�cnicas de crioterapia em dorso de l�ngua: estudo em ratos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>537</td><td>Avalia��o das atitudes do cirurgi�o dentista frente ao h�bito do fumo. </td></tr>
				</table>
			</span>

			<span id=atividade3 style="display:'none'" class="TextoComum">
				<b>PAIN�IS CL�NICOS</b><br><br>
				<table width=100%>
					<tr class=textoComumSmall>
						<td WIDTH=100% colspan=3>
							<b>17 e 18 de julho</b>
						</td>
					</tr>
					<tr bgcolor=#e8e8e8 class=textoComumSmall>
						<td WIDTH=10% align=center>
							No.
						</td>
						<td WIDTH=90%>
							T�TULO
						</td>
					</tr>
					<tr valign=top class=textoComumSmall><td align=center>007</td><td>Rabdomiossarcoma embrion�rio � relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>011</td><td>S�ndrome de Goldenhar: a import�ncia da interdisciplinaridade no diagn�stico e tratamento </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>016</td><td>Hiperplasia fibrosa inflamat�ria por c�mara de suc��o: tratamento cir�rgico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>018</td><td>Sialolit�ase em gl�ndula salivar menor. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>019</td><td>S�ndrome de Apert: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>024</td><td>Neurofibromatose com manifesta��o intrabucal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>025</td><td>Cisto epiderm�ide: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>028</td><td>Condrossarcoma em mand�bula: descri��o cl�nica e histopatol�gica de um caso. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>029</td><td>Les�o central de c�lulas gigantes em maxila: diagn�stico, tratamento e reabilita��o. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>030</td><td>Queratoacantoma: relato de caso. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>031</td><td>Diagn�stico e conduta em les�o leucoeritropl�sica recidivante de palato. Relato de caso e revis�o da literatura. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>032</td><td>Adenoma pleom�rfico de mucosa jugal em crian�a. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>034</td><td>Carcinoma mucoepiderm�ide de palato em paciente HIV+. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>038</td><td>S�ndrome de Sturge Weber - import�ncia da interdisciplinaridade no diagn�stico e tratamento </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>040</td><td>Um diagn�stico inusitado de AIDS </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>041</td><td>Amelog�nese imperfeita. Relato de caso familial com heran�a autoss�mica recessiva </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>043</td><td>Falha prim�ria de erup��o. Relato de caso familial. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>044</td><td>Tumor odontog�nico adenomat�ide: relato de caso cl�nico associado a incisivo lateral </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>045</td><td>Cisto paradent�rio: relato de dois casos cl�nicos incomuns </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>047</td><td>Osteoma periosteal mandibular: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>048</td><td>Gengivoestomatite herp�tica prim�ria em adulto </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>049</td><td>Apresenta��o cl�nica at�pica de linfangioma oral: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>050</td><td>Ameloblastoma unic�stico: relato de caso cl�nico com recidiva </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>052</td><td>Cisto derm�ide cong�nito de localiza��o paramediana </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>056</td><td>Paracoccidioidomicose: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>061</td><td>Odontoma complexo de grandes propor��es em maxila </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>062</td><td>Extenso odontoma complexo em maxila: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>063</td><td>Mucocele de seio maxilar em paciente de 4 anos de idade </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>064</td><td>Cisto dent�gero causando deslocamento dent�rio severo </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>069</td><td>O papel do cirurgi�o dentista no tratamento multidisciplinar do retinoblastoma </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>070</td><td>Cisto de Gorlin com aspectos histol�gicos sugestivos de tumor odontog�nico epitelial calcificante </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>071</td><td>Carcinoma mucoepiderm�ide de palato em adolescente mimetizando abscesso endod�ntico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>074</td><td>Hipoplasia de esmalte em paciente com insufici�ncia renal cr�nica </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>076</td><td>Cisto epiderm�ide submentual </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>077</td><td>Odontoma complexo em forma��o </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>078</td><td>Epiderm�lise bolhosa distr�fica recessiva, tipo Hallopeau-Siemens. Relato de caso. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>079</td><td>Abordagem cir�rgica ampla no tratamento da s�ndrome de Gorlin-Goltz </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>082</td><td>Melanoma de rebordo alveolar: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>083</td><td>Aspectos prodr�micos relacionados a um caso de sialometaplasia necrosante </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>093</td><td>Ameloblastoma: relato de um caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>094</td><td>�lcera com sequestro �sseo - apresenta��o de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>095</td><td>Neurofibroma: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>098</td><td>Lipoma com metaplasia �ssea: um achado histopatol�gico incomum </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>099</td><td>Osteossarcoma telangiect�sico: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>102</td><td>Hemangioma arteriovenoso: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>103</td><td>Avalia��o da contribui��o das imagens simult�neas de fus�o: spect com 99mtc-mdp e ct aplicadas no diagn�stico das disfun��es temporomandibulares </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>104</td><td>S�ndrome de Gorlin: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>105</td><td>Met�stase de carcinoma epiderm�ide bucal e carcinoma papil�fero de tire�ide para o mesmo linfonodo cervical </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>106</td><td>Les�o plasmocit�ide maligna de maxila: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>111</td><td>Epiderm�lise bolhosa- relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>122</td><td>Feo-hifomicose de l�bio inferior </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>124</td><td>S�filis secund�ria: diagn�stico atrav�s das les�es orais </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>125</td><td>Manifesta��o bucal do penfig�ide cicatricial: relato de caso cl�nico. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>126</td><td>Mioepitelioma-relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>127</td><td>Granuloma de corpo estranho em mand�bula relacionado a acidente de trabalho ocorrido h� 20 anos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>128</td><td>Arterite de c�lulas gigantes </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>136</td><td>Avalia��o do perfil psicol�gico de 8 pacientes com a s�ndrome da ard�ncia bucal e sua correla��o com as caracter�sticas cl�nicas. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>140</td><td>Displasia cleidocraniana : relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>142</td><td>Ameloblastoma X cisto dent�gero: um caso de transforma��o? </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>147</td><td>Les�o c�stica parot�dea com forma��o de cristal�ides de a-amilase </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>150</td><td>Marsupializa��o de cisto dent�gero na maxila em crian�a </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>151</td><td>Complica��es bucais decorrentes da osteodistrofia renal em paciente com doen�a renal cr�nica </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>152</td><td>Recidiva de hemangioma cavernoso bucal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>154</td><td>Import�ncia da associa��o cl�nico-radiogr�fica no diagn�stico da s�ndrome de Eagle </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>156</td><td>Carcinoma epitelial-mioepitelial de l�ngua </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>157</td><td>Melanoma desmopl�sico em mucosa bucal: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>158</td><td>Leishmaniose oral em um paciente HIV-positivo. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>161</td><td>Ceratocisto odontog�nico clinicamente mimetizando um cisto dent�gero </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>163</td><td>S�ndrome de Gardner </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>165</td><td>Fasci�te nodular em regi�o submandibular: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>166</td><td>Hemangiopericitoma em mucosa jugal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>167</td><td>Laserterapia aplicado em paciente com mucosite oral provocada pelo tratamento radioter�pico de cabe�a e pesco�o </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>168</td><td>Histoplasmose bucal em paciente HIV negativo </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>169</td><td>Cisto �sseo simples em pacientes sob tratamento ortod�ntico: trauma como fator etiol�gico ou simples achado radiogr�fico? </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>170</td><td>Fibroma: remo��o cir�rgica utilizando laser de diodo de alta pot�ncia </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>171</td><td>Manifesta��es bucais da hansen�ase </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>174</td><td>S�ndrome de Iagle: relato de caso c�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>176</td><td>Doen�a de Castleman - relato de quatro casos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>177</td><td>Melanoma prim�rio de boca: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>178</td><td>L�bio duplo cong�nito </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>237</td><td>Esclerodermia sist�mica - relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>240</td><td>Carcinoma aden�ide c�stico e ameloblastoma unic�stico acometendo a mesma paciente em menos de 6 meses � relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>241</td><td>Tratamento de odontoma composto associado a canino incluso em mand�bula com utiliza��o de barreira de alum�nio </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>243</td><td>Osteossarcoma em mand�bula: diagn�stico citol�gico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>245</td><td>Les�o de c�lulas gigantes central: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>246</td><td>Manifesta��es da paracoccidioidomicose na mucosa bucal - relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>247</td><td>Carcinoma espinocelular de pobre progn�stico. </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>248</td><td>Hemangioma cavernoso de mucosa jugal </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>249</td><td>Cisto dentigero associado a tumor odontog�nico adenomat�ide </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>252</td><td>Cisto �sseo traum�tico - relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>254</td><td>Queilite granulomatosa de miesche � discuss�o de um caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>255</td><td>Estomatite ur�mica em paciente transplantado renal - relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>257</td><td>Linfoma n�o-Hodgkin em regi�o maxilofacial: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>258</td><td>Cisto odontog�nico botri�ide - relato de caso mimetizando um cisto residual </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>259</td><td>Carcinoma de c�lulas escamosas em l�ngua pr�ximo � �rea de leucoplasia: qual �rea deve-se realizar a bi�psia? </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>260</td><td>Infec��o odontogenica com acometimento de espa�os profundos </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>261</td><td>Abscesso cerebral causado por infec��o odontog�nica: relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>262</td><td>Fibroma ossificante juvenil trabecular (tipo OMS)- relato de caso </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>301</td><td>Sarcoma alveolar de partes moles </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>302</td><td>Tumor marrom do hiperparatireoidismo </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>304</td><td>S�ndrome de Sj�gren: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>305</td><td>Mixoma odontog�nico de maxila </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>318</td><td>Micromarsupializa��o de r�nula </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>320</td><td>Condiloma acuminado em paciente infectado pelo HIV </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>323</td><td>Sialectasia cong�nita: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td align=center>327</td><td>Granuloma letal de linha m�dia ou linfoma de c�lulas TN/K � relato de caso </td></tr>
					<tr class=textoComumSmall>
						<td WIDTH=100% colspan=3>
							<br><br><b>19 e 20 de julho</b>
						</td>
					</tr>
					<tr bgcolor=#e8e8e8 class=textoComumSmall>
						<td WIDTH=10% align=center>
							No.
						</td>
						<td WIDTH=90%>
							T�TULO
						</td>
					</tr>
					<tr valign=top class=textoComumSmall><td align=center>329</td><td>As complica��es orais da terapia antineopl�sica e o tratamento odontol�gico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>331</td><td>Utiliza��o da citologia por imprint no diagn�stico de neoplasias de gl�ndulas salivares</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>332</td><td>Cisto do ducto nasopalatino: identifica��o a partir de m� adapta��o de pr�tese</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>335</td><td>Import�ncia do tratamento da sialolit�ase para restabelecimento funcional do paciente</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>337</td><td>A valoriza��o da queixa principal no diagn�stico final em estomatologia</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>339</td><td>Displasia fibrosa monost�tica relato de caso e acompanhamento cl�nico/radiogr�fico de 4 anos.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>340</td><td>S�ndrome de Sturge � Weber: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>342</td><td>S�ndrome de Von Recklinghausen: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>343</td><td>Cisto gengival do adulto � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>345</td><td>P�rpura trombocitop�nica: a import�ncia do cirurgi�o-dentista no diagn�stico.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>346</td><td>Adenocarcinoma polimorfo de baixo grau na maxila mimetizando cisto odontog�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>349</td><td>Tumor marrom � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>352</td><td>Schwannoma de l�ngua</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>353</td><td>Lipoma submandibular: papel da PAAF como auxiliar no diagn�stico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>354</td><td>Fibroma cemento-ossificante central de maxila</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>360</td><td>Cisto �sseo traum�tico com aspecto de les�o fibro-�ssea na mand�bula</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>362</td><td>Hiperparatireoidismo secund�rio: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>367</td><td>Inje��o de cortic�ide intralesional: uma alternativa para o tratamento do l�quen plano erosivo</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>370</td><td>Caracter�sticas cl�nico-citol�gicas da hiperplasia linf�ide c�stica da par�tida em pacientes HIV-positivos</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>372</td><td>Eritema multiforme associado a herpes simples: relato de dois casos cl�nicos</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>373</td><td>Diagn�stico citol�gico de cistos branquiais cervicais atrav�s da pun��o aspirativa por agulha fina: revis�o cito-histol�gica de 60 casos</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>374</td><td>Cementoblastoma ben�gno em dente retido: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>377</td><td>P�nfigo vulgar: manifesta��es em boca e pele</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>378</td><td>Cisto epiderm�ide em regi�o malar.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>379</td><td>Queratocisto simulando cisto radicular: relato de caso clinico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>381</td><td>Parotidite recorrente</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>383</td><td>Neuroblastoma prim�rio de boca em uma crian�a de 3 anos de idade: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>388</td><td>Herpes labial recorrente: relato de um caso cl�nico e op��o de tratamento com laser</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>391</td><td>Carcinoma epiderm�ide: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>393</td><td>Displasia cemento-�ssea florida � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>395</td><td>Extenso odontoma de maxila em crian�a de 6 anos de idade</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>396</td><td>Tumor odontog�nico adenomat�ide extrafolicular</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>397</td><td>Manifesta��o de criptococose em regi�o parot�dea.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>398</td><td>Periodontite ulcerativa necrosante como primeiro sinal de desenvolvimento da AIDS. Relato de caso.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>399</td><td>Leucoplasia pilosa oral em portador de diabetes mellitus: relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>400</td><td>Lipoma em mucosa jugal� relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>406</td><td>Sialolit�ase de tamanho avantajado</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>407</td><td>Doen�a �ssea de Paget poliost�tica.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>408</td><td>GVHD e papiloma oral em paciente imunossuprimido: relato de caso cl�nico.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>409</td><td>Linfoma de Hodgkin bucal: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>413</td><td>Carcinoma basocelular com invas�o da cavidade bucal</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>415</td><td>Linfoma n�o-Hodgkin associado � aids</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>418</td><td>Pelos na boca sem enxerto?</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>421</td><td>S�ndrome de Sotos- relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>422</td><td>Osteonecrose associada ao uso de bisfosfonatos</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>424</td><td>Granuloma piog�nico na gravidez</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>425</td><td>Tumor de c�lulas granulares em l�ngua</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>426</td><td>Pigmenta��o dent�ria por biliverdina</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>427</td><td>Ceratoacantoma � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>428</td><td>Hiperplasia linf�ide benigna bilateral em palato</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>430</td><td>Histiocitoma fibroso benigno intra-�sseo: relato de caso raro</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>434</td><td>Plasmocitoma solit�rio de mand�bula: uma manifesta��o cl�nica rara � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>435</td><td>Adenoma pleom�rfico em palato duro</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>436</td><td>Cisto odontog�nico ortoceratinizado: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>437</td><td>Linfoma de c�lulas B: relato de caso.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>441</td><td>Fechamento de f�stula buco-nasal atrav�s de enxerto de conjuntivo.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>444</td><td>Carcinoma aden�ide c�stico - relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>445</td><td>Avalia��o da a��o do diclofenaco em gel de �cido hialur�nico no tratamento da queilite act�nica � acompanhamento de 2 casos</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>448</td><td>Adenocarcinoma polimorfo de baixo grau: relato de caso com an�lise imuno-histoqu�mica</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>449</td><td>Papiloma ductal invertido: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>452</td><td>Cisto do ducto nasopalatino com extens�o para vestibular: tratamento inicial com marsupializa��o</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>455</td><td>S�ndrome de Gorlin: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>457</td><td>Neurofibromatose : ralato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>458</td><td>Osteoblastoma de mand�bula</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>459</td><td>Cisto dent�gero. Relato de caso.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>460</td><td>Met�stase gengival de carcinoma indiferenciado de pulm�o mimetizando um granuloma piog�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>461</td><td>Odontodisplasia regional</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>462</td><td>Reconstru��o �ssea de fenda palatina mediana - apresenta��o de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>474</td><td>R�nula mergulhante: relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>478</td><td>Ameloblastoma: relato de um caso com crescimento r�pido e agressivo</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>479</td><td>Fibrolipoma da cavidade oral: relato de um caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>480</td><td>Penfig�ide benigno de mucosas</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>484</td><td>Relato de dois casos cl�nicos semelhantes em crian�as com diagn�sticos histol�gicos diferentes: queratocisto e cisto dent�gero</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>485</td><td>Mixoma. Relato de caso cl�nico.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>487</td><td>Tumor de Warthin</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>488</td><td>Manifesta��es bucais relacionadas ao diabetes mellitus descompensado</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>489</td><td>Mixoma odontog�nico em maxila: relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>490</td><td>Piercing lingual. Implica��es e riscos para as estruturas bucais.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>491</td><td>Leishmaniose: manifesta��o exclusiva em boca.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>493</td><td>Manifesta��es bucais relacionadas � ansiedade cr�nica e depress�o</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>494</td><td>Cisto nasolabial</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>495</td><td>Carcinoma aden�ide c�stico tratado como sialoadenite</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>496</td><td>Nevo branco esponjoso simulando leucoplasia pilosa. Relato de caso cl�nico.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>497</td><td>A import�ncia de m�ltiplas t�cnicas radiogr�ficas na rotina diagn�stica: relato de um caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>498</td><td>Doen�a cel�aca X sa�de bucal</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>499</td><td>Fibroma ossificante em regi�o posterior de mand�bula, associado � raiz residual</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>500</td><td>Tumor odontog�nico adenomat�ide de aspecto c�stico em maxila: relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>501</td><td>Ceratoacantoma de l�bio inferior diagnosticado ap�s remiss�o espont�nea</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>503</td><td>Manifesta��o oral de mucormicose p�s quimioterapia � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>504</td><td>Carcinoma espinocelular em mucosa jugal</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>505</td><td>Doen�a de parkinson e sa�de bucal</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>506</td><td>Cisto nasopalatino � relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>509</td><td>L�quen plano erosivo em paciente HIV positivo e portador de hepatite C</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>510</td><td>Tumor odontog�nico adenomat�ide em rec�m nato - relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>511</td><td>Reconstru��o cond�lica em paciente com anquilose de articula��o t�mporo-mandibular</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>513</td><td>Tratamento conservador de ceratocisto odontog�nico � relato de um caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>516</td><td>Displasia epitelial severa associada � terapia anti-TNF alfa</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>518</td><td>Manifesta��o incomum de m�ltiplos sarcomas de kaposi em boca</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>521</td><td>Les�o recidivante em regi�o de papila interproximal � relato de caso cl�nico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>522</td><td>Amelog�nese imperfeita: relato de casos cl�nicos com reabilita��o oral</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>527</td><td>Neurofibromatose tipo 1: relato de caso com les�o bucal</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>528</td><td>Uso de corticoster�ide intra-lesional no tratamento do granuloma central de c�lulas gigantes</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>531</td><td>Protocolo cl�nico odontol�gico para o paciente oncol�gico: antes, durante e depois do tratamento anti-neopl�sico</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>536</td><td>Adenoma pleom�rfico em palato mole: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>538</td><td>Necrose �ssea avascular em cavidade bucal associada � quimioterapia e ao uso de bifosfanatos.</td></tr>
					<tr valign=top class=textoComumSmall><td align=center>541</td><td>Diagn�stico precoce de carcinoma mucoepiderm�ide]
				</table>
			</span>

			<span id=atividade4 style="display:'none'" class="TextoComum">
				<b>APRESENTA��O ORAL DE CASOS CL�NICOS</b><br><br>
				<br>
				<table WIDTH=100%>
					<tr class=textoComumSmall>
						<td WIDTH=100% colspan=3>
							<b>Domingo, 17 de julho</b>
						</td>
					</tr>
					<tr bgcolor=#e8e8e8 class=textoComumSmall>
						<td WIDTH=20%>
							HOR�RIO
						</td>
						<td WIDTH=10% align=center>
							No.
						</td>
						<td WIDTH=70%>
							T�TULO
						</td>
					</tr>
					<tr valign=top class=textoComumSmall><td>13:30 � 13:50</td><td align=center>454</td><td>Condrossarcoma mesenquimal em maxila</td></tr>
					<tr valign=top class=textoComumSmall><td>13:55 � 14:15</td><td align=center>250</td><td>Rabdomiossarcoma alveolar � relato de um caso</td></tr>
					<tr valign=top class=textoComumSmall><td>14:20 � 14:40</td><td align=center>300</td><td>Osteossarcoma condrobl�stico em seio maxilar e maxila: relato de caso, e acompanhamento cl�nico.</td></tr>
					<tr valign=top class=textoComumSmall><td>14:45 � 15:05</td><td align=center>512</td><td>Melanoma maligno oral: relato de caso cl�nico </td></tr>
					<tr valign=top class=textoComumSmall><td>15:10 � 15:30</td><td align=center>298</td><td>Leiomiossarcoma em maxila</td></tr>
					<tr valign=top class=textoComumSmall><td>15:35 � 15:55</td><td align=center>023</td><td>Mixoma com invas�o sinusal em paciente idosa - revis�o diagn�stica para mixofibrossarcoma</td></tr>
					<tr valign=top class=textoComumSmall><td>16:00 � 16:20</td><td align=center>296</td><td>Met�stase bucal de carcinoma de c�lulas renais</td></tr>
					<tr valign=top class=textoComumSmall><td colspan=3 bgcolor=#e8e8e8><b>Coffee Break</b><td></tr>
					<tr valign=top class=textoComumSmall><td>16:50 � 17:10</td><td align=center>517</td><td>Tratamento do sarcoma de kaposi com inje��es intra-lesionais de vimblastine realizado pelo cirurgi�o dentista, concomitante ao tratamento sist�mico</td></tr>
					<tr valign=top class=textoComumSmall><td>17:15 � 17:35</td><td align=center>392</td><td>Manifesta��o bucal e anal do linfoma de burkitt em paciente infectado pelo hiv.</td></tr>
					<tr valign=top class=textoComumSmall><td>17:40 � 18:00</td><td align=center>539</td><td>Linfoma plasmabl�stico em paciente soropositivo para o hiv. Relato de caso.</td></tr>
					<tr valign=top class=textoComumSmall><td>18:05 � 18:25</td><td align=center>039</td><td>Linfoma n�o-hodgkin de c�lulas t pleomorfo: relato de caso cl�nico em cavidade oral</td></tr>
					<tr valign=top class=textoComumSmall><td>18:30 � 18:50</td><td align=center>132</td><td>Cistoadenocarcinoma papilar: relato de um caso de alto grau de malignidade</td></tr>
					<tr valign=top class=textoComumSmall><td>18:55 � 19:15</td><td align=center>447</td><td>Pseudotumor hemof�lico em maxila: relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td>19:20 � 19:40</td><td align=center>253</td><td>Coristoma glial em l�ngua</td></tr>
					<tr valign=top class=textoComumSmall><td>19:45 � 20:05</td><td align=center>251</td><td>Tratamento medicamentoso da hipertrofia do m�sculo masseter com a utiliza��o da toxina botul�nica tipo a</td></tr>

					<tr class=textoComumSmall>
						<td WIDTH=100% colspan=3>
							<br><br><b>Ter�a-feira, 19 de julho</b>
						</td>
					</tr>
					<tr bgcolor=#e8e8e8 class=textoComumSmall>
						<td WIDTH=20%>
							HOR�RIO
						</td>
						<td WIDTH=10% align=center>
							No.
						</td>
						<td WIDTH=70%>
							T�TULO
						</td>
					</tr>
					<tr valign=top class=textoComumSmall><td>09:00 � 09:20</td><td align=center>238</td><td>Adenoma pleom�rfico multic�ntrico em gl�ndula submandibular � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td>09:25 � 09:45</td><td align=center>380</td><td>Schwanoma intra-�sseo em mand�bula</td></tr>
					<tr valign=top class=textoComumSmall><td>09:50 � 10:10</td><td align=center>325</td><td>Abordagem conservadora de um incomum caso de hemangioendotelioma intra-�sseo de mand�bula</td></tr>
					<tr valign=top class=textoComumSmall><td>10:15 � 10:35</td><td align=center>540</td><td>S�ndrome de stevens-johnson associado � s�ndrome do anticorpo anti-fosfol�pedes (saaf).</td></tr>
					<tr valign=top class=textoComumSmall><td>10:40 � 11:00</td><td align=center>507</td><td>L�quen plano bucal e complica��o da corticoterapia t�pica e sist�mica: rea��o de cushing</td></tr>
					<tr valign=top class=textoComumSmall><td>11:05 � 11:25</td><td align=center>530</td><td>Coexist�ncia de p�nfigo vulgar e infec��o pelo v�rus herpes simples na mucosa oral � diagn�stico citopatol�gico e histopatol�gico</td></tr>
					<tr valign=top class=textoComumSmall><td>11:30 � 11:50</td><td align=center>473</td><td>Mucosite bucal exacerbada em paciente transplantado de medula �ssea associado ao quadro de insuficiencia renal.</td></tr>
					<tr valign=top class=textoComumSmall><td>11:55 � 12:15</td><td align=center>002</td><td>Fibromatose gengival heredit�ria � aspectos cl�nicos, microsc�picos e gen�ticos de uma fam�lia com cinco gera��es afetadas</td></tr>
					<tr valign=top class=textoComumSmall><td colspan=3 bgcolor=#e8e8e8><b>Almo�o</b><td></tr>
					<tr valign=top class=textoComumSmall><td>15:30 � 15:50</td><td align=center>059</td><td>Extenso cementoblastoma em maxila: relato de caso e considera��es atuais sobre seu comportamento biol�gico.</td></tr>
					<tr valign=top class=textoComumSmall><td>15:55 � 16:15</td><td align=center>299</td><td>Fibroma odontog�nico central com componente semelhante a les�o de c�lulas gigantes</td></tr>
					<tr valign=top class=textoComumSmall><td>16:20 � 16:40</td><td align=center>324</td><td>Tratamento n�o invasivo de pacientes portadores de osteomielite cr�nica prim�ria de mand�bula</td></tr>
					<tr valign=top class=textoComumSmall><td>16:45 � 17:05</td><td align=center>508</td><td>Valor dos exames complementares no diagn�stico das les�es de c�lulas gigantes � relato de caso</td></tr>
					<tr valign=top class=textoComumSmall><td>17:10 � 17:30</td><td align=center>006</td><td>Tumor odontog�nico epitelial calcificante: relato de um caso de comportamento biol�gico agressivo</td></tr>
					<tr valign=top class=textoComumSmall><td>17:35 � 17:55</td><td align=center>486</td><td>Odontodisplasia regional: aspectos cl�nicos, radiogr�ficos e histol�gicos. Relato de caso.</td></tr>
					<tr valign=top class=textoComumSmall><td>18:00 � 18:20</td><td align=center>072</td><td>Osteopetrose maligna</td></tr>
					<tr valign=top class=textoComumSmall><td>18:25 � 18:45</td><td align=center>423</td><td>Marsupializa��o de cisto odontog�nico calcificante</td></tr>
					<tr valign=top class=textoComumSmall><td>18:50 � 19:10</td><td align=center>162</td><td>Cisto derm�ide terat�ide</td></tr>
			</span>
		</td>
	</tr>
</table>
