<p class="Titulo" align="center">Estat�stica das atividades</p>
<p class="textoComum">
	<b>1) CURSOS:</b> Ser�o ministrados 11 cursos conforme distribui��o prevista no <a href="default_old.asp?md=2&it=1" style="decoration:none;">Programa Cient�fico Preliminar</a>
</p>
<p class="textoComum">
	A escolha de nossa cidade para sediar um evento deste peso, representa para n�s uma honraria impar. Durante o evento as aten��es dos congressistas estar�o voltadas para a pauta em discuss�o, cuja excel�ncia � sabidamente indiscut�vel.<br><br>Para aqueles que optarem por chegar a Florian�polis antes da data de in�cio das atividades, ou nela permanecer al�m do encerramento, a capital do Estado de Santa Catarina tem a oferecer uma natureza que a coloca entre as mais belas do Brasil. 
</p>
<p class="textoComum">
	Com mais de oitenta por cento de seu territ�rio em uma ilha oce�nica, a cidade possu� uma centena de praias, duas lagoas, exuberante vegeta��o e s�tios hist�rico-culturais.<br><br>Al�m disso, Florian�polis � considerada pela ONU a capital brasileira com a melhor qualidade de vida, e a primeira cidade do pa�s em que o  �ndice de mortalidade infantil � de apenas um d�gito. � essa a cidade que os espera, de bra�os abertos. 
</p>
<p class="textoComum">
Sejam Bem Vindos.
</p>
<p class="textoComum">
<b>Angela Regina Heinzen Amin Helou</b><br>
Prefeita de Florian�polis
</p>