<script>
function popUp(URL,nomeJanela,props){
	window.open(URL,nomeJanela,props);
}

function mostrarNota(nota){

	if (nota == 1)
		altura = 150;
	else
		altura = 300;

	url = 'mostraNotas.asp?nota=' + nota;
	params = 'scrollbars=no,width=600,height=' + altura;
	id = 'mostraNotas';
	popUp(url,id,params);
}

function submeterFormTrabalho(pr4){
	document.Form1.pr4.value = pr4;
	submeter(7,3,1,0);
}
</script>
<% 
select case pr1_wk
	case 0
		%>
		<p class="Titulo" align="center">NORMAS PARA INSCRI��O DE TRABALHOS<br>(GERAL)</p>
		<p class="textoComum">
			<br>
			<b>Prazo para Inscri��o de Trabalhos Prorrogado: <U>11 de abril de 2005</U></b>
		</p>
		<p class="textoComum">
			<br>
			<b>COMO INSCREVER SEU TRABALHO</b>
		</p>
		<p class="textoComum">
			A inscri��o de trabalhos ser� feita exclusivamente via Internet (online), neste website (www.estomatologia.com.br/congresso2005). O <STRONG>apresentador</STRONG> do trabalho dever� necessariamente estar inscrito no Congresso (ser� solicitado o n�mero de inscri��o no Congresso para inscrever trabalhos). O mesmo apresentador poder� inscrever no m�ximo 03 (tr�s) trabalhos, incluindo todas as categorias. Ser� emitido apenas um certificado por trabalho apresentado.
		</p>
		<p class="textoComum">
			<b>MECANISMO DE AVALIA��O</b>
		</p>
		<p class="textoComum">
			Os resumos ser�o enviados, sem identifica��o, a tr�s avaliadores, que conceder�o uma nota de 0 (zero) a 10 (dez) ao trabalho, preenchendo uma ficha de avalia��o. <STRONG>N�o deve haver, nos resumos, nenhuma refer�ncia que permita identificar a origem (institui��o ou local) do trabalho, sob pena da exclus�o do mesmo</STRONG>. A m�dia aritm�tica das avalia��es ser� utilizada como crit�rio de sele��o, sendo selecionados os trabalhos com maior m�dia para o preenchimento consecutivo do n�mero de trabalhos a serem apresentados, em cada categoria. N�o haver� possibilidade de revis�o da avalia��o. Os autores ser�o informados por e-mail sobre a aceita��o do trabalho. A listagem dos trabalhos aceitos ser� tamb�m publicada no site do Congresso. 
		</p>
		<p class="textoComum">
			<b>T�PICOS A SEREM ABORDADOS :</b> 
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>Estomatologia 
				<li>Patologia Bucal 
				<li>Radiologia Odontol�gica e Imaginologia 
				<li>Cirurgia e Traumatologia Bucomaxilofacial
				<li>Outros
			</ul> 
		</p>
		<p class="textoComum">
			<b>CONTATO:</b><br>
			Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia<br>
			AC Cidade Universit�ria<br>
			Caixa Postal 5243<br>
			88040-970 Florian�polis � SC<br>
			<a href="mailto:sobe2005_ccientifica@tavares.eti.br">sobe2005_ccientifica@tavares.eti.br</a>
		</p>
		<p class="textoComum">
			<b>OP��ES DE INSCRI��O</b> <i>(CLIQUE PARA ABRIR A OP��O)</i>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li><a href="javascript:submeter(7,1,1,1);">Apresenta��o oral de caso cl�nico</a><br>
				<li><a href="javascript:submeter(7,1,1,2);">Caso consultivo</a><br>
				<li><a href="javascript:submeter(7,1,1,3);">Painel de Pesquisa</a>
				<li><a href="javascript:submeter(7,1,1,4);">Painel Cl�nico</a>
			</ul>
		</p>
		<%
	case 1
		%>
		<p class="Titulo" align="center">NORMAS PARA INSCRI��O DE TRABALHOS<br>(APRESENTA��O ORAL DE CASOS CL�NICOS)</p>
		<p class="textoComum">
			<small>(20 minutos: 15 minutos para exposi��o e 5 minutos para discuss�o) </small>
		</p>
		<p class="textoComum">
			A apresenta��o oral de casos cl�nicos � uma oportunidade para os congressistas compartilharem sua experi�ncia cl�nica no diagn�stico e tratamento de doen�as da boca. Uma banca composta por profissionais de reconhecida compet�ncia ser� respons�vel pela discuss�o do caso, bem como pela avalia��o do mesmo. A sala para esta atividade estar� equipada com 01 (um) projetor multim�dia (datashow). 
		</p>
		<p class="textoComum">
			<b>Os casos cl�nicos apresentados oralmente ser�o publicados na �ntegra, na forma de artigo, nos Anais Eletr�nicos do XIII Congresso da Sociedade Brasileira de Estomatologia</b>. Por este motivo, os trabalhos submetidos a esta forma de apresenta��o ter�o dois prazos importantes a cumprir: 
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li><STRONG>11 de abril de 2005</STRONG>: prazo final <b>prorrogado</b> para o envio do RESUMO do trabalho, que dever� cumprir orienta��es espec�ficas de confec��o, a serem divulgadas brevemente. Dever�o ser enviadas, tamb�m, TODAS AS IMAGENS a serem publicadas. A lista dos trabalhos selecionados ser� divulgada aos autores por e-mail, bem como no site do evento. 
				<li><STRONG>27 de maio de 2005</STRONG>: prazo final para o envio do ARTIGO COMPLETO para publica��o. Por quest�es de tempo h�bil para a confec��o dos Anais, este prazo n�o ser� prorrogado.
			</ul>
		</p>
		<p class="textoComum">
			<STRONG>IMPORTANTE</STRONG>: Lembramos que, de acordo com Resolu��o 196/96 do Conselho Nacional de Sa�de, as imagens a serem publicadas n�o devem identificar o paciente <a href="javascript:mostrarNota(1);">(1)</a>. No caso da imagem permitir a identifica��o do paciente, os autores dever�o encaminhar por carta registrada a <STRONG>c�pia autenticada da Autoriza��o para Uso de Imagem</STRONG> (que pode estar inclu�da como um item dentro do Termo de Consentimento Livre e Esclarecido), assinada pelo paciente ou por seu respons�vel legal. Nesta autoriza��o dever� constar a forma de utiliza��o prevista para a(s) imagem(ns). A obten��o desse consentimento � responsabilidade �nica do(s) autor(es) do trabalho. <STRONG>A n�o apresenta��o deste documento em at� 10 (dez) dias ap�s a divulga��o da lista dos trabalhos selecionados implicar� na exclus�o do trabalho</STRONG>. Para fins de comprova��o, ser� considerada a data de postagem; recomendamos que este documento seja enviado como encomenda registrada, de modo a permitir sua localiza��o em caso de extravio. 
		</p>
		<p class="textoComum">
			<STRONG>PREPARANDO O RESUMO E AS IMAGENS</STRONG>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>O resumo dever� ser digitado diretamente no <a href="javascript:submeterFormTrabalho(4);">formul�rio de inscri��o</a>, contendo no m�ximo 600 palavras (o n�mero de palavras j� digitadas constar� abaixo da caixa de digita��o do texto). 
				<li>O resumo deve ser o mais informativo poss�vel. N�o � poss�vel a inclus�o de tabelas.
				<li>Somente ser�o aceitos trabalhos que demonstrem claramente que o caso cl�nico foi conclu�do.
				<li>Abreviaturas: o termo dever� constar por extenso quando mencionado pela primeira vez, seguido da abreviatura em par�nteses.
				<li><STRONG>TODAS AS IMAGENS QUE CONSTAR�O NO ARTIGO DEVER�O SER ENVIADAS PARA A FASE DE SELE��O</STRONG>. As imagens - m�ximo de 6 (seis) - ser�o enviadas atrav�s do pr�prio formul�rio eletr�nico, bastando para isso seguir as instru��es online. Considerar que o artigo final dever� ter no m�ximo 8 p�ginas (adequar o n�mero de imagens). A resolu��o das imagens nesta fase de sele��o dever� ser de 72dpi (permitindo boa visualiza��o em monitores). Posteriormente, no envio do artigo, a resolu��o dever� ser de 300dpi (permitindo qualidade na impress�o).
			</ul>
		</p>
		<p class="textoComum">
			<STRONG>Observa��es</STRONG>: 
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>O n�mero de apresenta��es orais de casos cl�nicos � reduzido, em fun��o do tempo dispon�vel para tal durante o evento. Assim, ser�o selecionados para esta categoria os trabalhos que obtiverem as maiores m�dias no processo de avalia��o, a cargo de avaliadores externos � Comiss�o Cient�fica. 
				<li>No caso do trabalho n�o ser selecionado para apresenta��o oral, haver� a possibilidade de submeter o mesmo resumo para avalia��o na categoria Painel Cl�nico. Para tal, os autores dever�o autorizar a Comiss�o Cient�fica a proceder esta altera��o de categoria, escolhendo uma das alternativas no processo de inscri��o: exclus�o do trabalho ou altera��o de categoria.
			</ul>
		</p>
		<p class="textoComum">
			<STRONG>ENVIANDO A C�PIA DA AUTORIZA��O PARA USO DE IMAGEM</STRONG>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>A c�pia deve estar autenticada.
				<li>Enviar por encomenda registrada ou SEDEX para:<br>
				<b>Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia<br>
				Categoria: Casos Cl�nicos<br>
				AC Cidade Universit�ria<br>
				Caixa Postal 5243<br>
				88040-970 Florian�polis � SC<br><br>
				</b>
			</ul>
		</p>
		<p class="textoComum">
			<STRONG>PREPARANDO O ARTIGO</STRONG>
		</p>
		<p class="textoComum">
			Visando facilitar a confec��o do artigo, bem como uniformizar a formata��o do conjunto para os Anais, disponibilizamos um modelo para ser editado no MS Word 6.0 ou superior. <a href="downloads/modeloArtigoCompleto.doc" target=nova>Clique aqui</a> para fazer o download deste modelo.
		</p>
		<p class="textoComum">
			<STRONG>A APRESENTA��O</STRONG>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>A sala para a apresenta��o oral de casos cl�nicos estar� equipada com 01 (um) projetor multim�dia (datashow).
				<li>N�o estar�o dispon�veis ponteiras a laser, devendo o apresentador portar este equipamento, caso deseje utiliz�-lo.
				<li>A apresenta��o dever� ser preparada utilizando o software PowerPoint, para Windows. Os arquivos dever�o ser entregues (gravados em CDs ou em dispositivos de arquivamento com sa�da USB) no midiadesk nos prazos fixados na tabela abaixo. 
					<br>
					&nbsp;<br>
					<table width=70% align="center" border=0>
						<tr class="textoComum" align=center>
							<td><STRONG>Apresenta��o do caso</STRONG></td>
							<td><STRONG>Confer�ncia da apresenta��o</STRONG></td>
						<tr>
						<tr class="textoComum" align=center>
							<td>17/07 - tarde</td>
							<td>17/07 at� as 12:00h</td>
						<tr>
						<tr class="textoComum" align=center>
							<td>19/07 - manh�</td>
							<td>18/07 at� as 18:00h</td>
						<tr>
						<tr class="textoComum" align=center>
							<td>19/07 - tarde</td>
							<td>19/07 at� as 12:00h</td>
						</tr>
					</table>
					<br>
				<li>Eventualmente, podem ocorrer incompatibilidades entre diferentes vers�es do PowerPoint; por este motivo, o apresentador que desejar conferir sua apresenta��o dever� dirigir-se ao midiadesk com pelo menos duas horas de anteced�ncia, para efetuar pequenos ajustes de formata��o. 
				<li>O tempo da apresenta��o ser� rigorosamente controlado (15 minutos). 
			</ul>
		</p>
		<p class="textoComum">
			<a href="javascript:submeter(7,1,1,0);">RETORNAR</a><br><br>
		</p>
		<%
	case 2
		%>
		<p class="Titulo" align="center">NORMAS PARA INSCRI��O DE TRABALHOS<br>(CASO CL�NICO CONSULTIVO)</p>
		<p class="textoComum">
			<br>
			<small>(30 minutos: 20 minutos para exposi��o e 10 minutos para discuss�o)</small>
		</p>
		<p class="textoComum">
			Esta atividade tem por finalidade a discuss�o de casos cl�nicos de dif�cil diagn�stico ou tratamento. O apresentador ter� a oportunidade de consultar profissionais experientes, al�m de expor d�vidas e trocar conhecimentos e informa��es com os demais congressistas. O objetivo � auxiliar na elucida��o ou andamento do caso apresentado. 
		</p>
		<p class="textoComum">
			A sala para esta atividade estar� equipada com 01 (um) projetor multim�dia (datashow). Lembramos que para que uma imagem cl�nica possa ser utilizada em uma apresenta��o p�blica deve-se obter o consentimento (autoriza��o para uso de imagem) do paciente ou do seu respons�vel legal <a href="javascript:mostrarNota(1);">(1)</a>. A obten��o desse consentimento � responsabilidade �nica do(s) autor(es) do trabalho. A Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia n�o se responsabiliza pela inexist�ncia da obten��o do consentimento.
		</p>
		<p class="textoComum">
			<b>PREPARANDO O RESUMO</b>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>O resumo dever� ser digitado diretamente no <a href="javascript:submeterFormTrabalho(1);">formul�rio de inscri��o</a>, contendo no m�ximo 250 palavras (o n�mero de palavras j� digitadas constar� junto da caixa de digita��o do texto). 
				<li>O resumo deve ser o mais informativo poss�vel. N�o � poss�vel a inclus�o de tabelas.
				<li>Abreviaturas: o termo dever� constar por extenso quando mencionado pela primeira vez, seguido da abreviatura em par�nteses.
				<li>Os Anais Eletr�nicos do Congresso permitir�o a busca de trabalhos por palavras-chave. Visando facilitar a pesquisa, as palavras-chave dever�o ser determinadas de acordo com o DECs (<a href="http://decs.bvs.br" target=nova>http://decs.bvs.br</a>) ou listas de cabe�alhos de assunto do Index to Dental ou Index Medicus.
				<li>Ser�o consideradas importantes na avalia��o as imagens cl�nicas, de exames imaginol�gicos, histopatol�gicos ou outros.  Estas imagens dever�o ser enviadas para o processo de sele��o, no momento da inscri��o do trabalho.			</ul>
		</p>
		<p class="textoComum">
			<b>A APRESENTA��O</b>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>A sala para a apresenta��o de casos consultivos estar� equipada com 01 (um) projetor multim�dia (datashow).
				<li>N�o estar�o dispon�veis ponteiras a laser, devendo o apresentador portar este equipamento, caso deseje utiliz�-lo.
				<li>A apresenta��o dever� ser preparada utilizando o software PowerPoint, para Windows. Os arquivos dever�o ser entregues (gravados em CDs ou em dispositivos de arquivamento com sa�da USB) no midiadesk, at� as 12:00h do dia 20/07.
				<li>Eventualmente, podem ocorrer incompatibilidades entre diferentes vers�es do PowerPoint; por este motivo, o apresentador que desejar conferir sua apresenta��o dever� dirigir-se ao midiadesk com pelo menos duas horas de anteced�ncia, para efetuar pequenos ajustes de formata��o. 
			</ul>
		</p>
		<p class="textoComum">
			<a href="javascript:submeter(7,1,1,0);">RETORNAR</a>
		</p>
		<%
	case 3
		%>
		<p class="Titulo" align="center">NORMAS PARA INSCRI��O DE TRABALHOS<br>(PAINEL DE PESQUISA)</p>
		<p class="textoComum">
			<br>
			<ul class="textoComum">
				<li>Dimens�es m�ximas: 0,80m de largura � 1,30m de comprimento
				<li>O painel dever� ser auto-explicativo, e estruturado contemplando os seguintes subt�tulos: T�tulo, Autores e Afilia��es, Introdu��o, Justificativa, Objetivos, Metodologia, Resultados, Conclus�es e Bibliografia. 
				<li>No caso de terem sido realizados experimentos envolvendo seres humanos ou animais de laborat�rio <a href="javascript:mostrarNota(2);">(2)</a>, os autores dever�o encaminhar por carta registrada a <STRONG>c�pia autenticada da aprova��o da realiza��o da pesquisa por Comit� de �tica em Pesquisa</STRONG> � Coordena��o Cient�fica do Congresso, em at� 10 dias ap�s a notifica��o por e-mail da aceita��o do trabalho (ser� considerada a data da postagem). Recomendamos que este documento seja enviado como encomenda registrada, de modo a permitir sua localiza��o em caso de extravio. <STRONG>A n�o apresenta��o deste documento em at� 10 (dez) dias ap�s a divulga��o da lista dos trabalhos selecionados implicar� na exclus�o do trabalho</STRONG>.
			</ul>
		</p>
		<p class="textoComum">
			<STRONG>PREPARANDO O RESUMO</STRONG>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>O resumo dever� ser digitado diretamente no <a href="javascript:submeterFormTrabalho(2);">formul�rio de inscri��o</a>, contendo no m�ximo 300 palavras (o n�mero de palavras j� digitadas constar� abaixo da caixa de digita��o do texto). 
				<li>O resumo deve ser o mais informativo poss�vel. N�o � poss�vel a inclus�o de tabelas.
				<li>Abreviaturas: o termo dever� constar por extenso quando mencionado pela primeira vez, seguido da abreviatura em par�nteses.
				<li>Os Anais Eletr�nicos do Congresso permitir�o a busca de trabalhos por palavras-chave. Visando facilitar a pesquisa, as palavras-chave dever�o ser determinadas de acordo com o DECs (<a href="http://decs.bvs.br" target=nova>http://decs.bvs.br</a>) ou listas de cabe�alhos de assunto do Index to Dental ou Index Medicus.
			</ul>
		</p>
		<p class="textoComum">
			<STRONG>ENVIANDO A C�PIA DA APROVA��O PELO COMIT� DE �TICA</STRONG>
			<ul class="textoComum">
				<li>A c�pia deve estar autenticada.
				<li>Enviar por encomenda registrada ou SEDEX para:
				<br>
				<b>Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia<br>
				Categoria: Painel de Pesquisa<br>
				AC Cidade Universit�ria<br>
				Caixa Postal 5243<br>
				88040-970 Florian�polis � SC
				</b>
			</ul>
		</p>
		<p class="textoComum">
			<a href="javascript:submeter(7,1,1,0);">RETORNAR</a>
			<br><br>
		</p>
		<%
	case 4
		%>
		<p class="Titulo" align="center">NORMAS PARA INSCRI��O DE TRABALHOS<br>(PAINEL CL�NICO)</p>
		<p class="textoComum">
			<br>
			<ul class="textoComum">
				<li>Dimens�es m�ximas: 0,80m de largura � 1,30m de comprimento
				<li>O painel dever� ser auto-explicativo. Sugere-se que seja estruturado contemplando os seguintes subt�tulos (quando aplic�veis): T�tulo, Autores e Afilia��es, Introdu��o, Descri��o, Diagn�stico, Tratamento e Acompanhamento, Bibliografia.
				<li>Ser�o consideradas importantes na avalia��o, quando aplic�veis: imagens cl�nicas, de exames imaginol�gicos e histopatol�gicos, informa��es sobre o tratamento e acompanhamento.
				<li>Lembramos que para que uma imagem cl�nica possa ser utilizada em uma apresenta��o p�blica deve-se obter o consentimento (autoriza��o para uso de imagem) do paciente ou do seu respons�vel legal. A obten��o desse consentimento � responsabilidade �nica do(s) autor(es) do trabalho. A Comiss�o Cient�fica do XIII Congresso Brasileiro de Estomatologia n�o se responsabiliza pela inexist�ncia da obten��o do consentimento.			</ul>
		</p>
		<p class="textoComum">
			<STRONG>PREPARANDO O RESUMO</STRONG>
		</p>
		<p class="textoComum">
			<ul class="textoComum">
				<li>O resumo dever� ser digitado diretamente no <a href="javascript:submeterFormTrabalho(3);">formul�rio de inscri��o</a>, contendo no m�ximo 250 palavras (o n�mero de palavras j� digitadas constar� abaixo da caixa de digita��o do texto). 
				<li>O resumo deve ser o mais informativo poss�vel. N�o � poss�vel a inclus�o de tabelas.
				<li>Abreviaturas: o termo dever� constar por extenso quando mencionado pela primeira vez, seguido da abreviatura em par�nteses.
				<li>Os Anais Eletr�nicos do Congresso permitir�o a busca de trabalhos por palavras-chave. Visando facilitar a pesquisa, as palavras-chave dever�o ser determinadas de acordo com o DECs (<a href="http://decs.bvs.br" target=nova>http://decs.bvs.br</a>) ou listas de cabe�alhos de assunto do Index to Dental ou Index Medicus.
			</ul>
		</p>
		<p class="textoComum">
			<a href="javascript:submeter(7,1,1,0);">RETORNAR</a>
			<br><br>
		</p>
		<%
	case 5
	case 6
end select
%>