<%
strContato_wk = "SOLICITA��O DE CARTA DE CONVITE"
strNome_wk = "Nome Completo"
strInstituicao_wk = "Institui��o"
strEndereco_wk = "Endere�o Completo para Envio"
strMsg_wk = "Complementos"
strMensagemEnviada_wk = "Solicita��o enviada com sucesso! Obrigado!"
strMensagemNaoEnviada_wk = "Mensagem n�o enviada. Tente novamente mais tarde. Obrigado!"
strEnviar_wk = "Enviar"
strSolicita = "Este servi�o se destina aqueles que necessitam de uma carta de convite para solicitar em suas institui��es a libera��o para participa��o neste evento.<br><br>Por favor preencha corretamente o formul�rio abaixo. Sua carta ser� elaborada e enviada t�o logo sua solicita��o seja recebida."
strFim_wk = "Finalidade"
strErro1 = "Informe o Nome Completo."
strErro2 = "Informe a Institui��o."
strErro3 = "Informe o Endere�o para Envio."
strErro4 = "Informe a Finalidade."
%>
<script>
function verificaForm(){
	if (document.Form1.nome99.value.length == 0){
		alert('<%=strErro1%>');
		document.Form1.nome99.focus();
		return false;
	}

	if (document.Form1.instituicao99.value.length == 0){
		alert('<%=strErro2%>');
		document.Form1.instituicao99.focus();
		return false;
	}

	if (document.Form1.endereco99.value.length == 0){
		alert('<%=strErro3%>');
		document.Form1.endereco99.focus();
		return false;
	}
	
	if (!document.Form1.visa99.checked && !document.Form1.permissao99.checked){
		alert('<%=strErro4%>');
		document.Form1.visa99.focus();
		return false;
	}

	submeter(5,6,<%=id_wk%>,1);
}
</script>
<p class="Titulo" align="center"><%=strContato_wk%></p>
<%
if pr1_wk = 0 then
%>
	<p class=textoComum>
		<%=strSolicita%>
		<br><br>
		<%=strNome_wk%><br>
		<input type=text size=75 name=nome99 class=textbox01><br><br>
		<%=strInstituicao_wk%><br>
		<input type=text size=75 name=instituicao99 class=textbox01><br><br>
		<%=strEndereco_wk%><br>
		<input type=text size=75 name=endereco99 class=textbox01><br><br>
		<%=strFim_wk%><br>
		<input type=checkbox name=visa99 class=textbox01>&nbsp;VISA&nbsp;&nbsp;&nbsp;
		<input type=checkbox name=permissao99 class=textbox01>&nbsp;Permiss�o para participar do congresso<br><br>
		<%=strMsg_wk%><br>
		<textarea cols=75 rows=8 name=texto99 class=textarea01></textarea><br><br>
		<input type=button class=botaoPadrao name=enviar value=<%=strEnviar_wk%> onclick="verificaForm();">
	</p>
<% 
else

	strFinalidade = ""
	if request("visa99")<>"" then strFinalidade = strFinalidade & " VISA " 
	if request("permissao99")<>"" then strFinalidade = strFinalidade & "PERMISSAO" 

	'cria o objeto para o envio de e-mail 
	Set objCDOSYSMail = Server.CreateObject("CDO.Message")

	'cria o objeto para configura��o do SMTP 
	Set objCDOSYSCon = Server.CreateObject ("CDO.Configuration")

	'SMTP 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp2.locaweb.com.br"

	'porta do SMTP 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25

	'porta do CDO 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2

	'timeout 
	objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 

	objCDOSYSCon.Fields.update 

	'atualiza a configura��o do CDOSYS para o envio do e-mail 
	Set objCDOSYSMail.Configuration = objCDOSYSCon

	'e-mail do remetente 
	objCDOSYSMail.From = trim(request("nome99")) & " (por meio do web site)<XIII_CBE@tavares.eti.br>"

	'e-mail do destinat�rio 
	'objCDOSYSMail.To = "marcelo@tavares.eti.br"
	objCDOSYSMail.To = "Secretaria do XIII CBE <sobefloripa2005@ccs.ufsc.br>"

	'assunto da mensagem 
	objCDOSYSMail.Subject = "Solicita��o de Carta de Convite"

	'conte�do da mensagem 
	str_CrLf = chr(13) & chr(10)
	objCDOSYSMail.TextBody = "Nome: " & request("nome99") & str_CrLf & str_CrLf & "Institui��o : " & request("instituicao99") & str_CrLf & str_CrLf & "Endere�o : " & request("endereco99") & str_CrLf & str_CrLf & "Finalidade(s) : " & strFinalidade & str_CrLf & str_CrLf & "Outras Informa��es : " & request("texto99")
	'para envio da mensagem no formato html altere o TextBody para HtmlBody 
	'objCDOSYSMail.HtmlBody = "Teste do componente CDOSYS"

	'objCDOSYSMail.fields.update
	'envia o e-mail 
	objCDOSYSMail.Send 

	'destr�i os objetos 
	Set objCDOSYSMail = Nothing 
	Set objCDOSYSCon = Nothing 
%>
<center>
	<h5><%=strMensagemEnviada_wk%></h5>
</center>
<% end if %>