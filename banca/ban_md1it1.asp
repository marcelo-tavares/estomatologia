<%
function testaZero(q)
	if isnull(q) then
		testaZero = """"""
	else
		if q = 0 then
			testaZero = """"""
		else
			testaZero = """" & replace(Cstr(q),",",".") & """"
		end if
	end if
end function
%>
<script>
	function ordenar(pr1, ordem) {
		document.Form1.h_ordem.value = ordem;
		submeter(1,1,0);
	}
			
	function visualizar(cd_trabalho) {
		document.Form1.hcd_trabalho.value = cd_trabalho;
		submeter(1,1,1);
	}

	function  registrarNota(cd_trabalho) {
		nota = document.Form1.nota.value
		if (nota == '') nota = 'a';
		if (!(isFinite(nota))){
			alert('Valor inv�lido. Obs.: decimais devem ser indicados por "."')
		}
		else {
			if (confirm('Confirma a atribui��o da nota ' + nota + ' para o trabalho N� ' + cd_trabalho + ' ?')) {
					document.Form1.hcd_trabalho.value = cd_trabalho;
					submeter(1,1,2);
				}
		}
	}

	function paginar(codPag){
		document.Form1.paginacao.value = codPag;
		submeter(1,1,0);
	}
</script>
<p class="Titulo" align="center">VISUALIZA��O DE CASOS CL�NICOS</p>
<form name="Form1" method="Post">
	<input type="hidden" name="h_ordem" value="<%=ordenacao_wk%>">
	<input type="hidden" name="paginacao">
	<input type="hidden" name="hcd_trabalho">
	<input type="hidden" name="hcd_modo">
	<%
	select case pr1_wk
		case 0		
		
		' Conectar com o banco
		%>
		<!--#include file=conn.asp -->
		<%
		Set Conn = Server.CreateObject("ADODB.Connection") 
		Conn.Open cnpath

		set rsD = server.CreateObject("ADODB.Recordset")
		set rsD = Conn.Execute("Select vl_descricao from TB_Config where cd_codigo = 5")
		tm_pagina_wk = cdbl(rsD("vl_descricao"))
		%>
		
			<table BORDER="0" CELLSPACING="0" CELLPADDING="0" class=tabelaProgramacao>
				<tr>
					<td>
					<%
					' Recuperar vari�veis de formul�rio
					ordenacao_wk = request("h_ordem")
					if ordenacao_wk = "" then ordenacao_wk = 1
							
					filtroNome_wk = request("filtroNome")
					filtroTitle_wk = request("filtroTitle")
					filtroModo_wk = request("filtroModo")

					select case pr1_wk
						case 0
							' Tratamento de Paginacao
							paginacao_wk = request("paginacao")
							if paginacao_wk = "" then paginacao_wk = 0
								
							select case paginacao_wk
								case 0
									session("paginaAtual")=1
								case 1
									session("paginaAtual")=1
								case 2
									if session("paginaAtual") > 1 then
										session("paginaAtual") = session("paginaAtual") - 1
									end if
								case 3
									if session("paginaAtual") < session("tp") then
										session("paginaAtual") = session("paginaAtual") + 1
									end if			
								case 4
									session("paginaAtual") = session("tp")
							end select
							
							' Criar o recordSet
							set rsT = server.CreateObject("ADODB.RecordSet")	
								
							sqlStr = "SELECT t.* FROM TB_Trabalhos t " _
							& " WHERE t.cd_modo = 1 and sn_aceito = true" _
							& " ORDER BY " & ordenacao_wk & ";"

							'Response.Write  sqlStr
							'Response.Flush
							
							' pagina��o
							rsT.PageSize = tm_pagina_wk
							rsT.CacheSize = 5
							rsT.Open sqlStr, Conn, adOpenKeyset
							totalRegistros_wk = rsT.recordcount
							totalPaginas_wk = CInt(rsT.PageCount)
							tamanhoPagina_wk = rsT.PageSize
							session("tp") = totalPaginas_wk
							reg=0	
								
							%>
							<table width="100%" border="0">
								<tr class="CelulaTitulo">
									<b>
										<td width="100%" colspan="4" align=center>Resumos</td>
									</b>
								</tr>
							<% if rsT.RecordCount = 0 then %>
								<tr><td align=center><h5>Nenhum Trabalho encontrado.</h5></td><tr>
							<% else %>
								<% ' linha de t�tulo %>
								<tr class="CelulaCorpo">
									<b>
									<td width="5%">&nbsp;</td>
									<td width="5%"><a href="javascript:ordenar(0,1)">C�d.</a></td>
									<td width="85%"><a href="javascript:ordenar(0,11)">Titulo</a></td>
									<td width="5%" align="left">A��es</td>
									</b>
								</tr>
								<% ' linhas de detalhes %>
								<%
								flagLinha_wk = 1
								rsT.AbsolutePage = Cint(session("paginaAtual"))			
								while ((not rsT.EOF) and (reg < tamanhoPagina_wk))
									if flagLinha_wk then 
										nomeClasse = "txtTabelaLida"
									else	
										nomeClasse = "txtTabela"
									end if
									flagLinha_wk = abs(flagLinha_wk - 1)
									select case rsT("cd_modo")
										case 1
											strImg = "CCC"
											strCat = "Caso Cl�nico Consultivo"
										case 2
											strImg = "PP "
											strCat = "Painel de Pesquisa"
										case 3
											strImg = "PC"
											strCat = "Painel Consultivo"
										case 4
											strImg = "O  "
											strCat = "Apresenta��o Oral de Caso Cl�nico"
									end select
									%>
									<tr class="CelulaCorpo">
										<td width="100%" colspan="4"><hr></td>
									</tr>
									<tr class="CelulaCorpo">
										<td width="5%"><strong><%=strImg%></strong></td>
										<td width="5%" align="center" class="<%=strClasse%>"><%=rsT("cd_trabalho")%></td>
										<td width="85%" class="<%=strClasse%>"><%=Ucase(rsT("de_titulo"))%><%=strNota%></td>
										<td width="5%" align="center">
											<img src="../images/ico_lupa.gif" border="0" onclick="visualizar(<%=rsT("cd_trabalho")%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" alt="Visualizar Trabalho" WIDTH="22" HEIGHT="15">
										</td>
								</tr>
								<%
									rsT.MoveNext
									reg = reg + 1
								wend
							end if 
							%>
								<tr class="CelulaTitulo">
									<b>
										<td width="100%" colspan="4">&nbsp;</td>
									</b>
								</tr>
							</table>

							<%
							' Barra de Pagina��o	%>
							<table width="100%">
								<tr class="CelulaCorpo" valign="middle">
									<td width="25%">
										P�gina <%=session("paginaAtual")%> de <%=totalPaginas_wk%>
									</td>
									<td width="25%">
										Registros : <%=rsT.RecordCount%>
									</td>
									<td width="50%" align="right">
										<% 
										if session("paginaAtual") > 1 then %>
											<a href="javascript:paginar(1);"><img src="../images/Ico_PrimeiraOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(2);"><img src="../images/Ico_AnteriorOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_PrimeiraOff.gif" border="0" WIDTH="12" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_AnteriorOff.gif" border="0" WIDTH="8" HEIGHT="15">
										<% 
										end if %>
										&nbsp;
										<% 
										if session("paginaAtual") < session("tp") then %>
											<a href="javascript:paginar(3);"><img src="../images/Ico_ProximaOn.gif" border="0" WIDTH="8" HEIGHT="15"></a>
											&nbsp;
											<a href="javascript:paginar(4);"><img src="../images/Ico_UltimaOn.gif" border="0" WIDTH="12" HEIGHT="15"></a>
										<% 
										else %>
											<img src="../images/Ico_ProximaOff.gif" border="0" WIDTH="8" HEIGHT="15">
											&nbsp;
											<img src="../images/Ico_Ultimaoff.gif" border="0" WIDTH="12" HEIGHT="15">
										<% 
										end if %>
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			<%
			end select
			set rsT = nothing
			set rsD = nothing
			set rsCat = nothing
			set Conn = nothing
		case 1

			' Conectar com o banco
			%>
			<!--#include file=conn.asp -->
			<%
			Set Conn = Server.CreateObject("ADODB.Connection") 
			Conn.Open cnpath
			
			strSQL = "select * from TB_Trabalhos " _
			& " where cd_trabalho = " & request("hcd_trabalho")

			set rsT = server.CreateObject("ADODB.Recordset")
			set rsT = Conn.Execute(strSQL)
			cd_trabalho_wk = rsT("cd_trabalho")
			cd_modo_wk = rsT("cd_modo")
			modo_wk = "Caso Cl�nico Consultivo"
			%>
			<table class="TabelaProgramacao" width="100%" border=0>
				<tr class="CelulaTitulo">
					<td colspan=2 width=100%>
						<table width="100%" border=0>
							<tr class="CelulaTitulo">
								<td width=100% align="left">
									TRABALHO <%=cd_trabalho_wk%>
								</td>
							</tr>
						</table>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>T�tulo :&nbsp;</b><br>
						<%=replace(rsT("de_titulo"),chr(10),"<br>")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>Resumo :&nbsp;</b><br>
						<%=rsT("de_sumario")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>Palavras Chaves :&nbsp;</b><br>
						<%=rsT("de_palavrasChaves")%>
					</td>
				<tr>

				<tr class="CelulaCorpo">
					<td width="100%" align="left" colspan="2">
						<b>Refer�ncias Bibliogr�ficas :&nbsp;</b><br>
						<%=rsT("de_referencias")%>
					</td>
				<tr>

				<% 
				if rsT("cd_modo") = 1 then
					%>
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Exames imaginol�gicos :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Exames anatomopatol�gicos :&nbsp;</b><br>
							<%=rsT("de_anatomopatologico")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Outros exames complementares :&nbsp;</b><br>
							<%=rsT("de_outrosExames")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Informa��es sobre o tratamento :&nbsp;</b><br>
							<%=rsT("de_infoTratamento")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Informa��es sobre seguimento :&nbsp;</b><br>
							<%=rsT("de_infoSeguimento")%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Diagn�stico ou hip�teses diagn�sticas :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					<%
					select case cint(rsT("cd_dificuldade"))
						case 1
							strDificuldade_wk = "No diagn�stico"
						case 2
							strDificuldade_wk = "No tratamento"
						case 3
							strDificuldade_wk = "Em Ambos (diagn�stico e tratamento)"
					end select
					%>
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Dificuldade :&nbsp;</b><br>
							<%=strDificuldade_wk%>
						</td>
					<tr>
					
					<tr class="CelulaCorpo">
						<td width="100%" align="left" colspan="2">
							<b>Relev�ncia da apresenta��o do trabalho no congresso :&nbsp;</b><br>
							<%=rsT("de_imaginologico")%>
						</td>
					<tr>
					
					<%
				end if
				%>

				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<hr>
					</td>
				<tr>

				<% if ((rsT("cd_modo") = 1) or (rsT("cd_modo") = 4))  then  %>
				<tr class="CelulaCorpo">
					<td align="middle" colspan="2">
						<b>Imagens</b><br><br>
						<%
						strSQL = "select de_prefixoImagens from TB_Trabalhos where cd_trabalho = " & request("hcd_trabalho")
						set rs = Conn.Execute(strSQL)
							
						strSQL = "select * from TB_ImagensTrabalhos where cd_trabalho = " & rs("de_prefixoImagens") & " order by nu_seqImagem"
						
						set rs = Conn.Execute(strSQL)
						
						if not rs.eof then
							while not rs.EOF
								%>
								<img src="http://www.tavares.eti.br/upload_admin/estomatologia/<%=rs("cd_trabalho")%>_<%=rs("de_nomeArquivo")%>">
								<br>
								<%=rs("de_legenda")%>
								<hr>
								<%
								rs.MoveNext
							wend
						else
							Response.Write "<br>Nenhuma Imagem encontrada para este trabalho."
						end if
						%>
					</td>
				<tr>
				<% end if %>
				<tr class="CelulaTitulo">
					<td align="middle" colspan="2">
						<input type="button" name="Voltar" value="Voltar" class="BotaoPadrao" onclick="paginar(9);">
					</td>
				<tr>
			<%
		end select
		%>
	</table>
</form>