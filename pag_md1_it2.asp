<p class="Titulo" align="center">CONVITE DA PRESIDENTE DO CONGRESSO</p>

<p class="textoComum">
	<img SRC="images/Foto_Liliane.jpg" align="right" hspace="5" WIDTH="173" HEIGHT="200">Prezados Colegas,
</p>
<p class="textoComum">
	� com imensa satisfa��o e orgulho que assumimos a organiza��o do XIII Congresso Brasileiro de Estomatologia que ser� realizado no m�s de julho de 2005, em Florian�polis, Santa Catarina.
</p>
<p class="textoComum">
	O desafio foi lan�ado e j� iniciamos uma busca incans�vel para fazer deste evento um rico acontecimento dentro da �rea de Estomatologia, com profissionais de renome, casos cl�nicos e semin�rio de l�minas histopatol�gicas que possam trazer discuss�es importantes e enriquecedoras dentro da �rea do diagn�stico de doen�as bucais. Queremos faz�-lo inesquec�vel, tanto na programa��o cient�fica quanto na social, de maneira a proporcionar uma bela e calorosa estadia na nossa maravilhosa Ilha de Santa Catarina.
</p>
<p class="textoComum">
	Esperamos todos voc�s aqui em julho de 2005.
</p>
<p class="textoComum">
<b>Liliane Janete Grando</b><br>
Presidente do XIII Congresso Brasileiro de Estomatologia
</p>
