<% 
' Conectar com o banco
%>
<!--#include file=conn.asp -->
<%
Set Conn = Server.CreateObject("ADODB.Connection") 
Conn.Open cnpath
Set rs = server.CreateObject("ADODB.Recordset")
%>
<input type=hidden name=pr3 value=2>
<script>
function mostraAprovados(modo){
	for (i=1; i<=4; i++) {
		document.getElementById('modo'+i).style.display = 'none';
		if (modo == i){
			document.getElementById('modo'+i).style.display = 'inline';
		}
	}
}

</script>
<p class="Titulo" align="center">Trabalhos Aprovados</p>
<table width=100% border=0>
	<tr>
		<td width=100% class=TextoComum align=center>
			<small>
				<a href="javascript:mostraAprovados(1);">Caso Cl�nico Consultivo</a>
				&nbsp;|&nbsp;<a href="javascript:mostraAprovados(2);">Painel de Pesquisa</a>
				&nbsp;|&nbsp;<a href="javascript:mostraAprovados(3);">Painel Cl�nico</a>
				&nbsp;|&nbsp;<a href="javascript:mostraAprovados(4);">Apresenta��o Oral</a>
			</small>
		</td>
	</tr>
	<tr>
		<td width=100% class=TextoComum>
			<hr noshade color="#7A64A5">			
		</td>
	</tr>
	<tr>
		<td width=70%>
			<span id=modo1 style="display:'inline'">
				<table width=100% border=0>
					<tr>
						<td class=TextoComum colspan=3 align=center>
							<b>Caso Cl�nicos Consultivos</b><hr noshade color="#7A64A5">
						</td>
					</tr>
					<%
					strSQL = "select a.*, p.nm_participante, t.de_titulo from ([SC05 - Consolidacao] a " _
					& " inner join TB_Trabalhos t on a.cd_trabalho = t.cd_trabalho) " _
					& " inner join TB_Participantes p on t.cd_apresentador = p.cd_participante " _
					& " where a.cd_modo=1 and t.sn_aceito = true order by 1 ASC"
					Set rs = Conn.execute(strSQL)
					
					bgc="#ffffff"
					while not rs.EOF
						%>
						<tr bgcolor="<%=bgc%>">
							<td class=TextoComum width=5% valign=top>
								<%=string(3-len(rs("cd_trabalho")),"0") & rs("cd_trabalho")%>
							</td>
							<td class=TextoComum width=35% valign=top>
								<%=ucase(rs("nm_participante"))%>
							</td>
							<td class=TextoComum width=60% valign=top>
								<%=ucase(rs("de_titulo"))%>
							</td>
						</tr>
						<%
						if bgc="#ffffff" then
							bgc="#e8e8e8"
						else
							bgc="#ffffff"
						end if
						rs.moveNext
					wend
					%>
				</table>
			</span>

			<span id=modo2 style="display:'none'">
				<table width=100% border=0>
					<tr>
						<td class=TextoComum colspan=3 align=center>
							<b>Painel de Pesquisa</b><hr noshade color="#7A64A5">
						</td>
					</tr>
					<%
					strSQL = "select a.*, p.nm_participante, t.de_titulo from ([SC05 - Consolidacao] a " _
					& " inner join TB_Trabalhos t on a.cd_trabalho = t.cd_trabalho) " _
					& " inner join TB_Participantes p on t.cd_apresentador = p.cd_participante " _
					& " where a.cd_modo=2 and t.sn_aceito = true order by 1 ASC"
					Set rs = Conn.execute(strSQL)
					
					bgc="#ffffff"
					while not rs.EOF
						%>
						<tr bgcolor="<%=bgc%>">
							<td class=TextoComum width=5% valign=top>
								<%=string(3-len(rs("cd_trabalho")),"0") & rs("cd_trabalho")%>
							</td>
							<td class=TextoComum width=35% valign=top>
								<%=ucase(rs("nm_participante"))%>
							</td>
							<td class=TextoComum width=60% valign=top>
								<%=ucase(rs("de_titulo"))%>
							</td>
						</tr>
						<%
						if bgc="#ffffff" then
							bgc="#e8e8e8"
						else
							bgc="#ffffff"
						end if
						rs.moveNext
					wend
					%>
				</table>
			</span>

			<span id=modo3 style="display:'none'">
				<table width=100% border=0>
					<tr>
						<td class=TextoComum colspan=3 align=center>
							<b>Painel Cl�nico</b><hr noshade color="#7A64A5">
						</td>
					</tr>
					<%
					strSQL = "select a.*, p.nm_participante, t.de_titulo from ([SC05 - Consolidacao] a " _
					& " inner join TB_Trabalhos t on a.cd_trabalho = t.cd_trabalho) " _
					& " inner join TB_Participantes p on t.cd_apresentador = p.cd_participante " _
					& " where a.cd_modo=3 and t.sn_aceito = true order by 1 ASC"
					Set rs = Conn.execute(strSQL)
					
					bgc="#ffffff"
					while not rs.EOF
						%>
						<tr bgcolor="<%=bgc%>">
							<td class=TextoComum width=5% valign=top>
								<%=string(3-len(rs("cd_trabalho")),"0") & rs("cd_trabalho")%>
							</td>
							<td class=TextoComum width=35% valign=top>
								<%=ucase(rs("nm_participante"))%>
							</td>
							<td class=TextoComum width=60% valign=top>
								<%=ucase(rs("de_titulo"))%>
							</td>
						</tr>
						<%
						if bgc="#ffffff" then
							bgc="#e8e8e8"
						else
							bgc="#ffffff"
						end if
						rs.moveNext
					wend
					%>
				</table>
			</span>

			<span id=modo4 style="display:'none'">
				<table width=100% border=0>
					<tr>
						<td class=TextoComum colspan=3 align=center>
							<b>Apresenta��o Oral de Casos Cl�nicos</b><hr noshade color="#7A64A5">
						</td>
					</tr>
					<%
					strSQL = "select a.*, p.nm_participante, t.de_titulo from ([SC05 - Consolidacao] a " _
					& " inner join TB_Trabalhos t on a.cd_trabalho = t.cd_trabalho) " _
					& " inner join TB_Participantes p on t.cd_apresentador = p.cd_participante " _
					& " where a.cd_modo=4 and t.sn_aceito = true order by 1 ASC"
					Set rs = Conn.execute(strSQL)
					
					bgc="#ffffff"
					while not rs.EOF
						%>
						<tr bgcolor="<%=bgc%>">
							<td class=TextoComum width=5% valign=top>
								<%=string(3-len(rs("cd_trabalho")),"0") & rs("cd_trabalho")%>
							</td>
							<td class=TextoComum width=35% valign=top>
								<%=ucase(rs("nm_participante"))%>
							</td>
							<td class=TextoComum width=60% valign=top>
								<%=ucase(rs("de_titulo"))%>
							</td>
						</tr>
						<%
						if bgc="#ffffff" then
							bgc="#e8e8e8"
						else
							bgc="#ffffff"
						end if
						rs.moveNext
					wend
					%>
				</table>
			</span>
		</td>
	</tr>
	<tr>
		<td width=100% class=TextoComum>
			<hr noshade color="#7A64A5">			
		</td>
	</tr>
</table>