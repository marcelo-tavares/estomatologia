<%
function strRecibo(cd_participante)
	%>
	<!--#include file=conn.asp -->
	<%
	Set Conn = Server.CreateObject("ADODB.Connection") 
	set rs = server.CreateObject("ADODB.Recordset")

	Conn.Open cnpath
	set rs = Conn.Execute("select * from TB_Participantes where cd_participante = " & cd_participante)

	strRecibo = "RECEBEMOS de <b>" & trim( ucase( rs("nm_participante"))) & "</b> " &  _
					"a import�ncia de<br><br> " & _
					strReaisNumero(rs("vl_inscricaoR")) & " " & strReaisExtenso(rs("vl_inscricaoR")) & _
					",<br><br>referente ao total pago pela inscri��o de n�mero <b>" & cd_participante & _
					"</b> no<br><br>XIII CONGRESSO BRASILEIRO DE ESTOMATOLOGIA.<br><br><br>" & _
					"Florian�polis, " & day(now) & "/" & month(now) & "/" & year(now) & "<br><br><br>" & _
					"<b>M�rcio Corr�a</b><br>Tesoureiro"

end function

function strReaisNumero(valor)
	strvalor = Cstr(int(valor*100))
	strReaisNumero = "<b>R$ "
	strReais = left(strValor, len(strValor)-2)
	strCentavos = right(strValor, 2)		
	strReaisNumero = strReaisNumero & strReais & "," & strCentavos
	strReaisNumero = strReaisNumero & "</b>"
end function

function strDollarsNumero(valor)
	strvalor = Cstr(int(valor*100))
	strDollarsNumero = "<b>US$ "
	strDollars = left(strValor, len(strValor)-2)
	strCents = right(strValor, 2)		
	strDollarsNumero = strDollarsNumero & strDollars & "." & strCents
	strDollarsNumero = strDollarsNumero & "</b>"
end function

function strReaisExtenso(valor)
	strvalor = Cstr(int(valor*100))
	strReaisExtenso = "(<b>"

	strReais = left(strValor, len(strValor)-2)
	strCentavos = right(strValor, 2)
	
	if len(strReais) > 2 then
		centena = left(strReais,1)
		dezena = right(strReais,2)
		strE = " e "
	else
		centena = "0"
		dezena = right(strReais,2)
		strE = ""
	end if
			
	strReaisExtenso = strReaisExtenso & strCentena(centena, dezena) & strDezenaReais(dezena) & strDezenaCentavos(strCentavos)

	strReaisExtenso = strReaisExtenso & "</b>)"
	
end function

function strCentena(centena, dezena)
	select case centena
		case 1
			if dezena = 0 then 
				strCentena = "cem"
			else
				strCentena = "cento"
			end if
		case 2
			strCentena = "duzentos"
		case 3
			strCentena = "trezentos"
		case 4
			strCentena = "quatrocentos"
		case 5
			strCentena = "quinhentos"
		case 6
			strCentena = "seiscentos"
		case 7
			strCentena = "setecentos"
		case 8
			strCentena = "oitocentos"
		case 9
			strCentena = "novecentos"
	end select	
end function

function strDezenaReais(dezena)

	if dezena < 10 then
		select case dezena
			case 0
				strDezenaReais = " reais"
			case 1
				strDezenaReais = " e um reais"
			case 2
				strDezenaReais = " e dois reais"
			case 3
				strDezenaReais = " e tr�s reais"
			case 4
				strDezenaReais = " e quatro reais"
			case 5
				strDezenaReais = " e cinco reais"
			case 6
				strDezenaReais = " e seis reais"
			case 7
				strDezenaReais = " e sete reais"
			case 8
				strDezenaReais = " e oito reais"
			case 9
				strDezenaReais = " e nove reais"
		end select
	else
		if dezena < 20 then
			select case dezena
				case 10
					strDezenaReais = " e dez reais"
				case 11
					strDezenaReais = " e onze reais"
				case 12
					strDezenaReais = " e doze reais"
				case 13
					strDezenaReais = " e treze reais"
				case 14
					strDezenaReais = " e catorze reais"
				case 15
					strDezenaReais = " e quinze reais"
				case 16
					strDezenaReais = " e dezesseis reais"
				case 17
					strDezenaReais = " e dezessete reais"
				case 18
					strDezenaReais = " e dezoito reais"
				case 19
					strDezenaReais = " e dezenove reais"
			end select
		else
			algDezena  = left(Cstr(dezena),1)
			select case algDezena
				case 2
					strDezenaReais = strE & "vinte"
				case 3
					strDezenaReais = strE & "trinta"
				case 4
					strDezenaReais = strE & "quarenta"
				case 5
					strDezenaReais = strE & "cinquenta"
				case 6
					strDezenaReais = strE & "sessenta"
				case 7
					strDezenaReais = strE & "setenta"
				case 8
					strDezenaReais = strE & "oitenta"
				case 9
					strDezenaReais = strE & "noventa"
			end select
			
			algUnidade  = right(Cstr(dezena),1)
			select case algUnidade
				case 0
					strDezenaReais = strDezenaReais & " reais"
				case 1
					strDezenaReais = strDezenaReais & " e um reais"
				case 2
					strDezenaReais = strDezenaReais & " e dois reais"
				case 3
					strDezenaReais = strDezenaReais & " e tr�s reais"
				case 4
					strDezenaReais = strDezenaReais & " e quatro reais"
				case 5
					strDezenaReais = strDezenaReais & " e cinco reais"
				case 6
					strDezenaReais = strDezenaReais & " e seis reais"
				case 7
					strDezenaReais = strDezenaReais & " e sete reais"
				case 8
					strDezenaReais = strDezenaReais & " e oito reais"
				case 9
					strDezenaReais = strDezenaReais & " e nove reais"
			end select
		end if
	end if
end function



function strDezenaCentavos(dezena)

	if dezena < 10 then
		select case dezena
			case 0
				strDezenaCentavos = ""
			case 1
				strDezenaCentavos = " e um centavo"
			case 2
				strDezenaCentavos = " e dois centavos"
			case 3
				strDezenaCentavos = " e tr�s centavos"
			case 4
				strDezenaCentavos = " e quatro centavos"
			case 5
				strDezenaCentavos = " e cinco centavos"
			case 6
				strDezenaCentavos = " e seis centavos"
			case 7
				strDezenaCentavos = " e sete centavos"
			case 8
				strDezenaCentavos = " e oito centavos"
			case 9
				strDezenaCentavos = " e nove centavos"
		end select
	else
		if dezena < 20 then
			select case dezena
				case 10
					strDezenaCentavos = " e dez centavos"
				case 11
					strDezenaCentavos = " e onze centavos"
				case 12
					strDezenaCentavos = " e doze centavos"
				case 13
					strDezenaCentavos = " e treze centavos"
				case 14
					strDezenaCentavos = " e catorze centavos"
				case 15
					strDezenaCentavos = " e quinze centavos"
				case 16
					strDezenaCentavos = " e dezesseis centavos"
				case 17
					strDezenaCentavos = " e dezessete centavos"
				case 18
					strDezenaCentavos = " e dezoito centavos"
				case 19
					strDezenaCentavos = " e dezenove centavos"
			end select
		else
			algDezena  = left(Cstr(dezena),1)
			select case algDezena
				case 2
					strDezenaCentavos = " e vinte"
				case 3
					strDezenaCentavos = " e trinta"
				case 4
					strDezenaCentavos = " e quarenta"
				case 5
					strDezenaCentavos = " e cinquenta"
				case 6
					strDezenaCentavos = " e sessenta"
				case 7
					strDezenaCentavos = " e setenta"
				case 8
					strDezenaCentavos = " e oitenta"
				case 9
					strDezenaCentavos = " e noventa"
			end select
			
			algUnidade  = right(Cstr(dezena),1)
			select case algUnidade
				case 0
					strDezenaCentavos = strDezenaCentavos & " centavos"
				case 1
					strDezenaCentavos = strDezenaCentavos & " e um centavos"
				case 2
					strDezenaCentavos = strDezenaCentavos & " e dois centavos"
				case 3
					strDezenaCentavos = strDezenaCentavos & " e tr�s centavos"
				case 4
					strDezenaCentavos = strDezenaCentavos & " e quatro centavos"
				case 5
					strDezenaCentavos = strDezenaCentavos & " e cinco centavos"
				case 6
					strDezenaCentavos = strDezenaCentavos & " e seis centavos"
				case 7
					strDezenaCentavos = strDezenaCentavos & " e sete centavos"
				case 8
					strDezenaCentavos = strDezenaCentavos & " e oito centavos"
				case 9
					strDezenaCentavos = strDezenaCentavos & " e nove centavos"
			end select
		end if
	end if
end function
%>

