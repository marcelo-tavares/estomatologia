<% if id_wk = 1 then
	strTitulo = "Rela��o dos Expositores"
	strTituloTab = "Empresas"
	strTexto="Novidades tecnol�gicas de interesse da Radiologia estar�o sendo apresentadas na Feira de Exposi��o da Ind�stria e Com�rcio, com acesso a todos os usu�rios do evento. A �rea da exposi��o comercial, de mais de 1.000m2, ser� composta por aproximadamente 64 estandes comerciais, todos eles j� locados. Tal �rea estar� localizada no centro da �rea cient�fica do Congresso, o que permitir� que congressitas permane�am em contato com as diferentes empresas e seus produtos. As seguintes empresas confirmaram sua participa��o no 14th ICDMFR:"
else
	strTitulo = "List of Companies"
	strTituloTab = "Companies"
	strTexto="Technological innovations of interest in the field of Dentomaxillofacial Radiology will be presented at the Commercial Exhibition Area (1080 m2), with free access to all participants of the Congress. This area will be located in the middle of the Scientific Area so that people can stay in close contact with the different companies and their products. The following companies have confirmed their participation in the event:"
end if %>
	

<p class="Titulo" align="center">
	<%=strTitulo%>
</p>
<p class="textoComum">
	<%=strTexto%>
</p>
<table width="100%" class=TabelaProgramacao>
	<tr class=CelulaCorpo>
		<td align=middle class=CelulaTitulo colspan=2>
			<%=strTituloTab%>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>Astex</STRONG> 
      <SMALL>(71)</SMALL>
		</td>
		<td align=middle>
			<b>Kodak Brasileira</b> <small>(11/12)</small>
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=middle><STRONG>C.A.S.</STRONG> 
      <SMALL>(51/51/54/55)</SMALL>
		</td>
		<td align=middle>
			<b>Loska</b> <small>(20)</small>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>CDROM</STRONG> 
      <SMALL>(44/45/46/47/48/49)</SMALL>
		</td>
		<td align=middle><STRONG>N. 
      Martins</STRONG>&nbsp;<SMALL>(24)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=middle><STRONG>CDT</STRONG> 
      <SMALL>(56/57/58)</SMALL>
		</td>
		<td align=middle><STRONG>N.D.T. 
      </STRONG><SMALL>(33)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>Cirrus</STRONG> 
      <SMALL>(73)</SMALL>
		</td>
		<td align=middle><STRONG>Odonto Atual</STRONG> 
      <SMALL>(70)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=middle><STRONG>Dabi Atlante</STRONG> 
      <SMALL>(59/60/61)</SMALL>
		</td>
		<td align=middle><STRONG>Odontomed</STRONG> 
      <SMALL>(09/10)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>Del Grandi</STRONG> 
      <SMALL>(29/30)</SMALL>
		</td>
		<td align=middle><STRONG>Ortho Gesso</STRONG> 
      <SMALL>(18)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=middle><STRONG>Fabinject</STRONG> 
      <SMALL>(27/28)</SMALL>
		</td>
		<td align=middle><STRONG>Panathena</STRONG> 
      <SMALL>(34)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>Flodontomed</STRONG> 
      <SMALL>(04)</SMALL>
		</td>
		<td align=middle><STRONG>Planmeca</STRONG> 
      <SMALL>(50/53)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=middle><STRONG>Gnatus</STRONG> 
      <SMALL>(13/14/15/16/17)</SMALL>
		</td>
		<td align=middle><STRONG>Radiomemory</STRONG> 
      <SMALL>(62/63/64/65/66/67)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>Hereaus Kulzer</STRONG> 
      <SMALL>(07/08)</SMALL>
		</td>
		<td align=middle><STRONG>Templ�s</STRONG> 
      <SMALL>(68)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=middle><STRONG>Imaging Science</STRONG> 
      <SMALL>(25/26)</SMALL>
		</td>
		<td align=middle><STRONG>TOL Total</STRONG> 
      <SMALL>(35)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>Instrumentarium</STRONG> 
      <SMALL>(40/41/42/43)</SMALL>
		</td>
		<td align=middle><STRONG>Trollplast</STRONG> 
      <SMALL>(72)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=middle><STRONG>J. Morita do Brasil</STRONG> 
      <SMALL>(36/37/38/39)</SMALL>
		</td>
		<td align=middle><STRONG>Trophy 
      </STRONG><SMALL>(72)</SMALL>
		</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=middle><STRONG>Jo Suarez 
      </STRONG><SMALL>(02/03)</SMALL>
		</td>
		<td align=middle>
			<b>X-TEC</b> <small>(69)</small>
		</td>
	</tr>
</table>
