<% if id_wk = 1 then
	strTitulo = "TRANSPORTE"
	strFrase1 = "O Aeroporto Herc�lio Luz International est� localizado a 15min (em taxi) ao sul do centro da cidade, onde os hot�is recomendados est�o situados. A maioria dos delegados estrangeiros  chegar�o a Florian�polis ap�s realizar conex�es de vo�s em S�o Paulo ou Rio de Janeiro, duas cidades principais."
	strFrase2 = "Florian�polis tem um bom sistema de transportes p�blicos, com a maioria das linhas de �nibus funcionando at� o inicio da madrugada. Taxis tamb�m est�o dispon�veis. Ambos os meios de transporte podem levar voc� a todos os lugares dentro e fora da ilha."
else
	strTitulo = "TRANSPORTATION"
	strFrase1 = "The Herc�lio Luz International Airport is located 15min (by taxi) south from the downtown area of the city, where the recommended hotels are located. Most of the foreign delegates will arrive in Florianopolis after flight connections in S�o Paulo or Rio de Janeiro, two main Brazilian cities."
	strFrase2 = "Florian�polis has a good public transportation system, with most of the bus lines running until late in the evening. Taxis are readily available. Both means of transportation can take you to all places within and outside the island."
end if %>

<p class="Titulo" align="center">TRANSPORTE E DESLOCAMENTOS</p>
<p class="textoComum">
	<b>Transporte Coletivo</b><br><br>
	<a href="downloads/tc.pdf" target="_nova">Veja as orienta��es detalhadas</a><sup>*</sup> sobre o uso do Transporte Coletivo de Florian�polis para chegar ao evento.
	<br><br>
	<small><sup>*</sup>Para visualizar esta informa��o � necess�rio o software Acrobat Reader.<br>Caso n�o o tenha instalado use o bot�o abaixo para instal�-lo gratuitamente:</small><br>
	<a href="http://www.brasil.adobe.com/products/acrobat/readstep2.html" target="_nova"><img src="images/get_adobe_reader.gif" border="0" WIDTH="88" HEIGHT="31"></a>
</p>
<p class="textoComum">
	<b>Servi�os de Deslocamento</b><br><br>
	<table width="100%" class="TabelaProgramacao">
		<tr class="CelulaTitulo">
			<td><b>TRAJETO</b></td>
			<td align="center"><b>Valor</b></td>
		</tr>
		<tr class="CelulaCorpoCinza">
			<td>Aeroporto/Hotel Centro/Aeroporto</td>
			<td align="center">R$ 40.00</td>
		</tr>
		<tr class="CelulaCorpo">
			<td>Hotel/Coktail UFSC/Hotel</td>
			<td align="center">R$ 10.00</td>
		</tr>
		<tr class="CelulaCorpoCinza">
			<td>Hotel/Jantar AABB/ Hotel</td>
			<td align="center">R$ 10.00</td>
		</tr>
		<tr class="CelulaCorpo">
			<td>Hotel/Jantar Clube12/Hotel</td>
			<td align="center">R$ 10.00</td>
		</tr>
		<tr class="CelulaCorpoCinza">
			<td>Hotel Centro/evento/Hotel Centro</td>
			<td align="center">R$ 15.00</td>
		</tr>
	</table>	
	<br>
	- Valores por pessoa e por dia<br>
	- Forma de pagamento Transporte: dep�sito � vista<br>
	<br>
	Contato com:<br>
	<b>Ana Paula Oliveira</b><br>
	Amplestur Turismo<br> 
	anapaula@amplestur.com.br<br> 
	fone (48) 2108-9422<br>
	fax (48) 2108-9433<br>
</p>
