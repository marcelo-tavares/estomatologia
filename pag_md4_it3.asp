<% 
if id_wk = 1 then
	strTitulo = "Hospedagem"
	strDistancia = "Distancia<br>at� o congresso"
	strCaminhar = "a p�"
	strDirigir = "em carro"
	strObs = "Obs.:<br>As tarifas acima representam valores aproximados em d�lar devido �s recentes flutua��es cambiais do Real, est�o sujeitas a altera��es sem aviso pr�vio e devem ser confirmadas na reserva."
	strObs2 = "Para garantir os melhores pre�os de di�rias, as reservas de hotel devem ser feitas diretamente com a ag�ncia oficial de turismo do 14th ICDMFR:"	
	strLocalizacao = "Localiza��o dos hot�is pr�ximos ao Centro de Conven��es (CentroSul)"
end if 
%>

<p class="Titulo" align="center"><%=strTitulo%></p>

<p class="textoComum" align="left">
	A ag�ncia oficial do evento est� preparada para lhe dar todas as informa��es acerca das op��es de hospedagem. 
</p>

<p class="textoComum" align="left">
	<b>Ag�ncia de Turismo Oficial do Evento:</b><br><br>
	<a href="http://www.amplestur.com.br" target="_topo">
		<img SRC="images/amplestur.jpg" border="0" WIDTH="200" HEIGHT="24">
	</a>
	<br>
	Rua Jer�nimo Coelho 293, Centro<br>
	88010-030 - Florian�polis/SC<br>
	Fax: 0 XX 48 2108-9433 / 2108-9444<br>
	Fone:0 XX 48 2108-9422<br>
	<br>
	e-mail: <a href="mailto:amplestur@amplestur.com.br">amplestur@amplestur.com.br</a>
</p>
<IMG SRC="images/mapaCentroSobe.jpg">
<p class="textoComum" align="left">
	1 - Best Western Baia Norte<br>
	2 - Blue Tree Towers<br>
	3 - Castelmar Hotel<br>
	4 - Coral Plaza Hotel<br>
	5 - Hotel Daifa<br>
	6 - Hotel Porto da Ilha<br>
	7 - Ibis Hotel<br>
	8 - Intercity Hotel<br>
	9 - Majestic Palace Hotel<br>
	10 - Maria do Mar<br>
	11 - Quinta da Bica D��gua
</p>

