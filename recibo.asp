<%
cd_participante_wk=request("cd_participante")
id_wk = request("idioma")

if id_wk = 1 then
	titulo_wk = "RECIBO"
else
	titulo_wk = "RECEIPT"
end if
%>

<!--#include file="strRecibo.asp" -->
<html>

<head>
<title><%=titulo_wk%></title>
<link REL="stylesheet" HREF="estilos.css">
</head>

<body onload="window.print(); window.close();">

<table class="TabelaComprovante" width="100%">
	<tr>
		<td align="center" colspan="3">
			<p class="textoComum" align="center">
				&nbsp;<br>
				<img src="images/logoSOBEBoleto.jpg">
				<br>
				<b>
					SOBE - SOCIEDADE BRASILEIRA DE ESTOMATOLOGIA
				</b>
				<br>
				C.N.P.J. 46.367.215/0001-46
				<br>
				Insc. Estadual: Isenta
			</p>
			<br>
			<h5><big><%=titulo_wk%></big></h5>
		</td>
	<tr>
	<tr class="CelulaCorpo" align="center" colspan="3">
		<td>
				<%=strRecibo(cd_participante_wk)%>
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="3">
			&nbsp;
		</td>
	</tr>
</table>
</body>
</html>
