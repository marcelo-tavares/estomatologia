<% if id_wk = 1 then %>
<p class="Titulo" align="center">Programa��o Tur�stica</p>

<p class="textoComum" align="center">
	<a class="Link" href="javascript:submeter(2,1,<%=id_wk%>,0)">Programa��o Cient�fica</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;&nbsp;
	<a class="Link" href="javascript:submeter(2,1,<%=id_wk%>,1)">Programa��o Social</a>
</p>

<p class="textoComum" align="left">
	Todas as op��es tur�sticas ser�o administradas diretamente pela ag�ncia oficial de turismo, com a qual voc� pode obter informa��es adicionais. A ag�ncia estar� respons�vel pelas reservas de passeios nos quais voc� estiver interessado.
</p>
<p class="textoComum" align="left">
	<img SRC="images/bbtur_logo.gif" border="0" WIDTH="120" HEIGHT="20"><br><b>Ag�ncia de Viagens e Turismo</b><br>
	Rua Trajano, 265 - Loja 3<br>
	Fone: + 55 (48) 229-8500 / Fax: + 55 (48) 322-0460
	e-mail: <a href="mailto:andredutra@bbtur.com.br">andredutra@bbtur.com.br</a>
</p>
<p class="textoComum" align="left">
	<b>City Tour B�sico de Florian�polis</b><br>
	<b>Pre�o</b>: U$7.00 (gr�tis para os acompanhantes devidamente inscritos no Congresso)<br>
	<b>Data</b>: Wednesday � 21 May 2003 (a ser confirmado)<br>
	<b>Dura��o</b>: 3 horas<br>
	Visita ao centro de Florian�polis, visitando seus pomtos tur�sticos principais, tais como o Largo da Alf�ndega, Centro C�vico, Catedral Metropolitana, Pra�a XV de Novembro, Pal�cio Cruz e Souz, Ponte Herc�lio Luz, etc.
<br><br>
	<b>Florian�polis Full City Tour</b><br>
	<b>Pre�o</b>: U$11.00<br>
	<b>Data</b>: di�rio<br>
	<b>Dura��o</b>: 8 horas<br>
	Centro: Visita ao Largo da Alf�ndega, Centro C�vico, Catedral Metropolitana, Pra�a XV de Novembro, Pal�cio Cruz e Souza e Mirante da Ponte Herc�lio Luz.<br>
	Sul: Morro das Pedras, Mirante Sul da Ilha, Lagoa do Peri, Praia da Arma��o e a Col�nia de Pescadores do P�ntano do Sul.<br>
	Leste: Lagoa da Concei��o, Praia da Joaquina, tempo para almo�o e banho, Praia Mole e Barra da Lagoa.<br>
	Norte: Praia dos Ingleses, Ponta das Canas, Mirante da Praia Brava, Cachoeira do Bom Jesus, Canasvieiras, Balne�rio de Jurer�, visita ao Forte de S�o Jos� da Ponta Grossa (constru��o de 1740). 
<br><br>
	<b>Passeio de Escuna</b><br>
	<b>Pre�o</b>: U$17.00 (com almo�o inclu�do)<br>
	<b>Data</b>: di�rio<br>
	<b>Dura��o</b>: 6 horas<br>
	Dia da Fantasia. Sa�da do trapiche da Scuna Sul, passagem pela ponte Herc�lio Luz e proximidades do Forte Santana, vista panor�mica da cidade, passagem pelas ilhas dos Guarazes, visita � ilha de Ratones Grande � Fortaleza de Santa Cruz (s�c. XVIII) e almo�o opcional. Passagem pela Ba�a dos Golfinhos e retorno ao trapiche.
<br><br>
	<b>Balne�rio Cambori� � Blumenau � Brusque</b><br>
	<b>Pre�o</b>: U$13.00<br>
	<b>Data</b>: 2�, 4� e 6� feiras � 19, 21 e 23/05/2003<br>
	<b>Dura��o</b>: 11 horas<br>
	Balne�rio Cambori� - Visita ao Parque Unipraias com seus 47 bondinhos num percurso de 3.250 metros de belas paisagens e Praia de Laranjeiras. Ap�s, passagem por toda a Avenida Atl�ntica com 7 km de beira-mar.<br>
	Blumenau - Percurso ao logo do Rio Itaja�-A�u, com sua t�pica paisagem europ�ia, caracterizada pela arquitetura germ�nica. City tour, tempo livre para almo�o. Ap�s, visita ao com�rcio local.<br>
	Brusque - Tour de compras, Havan (Casa Branca Brasileira), Feira de Malhas e Feira de Cal�ados em S�o Jo�o Batista.
<br><br>
	<b>Cambori� � Bombinhas � Quatro Ilhas</b><br>
	<b>Pre�o</b>: U$13.00<br>
	<b>Data</b>: 3� feira � 20/05/2003<br>
	<b>Dura��o</b>: 9 horas<br>
	Cambori� - Visita aos principais pontos tur�sticos da cidade, Parque Unipraias, com seus 47 bondinhos, num percurso de 3.250 metros de belas paisagens e Praia de Laranjeira.<br>
	Bombas, Bombinhas e Quatro Ilhas - Visita ao pequeno povoado de Porto Belo, Praia de Bombas, Bombinhas e Quatro Ilhas, conhecidas por suas �guas cristalinas; parada para banho.
<br><br>
	<b>Turismo Termal � Santo Amaro da Imperatriz  </b><br>
	<b>Pre�o</b>: U$13.00<br>
	<b>Data</b>: 4� feira � 21/05/2003<br>
	<b>Dura��o</b>: 6 horas<br>
	A 40Km de Florian�polis, a �gua do mar � substitu�da por �guas termais, que brotam do ch�o a uma temperatura de 39�C, sendo considerada a segunda melhor do mundo. Al�m de relaxantes, t�m propriedades terap�uticas, auxiliando no tratamento de reumatismos, artroses, depress�es, etc.
<br><br>
	<b>Joinville � City of Flowers</b><br>
	<b>Pre�o</b>: U$13.00<br>
	<b>Data</b>: 6� feira � 23/05/2003<br>
	<b>Dura��o</b>: 10 horas<br>
	City tour - Visita aos principais pontos tur�sticos da maior e mais rica cidade de Santa Catarina, conhecida como a Cidade das Flores e dos Pr�ncipes. Sua beleza natural e riqueza cultural est�o retratadas na arquitetura germ�nica e em seus jardins floridos. Joinville tamb�m � conhecida como a capital brasileira da dan�a, possuindo a �nica escola do bal� Bolshoi fora da R�ssia e realizando anualmente o maior festival de dan�a do pa�s.
<br><br>
	<b>Beto Carrero World</b><br>
	<b>Pre�o</b>: U$13.00 (tickets to the park not included)<br>
	<b>Data</b>: 3�, 5�, S�bado � 20, 22  e 24/05/2003<br>
	<b>Dura��o</b>: 12 horas<br>
	Visita ao maior centro de lazer da Am�rica Latina, apresentando shows, parque de divers�es, zool�gicos e muitas outras atra��es. Um dia inteiro de lazer e entretenimento.
<br><br>
<b><i>Obs.: Um m�nimo de 6 pessoas por passeio � necess�rio, exceto o �Passeio de Escuna�, que necessita um m�nimo de 30 pessoas. Pre�os representam valores aproximados em d�lar e est�o sujeitos a altera��es sem aviso pr�vio.</i></b>
</p>
<p class="textoComum" align="left">
	<a href="javascript:submeter(0,2,<%=id_wk%>,0);"><img SRC="images/varig_logo.gif" border="0" WIDTH="100" HEIGHT="66"></a><br>
	<b>TRANSPORTADORA OFICIAL</b><br>
	<a href="http://www.varig.com.br" target="_outro">http://www.varig.com.br</a><br>
	Tour Code ERIO 031501
	<br>
	Varig Diretoria de Turismo de Neg�cio de Lazer.<br><br>
</p>
<% else %>
<p class="Titulo" align="center">Tour Program</p>

<p class="textoComum" align="center">
	<a class="Link" href="javascript:submeter(2,1,<%=id_wk%>,0)">Scientific Program</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;&nbsp;
	<a class="Link" href="javascript:submeter(2,1,<%=id_wk%>,1)">Social Program</a>
</p>

<p class="textoComum" align="left">
	All tour options will be administered directly by the official travel agency, with whom you can get further information. They will make the necessary arrangements for any optional tours you are interested.
</p>
<p class="textoComum" align="left">
	<img SRC="images/bbtur_logo.gif" border="0" WIDTH="120" HEIGHT="20"><br><b>Travel Agency</b><br>
	Mailing address: Rua Trajano, 265 - Loja 3 - Florian�polis - SC - Brasil - 88010-010<br>
	Phone: + 55 (48) 229-8500 / Fax: + 55 (48) 322-0460
	e-mail: <a href="mailto:andredutra@bbtur.com.br">andredutra@bbtur.com.br</a>
</p>
<p class="textoComum" align="left">
	<b>Florian�polis Basic City Tour</b><br>
	<b>Price</b>: U$7.00 (free of charge for accompanying persons registered for the Congress)<br>
	<b>Date</b>: Wednesday � 21 May 2003 (to be confirmed)<br>
	<b>Duration</b>: 3 hours<br>
	A tour around the downtown area of the city, with visits to the Custom�s House, Civic Center, Metropolitan Cathedral, XV de Novembro Square, Cruz e Sousa Palace, Herc�lio Luz Bridge, etc.
<br><br>
	<b>Florian�polis Full City Tour</b><br>
	<b>Price</b>: U$11.00<br>
	<b>Date</b>: daily<br>
	<b>Duration</b>: 8 hours<br>
	Downtown: Custom�s House, Civic Center, Metropolitan Cathedral, XV de Novembro Square, Cruz e Sousa Palace, Herc�lio Luz Bridge, etc.<br>
	Southern Island: visit to Morro das Pedras beach, viewpoint in the southern part of the island, Peri Lagoon, Arma��o Beach, Village of Fishermen, and P�ntano do Sul Beach.<br>
	Eastern Island: Concei��o Lagoon, Joaquina Beach, time for lunch and swimming, Mole Beach, Barra da Lagoa Beach.<br>
	Northern Island: Ingleses Beach, Ponta das Canas Beach, viewpoint at Brava Beach, Cachoeira do Bom Jesus Beach, Canasvieiras and Jurer� Beaches, visit to S�o Jos� da Ponta Grossa Fortress (construction of 1740) <br>
<br><br>
	<b>Sailing Tour around the North Bay</b><br>
	<b>Price</b>: U$17.00 (with lunch included)<br>
	<b>Date</b>: daily<br>
	<b>Duration</b>: 6 hours<br>
	Fantasy Day. Leaves the pier beneath Herc�lio Luz Bridge, passes by Santana Fortress, with a beautiful panoramic view of the city, passing by the Guarazes Islands and visiting the Ratones Grande Island, where the Santa Cruz Fortress (18th century) is located. Optional lunch on the island. Visit to the Dolphins� Bay and return to the pier.<br>
<br><br>
	<b>Balne�rio Cambori� � Blumenau � Brusque</b><br>
	<b>Price</b>: U$13.00<br>
	<b>Date</b>: Monday, Wednesday and Friday � 19, 21 and 23 May, 2003<br>
	<b>Duration</b>: 11 hours<br>
	Balne�rio Cambori� � on board of a state-of-the-art cable car system, you will visit the Unipraias Park, an ecological park located on a hill where you will have a chance to get in touch with a sample of the Atlantic forest, visit a zoo, and have incredible views of the most visited beach in the state of Santa Catarina. The cable car runs for 3,250m and will take you to Laranjeiras Beach. After sightseeing the 7Km of Cambori��s seaside avenue, we will depart to Blumenau.<br>
	Blumenau � drive along the Itaja�-a�u river, with its typical European scenery. Visit to the city of Blumenau, characterized by its German architecture. City tour, free time for lunch and shopping.<br>
	Brusque - shopping tour to outlet shops, shoes stores and others.<br>
<br><br>
	<b>Cambori� � Bombinhas � Quatro Ilhas</b><br>
	<b>Price</b>: U$13.00<br>
	<b>Date</b>: Tuesday � 20 May 2003<br>
	<b>Duration</b>: 9 hours<br>
	Cambori� � visit to the main attractions of Cambori�, the Unipraias Park, Laranjeiras Beach.<br>
	Tour to the village of Porto Belo (Beautiful Harbor), approximately 40 minutes north of Florian�polis, where you will have the chance to visit Bombas, Bombinhas and Quatro Ilhas Beaches, known for their crystalline waters and perfect diving conditions<br>
<br><br>
	<b>Thermal Waters � Santo Amaro da Imperatriz </b><br>
	<b>Price</b>: U$13.00<br>
	<b>Date</b>: Wednesday � 21 May 2003<br>
	<b>Duration</b>: 6 hours<br>
	A 40km drive from Florian�polis will replace the ocean waters for thermal waters that gush from the ground with a temperature of 39�C, being considered the second best in the world. Besides being relaxing, its therapeutical properties are helpful in the treatment of rheumatic and arthritic diseases, depressions, etc.<br>
<br><br>
	<b>Joinville � City of Flowers</b><br>
	<b>Price</b>: U$13.00<br>
	<b>Date</b>: Friday � 23 May 2003<br>
	<b>Duration</b>: 10 hours<br>
	City tour � Visit to the main attractions of the largest and richest city in Santa Catarina, known as the city of flowers and of the princes. Its beauty and cultural wealth are symbolized by its German architecture and enchanting gardens. Joinville, also known as the Brazilian capital of Dance, has the only Bolshoi Ballet School outside Russia, and annually organizes the largest Dance Festival of the country.<br>
<br><br>
	<b>Beto Carrero World</b><br>
	<b>Price</b>: U$13.00 (tickets to the park not included)<br>
	<b>Date</b>: Tuesday, Thursday, Saturday � 20, 22 and 24 May 2003<br>
	<b>Duration</b>: 12 hours<br>
	Visit to the largest amusement park in Latin America, which offers shows, rides, zoos and other surprises. A full day of entertainment.<br>
<br><br>
<b><i>P.S.: A minimum of 6 (six) people per tour is required, except the �Sailing Tour�, that requires a minimum of 30 people. Prices are in US dollars and are subject to changes without prior notice.</i></b>
<br><br>
Trips to farther destinations such as <b>Rio de Janeiro, the Igua�u Falls, Pantanal, or the Amazon</b>, can also be arranged with the official travel agency of the 14th ICDMFR.
</p>
<p class="textoComum" align="left">
	<a href="javascript:submeter(0,2,<%=id_wk%>,0);"><img SRC="images/varig_logo.gif" border="0" align="center" WIDTH="100" HEIGHT="66"></a><br>
	<b>OFFICIAL AIRLINE</b><br>
	<a href="http://www.varig.com.br" target="_outro">http://www.varig.com.br</a><br>
	Tour Code ERIO 031501
	<br>
	VARIG &#150; Department of Tourism and Leisure Affairs.<br><br>
</p>
<% end if %>
