<%
'******************************************************************************************************************************
'	Rotina para gerar c�digos de barra padr�o 2of5 .
'	Luciano Lima Silva 09/01/2003
'	netdinamica@netdinamica.com.br
'	Site: www.netdinamica.com.br
'	
'	Este script foi testado com o leitor de c�digo de barras e esta OK.
'   Tenho o sistema de boleto</b>, para o ITAU, UNIBANCO, CAIXA, BBV, BRADESCO, HSBC, UNIBANCO, REAL e varios outros, em PHP ou ASP<br>
'   Para maiores informa&ccedil;&otilde;es entre em contato boleto@netdinamica.com.br ou visite: www.netdinamica.com.br/boleto
'	 
'	Basta chamar a fun��o fbarcode("01202") com o valor 
'*********************************************************************************************************************************
'/	

%>
<html>
<head>
<title>NetDinamica.com.br - C&oacute;digo de Barras 2 of 5 em ASP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000">

<%
valor = request("valor")
if valor = "" then valor = "34191183400000292011090000107160253500375000"


function fbarcode(valor)
Dim f, f1, f2, i
Dim texto
Const fino = 1
Const largo = 3
Const altura = 50
Dim BarCodes(99)

if isempty(BarCodes(0)) then
  BarCodes(0) = "00110"
  BarCodes(1) = "10001"
  BarCodes(2) = "01001"
  BarCodes(3) = "11000"
  BarCodes(4) = "00101"
  BarCodes(5) = "10100"
  BarCodes(6) = "01100"
  BarCodes(7) = "00011"
  BarCodes(8) = "10010"
  BarCodes(9) = "01010"
  for f1 = 9 to 0 step -1
    for f2 = 9 to 0 Step -1
      f = f1 * 10 + f2
      texto = ""
      for i = 1 To 5
        texto = texto & mid(BarCodes(f1), i, 1) + mid(BarCodes(f2), i, 1)
      next
      BarCodes(f) = texto
    next
  next
end if

'Desenho da barra


' Guarda inicial
%>
<img src=p.gif width=<%=fino%> height=<%=altura%> border=0><img 
src=b.gif width=<%=fino%> height=<%=altura%> border=0><img 
src=p.gif width=<%=fino%> height=<%=altura%> border=0><img 
src=b.gif width=<%=fino%> height=<%=altura%> border=0><img 

<%
texto = valor
if len( texto ) mod 2 <> 0 then
  texto = "0" & texto
end if


' Draw dos dados
do while len(texto) > 0
  i = cint( left( texto, 2) )
  texto = right( texto, len( texto ) - 2)
  f = BarCodes(i)
  for i = 1 to 10 step 2
    if mid(f, i, 1) = "0" then
      f1 = fino
    else
      f1 = largo
    end if
    %>
    src=p.gif width=<%=f1%> height=<%=altura%> border=0><img 
    <%
    if mid(f, i + 1, 1) = "0" Then
      f2 = fino
    else
      f2 = largo
    end if
    %>
    src=b.gif width=<%=f2%> height=<%=altura%> border=0><img 
    <%
  next
loop

' Draw guarda final
%>
src=p.gif width=<%=largo%> height=<%=altura%> border=0><img 
src=b.gif width=<%=fino%> height=<%=altura%> border=0><img 
src=p.gif width=<%=1%> height=<%=altura%> border=0>

<%
end function
%>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b>Script Gera C&oacute;digo 
  de Barras 2 of 5 - Padr&atilde;o utilizados para boletos bancarios.</b><br>
  <br>
  Este script foi testado com o leitor de c&oacute;digo de barras e esta OK. <br>
  <b><br>
  Tenho o sistema de boleto</b>, para o ITAU, UNIBANCO, CAIXA, BBV, BRADESCO, 
  HSBC, UNIBANCO, REAL e varios outros, em PHP ou ASP<br>
  Para maiores informa&ccedil;&otilde;es entre em contato <a href="mailto:boleto@netdinamica.com.br%20">boleto@netdinamica.com.br 
  </a></font> <font face="Arial, Helvetica, sans-serif" size="2">ou visite: <a href="http://www.netdinamica.com.br/boleto" target="_blank">www.netdinamica.com.br/boleto</a></font><br>
  </p>
<%=fbarcode(valor)  %>
<form name="form1" method="post" action="">
  <font face="Arial, Helvetica, sans-serif" size="2"><b>Digite o valor do c&oacute;digo 
  de barras:</b></font><br>
  <input type="text" name="valor" maxlength="50" size="50" value="<% =valor %>">
  <input type="submit" name="Submit" value="Gerar C&oacute;digo de Barrar">
</form>
<br>
<font face="Arial, Helvetica, sans-serif" size="2">Desenvolvidor por: Luciano 
Lima -</font> <a href="http://www.netdinamica.com.br?origem=script-barcode" target="_blank"><font face="Arial, Helvetica, sans-serif" size="2">www.netdinamica.com.br</font></a>

</body>
</html>



