<input type=hidden name=pr3 value=8>

<p class="Titulo" align="center">Pr�-Congresso<br>III Encontro Internacional de Odontogeriatria</p>
<p class="TextoComum">
A Sociedade Brasileira de Estomatologia (SOBE) e a Sociedade Brasileira de Odontogeriatria (SBOG) unem for�as neste evento. Assim, no Pr�-Congresso ter� lugar o III Encontro Internacional de Odontogeriatria, com um curso proferido pelo <a href="javascript:submeter(2,4,1,11);">Prof. Dr. Guillermo Machuca Portillo</a> (Especialista em Estomatologia e Doutor em Medicina e Cirurgia), da Faculdade de Medicina da Universidade de Sevilla, na Espanha. 
</p>
<p class="TextoComum" align=center>
	<b>TEMA DO CURSO: "ENFERMIDADES SIST�MICAS CR�NICAS MAIS FREQ��NTES NOS IDOSOS: REPERCUSS�ES NA CAVIDADE BUCAL E IMPLICA��ES NOS PROCEDIMENTOS ODONTOL�GICOS".</b>
</p>
<p class="TextoComum">
DURA��O: 7 horas
</p>
<p class="TextoComum">
PROGRAMA:
</p>
<ul class="TextoComum">
	<li>Considera��es gerais sobre a sa�de bucal na terceira idade
		<ul>
			<li>Envelhecimento da cavidade bucal
		</ul>
	<li>Aspectos espec�ficos do controle da placa bacteriana em idosos
		<ul>
			<li>Conclus�es do 1�. Workshop Ib�rico sobre controle da placa bacteriana e higiene bucal
		</ul>
	<li>Aspectos espec�ficos da sa�de bucal na menopausa
   
	<li>Enfermidades sist�micas com repercuss�o no tratamento odontol�gico dos idosos:
	<ul>
		<li>Enfermidades cardiovasculares:
		<ul>
			<li>Hipertens�o arterial
			<li>Cardiopatia isqu�mica
			<li>Outras (enfermidades vasculares cerebrais, valvulopatias)
			<li>Repercuss�es bucais do tratamento farmacol�gico
			<li>Rela��o entre arterioesclerose e enfermidades periodontais
		</ul>
		<li>Enfermidades psiqui�tricas e neurodegenerativas na terceira idade:
		<ul>
			<li>O paciente com dem�ncia:
			<ul>
				<li>Revers�vel
				<li>Irrevers�vel: Enfermidade de Alzheimer, Parkinson
			</ul>
			<li>O paciente esquizofr�nico
			<li>O paciente depressivo
			<li>Repercuss�es bucais do tratamento farmacol�gico
		</ul>
	</ul>
</ul>