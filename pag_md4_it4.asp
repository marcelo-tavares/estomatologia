<% if id_wk = 1 then
	strTitulo = "Aluguel de Carros"
	strCategoria = "Categoria"
	strDiarias = "Di�rias<BR>(em D�lares EUA)"
	strDescricaoTitulo = "Descri��o"
	strCatA = "Ve�culo B�sico / Motor 1.0, 2 Portas"
	strCatC = "Ve�culo B�sico / Motor 1.0, 4 Portas, Ar Cond."
	strCatD = "Ve�culo M�dio / Motor 1.6, 4 Portas, Ar Cond., Dire��o"
	strCatE = "Ve�culo Perua / Motor 1.6, Ar Cond., Dire��o"
	strCatF = "Ve�culo Sedan / 4 Portas, Ar Cond., Dire��o"
	strCatM = "Luxo"
	strPS = "<b>P.S.: Taxas, sujeitas a mudan�as, incluem quilometragem ilimitada mas n�o incluem seguro</b> (US$4,00 por dia para todas as classes de autom�veis) <b>ou outras taxas</b>."
	strFrase1 = "Aluguel de carros podem tamb�m ser efetuados atrav�s da  Agencia de Turismo oficial do 14th ICDMFR:"
else
	strTitulo = "Car Rental"
	strCategoria = "Category"
	strDiarias = "Daily Rates<BR>(in US$ dollars)"
	strDescricaoTitulo = "Description"
	strCatA = "Economy Class / 1.0L Engine, 2 Doors"
	strCatC = "Economy Class / 1.0L Engine, 4 Doors, A/C"
	strCatD = "Compact Class / 1.6L Engine, 4 Doors, A/C, P/S"
	strCatE = "Station Wagon / 1.6L Engine, A/C, P/S"
	strCatF = "Medium Class / 4 Doors, A/C, P/S"
	strCatM = "Luxury"
	strPS = "<b>P.S.: Rates, subject to changes, include unlimited mileage but do not include insurance</b> (US$4,00 per day for all car classes) <b>or other taxes</b>."
	strFrase1 = "Car rental can also be arranged with the official travel agency of the 14th ICDMFR:"
end if %>

<p class="Titulo" align="center"><%=strTitulo%></p>
<p class="textoComum">
	<%=strFrase1%>
</p>

<table width=100% class=TabelaProgramacao>
	<tr class=CelulaTitulo>
		<td align=center><b><%=strCategoria%></b></td>
		<td align=center><b><%=strDescricaoTitulo%></b></td>
		<td align=center><%=strDiarias%></td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=center><b>A</b></td>
		<td><%=strCatA%></td>
		<td align=center>US$ 28.00</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=center><b>C</b></td>
		<td><%=strCatC%></td>
		<td align=center>US$ 33.00</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=center><b>D</b></td>
		<td><%=strCatD%></td>
		<td align=center>US$ 41.00</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=center><b>E</b></td>
		<td><%=strCatE%></td>
		<td align=center>US$ 49.00</td>
	</tr>
	<tr class=CelulaCorpoCinza>
		<td align=center><b>F</b></td>
		<td><%=strCatF%></td>
		<td align=center>US$ 68.00</td>
	</tr>
	<tr class=CelulaCorpo>
		<td align=center><b>M</b></td>
		<td><%=strCatM%></td>
		<td align=center>US$ 72.00</td>
	</tr>
</table>

<p class="textoComum">
<%=strPS%>
</p>
