<% 
Response.Buffer = true
Response.Expires = -1

parcela_wk = request("parcela")
cd_codigo = request("cd_codigo")
cd_wk = request("wk")
%>
<!--#include file="conn_wk.asp"-->
<%
set Conn = server.CreateObject("ADODB.Connection")
Conn.Open cnpath
set rs = server.CreateObject("ADODB.Recordset")
strSQL = "SELECT de_descricao1,de_descricao2 FROM TB_AreaDeTrabalho WHERE cd_codigo = " & cd_wk
set rs = Conn.Execute(strSQL)
de_participante = rs("de_descricao1")
if len(rs("de_descricao2")) > 0 then 
	de_participante = de_participante & "|" & rs("de_descricao2")
end if
set Conn = nothing
set rs = nothing

if instr(de_participante, "|") > 0 then
	ar_wk = split(de_participante, "|")
	ar_participante = split(ar_wk(0), "#")
	ar_cartaoVisa = split(ar_wk(1), "#")
else
	ar_participante = split(de_participante, "#")
	redim ar_cartaoVisa(2)
	ar_cartaoVisa(0) = ""
	ar_cartaoVisa(1) = ""
end if

' Calculo das datas
if parcela_wk = 1 then
	dt_vencimento = Cdate(ar_participante(25)) + 3
else
	dt_vencimento = Cdate(ar_participante(25)) + (parcela_wk-1) * 30
end if	

%>
<!--#include file="conn.asp"-->
<%
set Conn = server.CreateObject("ADODB.Connection")
Conn.Open cnpath

set rs = server.CreateObject("ADODB.Recordset")
set rs = Conn.Execute("select de_estado from TB_Estados where cd_estado = " & ar_participante(9))
strEstado = rs("de_estado")

valor_wk = replace(Cstr(ar_participante(29)),".",",")
str_dt_inscricao = day(ar_participante(25)) & "/" & month(ar_participante(25)) & "/" & year(ar_participante(25)) 
str_dt_vencimento = day(dt_vencimento) & "/" & month(dt_vencimento) & "/" & year(dt_vencimento)
strEndereco1 = ar_participante(7)
strEndereco2 = ar_participante(11) & " - " & ar_participante(8) & " - " & strEstado & " - " & ar_participante(10)
' -----------------------------------------------------
nome_wk = replace(trim(ar_participante(0))," ","+")
%>
<html>

<head>
<link rel="stylesheet" type="text/css" href="estilos.css">
<title>Boleto</title>
<%
Function zerosAntes(cod, tam)
	auxLen = tam - Len(cod)
	do until auxLen = 0
		cod = "0" & cod
		auxLen = auxLen - 1
	loop
	zerosAntes = cod
End Function

Function dvModuloDez(campo)

	varLen = Len(campo)
	soma = 0

	peso = 2
	if (Cint(varLen/2)*2 = varLen) then
		peso = 1
	end if

	for i=1 to varLen
		varInt = Cint(Mid(campo,i,1))
		varInt = VarInt * peso
		if (varInt > 9) then
			varInt = (varInt - Int(varInt/10)*10) + Int(varInt/10)
		end if
		soma = soma + varInt
		peso = peso + 1
		if peso = 3 then
			peso = 1
		end if
	next

	dezenaSuperior = (Int(soma/10)+1) * 10

	dv = dezenaSuperior - soma
	if (dv = 10) then
		dv = 0
	end if

	dvModuloDez = dv
End Function


Function dvModuloOnzeCB(campo)
	varLen = Len(campo)
	soma = 0

	peso = 4

	for i=1 to varLen
		if i<>5 then
			varInt = Cint(Mid(campo,i,1))
			varInt = varInt * peso
			soma = soma + varInt
			peso = peso - 1
			if peso = 1 then
				peso = 9
			end if
		end if
	next
	
	resto = soma - Int(soma/11)*11
	dv = 11 - resto
	if (dv > 9) then
		dv = 0
	end if

	dvModuloOnzeCB = dv
End Function

Function dvModuloOnze(campo)

	varLen = Len(campo)
	soma = 0

	peso = 1
	for i=1 to varLen
		peso = peso + 1
		if peso = 10 then
			peso = 2
		end if
	next

	for i=1 to varLen
		varInt = Cint(Mid(campo,i,1))
		varInt = varInt * peso
		soma = soma + varInt
		peso = peso - 1
		if peso = 1 then
			peso = 9
		end if
	next

	resto = soma - Int(soma/11)*11
	dv = 11 - resto

	if (dv = 11) then
		dv = 1
	end if

	dvModuloOnze = dv
End Function


function fbarcode(valor)
Dim f, f1, f2, i
Dim texto
Const fino = 1
Const largo = 3
Const altura = 50
Dim BarCodes(99)

if isempty(BarCodes(0)) then
  BarCodes(0) = "00110"
  BarCodes(1) = "10001"
  BarCodes(2) = "01001"
  BarCodes(3) = "11000"
  BarCodes(4) = "00101"
  BarCodes(5) = "10100"
  BarCodes(6) = "01100"
  BarCodes(7) = "00011"
  BarCodes(8) = "10010"
  BarCodes(9) = "01010"
  for f1 = 9 to 0 step -1
    for f2 = 9 to 0 Step -1
      f = f1 * 10 + f2
      texto = ""
      for i = 1 To 5
        texto = texto & mid(BarCodes(f1), i, 1) + mid(BarCodes(f2), i, 1)
      next
      BarCodes(f) = texto
    next
  next
end if

'Desenho da barra


' Guarda inicial
%>
<img src="images/p.gif" width="<%=fino%>" height="<%=altura%>" border="0"><img src="images/b.gif" width="<%=fino%>" height="<%=altura%>" border="0"><img src="images/p.gif" width="<%=fino%>" height="<%=altura%>" border="0"><img src="images/b.gif" width="<%=fino%>" height="<%=altura%>" border="0"><img <%
texto = valor
if len( texto ) mod 2 <> 0 then
  texto = "0" & texto
end if


' Draw dos dados
do while len(texto) > 0
  i = cint( left( texto, 2) )
  texto = right( texto, len( texto ) - 2)
  f = BarCodes(i)
  for i = 1 to 10 step 2
    if mid(f, i, 1) = "0" then
      f1 = fino
    else
      f1 = largo
    end if
    %> src="images/p.gif" width="<%=f1%>" height="<%=altura%>" border="0"><img <%
    if mid(f, i + 1, 1) = "0" Then
      f2 = fino
    else
      f2 = largo
    end if
    %> src="images/b.gif" width="<%=f2%>" height="<%=altura%>" border="0"><img <%
  next
loop

' Draw guarda final
%> src="images/p.gif" width="<%=largo%>" height="<%=altura%>" border="0"><img src="images/b.gif" width="<%=fino%>" height="<%=altura%>" border="0"><img src="images/p.gif" width="<%=1%>" height="<%=altura%>" border="0">

<%
end function

varLocal = "At� o vencimento pag�vel em qualquer banco"
varCedente = "Sociedade Brasileira de Estomatologia"
varAgencia  = "9999"
varAgenciaDV = "-9"
varCodCedente = "2916"
varCodCedenteDV = ""
varDataDoc = Date
varEspecieDoc = "RC"
varAceite = "N"
varDataProc = Date
varUsoBanco = " "
varCarteira = "99"
varCarteiraVariacao = "-019"
varEspecie = "R$"
varQuantidade = " "
varValor = " "

varValorDoc = valor_wk
varVencimento = cdate(str_dt_vencimento)
varReferencia = "XIII Congresso Brasileiro de Estomatologia" _
				& "<br>Inscri��o " & cd_codigo & " efetuada em " & str_dt_inscricao _
				& "<br>Parcela " & parcela_wk & " / " & ar_participante(20)  
varReferencia_1 = "Categoria "
categoria = 1
select case categoria
	case 1
		varReferencia_1 = varReferencia_1 & "S�CIO DA SOBE"
	case 2
		varReferencia_1 = varReferencia_1 & "N�O S�CIO"
	case 3
		varReferencia_1 = varReferencia_1 & "ESTUDANTE DE P�S-GRADUA��O"
	case 4
		varReferencia_1 = varReferencia_1 & "ACAD�MICO DE GRADUA��O"
end select


varSacado1 = ucase(nome_wk)

strNaoReceber = Day(varVencimento+3) & "/" & Month(varVencimento+3) & "/" & Year(varVencimento+3)

varInstrucao1 = "Pagar preferencialmente no Banco do Brasil"
varInstrucao2 = "N�o receber ap�s " & strNaoReceber 
varInstrucao3 = "N�o cobrar juros de mora"

varSacado2 = strEndereco1

complemento = ""

if len(trim(complemento)) <> 0 then varSacado2 = varSacado2 + " - " & complemento
varSacado3 = strEndereco2

varDemonstrativo = ""
varContrato = "988317"

codInscricao = zerosAntes(request("codigo"),5)

varNossoNumero = varContrato & codInscricao 
dvNossoNumero = dvModuloOnze(varNossoNumero)
varNumDoc = codInscricao


varCB1 = "0019*00000000000000"
varCB2 = "29210000327818"
varCodigoBarra = varCB1 & varNossoNumero & varCB2
dvCB = dvModuloOnzeCB(varCodigoBarra)
varCodigoBarra = Mid(varCodigoBarra,1,4) & Cstr(dvCB) & Mid(varCodigoBarra,6,44)

varLinhaDig1 = "0019" & Left(varContrato,5)
varLinhaDig1 = varLinhaDig1 & Cstr(dvModuloDez(varlinhaDig1)) & " "
varLinhaDig1 = Mid(varLinhaDig1,1,5) & "." & Right(varLinhaDig1,6)

varLinhaDig2 = Right(varContrato,1) & codInscricao & varAgencia
varLinhaDig2 = varLinhaDig2 & Cstr(dvModuloDez(varlinhaDig2)) & " "
varLinhaDig2 = Mid(varLinhaDig2,1,5) & "." & Right(varLinhaDig2,7)

varLinhaDig3 = zerosAntes(varCodCedente,8) & varCarteira
varLinhaDig3 = varLinhaDig3 & Cstr(dvModuloDez(varlinhaDig3)) & " "
varLinhaDig3 = Mid(varLinhaDig3,1,5) & "." & Right(varLinhaDig3,7)

varLinhaDig4 = Cstr(dvCB) & " 000"
%>
<script>
	function load(){
		window.print(); 
		window.close();
	}
</script>
</head>

<body BGCOLOR="#FFFFFF" onload="">
	<div align="left">
		<table class="tw170" CELLSPACING="0" CELLPADDING="0" BORDER="0">
			<tr>
				<td class="w85">
					<img src="images/adesivoBoleto.jpg" WIDTH="263" HEIGHT="75">
				</td>
				<td class="w85" ALIGN="RIGHT">
					<img src="images/logoSOBEBoleto.jpg" alt="Sociedade Brasileira de Estomatologia" WIDTH="225" HEIGHT="75">
				</td>
			</tr>
			<tr>
				<td VALIGN="BOTTOM" class="w85">
					<img src="images/bancodobrasil.gif" alt="Unicred" WIDTH="107" HEIGHT="20">
					<img src="images/bancodobrasil0019.gif" alt="Unicred" WIDTH="60" HEIGHT="18">
				</td>
				<td ALIGN="RIGHT" VALIGN="BOTTOM" class="w85">
					<font FACE="Arial, Helvetica" SIZE="2">
						<b>RECIBO DO SACADO</b>
					</font>
				</td>
			</tr>
		</table>
	</div>

	<div align="left">
		<table class="tw170" BORDER="1" CELLSPACING="0" CELLPADDING="1">

			<tr>
				<td class="w70">
					<font FACE="Arial, Helvetica" SIZE="1">Cedente</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varCedente%></font>
				</td>
				<td class="w40">
					<font FACE="Arial, Helvetica" SIZE="1">Ag�ncia / C�digo Cedente</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varAgencia & varAgenciaDV & " / " & varCodCedente & varCodCedenteDV%></font>
				</td>
				<td class="w25">
					<font FACE="Arial, Helvetica" SIZE="1">Data Emiss�o</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=Day(varDataDoc) & "/" & Month(varDataDoc) & "/" & Year(varDataDoc)%></font>
				</td>
				<td ALIGN="RIGHT" class="w35">
					<font FACE="Arial, Helvetica" SIZE="1">Vencimento</font><br>
					<font FACE="Arial, Helvetica"><b><%=Day(varVencimento) & "/" & Month(varVencimento) & "/" & Year(varVencimento)%></b></font>
				</td>
			</tr>

			<tr>
				<td class="w70">
					<font FACE="Arial, Helvetica" SIZE="1">Sacado</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1">
						<strong><%=varSacado1%></strong>
					</font>
				</td>
				<td class="w40">
					<font FACE="Arial, Helvetica" SIZE="1">Nosso Numero</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1">
						<%
						Response.Write varNossoNumero & "-"
						if dvNossoNumero = 10 then
							response.write "x"
						else 
							response.write dvNossoNumero
						end if
						%> 
					</font>
				</td>
				<td class="w25">
					<font FACE="Arial, Helvetica" SIZE="1">Numero da Guia</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varNumDoc%></font>
				</td>
				<td ALIGN="RIGHT" class="w35">
					<font FACE="Arial, Helvetica" SIZE="1">Valor do Documento</font><br>
					<font FACE="Arial, Helvetica"><b><%=formatnumber(varValorDoc,2)%></b></font>
				</td>
			</tr>
		
			<tr>
				<td COLSPAN="4" class="w170">
					<font FACE="Arial, Helvetica" SIZE="1">Demonstrativo</font>
					<strong>
						<font FACE="LucidaTypewriter, Courier" size=2>
							<br><br>
							<%
							response.write varReferencia & "<p>"
							response.write varReferencia_1
							%>
						</font>
					</strong>
				</td>
			</tr>
		</table>
	</div>


	<div align="left">
		<table class="tw170" CELLSPACING="0" CELLPADDING="0" BORDER="0">
			<tr>
				<td class="w170">
					<p ALIGN="RIGHT">
						<font SIZE="-2">Autentica��o Mec�nica</font>
					</p>
					<p>
					<br>
					<br>
				</td>
			</tr>
		</table>
	</div>

	<hr size="1" align="center">

	<div align="left">
		<table class="tw170" CELLSPACING="0" CELLPADDING="0" BORDER="0">
			<tr>
				<td VALIGN="BOTTOM" class="w50">
					<img src="images/bancodobrasil.gif" alt="UNICRED" WIDTH="107" HEIGHT="20">
					<img src="images/bancodobrasil0019.gif" alt="UNICRED" WIDTH="60" HEIGHT="18">
				</td>
				<td VALIGN="BOTTOM" ALIGN="RIGHT" class="w120">
					<font FACE="LucidaTypewriter, Courier">
						<tt>
							<b><%=varLinhaDig1 & varLinhaDig2 & varLinhaDig3 & varLinhaDig4%></b>
						</tt>
					</font>
				</td>
			</tr>
		</table>
	</div>


	<div align="left">
		<table class="tw170" BORDER="1" CELLSPACING="0" CELLPADDING="1">
			<tr>
				<td COLSPAN="5" class="w129" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">
						Local de Pagamento
					</font>
					<br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varLocal%></font>
				</td>
				<td class="w41" ALIGN="RIGHT" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">Vencimento</font><br>
					<b>
						<font FACE="Arial, Helvetica">
							<%=Day(varVencimento) & "/" & Month(varVencimento) & "/" & Year(varVencimento)%>
						</font>
					</b>
				</td>
			</tr>
			
			<tr>
				<td COLSPAN="5" class="w129" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">Cedente</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varCedente%></font>
				</td>
				<td class="w41" ALIGN="RIGHT" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">Ag�ncia / C�digo Cedente</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varAgencia & varAgenciaDV & " / " & varCodCedente & varCodCedenteDV%> </font>
				</td>
			</tr>
			
			<tr>
				<td valign="top" class="w28">
					<font FACE="Arial, Helvetica" SIZE="1">Data Documento</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=Day(varDataDoc) & "/" & Month(varDataDoc) & "/" & Year(varDataDoc)%></font>
				</td>
				<td valign="top" class="w28">
					<font FACE="Arial, Helvetica" SIZE="1">Numero Documento</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varNumDoc%></font>
				</td>
				<td valign="top" class="w28">
					<font FACE="Arial, Helvetica" SIZE="1">Especie Documento</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varEspecieDoc%></font>
				</td>
				<td valign="top" class="w15">
					<font FACE="Arial, Helvetica" SIZE="1">Aceite</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varAceite%></font>
				</td>
				<td valign="top" class="w30">
					<font FACE="Arial, Helvetica" SIZE="1">Data Processamento</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=Day(varDataDoc) & "/" & Month(varDataDoc) & "/" & Year(varDataDoc)%></font>
				</td>
				<td class="w41" ALIGN="RIGHT" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">Nosso Numero</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1">
						<% 
						response.write varNossoNumero & "-"
						if dvNossoNumero = 10 then
							response.write "x"
						else 
							response.write dvNossoNumero
						end if
						%>
					</font>
				</td>
			</tr>
			
			<tr>
				<td valign="top" class="w28">
					<font FACE="Arial, Helvetica" SIZE="1">Uso Banco</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varUsoBanco%></font>
				</td>
				<td valign="top" class="w28">
					<font FACE="Arial, Helvetica" SIZE="1">Carteira</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varCarteira%></font>
				</td>
				<td valign="top" class="w28">
					<font FACE="Arial, Helvetica" SIZE="1">Especie</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varEspecie%></font>
				</td>
				<td valign="top" class="w15">
					<font FACE="Arial, Helvetica" SIZE="1">Quantidade</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varQuantidade%></font>
				</td>
				<td valign="top" class="w30">
					<font FACE="Arial, Helvetica" SIZE="1">Valor</font><br>
					<font FACE="Arial, Helvetica" SIZE="-1"><%=varValor%></font>
				</td>
				<td class="w41" ALIGN="RIGHT" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">Valor do Documento</font><br>
					<font FACE="Arial, Helvetica">
						<b><%=formatnumber(varValorDoc,2)%></b>
					</font>
				</td>
			</tr>
	
			<tr>
				<th COLSPAN="5" ROWSPAN="4" valign="top" class="w120">
					<p ALIGN="LEFT">
						<font FACE="Arial, Helvetica" SIZE="1">Instru��es</font><br>
						<font FACE="Arial, Helvetica" SIZE="-1">
							<%=varInstrucao1%><br>
							<%=varInstrucao2%><br>
							<%=varInstrucao3%>
						</font>
					</p>
				</th>
				<td class="w41" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">(+) Outros Acr�cimos</font>
				</td>
			</tr>

			<tr>
				<td class="w41" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">(-) Descontos/Abatimento</font>
				</td>
			</tr>

			<tr>
				<td class="w41" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">(+) Mora/Multa</font>
				</td>
			</tr>

			<tr>
				<td class="w41" valign="top">
					<font FACE="Arial, Helvetica" SIZE="1">(=) Valor Cobrado</font><br>
					&nbsp;
				</td>
			</tr>

			<tr>
				<td COLSPAN="6" valign="top" class="w170">
					<font FACE="Arial, Helvetica" SIZE="1">Sacado</font>
					<font FACE="Arial, Helvetica" SIZE="-1">
						<br>
						<%=varSacado1%><br>
						<%=varSacado2%> - <%=varSacado3%><br>
					</font>
					<font FACE="Arial, Helvetica" SIZE="1">Sacador/Cedente</font>
				</td>
			</tr>
		</table>
	</div>
	
	
	<div align="left">
		<table class="tw170" CELLSPACING="0" CELLPADDING="0" BORDER="0">
			<tr>
				<td>
					<p ALIGN="RIGHT">
						<font SIZE="-2">Autentica��o Mec�nica</font> / 
						<font FACE="Arial, Helvetica" SIZE="2">
							<b>FICHA DE COMPENSA��O</b>
						</font>
					</p>
					<p ALIGN="LEFT">
						<%'=fbarcode(valorvarCodigoBarra)%>
						<%=fbarcode("12345678901234567890123456789012345678901234")%>
				</td>
			</tr>
		</table>
	</div>

	<hr size="1" align="center">

	<div align="left">
		<table WIDTH="100%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
			<tr>
				<td>
					<p ALIGN="RIGHT">
						<font SIZE="-2">corte aqui</font>
					</p>
				</td>
			</tr>
		</table>
	</div>
	
</body>
</html>
