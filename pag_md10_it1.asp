<%
select case pr1_wk
	case 1
	%>
		<p class="Titulo" align="center">O ARTISTA</p>
		<p class="TextoComum">
			<img SRC="images/jphilippi.jpg" border="0" hspace="5" align="right" WIDTH="124" HEIGHT="165">
			<b>Juarez Philippi</b>, cirurgi�o-dentista e professor, ap�s dedicar-se 45 anos � pesquisa e ao ensino de histologia, tanto pela Universidade Federal de Santa Catarina, quanto pela Universidade do Vale do Itaja�, optou pela &#147;Releitura da Microscopia&#148;. 
		</p>
		<p class="TextoComum">
			Esta amostra � composta de doze quadros, onde utilizou-se a t�cnica de acr�lico sobre tela. Estes quadros foram inspirados em fotomicrografias de l�minas histol�gicas e histopatol�gicas, confeccionadas e admiradas pelo autor durante estes anos de dedica��o profissional.
		</p>
		<p class="TextoComum">
			Sua paix�o por desenho e pintura, juntamente com sua experi�ncia laboratorial, o ajudaram na elabora��o e cria��o destas obras.
		</p>
	<%
	case 2
	%>
		<p class="Titulo" align="center">A MOSTRA</p>
		<p class="TextoComum" align=center>
			Nome da Mostra: <b>A Releitura da Microscopia</b>
		</p>
		<table border=0 width=100%>
			<tr>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra1_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra1.jpg', 'M�sculo Estriado', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					M�sculo<br>Estriado
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra2_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra2.jpg', 'Reabsor��o Dentin�ria', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					Reabsor��o<br>Dentin�ria
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra3_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra3.jpg', 'Cisto Dent�rio', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					Cisto<br>Dent�rio
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra4_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra4.jpg', 'Pele Fina', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					Pele Fina
				</td>
			</tr>
			<tr><td colspan=4>&nbsp;</td></tr>
			<tr>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra5_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra5.jpg', 'Intestino Grosso', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					Intestino<br>Grosso
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra6_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra6.jpg', 'Gl�ndulas Intestinais', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					Gl�ndulas<br>Intestinais
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra7_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra7.jpg', 'Osso Compacto', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					Osso<br>Compacto
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra8_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra8.jpg', 'Fibras Col�genas', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">
					<br>
					Fibras<br>Col�genas
				</td>
			</tr>
			<tr><td colspan=4>&nbsp;</td></tr>
			<tr>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra9_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra9.jpg', 'M�sculo Card�aco', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">&nbsp;&nbsp;
					<br>
					M�sculo<br>Card�aco
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra10_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra10.jpg', 'Fibras Reticulares', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">&nbsp;&nbsp;
					<br>
					Fibras<br>Reticulares
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra11_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra11.jpg', 'Displasia Fibrosa', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">&nbsp;&nbsp;
					<br>
					Displasia<br>Fibrosa
				</td>
				<td align=center class="TextoComum">
					<img SRC="images/mostraArtistica/obra12_tn.jpg" onclick="ampliarFotoObra('mostraArtistica/obra12.jpg', 'Parafina', 500, 375 )" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';" width="100" height="75">&nbsp;&nbsp;
					<br>
					Parafina
				</td>
			</tr>
		</table>
		<p class="TextoComum" align=center>
		</p>
	<%
end select
%>