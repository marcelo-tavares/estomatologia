<%
if id_wk = 1 then
	strClick = "Clique para ampliar"
	strTitulo = "A CIDADE ANFITRI� - FLORIAN�POLIS"
	strMenu1 = "FLORIAN�POLIS"
	strMenu2 = "ILHA DE SANTA CATARINA"
	strMenu3 = "HIST�RIA"
	strMenu4 = "PRAIAS"
	strMenu5 = "CONTINENTE"
	strMenu6 = "FORTALEZAS"
	strMenu7 = "CULIN�RIA"
	strFrase1 = "Florian�polis, conhecida como a ilha da magia, � a capital do estado de Santa Catarina e situa-se parte em uma ilha e parte no continente, ambos separados, em sua por��o mais pr�xima, por um canal de aproximadamente 500m de extens�o e tr�s pontes. A ilha em si tem um formato alongado e 50Km de extens�o por 10Km de largura, em m�dia. Seus quase 500.000 habitantes (aproximadamente um milh�o no ver�o) constantemente aproveitam suas 42 praias, 2 lagoas e uma natureza espetacular."
	strFrase2 = "<img src=""images/floripa.jpg"" id=""imgMapa"" align=right hspace=10 vspace=10>Florian�polis tem clima temperado e quatro esta��es bem definidas. Em Florian�polis, as temperaturas geralmente sobem um pouco novamente em maio ap�s o ver�o ter-se encerrado. � o chamado �veranico� de maio. Tipicamente nesta �poca do ano, os dias s�o ensolarados e as noites estreladas, com temperaturas m�ximas por volta dos 25-27oC, e as m�nimas, � noite, por volta dos 12-15oC. Roupas leves de inverno s�o recomendadas."
	strFrase3 = "A corrente el�trica em Florian�polis � de 220 volts, portanto n�o se esque�a de alterar a voltagem de aparelhos eletro-eletr�nicos que porventura voc� estiver trazendo consigo."
	strFrase4 = "As atividades do setor terci�rio (servi�os) e o turismo s�o as principais fontes de renda da popula��o da capital catarinense. O servi�o p�blico tamb�m est� centralizado na capital, incluindo a Universidade Federal de Santa Catarina, com aproximadamente 25.000 alunos."
	strFrase5 = "N�o somente Florian�polis merece sua visita. Todo o estado de Santa Catarina tem muitas atra��es a lhe oferecer. Ele cracteriza-se pela diversidade de paisagens, climas, culturas, descend�ncias e culin�ria. Nossa costa � uma das mais belas do Brasil e com certeza uma das mais procuradas pelos turistas, tanto nacionais como internacionais. A duas horas a oeste do litoral voc� encontrar� a regi�o serrana, com montanhas encantadoras, onde a neve cai algumas vezes no ano, sendo um dos raros locais no Brasil onde este fen�meno clim�tico ocorre. �guas termais, vilas de pescadores e pontos hist�ricos tamb�m s�o de grande atra��o para o turismo durante o ano todo. De pr�dios modernos � t�pica cena rural, voc� pode encontrar tudo isto em nosso estado, com 95.400Km2 de �rea, correspondendo a 1,12% do territ�rio brasileiro."
	strFrase6 = ""
else
	strClick = "Click to enlarge"
	strTitulo = "The host city � Florian�polis"
	strMenu1 = "FLORIAN�POLIS"
	strMenu2 = "SANTA CATARINA ISLAND"
	strMenu3 = "HISTORY"
	strMenu4 = "BEACHES"
	strMenu5 = "THE MAINLAND"
	strMenu6 = "FORTS"
	strMenu7 = "CUISINE"
	strFrase1 = "Florian�polis, known as the �Island of Magic�, is the Capital of Santa Catarina, the second southernmost Brazilian state. The city is located part in an island and part in the mainland, both separated by a 500m channel and three bridges. The island itself has an elongated shape and is 50Km long by 10Km wide, on average. Its nearly 500,000 inhabitants (about a million during summer) constantly enjoy its 42 beaches, 2 major lagoons and a fascinating landscape."
	strFrase2 = "<img src=""images/floripa.jpg"" id=""imgMapa"" align=right hspace=10 vspace=10>Florian�polis has a mild climate and four well-defined seasons. May, the month of the 14th ICDMFR, is Fall in Brasil. In Florian�polis, temperatures usually get a little higher again after summer has ended (May�s �little summer�). It is typically sunny during this time of the year, with starry nights; highs are usually near 25-27oC, and the lows, during the evenings, are around 12-15oC. A light jacket is usually recommended."
	strFrase3 = "Power supply in Florian�polis is 220 volts, so remember to switch the voltage in your electric/electronic devices when you arrive."
	strFrase4 = "A small number of people speaks English fluently in Brasil. Therefore, it is not a bad idea to get a Portuguese dictionary and start learning a few words in the new language! However, people in general will do whatever they can to help you (e.g., giving you directions to places, etc). We are known for our hospitality."
	strFrase5 = "Tertiary activities (services in general) and tourism are the main sources of income to the population. The public service of the state is also centered in the Capital, including its public federal University, with approximately 25,000 students."
	strFrase6 = "Not only Florian�polis deserves your visit. The whole state of Santa Catarina has may attractions to offer. It is characterized by a diversity of sceneries, climates, cultures and people. Our coastline is one of the most beautiful in Brasil and one that receives a large number of national and international tourists. Two hours west of the coast you will find enchanting mountains where snow falls a few times per year, something rare in Brasil. Thermal waters, fishing villages and historical places are also very appealing and visited by many people during the whole year. From skyscrapers to the typical rural scene, you can enjoy it all in our state, with 95,400Km2, representing 1.12% of the Brazilian territory."
end if
%>
<p class="Titulo" align="center"><%=strTitulo%></p>

<script>
	function ativaDefinicao(elemento){
		elemento.style.cursor='hand';
		elemento.style.color='#FFD359';
		elemento.style.fontWeight = 700;
	}

	function ativaTabela(id){
		for  (i=1; i<=7; i++) {
			tabela='tab' + i
			document.getElementById(tabela).style.display = "none";
		}
		
		tabela='tab' + id
		document.getElementById(tabela).style.display = "inline";

		document.getElementById("hr1").style.display = "inline";
		document.getElementById("imgPonte").style.display = "none";
	}

	function desativaDefinicao(elemento){
		elemento.style.cursor='default'; 
		elemento.style.color='#000000';
		elemento.style.fontWeight = 700;
	}

	function desativaTabela(id){
		tabela='tab' + id
		document.getElementById(tabela).style.display = "none";
	}

	function abrirJanelaNews(URL,nomeJanela,props){
		window.open(URL,nomeJanela,props);
	}
		 
	function ampliarFoto(nm_foto, de_foto,cd_idioma,cd_orientacao){
		if (cd_orientacao == 1) {
			url = 'ampliaFoto.asp?nm_foto='+nm_foto+'&de_foto='+de_foto+'&cd_idioma='+cd_idioma
			params = 'scrollbars=no,width=546,height=396';
		}
		else {
			url = 'ampliaFotoV.asp?nm_foto='+nm_foto+'&de_foto='+de_foto+'&cd_idioma='+cd_idioma
			params = 'scrollbars=no,width=396,height=546';
		}
		id = 'Imagem';
		abrirJanelaNews(url,id,params);
	}
</script>

<table class="Visivel" border="0" cellpadding="5" cellspacing="0" WIDTH="100%">
	<tr>
		<td align="middle" class="textocomum">
		  <b>
			<span id="sp1" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="submeter(4,2,<%=id_wk%>,1);"><%=strMenu1%></span>&nbsp;&#149;&nbsp;

			<span id="sp2" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="submeter(4,2,<%=id_wk%>,2);"><%=strMenu2%></span>&nbsp;&#149;&nbsp;

			<span id="sp3" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="submeter(4,2,<%=id_wk%>,3);"><%=strMenu3%></span><br><br>
			
			<span id="sp4" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="submeter(4,2,<%=id_wk%>,4);"><%=strMenu4%></span>&nbsp;&#149;&nbsp;

			<span id="sp5" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="submeter(4,2,<%=id_wk%>,5);"><%=strMenu5%></span>&nbsp;&#149;&nbsp;

			<span id="sp6" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="submeter(4,2,<%=id_wk%>,6);"><%=strMenu6%></span>&nbsp;&#149;&nbsp;

			<span id="sp7" onmouseover="ativaDefinicao(this);" onmouseout="desativaDefinicao(this);" onclick="submeter(4,2,<%=id_wk%>,7);"><%=strMenu7%></span>
		  </b>
		</td>
	</tr>
</table>
<% 
select case pr1_wk
case 0
%>
<p class="textoComum">
	<%=strFrase1%>
</p>
<p class="textoComum">
	<%=strFrase2%>
</p>
<p class="textoComum">
	<%=strFrase3%>
</p>
<p class="textoComum">
	<%=strFrase4%>
</p>
<p class="textoComum">
	<%=strFrase5%>
</p>
<p class="textoComum">
	<%=strFrase6%>
	<br><br>
</p>
<% 
case 1 
	
	if id_wk = 1 then
	%>

		<hr id="hr1">

		<table border="0" id="tab1">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>FLORIAN�POLIS � SOL, MAR, TRADI��O E HIST�RIA</b>
						<br>
					</p>
					<p>
						Seus primeiros habitantes foram os �ndios tupis guaranis, que lhe deram as primeiras denomina��es: Miembipe e Jurer� Mirim. O primeiro a aportar nela foi o navegador espanhol Cabeza de Vaca em 1541. Os bandeirantes chegaram mais de um s�culo depois com Francisco Dias Velho, que fundou a povoa��o de Nossa Senhora do Desterro. Para mant�-la, o governo portugu�s enviou para ela, de 1748 � 1756, cerca de 5 mil colonos sa�dos das ilhas dos A�ores e Madeira. 
					</p>
					<p>
						Em 1894, numa homenagem ao marechal Floriano Peixoto, a cidade recebeu a atual denomina��o &#150; Florian�polis, a musa de milhares de turistas atra�dos n�o apenas pela hist�ria, mas tamb�m pelos atributos naturais da Ilha de Santa Catarina, onde fica localizada a maior parte da cidade. Com 370 mil habitantes, Florian�polis � a �nica capital de Estado brasileiro cuja popula��o � sobrepujada, numericamente, por outra do interior, Joinville, com cerca de 440 mil habitantes.
					</p>
					<p>
						Riqueza e fartura s�o uma esp�cie de marca de Florian�polis quando o assunto � praia. Com 42 , algumas selvagens, outras de beleza incomum e voltadas para o turismo sofisticado e internacional, algumas de �guas quentes e calmas, outras agitadas e frias. 
					</p>
					<p>
						Entre as mais conhecidas est�o Canasvieiras, a mais internacionalizada, preferida pelos argentinos e uruguaios; Ribeir�o da Ilha, onde a heran�a a�oriana � vista nas casas, igrejas e rostos; Lagoa da Concei��o, um de seus mais reverenciados cart�es postais; Joaquina, o palco do surf do centro sul brasileiro; e a badalada Jurer�, not�vel pelos esportes n�uticos e vida noturna.
					</p>
					<p>
					<br>
					<p>
						<b>CAPITAL DA DIVERSIDADE</b>
					</p>
					<p>
						O visitante que chega � Florian�polis em qualquer �poca do ano depara-se com um intrigante problema: o excesso de diversidade. Por isso o bom turista deve eleger algumas prioridades. Se a oferta de praias pode ser considerada exagerada, h� tamb�m hist�ria, como as v�rias fortalezas que cercam a Ilha de Santa Catarina. Elas n�o passam despercebidas por ningu�m. Algumas restauradas, outras em ru�nas, s�o um espet�culo � parte e representam hoje o mais significativo patrim�nio da Marinha brasileira em todo o Atl�ntico.
					</p>
					<p>
						Para defender Florian�polis de invas�es, Portugal edificou v�rios fortes em locais estrat�gicos para controlar o acesso pelo norte e sul � vila do Desterro. De 1738 � 1761, foram constru�das cinco fortalezas. A de S�o Jos� de Ponta Grossa foi a primeira a ser erguida. A mais imponente � a de Anhatomirim, onde foram investidos mais de US$ 1 milh�o na sua recupera��o.
					</p>
					<p>
						V�rios outros monumentos est�o incorporados � hist�ria de Florian�polis. Um dos principais � a ponte Hercilio Luz, por d�cadas tido como principal cart�o postal da cidade, interditada ao tr�fego por estar com suas estruturas avariadas. Com 819 metros de extens�o, foi inaugurada em 1926, com duas torres de 75 metros de altura. � um raro exemplar de ponte p�nsil existente no mundo.
					</p>
					<p>
						Outro ponto que merece visita��o � a Catedral Metropolitana, localizada na parte mais central da cidade e no mesmo local onde Francisco Dias Velho construiu, em 1675, uma capela em homenagem � Nossa Senhora do Desterro. Nela, foi morto num ataque de piratas. Posteriormente foi substitu�da por uma pequena igreja que mais tarde transformou-se em catedral. Distante poucos metros fica o Pal�cio Cruz e Sousa, constru�do no s�culo XVIII, inicialmente utilizado como resid�ncia e local de trabalho do Presidente da Prov�ncia, passando a abrigar os governadores de Santa Catarina at� a metade deste s�culo. Hoje sedia o Museu Hist�rico de Santa Catarina.
					</p>
					<p>
						O pr�dio da Alf�ndega � outro belo exemplar da hist�ria de Florian�polis. Foi constru�do entre 1875 e 1876, com uma arquitetura neo-cl�ssica, rica em detalhes. Nas proximidades da Alf�ndega fica o mais tradicional centro comercial da cidade &#150; o Mercado P�blico, com 140 boxes abrigando estabelecimentos que vendem produtos t�picos ( peixes, carnes, frutos do mar, m�veis em vime e lou�as de barro ), onde transitam cerca de 10 mil pessoas por dia. O pr�dio foi inaugurado em 1898. Al�m do centro da cidade, h� v�rios monumentos de import�ncia hist�rica e arquitet�nica no interior da Ilha, especialmente nas localidades do Ribeir�o da Ilha e Lagoa da Concei��o.
					</p>
					<p>
						� especialmente no ver�o que o visitante de Florian�polis pode ver algumas das mais significativas manifesta��es folcl�ricas da cidade, todos com forte influ�ncia a�oriana e espanhola, como o boi-de-mam�o.
					</p>
					<br><br>&nbsp;
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(9)
					nm_figuras( 1) = "Rua Felipe Schmidt"
					nm_figuras( 2) = "Rodoviaria"
					nm_figuras( 3) = "Ponte hercilio luz"
					nm_figuras( 4) = "Ponte hercilio luz2"
					nm_figuras( 5) = "Beiramar Norte 1"
					nm_figuras( 6) = "Veleiros da Ilha"
					nm_figuras( 7) = "Ciclovia"
					nm_figuras( 8) = "Beiramar Norte 2"

					redim alt_figuras(9)
					alt_figuras( 1) = "Rua Felipe Schmidt"
					alt_figuras( 2) = "Rodovi�ria"
					alt_figuras( 3) = "Ponte Herc�lio Luz"
					alt_figuras( 4) = "Ponte Herc�lio Luz"
					alt_figuras( 5) = "Av. Beira Mar Norte"
					alt_figuras( 6) = "Vista do Iate Clube"
					alt_figuras( 7) = "Ciclovia da Av. Beira Mar Norte"
					alt_figuras( 8) = "Av. Beira Mar Norte"
					
					redim orientacao(9)
					orientacao( 1) = 1
					orientacao( 2) = 1
					orientacao( 3) = 1
					orientacao( 4) = 2
					orientacao( 5) = 1
					orientacao( 6) = 1
					orientacao( 7) = 1
					orientacao( 8) = 1
					
					for i=1 to 8
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="30" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>,<%=orientacao(i)%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

	<%
	else
	%>

		<hr id="hr1">

		<table border="0" id="tab1">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>FLORIAN�POLIS IS SUN, SEA, TRADITION E HISTORY</b>
						<br>
					</p>
					<p>
						Its first inhabitants were the Tupi Guarani Indians, who gave it its first names: Miembipe and Jurer� Mirim. The first to land here was the Spanish explorer Cabeza de Vaca in 1541. The bandeirantes pioneers came a century later with Francisco Dias Velho, who founded the population of Nossa Senhora do Desterro. To maintain the settlement, from 1748 to 1756 the Portuguese government sent nearly 5 thousand settlers from the Azorean Islands and Madeira. 
					</p>
					<p>
						In 1894, in homage to Marechal Floriano Peixoto, the city received its current name - Florian�polis. It is now the muse of thousands of tourists attracted not only by history, but also by the natural attributes of Santa Catarina Island, where the largest portion of the city is located. With 370 inhabitants, Florian�polis is the only capital of a Brazilian state where the population is exceeded by a city in the interior, in this case Joinville, which has some 440 thousand inhabitants.
					</p>
					<p>
						When the subject is beaches, wealth and abundance are distinctions of Florian�polis. With 42 beaches, some of them are wild, others of uncommon beauty and prepared for sophisticated international tourism, some have warm and calm waters, others have rough and chilly waters.
					</p>
					<p>
						Among the best known are Jurer�, which is the most internationalized, preferred by Argentines and
					</p>
					<p>
						Uruguayans; Ribeir�o da Ilha, where the Azorean heritage is seen in the homes, churches and faces; Lagoa da Concei��o, which has one of the most revered views; Joaquina, the stage for surf in central southern Brazil; and the much commented Canasvieiras, notable for nautical sports and night life.
					</p>
					<p>
						<b>
							CAPITAL OF DIVERSITY
						</b>
						<br>
					</p>
					<p>
						The visitor who arrives at Florian�polis at any season of the year faces an intriguing problem: the excess of diversity. For this reason, a smart tourist should set some priorities. In addition to the plentiful supply of beaches, there is also history, with various forts that surround Santa Catarina Island. No one passes without noticing them. Some are restored, others are in ruins, they are unique spectacles which represent the most significant heritage of the Brazilian Navy on the entire Atlantic Coast.
					</p>
					<p>
						To defend Florian�polis from invasions, Portugal built various forts in strategic locations to control access to the village of Desterro from the north and south. From 1738 to 1761, five forts were built. That of S�o Jos� de Ponta Grossa was the first. The most imposing is Anhatomirim, in which US$ 1 million was invested for restoration.
					</p>
					<p>
						Various other monuments are incorporated to the history of Florian�polis. One of the principal ones is the Hercilio Luz Bridge. For decades it was considered the most important landmark of the city. It is now closed to traffic because its structure is damaged. With 819 meters in length, it was inaugurated in 1926, with two towers 75 meters high. It is a rare example of a suspension bridge.
					</p>
					<p>
						Another place that deserves a visit is the Metropolitan Cathedral, located at the most central portion of the city and in the same location where in 1675 Francisco Dias Velho built a chapel in homage of Nossa Senhora do Desterro. He was killed there in a pirate attack. It was later substituted by a small church that was transformed into the cathedral. A few meters away is the Cruz e Sousa Palace, built in the eighteenth century and initially used as a residence and workplace of the Provincial President, it was home to the governors of Santa Catarina until the first half of this century. Today it houses the Santa Catarina Historic Museum.
					</p>
					<p>
						The Alf�ndega or Customs House building is another fine example of Florian�polis history. It was built between 1875 and 1876, with neo-classical architecture, and is rich in details. Nearby Alf�ndega is the most traditional commercial center of the city - the Public Market, with 140 stands housing establishments that sell typical local products (fish, meat, seafood, bamboo furniture and clay dishes). Ten thousand people pass through the Market each day. The building was inaugurated in 1898. There are also various monuments of historic and architectural importance at the interior of the island, especially at Ribeir�o da Ilha and Lagoa da Concei��o.
					</p>
					<p>
						It is especially in the summer that the visitor to Florian�polis can see some of the most significant folkloric displays of the city, all with a strong Azorean and Spanish influence, such as the boi-de-mam�o dramatic dance.
					</p>
					<br><br>&nbsp;
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(9)
					nm_figuras( 1) = "Rua Felipe Schmidt"
					nm_figuras( 2) = "Rodoviaria"
					nm_figuras( 3) = "Ponte hercilio luz"
					nm_figuras( 4) = "Ponte hercilio luz2"
					nm_figuras( 5) = "Beiramar Norte 1"
					nm_figuras( 6) = "Veleiros da Ilha"
					nm_figuras( 7) = "Ciclovia"
					nm_figuras( 8) = "Beiramar Norte 2"

					redim alt_figuras(9)
					alt_figuras( 1) = "Felipe Schimidt Street"
					alt_figuras( 2) = "Bus Station"
					alt_figuras( 3) = "Herc�lio Luz Bridge"
					alt_figuras( 4) = "Herc�lio Luz Bridge"
					alt_figuras( 5) = "Beira Mar Norte Avenue"
					alt_figuras( 6) = "View from the Iatch Club"
					alt_figuras( 7) = "Beira Mar Norte Bicycle Lane"
					alt_figuras( 8) = "Beira Mar Norte Avenue"

					redim orientacao(9)
					orientacao( 1) = 1
					orientacao( 2) = 1
					orientacao( 3) = 1
					orientacao( 4) = 2
					orientacao( 5) = 1
					orientacao( 6) = 1
					orientacao( 7) = 1
					orientacao( 8) = 1
					
					for i=1 to 8
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="30" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>,<%=orientacao(i)%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>
		<%
	end if 

case 2 
	
	if id_wk = 1 then
		%>

		<hr id="hr1">

		<table border="0" id="tab2">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>ILHA DE SANTA CATARINA: UM MUNDO A SER DESCOBERTO</b>
						<br>
					</p>
					<p>
						Dos seus 1.045 km2, a Ilha de Santa Catarina reserva muito o que descobrir. A p� pelas trilhas de mata fechada, a cavalo, de barco at� comunidades isoladas, e pelo c�u, de asa-delta ou &quot;paraglider&quot;.A paisagem nada mon�tona revela uma diversidade que enobrece o territ�rio das 42 praias mais encantadoras da regi�o sul. Tem plan�cies, dunas, lagoas, mangues, montanhas, vegeta��o atl�ntica nativa. Al�m disso, na sua vizinhan�a, existem outras 30 pequenas ilhas paradis�acas.
					</p>
					<p>
						Florian�polis, que ocupa toda a Ilha de Santa Catarina e mais um trecho do continente, guarda at� hoje tra�os dos colonizadores do s�culo XVIII. Na arquitetura , na pesca, na agricultura, na gastronomia e at� no jeit�o do seu povo. � uma terra desenhada para exploradores dos mais variados interesses. 
					</p>
					<p>
						As trilhas mais selvagens est�o no sul da ilha. As tr�s que levam � praia do Saquinho, partindo da praia da Solid�o, da Caieira da Barra do Sul ou de Naufragados, s�o �ngremes e exigem um certo f�lego, mas pagam o esfor�o ao revelar um peda�o imaculado da Mata Atl�ntica ou, para quem sai da praia da Solid�o, uma costa magnificamente recortada e enfeitada de gaivotas. No Saquinho, a praia em formato de concha � pouco habitada, uma casa aqui e outra acol�.
					</p>
					<p>
						Uma das mais espetaculares praias desertas da ilha, que os &quot;habitu�s&quot; fazem quest�o de manter no anonimato, � a Lagoinha do Leste. Suas �guas s�o um pouco agitadas e boas para o surf, sua areia � fina e fofa, e tem pouco mais de um quil�metro de extens�o. � cercada por um morro e h� uma lagoa perto da praia, com vegeta��o farta, em cujas margens os campistas costumam armar suas barracas. Pode-se chegar � Lagoinha do Leste a partir do P�ntano do Sul, depois de uma escalada de pouco mais de uma hora, ou pela trilha que sai da praia da Arma��o percorrendo o que os ilh�us chamam de &quot;cost�o&quot;, a costa irregular formada por grandes pedras. 
					</p>
					<p>
						Entre Arma��o e Lagoinha fica a mi�da praia de Matadeiro, com apenas 200 metros, que tem ondas melhores para o surf do que a agitada Joaquina, segundo muitos surfistas. Mas as trilhas nativas n�o revelam apenas praias. A que sai um pouco ao sul do centrinho do Ribeir�o da Ilha, passa pela Lagoa do Peri e termina na praia da Arma��o, oculta numa de suas vertentes o �ltimo engenho de cangalha ativo de Florian�polis. Por uma estradinha intercalada por meia d�zia de porteiras chega-se ao Engenho do Chico, na regi�o curiosamente denominada de Sert�o do Ribeir�o. As nascentes brotam por toda parte, n�o faltam pessegueiros e o pasto para o gado � de um verde que impressiona. Enfim, nada se parece com o sert�o de verdade. H� o engenho da farinha movido a boi, que segundo a sabedoria dos antigos m�i durante os meses que n�o t�m a letra &quot;r&quot; de maio a agosto . E h� o alambique, que produz por semana 500 litros da melhor cacha�a da Ilha. 
					</p>
					<p>
						Seguindo a mesma trilha, a 8 km do Engenho do Chico, est� o Parque Municipal da Lagoa do Peri, a maior reserva de �gua doce da Ilha de Santa Catarina, com 5 km2. O lugar � excelente para pescas e caminhadas. Caminhar nas trilhas antigas exige cuidados e muita aten��o. Apesar do esfor�o, s�o garantidos o suor e o prazer dos aventureiros.
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(6)
					nm_figuras( 1) = "BarcosLagoadaConceicao"
					nm_figuras( 2) = "LagoadaConceicao"
					nm_figuras( 3) = "LagoadaConceicao1"
					nm_figuras( 4) = "LagoinhadoLeste"
					nm_figuras( 5) = "LagoinhadoLeste1"

					redim alt_figuras(11)
					alt_figuras( 1) = "Barcos na Lagoa da Concei��o"
					alt_figuras( 2) = "Lagoa da Concei��o"
					alt_figuras( 3) = "Lagoa da Concei��o"
					alt_figuras( 4) = "Lagoinha do Leste"
					alt_figuras( 5) = "Lagoinha do Leste"
					
					redim orientacao(11)
					orientacao( 1) = 1
					orientacao( 2) = 1
					orientacao( 3) = 1
					orientacao( 4) = 1
					orientacao( 5) = 1
					
					for i=1 to 5
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="40" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>,<%=orientacao(i)%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

	<% 
	else
	%>

		<hr id="hr1">

		<table border="0" id="tab2">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>SANTA CATARINA ISLAND: A WORD TO BE DISCOVERED</b>
						<br>
					</p>
					<p>
						Among its 1,045 km2, Santa Catarina Island has a lot to be explored. By foot there are trails through dense forest, by horse, or boat one reaches isolated communities. Or one can tour through the sky, by hang-glider or &quot;para-glider&quot;. The never monotonous landscape reveals a diversity that ennobles the territory with the 42 most enchanting beaches of southern Brazil. There are plains, dunes, lagoons, mangrove swamps, mountains and native Atlantic Forest. Nearby there are 30 other small paradise islands.
					</p>
					<p>
						Florian�polis, which occupies all of Santa Catarina plus part of the mainland, still preserves traces of the eighteenth century colonizers. In the architecture, the fishing, the agriculture, the cooking and even in the manner of the people. It is a land designed for explorers with the most different interests. 
					</p>
					<p>
						The most wild trails are on the southern portion of the island. The three that lead to Saquinho beach, leaving from Solid�o beach, from Caieira da Barra do Sul or from Naufragados; are steep and demand stamina, but compensate the effort by revealing an immaculate portion of Atlantic forest - or for those who leave from Solid�o beach, a magnificent coast decorated by seagulls. Saquinho beach, which is shaped like a conch is inhabited by just a house or two.
					</p>
					<p>
						One of the most spectacular deserted beaches on the island, which the &quot;regulars&quot; make a point of keeping anonymous, is Lagoinha do Leste. Its waters have good waves for surfing. The sand is fine and soft. It is a bit more than a kilometer long and circled by a mountain. There is a lagoon close to the beach and rich vegetation. Campers mount their tents along the margin. Lagoinha do Leste can be reached from P�ntano do Sul, after a walk of a just over an hour, or by the trail that leaves from Arma��o Beach walking along what the locals call the &quot;cost�o &quot;, the jagged coast formed by large rocks. 
					</p>
					<p>
						Between Arma��o and Lagoinha there is the tiny beach of Matadeiro, only 200 meters long, which has better waves than the more crowded Joaquina, according to many surfers. But the native trails reveal not only beaches. A trip departing from just south of the center of Ribeir�o da Ilha, passing by Lagoa do Peri and ending at Arma��o beach, hides on one of the slopes the last working ox-driven mill of Florian�polis. Over a route sprinkled by half a dozen gates one reaches the Engenho do Chico [Chico's Mill], in a region curiously known as Sert�o do Ribeir�o. There are lots of streams, an abundance of peach trees and the pasture is so green it stings the eyes. In sum, the region is nothing like the true desert-like &quot;Sert�o&quot; region of the Brazilian northeast. There is an ox-driven flour mill, which according to the old-timers grinds during the months that do not have the letter &quot;r&quot; from May to June. And there is a still which makes 500 liters of the island's best cacha�a (sugar-cane whiskey) per week. 
					</p>
					<p>
						Following the same trail, the 8 km from the Engenho do Chico, [Chico's Mill] there is the Peri Lagoon Municipal Park, the largest fresh water reserve on Santa Catarina Island, 5 km2 in size. It is an excellent place for fishing and hiking. To walk on the old trails demands care and attention. Despite the effort, the sweat and pleasure of adventure is guaranteed.
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(6)
					nm_figuras( 1) = "BarcosLagoadaConceicao"
					nm_figuras( 2) = "LagoadaConceicao"
					nm_figuras( 3) = "LagoadaConceicao1"
					nm_figuras( 4) = "LagoinhadoLeste"
					nm_figuras( 5) = "LagoinhadoLeste1"

					redim alt_figuras(11)
					alt_figuras( 1) = "Ships - Lagoa da Concei��o"
					alt_figuras( 2) = "Lagoa da Concei��o Lagoon"
					alt_figuras( 3) = "Lagoa da Concei��o Lagoon"
					alt_figuras( 4) = "East Lagoon"
					alt_figuras( 5) = "East Lagoon"
					
					redim orientacao(11)
					orientacao( 1) = 1
					orientacao( 2) = 1
					orientacao( 3) = 1
					orientacao( 4) = 1
					orientacao( 5) = 1
					
					for i=1 to 5
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="40" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>,<%=orientacao(i)%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<%
	end if 
case 3 
	
	if id_wk = 1 then
		%>

		<hr id="hr1">

		<table border="0" id="tab3">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>HIST�RICO DA OCUPA��O HUMANA DA ILHA DE SANTA CATARINA</b>
						<br>
					</p>
					<p>
						A constata��o de presen�a humana na Ilha de Santa Catarina pode ser considerada recente quando comparada � antiga civiliza��o amer�ndia, que t�m ossadas datando de 30 mil anos, aproximadamente. Assim, como em todo litoral catarinense, os vest�gios humanos mais antigos encontrados e catalogados na Ilha remontam para 5 mil anos de ocupa��o, sempre diretamente ligados com a cultura de sambaquis.
					</p>
					<p>
						Tamb�m conhecido como casqueiro, concheiro e berbigueiro, entre outros nomes, trata-se de um s�tio arqueol�gico que em sua origem guarani significa monte de conchas. Durante centenas ou milhares de anos os primitivos habitantes, que eram naturalmente dependentes da coleta de frutos do mar, iam acumulando em locais apropriados, os restos e cascas de moluscos. Estes montes cresciam tanto a cada gera��o que passaram a ser um local bastante apropriado para suas habita��es. Al�m da proximidade do mar, os sambaquis eram locais secos e seguros.
					</p>
					<p>
						At� o ano de 1989 haviam sido registrados 120 sambaquis s� na Ilha de Santa Catarina em pesquisas arqueol�gicas realizadas em 20% dos s�tios existentes. Por�m sabe-se que muitos deles foram destru�dos pela ocupa��o hist�rica que se sucedeu. Transformaram-se em mat�ria-prima para o fabrico de cal. Suspeita-se que nas partes mais elevadas dos sambaquis tenha havido a presen�a de um novo grupo humano, os itarar�s, que apresentavam h�bitos diferentes dos primeiros habitantes dos sambaquis, revelados em vest�gios cer�micos e uma suposta pr�tica agr�cola. Houve, neste segundo grupo que ocupou a Ilha, uma sens�vel diminui��o no consumo de moluscos em sua dieta alimentar. Sup�e-se tamb�m que estes povos n�o tiveram contato entre si, habitando a Ilha em momentos que se sucederam historicamente.
					</p>
					<p>
						O terceiro grupo ind�gena que migrou para a Ilha, no s�culo XIV, foi o dos carij�s, da fam�lia lingu�stica tupi-guarani, tradicional no litoral sul do Brasil. Instalaram-se aproximadamente dois s�culos antes da chegada dos primeiros europeus. Suas aldeias variaram de 30 a 80 habita��es, o que imprimiu uma maior e mais densa ocupa��o efetiva da Ilha de Santa Catarina. Os primeiros contatos entre os carij�s e estrangeiros ocorreu por volta de 1556, quando expedi��es deixavam na Ilha n�ufragos ou desertores. Desde o princ�pio, este conv�vio deu-se de forma pac�fica. Por um lado, os amistosos carij�s abasteciam os visitantes espanh�is de alimentos e forneciam seguras informa��es acerca de caminhos que os levassem a rios como Igua�� e Itapoc�, para alcan�arem o Paraguai; por outro, os estrangeiros regalavam os �ndios com muitos presentes.
					</p>
					<p>
						Entretanto, esse conv�vio durou menos de um s�culo pois tornou-se prejudicial a cultura dos carij�s, que migravam continente adentro.
					</p>
					<p>
						Suspeita-se que j� em 1600 n�o haviam tribos na Ilha de Santa Catarina. A fuga dos carij�s do lugar onde estavam e que chamavam Meiembipe, que traduzido do tupi significa &quot;montanha ao longo do canal&quot;, n�o implica em rompimento mas simplesmente como uma a��o defensiva. Os nativos passaram a sentir-se muito vulner�veis na medida em que as necessidades dos visitantes, cada vez mais numerosos, exigiam maior produ��o de alimentos, o que alterava profundamente seus h�bitos, cren�as e at� mesmo a liberdade f�sica.
					</p>
					<p>
						At� meados do s�culo XVIII, embora o Tratado de Tordesilhas, em 1494, garantisse legalmente a posse das terras da Ilha de Santa Catarina � coroa portuguesa, Portugal n�o havia tomado nenhuma medida efetiva de povoamento. Percebia-se apenas a a��o espanhola. Depois de algumas ocupa��es inst�veis e de v�rias disputas pela posse das terras, vai caber a Francisco Dias Velho, nobre de S�o Vicente, fixar-se na Ilha. � prov�vel que o assentamento tenha se dado por volta de 1675, uma vez que tr�s anos ap�s essa data Dias Velho ir� requerer do governo da capitania paulista duas l�guas em quadro na Ilha, justificando ter instalado uma igreja em devo��o a Nossa Senhora do Desterro, benfeitorias e culturas na referida �rea. 
					</p>
					<p>
						Cresce a import�ncia estrat�gica da Ilha de Santa Catarina, exigindo que fosse melhor defendida e ocupada de modo mais contundente. Desta forma, em 1726, Desterro � elevada � categoria de vila e iniciam-se projetos militares de fortifica��es na Ilha, estabelecendo-se a coloniza��o definitiva, feita por a�orianos.
					</p>
					<p>
						No dia 6 de janeiro de 1748 desembarcaram na Ilha de Santa Catarina 461 pessoas provenientes da ilha de A�ores. At� 1756, cerca de 6 mil a�orianos e madeirenses j� estavam instalados na Ilha, mesmo que precariamente. Foram distribu�das �s fam�lias colonizadoras sementes, armas e ferramentas, cavalos e touros para o arado, o que dificultou ainda mais a adapta��o dos colonos. Al�m disso sua cultura de trigo n�o se adequou ao clima da regi�o, o que os obrigou a produzir culturas herdadas dos �ndios.
					</p>
					<p>
						Assim, a mandioca tornou-se a base da alimenta��o a�oriana e em pouco tempo (cerca de 30 anos) j� existiam 300 pequenos engenhos, sendo alguns de cana-de-a��car). Muitos colonos abandonaram suas terras e passaram a se dedicar a of�cios urbanos e � pesca artesanal. Outros tantos eram recrutados para treinamentos militares, o que de certa forma serviu de entrave e comprometeu o desenvolvimento da pequena produ��o mercantil na promissora Desterro. Na virada do s�culo XVIII para o s�culo XIX j� se podia perceber um in�cio de �xodo rural motivado pelo recrutamento, of�cios da pesca e trabalhos urbanos que, al�m de aumentar o n�cleo da cidade permitia a forma��o dos primeiros latif�ndios.
					</p>
					<p>
						<b>DE DESTERRO A FLORIAN�POLIS</b>
						<br>
					</p>
					<p>
						Numa homenagem prestada pelo ent�o governador Herc�lio Pedro da Luz ao marechal Floriano Peixoto, largamente contestada por muitos catarinenses e conhecedores de sua hist�ria, Desterro teve seu nome substitu�do, em 10 de outubro de 1894, por Florian�polis. Com fortalecimento de uma emergente burguesia comercial, a administra��o da cidade saiu do comando de militares e passou �s m�os dos comerciantes, que acumularam capital intermediando as exporta��es locais e escoando produtos no principal porto de Santa Catarina, que nesta �poca ainda existia na Ilha.
					</p>
					<p>
						Devido a crescente e not�vel concentra��o populacional propiciada pelo com�rcio, a capital catarinense entra no s�culo XX com caracter�sticas bem peculiares aos grandes centros urbanos.
					</p>
					<p>
						O per�metro urbano sofreu grandes agress�es, aumentando tamb�m seus problemas. Em 1926 foi inaugurada a primeira das tr�s pontes que viriam a ligar a Ilha ao Continente, a ponte Herc�lio Luz. Isto representou um marco consider�vel para o circuito econ�mico, pois imprimiu � cidade situa��o privilegiada de p�lo regional em rela��o ao interior do Estado, deflagrando um franco processo de moderniza��o.
					</p>
					<p>
						Com a Revolu��o de 30 houve ainda mais incremento ao com�rcio, agora estabelecido por meio de rodovias, haja vista a plena decad�ncia do antigo porto de Desterro. Entretanto, as principais transforma��es urbanas na cidade de Florian�polis vieram a acontecer com mais intensidade a partir da segunda metade deste s�culo. No final da d�cada de 50 foi implantada a Universidade Federal de Santa Catarina que aliada � cria��o da Eletrosul e de v�rias outras empresas estatais, atraiu um grande fluxo migrat�rio, n�o s� do interior como de outros Estados. Multiplicaram-se as �reas loteadas, os bairros residenciais, os pr�dios de apartamentos, as empresas e o com�rcio.
					</p>
					<p>
						Paralelamente ao crescimento das classes m�dias (que deu-se gra�as ao crescimento do setor p�blico devido � inje��o de recursos federais e estaduais), houve tamb�m a invas�o de grande n�mero de migrantes pobres expropriados do campo e evadidos de outras cidades, que atra�dos pelas possibilidades promissoras de emprego que passaram a existir em Florian�polis acabaram por multiplicar as �reas de periferia urbana.
					</p>
					<p>
						Cercada por �guas cristalinas, com praias de �guas claras e agrad�vel clima, Florian�polis fica localizada a mais ou menos um quil�metro do continente, no ponto mais pr�ximo.Com suas condi��es clim�ticas e geogr�ficas favor�veis, � natural que seus habitantes dediquem-se as atividades n�uticas, como pesca esportiva, pesca submarina, vela, surf e remo. Com um calend�rio ativo durante todo ano, al�m da Fenaostra (Festa Nacional da Ostra) e da Festa da Tainha, onde h� diferentes pratos deste peixe, destacam-se a festa do marisco e o carnaval, com barcos fantasiados e muita alegria.
					</p>
					<p>
						Na Ilha encontram-se praias para todos os gostos, desde as mais movimentadas, com hot�is internacionais, boa infra-estrutura e vida noturna, �s virgens, onde o meio ambiente � totalmente preservado.
					</p>
					<p>
						Chamada pelos �ndios guaranis &quot;jurer� mirim&quot; (pequena &quot;boca d&#146;�gua&quot; pelo estreito que a separa do continente), a Ilha de Santa Catarina � conhecida desde o s�culo XVI, descoberta pelos veleiros que procuravam o caminho das �ndias. Quando os portugueses decidiram colonizar a costa sul brasileira, v�rias vilas foram instaladas, entre elas, Nossa Senhora do Desterro, hoje Florian�polis. Ap�s v�rios ataques de cors�rios, as pequenas vilas quase desapareceram.
					</p>
					<p>
						Nessa �poca, o governo portugu�s resolveu facilitar a vinda de fam�lias das ilhas dos A�ores para repovoar a costa e preservar a coloniza��o. Tamb�m para garantir a posse da llha, durante a coloniza��o os portugueses constru�ram v�rias fortifica��es. Entre elas destaca-se Anhatomirim, a maior e melhor preservada. Foi constru�da em uma pequena ilha, � 5 milhas da cidade, em 1744.
					</p>
					<p>
						Hoje faz parte do patrim�nio hist�rico de Santa Catarina. No local encontra-se um centro de pesquisa marinha, um museu e um aqu�rio mar�timo. 
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(9)
					nm_figuras( 1) = "casario"
					nm_figuras( 2) = "IgrejadoRibeiraodailha"
					nm_figuras( 3) = "MercadoPublico"
					nm_figuras( 4) = "MercadoPublicoTorres"
					nm_figuras( 5) = "MercadoPublicoVaoCentral"
					nm_figuras( 6) = "PalacioCruzeSousa"
					nm_figuras( 7) = "Catedral Metropolitana"
					nm_figuras( 8) = "Altar lateral"

					redim alt_figuras(9)
					alt_figuras( 1) = "Casario Restaurado"
					alt_figuras( 2) = "Igreja do Ribeir�o da Ilha"
					alt_figuras( 3) = "Mercado P�blico"
					alt_figuras( 4) = "Torres do Mercado P�blico"
					alt_figuras( 5) = "V�o Central do Mercado P�blico"
					alt_figuras( 6) = "Pal�cio Cruz e Sousa"
					alt_figuras( 7) = "Catedral Metropolitana"
					alt_figuras( 8) = "Altar Lateral da Catedral Metropolitana"
					
					redim orientacao(9)
					orientacao( 1) = 1
					orientacao( 2) = 2
					orientacao( 3) = 1
					orientacao( 4) = 1
					orientacao( 5) = 1
					orientacao( 6) = 1
					orientacao( 7) = 2
					orientacao( 8) = 2
					
					for i=1 to 8
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="51" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, <%=orientacao(i)%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<% 
	else
		%>

		<hr id="hr1">

		<table border="0" id="tab3">
			<tr>
				<td class="textoComum" width="436" valign="top">

					<p>
						<b>HUMAN OCCUPATION SANTA CATARINA ISLAND</b>
						<br>
					</p>
					<p>
						Evidence of human presence on Santa Catarina Island can be considered recent when compared to ancient Amerindian Civilization which has remains dating back some 30 thousand years. As along the entire coast of Santa Catarina state, the oldest human vestiges found and cataloged on the island date back 5 thousand years of occupation always directly linked to the sambaqui culture.
					</p>
					<p>
						Also known as casqueiro, concheiro and berbigueiro, or by other names, this is an archeological site that in its Guarini origin means a mountain of shells. For hundreds or thousands of years the primitive inhabitants who were naturally dependent on the collection of sea food, would accumulate in appropriate locations the remains and shells of mollusks. These mounds grew so much with each generation that they came to be a locationte suitable for residences. In addition to being near the sea, the sambaquis were dry and safe places.
					</p>
					<p>
						By 1989, 120 sambaquis had been registered on Santa Catarina island and archeological research conducted on 20% of the sites. Many of the shell mounds were destroyed by later occupation, often transformed into raw material for making lime. It is suspected that in the highest portions of the sambaquis another new human group was present, the Itarar�s, who had habits different from the first residents of the sambaquis. The ceramic remains indicate that they probably had an agricultural culture. With this second group that occupied the island, there was a noticeable decrease in the consumption of mollusks. It is also believed that these peoples did not have contact with each other, inhabiting the island at different moments, one after the other.
					</p>
					<p>
						The third indigenous group that migrated to the island in the fourteenth century were the Carij�s, from the traditional Tupi-Guarani linguistic family from Brazil's southern coast. They settled approximately two centuries before the arrival of the first Europeans. Their villages varied from 30 to 80 residences, which left a larger and denser occupation of Santa Catarina Island. The first contacts with the Carij�s and foreigners occurred around 1556 when expeditions left on the island people who were shipwrecked or deserters. From the beginning, this co-existence was peaceful. On one hand, the friendly Carij�s supplied the Spanish visitors foods and sound information about the routes that led to rivers such as the Igua�� and Itapoc�, to reach Paraguay. On the other hand, the strangers regaled the Indians with many presents. Nevertheless, this coexistence lasted less than one century because it became damaging to the Carij�s culture, which migrated to the interior of the mainland.
					</p>
					<p>
						It is believed that by 1600 there were no more tribes on Santa Catarina Island. The flight of the Carij�s from the place where they lived, and called Meiembipe, which means &quot;mountain along the canal&quot; in Tupi, did not imply a break with the Portuguese, but simply a defensive action. The natives came to feel vulnerable to the degree in which the needs of the increasing number of visitors required greater food production which deeply changed the Carij�s' habits, believes and even their physical liberty. By the middle of the eighteenth century, although the Tordesilhas Treaty of 1494 legally guaranteed the possession of Santa Catarina Island to the Portuguese crown, Portugal had not taken any effective measure towards settlement. Only Spanish activity was noticed. After a few unstable occupations and various disputes over land possession, it would be left up to Francisco Dias Velho, a noble from S�o Vicente, to establish himself on the island. His settlement dates from about 1675, given that three years later Dias Velho requested from the governor of the Paulista captaincy, two square leagues on the island, explaining that a church had been established to worship Our Lady of Exile, and improvement and planting had begun in the region.
					</p>
					<p>
						The strategic importance of Santa Catarina Island grew, requiring that it be better defended and occupied more substantially. In this way, in 1726, Desterro was raised to the category of a village and military projects and fortifications were begun on the Island, establishing definitive colonization by Azoreans,
					</p>
					<p>
						On January 6, 1748, 461 people arrived on Santa Catarina Island from the Azores. By 1756, nearly six thousand Azoreans and Madeirenses were settled on the Island, if precariously. Seeds, weapons, tools, and horses and bulls for plowing were supplied to the settlers. Unfortunately their adaptation to the island was made more difficult because their traditional wheat culture was not suitable to the region, which required the Azoreans to produce crops inherited from the Indians.
					</p>
					<p>
						Thus, cassava became the basis of the diet and in a short period of about 30 years, there were about 300 small mills, some for sugar cane. Many settlers abandoned their lands and dedicated themselves to urban tasks and fishing. Others were recruited for military training, which in a certain way restrained and limited development of the small commercial production of the promising Desterro. At the turn of the nineteenth century, the beginning of a rural exodus was perceptible, motivated by military enlistment, fishing and urban jobs, which in addition to increasing the center of the city, allowed the formation of the first large estates.
					</p>
					<p>
						<b>FROM DESTERRO TO FLORIAN�POLIS</b>
						<br>
					</p>
					<p>
						In an homage from then governor Herc�lio Pedro da Luz to Marechal Floriano Peixoto, and a decision now widely contested by many Catarinenses and those aware of Peixoto's history, Desterro had its name changed, on October 10, 1894 to Florian�polis. Strengthened by an emerging commercial bourgeoisie, the administration of the city passed from the hands of the military to the hands of merchants, who accumulated capital by handling local exports and the flow of products at the principal port of Santa Catarina, which at that time was still on the Island.
					</p>
					<p>
						Due to the notable and growing concentration of population encouraged by commerce, the Santa Catarina capital entered the twentieth century with features very unique to large urban centers.
					</p>
					<p>
						The urban district suffered greatly, and its problems increased. In 1926 the first of three bridges that link the island to the mainland was inaugurated, and called the Herc�lio Luz bridge. This represented a considerable mark for economic activity, because it offered a privileged position to the city as a regional pole in relation to the interior of the State, and triggered a true modernization process.
					</p>
					<p>
						The Revolution of 1930 caused another boost to trade, now conducted by highway, and activity at the old port of Desterro declined. Nevertheless, the main urban transformations in the city of Florian�polis came to occur with greater intensity after the second half of this century. At the end of the 1950's the Federal University at Santa Catarina was established and in association with the creation of the Eletrosul utility and other state companies, the city attracted a large migratory flow, not only from the interior but also from other States. The sub-divisions and residential neighborhoods multiplied, as well as apartment buildings, companies and commerce.
					</p>
					<p>
						In parallel to the growth of the middle class (which took place thanks to the growth of the public sector due to the injection of federal and state resources), there was also an invasion of a large number of poor immigrants from the countryside and other cities, who were attracted by promising opportunities for employment. At the same time the number of favelas and precarious occupations of the urban periphery grew in Florian�polis. 
					</p>
					<p>
						Circled by crystal waters, beaches with clear waters and a pleasant climate, Florian�polis is about one kilometer from the continent at the closest point of contact. With its favorable geographic and climatic conditions, it is natural that its inhabitants dedicate themselves to nautical activities, such as sport fishing, underwater fishing, sailing, surfing and rowing. With many events year round, in addition to the Fenaostra (National Oyster Festival) and the Festa da Tainha, which offers different fish dishes, there is also a mussel festival and Carnival, with costumed floats and much joy.
					</p>
					<p>
						Beaches for all tastes are found on the island, from the busiest, with international hotels, good infrastructure and night life, to virgin beaches, where the environment is totally preserved.
					</p>
					<p>
						Called by the Guaraini Indians &quot;Jurer� Mirim&quot; (small &quot;mouth of water&quot;) because of the strait that separates it from the mainland) Santa Catarina Island has been known since the sixteenth century, discovered by sailors seeking a route to the Indies. When the Portuguese decided to colonize the southern Brazilian coast, various villas were established, among them, at that time, Nossa Senhora do Desterro [Our Lady of Exile[, which is now Florian�polis. After various attacks by pirates, the small villages nearly disappeared.
					</p>
					<p>
						At this time, the Portuguese government decided to facilitate the arrival of families from the Azorean Islands to populate the coast and consolidate the colonization process. To guarantee possession of the island, the Portuguese built various forts. Among these stand out Anhatomirim, the largest and best preserved. It was built on a small island, 5 miles from the city, in 1744.
					</p>
					<p>
						It is now part of the historic heritage of Santa Catarina. There is a marine research center at the site, a museum and a maritime aquarium.
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(9)
					nm_figuras( 1) = "casario"
					nm_figuras( 2) = "IgrejadoRibeiraodailha"
					nm_figuras( 3) = "MercadoPublico"
					nm_figuras( 4) = "MercadoPublicoTorres"
					nm_figuras( 5) = "MercadoPublicoVaoCentral"
					nm_figuras( 6) = "PalacioCruzeSousa"
					nm_figuras( 7) = "Catedral Metropolitana"
					nm_figuras( 8) = "Altar lateral"

					redim alt_figuras(9)
					alt_figuras( 1) = "Downtown old houses restored"
					alt_figuras( 2) = "Ribeir�o da Ilha Church"
					alt_figuras( 3) = "Public Market"
					alt_figuras( 4) = "Public Market Towers"
					alt_figuras( 5) = "Public Market Central Space"
					alt_figuras( 6) = "Cruz e Sousa Palace"
					alt_figuras( 7) = "Metropolitan Cathedral"
					alt_figuras( 8) = "Lateral Altar of the Metropolitan Cathedral"
					
					redim orientacao(9)
					orientacao( 1) = 1
					orientacao( 2) = 2
					orientacao( 3) = 1
					orientacao( 4) = 1
					orientacao( 5) = 1
					orientacao( 6) = 1
					orientacao( 7) = 2
					orientacao( 8) = 2

					for i=1 to 8
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="51" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, <%=orientacao(i)%>);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<%
	end if 
case 4 
	
	if id_wk = 1 then
		%>

		<hr id="hr1">

		<table border="0" id="tab4">
			<tr>
				<td class="textoComum" width="436">
					<p>
						<b>CADA PRAIA TEM SUA PR�PRIA CARACTER�STICA</b>
						<br>
					</p>
					<p>
						Os munic�pios lim�trofes � Ilha de Santa Catarina &#150; Governador Celso Ramos, ao norte; Palho�a, ao sul; e S�o Jos� e Bigua�u, a oeste - tem em comum a coloniza��o a�oriana e o fato de sediarem ind�strias da regi�o metropolitana. No quesito praias, as grandes concorrentes de Florian�polis s�o Palho�a e Governador Celso Ramos.
					</p>
					<p>
						Em Palho�a, Praia de Fora, Enseada do Brito, Praia do Sonho e Pinheira s�o comunidades pesqueiras bastante procuradas pelos turistas. Pedras Altas � a praia oficial do naturismo no munic�pio, e a Guarda do Emba� � reconhecida como uma das melhores para a pratica do surf. Governador Celso Ramos � mais conhecida pela pol�mica farra do boi do que por sua bela costa, composta por enseadas, ba�as e pen�nsulas. O munic�pio � tamb�m o maior produtor de mariscos cultivados do Brasil. Al�m das praias de Palmas, Praia Grande, Arma��o, e Costeira, pertence a seu territ�rio a ilha de Anhatomirim e a Ba�a dos Golfinhos. Ainda pouco explorado turisticamente e sem muitas op��es de lazer al�m das praias, Governador Celso Ramos � um passeio que vale pela natureza bastante preservada.
					</p>
					<p>
						No centro de todas as atra��es est� a Ilha de Santa Catarina, com seu misto de plan�cies, dunas, lagoas, mangues e montanhas com vegeta��o atl�ntica que se evidenciam de variadas formas. Al�m disso, na sua vizinhan�a, existem outras 30 pequenas ilhas, dentre elas as que sediam as fortalezas de Anhatomirim e Ratones Grande, al�m de Ratones Pequena, do Franc�s, Guar�s Grande, Diamante e dos Noivos, todas na ba�a norte. Na baia sul encontra-se a das Vinhas, Laranjeiras, Maria Francisca, do Largo, do Andrade e ilha dos Carlos. Ao norte, sobressaem-se as ilhas Deserta e a do Arvoredo, que constitui uma reserva biol�gica marinha. A leste localizam-se as ilhas de Moleques do Norte, Mata-Fome, Badejo, das Aranhas, do Xavier e Campeche. Nesta �ltima, a natureza exuberante e os vest�gios de uma antiga civiliza��o pr�-hist�rica s�o preservados. Com 1.600 metros de extens�o, a ilha � um passeio imperd�vel. Com ajuda de um bom guia d� para percorr�-la num dia. Nela � poss�vel ver inscri��es rupestres, visitar a caverna dos morcegos- que corta a ilha numa extens�o de 30 metros - fazer treking e ainda mergulhar ou aproveitar sua linda praia.
					</p>
					<p>
						Ao sul localizam-se as ilhas Tr�s Irm�s, Moleques do Sul, do Coral, Ara�atuba e dos Papagaios. Na costa norte da Ilha de Santa Catarina ficam as praias com maior infra-estrutura tur�stica, �guas calmas e mornas, ideais para a fam�lias com crian�as, com exce��o da praia Brava. 
					</p>
					<p>
						<b>COSTA NORTE</b>
						<br>
					</p>
					<p>
						<b>Santinho</b> - De mar agitado, igualmente bom para o surf, fica a 40 km do centro e possui uma extens�o de 2,5 km de praia. Uma de suas atra��es s�o os petr�glifos junto ao cost�o. S�o inscri��es feitas nas pedras h� mais de 5 mil anos pelos �ndios carij�s. A faixa de areia clara e macia � bastante larga. Com suas dunas, formam um ecossistema de rara beleza.
					</p>
					<p>
						<b>Ingleses</b> - A praia, a 36 quil�metros do centro, � a que fica mais ao norte da Ilha de Santa Catarina. Seu nome prov�m de um navio ingl�s que naufragou ali em 1700. � uma das que tem infra-estrutura tur�stica mais completa. Apesar de n�o ter uma �gua t�o quente, proporciona bons momentos aos banhistas que buscam lazer. Tem aproximadamente 5 quil�metros de extens�o e � uma das mais solicitadas no ver�o.
					</p>
					<p>
						<b>Brava</b> - O mar agitado e frio, geralmente com ondas fortes, � �timo para o surf e proporciona excelentes banhos. Distante 38 quil�metros do centro, a Brava fica atr�s de um morro que permite uma vis�o magn�fica de seu alto, antes da chegada � praia. Sua extens�o � de 1,7 quil�metros de areia macia. A faixa de areia inclinada faz com que o mar tenha uma profundidade consider�vel logo na beira. As op��es de com�rcio nesta praia, que foi recentemente urbanizada com a constru��o de um imponente conjunto de pr�dios em estilo mediterr�neo, vem aumentando ano a ano.
					</p>
					<p>
						<b>Lagoinha</b> - De mar calmo e �guas muito frias, a Lagoinha est� localizada a 37 quil�metros do centro. Sua faixa de areia � de 900 metros e vem recebendo um p�blico cada vez maior nos �ltimos anos. Seu acesso ocorre antes da entrada de Ponta das Canas. � um dos recantos mais agrad�veis da Ilha de Santa Catarina. � considerada uma �tima op��o para fam�lias com crian�as.
					</p>
					<p>
						<b>Ponta das Canas</b> - Segue a tend�ncia natural do norte da Ilha que, historicamente, em fun��o da necessidade de defesa contra os ataques espanh�is, foi ocupado e urbanizado em escala muito maior do que o sul. Distante 34 quil�metros do centro, se caracteriza por apresentar �guas mansas e geralmente quentes. Sua extens�o � de 1,9 quil�metro de areia clara e fina. Boa op��o para a pr�tica de esportes n�uticos, � um dos balne�rios mais procurados por turistas argentinos e uruguaios.
					</p>
					<p>
						<b>Cachoeira do Bom Jesus</b> - Localizada entre Ponta das Canas e Canasvieiras, esta praia de mar calmo e quase 3 quil�metros de extens�o de areia branca, situa-se a 27 quil�metros do centro. � um dos balne�rios onde se encontram belas resid�ncias e uma rede de servi�os bem estruturada. 
					</p>
					<p>
						<b>Canasvieiras</b> - Principal reduto dos argentinos em Florian�polis, a praia de Canasvieiras � a mais cosmopolita dentre todas as da Ilha de Santa Catarina a ponto de ter &quot;portunhol&quot; como l�ngua oficial durante os meses de ver�o. � um dos pontos mais agitados da Ilha, tanto durante o dia como � noite. S�o bares, restaurantes, hot�is, boates, fliperamas, campings. Enfim, toda a estrutura necess�ria a uma estada confort�vel e repleta de op��es. Tamb�m s�o oferecidos equipamentos para a pr�tica de diversos esportes e h� uma grande quantidade de boas quadras de t�nis nos hot�is , clubes e pousadas. Com uma extens�o de 3 quil�metros, Canasvieiras possui �guas extremamente agrad�veis e quentes, al�m de calmas. No balne�rio pode-se alugar baleeiras que, conforme as condi��es do vento, demoram 40 minutos para chegar at� a ilha de Anhatomirim. Mais perto da praia est� a ilha do Franc�s, quase toda destinada ao uso de seu �nico propriet�rio. Outras op��es nesta praia, distante 27 quil�metros do centro, s�o os passeios de escuna e o aluguel de pedalinhos.
					</p>
					<p>
						<b>Daniela</b> - Com uma extens�o de 3 quil�metros e distante 26 quil�metros do centro, esta praia � ocupada essencialmente por casas de veraneio. De �guas claras e mar calmo, � muito procurada por fam�lias com crian�as. N�o oferece muitas op��es em servi�os.
					</p>
					<p>
						<b>COSTA OESTE</b>
						<br>
					</p>
					<p>
						A Costa Oeste da Ilha de Santa Catarina, denominada Mar de Dentro, fica de frente para o continente, dividido entre as ba�as norte e sul. Na ba�a norte encontram-se as praias de Saco Grande, Santo Ant�nio de Lisboa e Sambaqui, formadas por comunidades localizadas em enseadas de �guas abrigadas que conservam caracter�sticas dos imigrantes a�orianos. 
					</p>
					<p>
						<b>Sambaqui</b> &#150; Pertencente ao distrito de Santo Ant�nio de Lisboa, Sambaqui � uma op��o para quem procura tranq�ilidade numa das �reas mais agrad�veis para o lazer e o descanso. Distante 17 km do centro, sua praia tem 1,10 km de extens�o. O nome Sambaqui teve origem no amontoado de conchas dispersas em suas areias, chamando a aten��o dos pesquisadores por revelar, de tempos em tempos, algum animal ou vegetal fossilizado, al�m de resqu�cios de comunidades ind�genas que habitavam a regi�o. Suas �guas claras s�o calmas, quase paradas. A linha de praia apresenta um relevo formado por cost�es. Local da Ilha onde ainda sobrevive a dan�a folcl�rica do boi-de- mam�o, Sambaqui tem v�rios bares na praia e bons restaurantes.
					</p>
					<p>
						<b>Santo Ant�nio de Lisboa</b> - Freguesia mais antiga da Ilha, Santo Ant�nio � um importante reduto da cultura a�oriana. Sua praia tem �guas calmas e quentes. � um local distante 13 quil�metros do centro, not�vel por sua extrema beleza e tranq�ilidade, com praia de areias grossas. Em substitui��o � pesca, uma das atividades que vem ganhando cada vez mais espa�o � o cultivo de ostras e mariscos. Outro recanto ideal para quem busca paz � o vizinho Cacup�. Tem uma extens�o de 900 metros. Al�m de v�rios restaurantes, � uma �tima op��o para um piquenique ou passeio familiar. 
					</p>
					<p>
						<b>SUL DA ILHA</b>
						<br>
					</p>
					<p>
						<b>Tapera</b> - Localizada na costa sul da Ilha, nas proximidades do aeroporto, fica � 29 km do centro e possui �guas limpas e paradas. Em seu 1,5 km de extens�o a areia � branca, dura e grossa. A praia tem in�meros bares e � uma op��o de passeio.
					</p>
					<p>
						<b>Ribeir�o da Ilha</b> - Localizado a 36 km do centro, o distrito do Ribeir�o da Ilha � um dos mais antigos povoados de Florian�polis. Registros indicam que em 1526 o espanhol Sebasti�o Caboto aportou ali e alguns de seus comandados se juntaram aos n�ufragos de uma expedi��o anterior, de Dias de Solis, de 1515. O Ribeir�o � um composto de v�rias pequenas praias de �guas calmas e areia grossa. � um dos locais mais t�picos da Ilha, como o casario a�oriano, a Igreja Nossa Senhora da Lapa do Ribeir�o e o Museu Etnol�gico, com acervo de pe�as raras que retratam a coloniza��o a�oriana.
					</p>
					<p>
						<b>COSTA LESTE</b>
						<br>
					</p>
					<p>
						Na Costa Leste situam-se as praias voltadas para o mar aberto, de frente para o Oceano Atl�ntico, assentadas acima do cord�o arenoso. Boas para o surf, suas �guas s�o limpas e frias.
					</p>
					<p>
						<b>Joaquina</b> - Mundialmente famosa por suas excelentes condi��es para a pr�tica do surf e pela infinidade de competi��es deste esporte nela sediadas, as dunas da Joaquina oferecem condi��es para uma nova modalidade de esporte &#150; o sandbord. Seu nome origina de uma lenda na qual uma velha rendeira, de nome Joaquina, estava em suas areias realizando seu of�cio quando foi tragada por uma enorme onda. Praia de mar agitado, distante 17 km do centro, � chamada de &quot;Joaca&quot; pelos surfistas. Com extens�o de 3,5 km, � uma das que oferece melhor infra-estrutura aos visitantes, com hot�is, restaurantes, bares, terminal tur�stico com posto policial e de salva- vidas, chuveiros p�blicos e sanit�rios. A pr�tica da nata��o exige aten��o devido as grandes ondas e correntes. Invariavelmente a Joaquina est� repleta de surfistas e bodyboarders com suas roupas coloridas. O futebol de areia e o v�lei de praia s�o constantemente praticados ali. Dotada de um amplo estacionamento pago, tamb�m possui sorveterias e lojas de artesanato. Um eficiente sistema de ilumina��o permite o surf e a pr�tica de outros esportes � noite.
					</p>
					<p>
						<b>Mole</b> - �tima op��o para o surf, a Mole n�o � recomendada para crian�as. H� buracos logo na beira. Localizada � 15 km do centro, entre a Lagoa da Concei��o e a Barra da lagoa, tem tal nome por causa da maciez de suas areias. Tamb�m � dotada de estacionamento p�blico, chuveiros, sanit�rios e posto de salva-vidas. H� bares que servem frutos do mar e lanches. Com suas �guas sempre frias, � o &quot;point&quot; da moda na cidade. Freq�entada essencialmente por jovens, agita-se nas noites de ver�o com seus bares de m�sica ao vivo. Tamb�m � uma das preferidas pelos praticantes de frescobol.
					</p>
					<p>
						<b>Lagoa da Concei��o</b> - A Lagoa da Concei��o disputa com a Ponte Herc�lio Luz a condi��o de principal cart�o postal da cidade. Pode-se dizer tamb�m que diferentes mundos que habitam Florian�polis- o cosmopolita e o interiorano- se cruzam na Lagoa. Apesar de n�o ter ondas, ela � o lugar onde mora a maioria dos surfistas. Apesar de n�o ter bibliotecas, teatros e cinemas, ela � o reduto de artistas e intelectuais. Apesar de n�o ter um grande palco para shows, abriga em seus bares os mais distintos ritmos para embalar suas noites. Quem procura divers�o e arte n�o pode perder a Lagoa de vista. Ao seu redor ficam as melhores praias para o surf e outros esportes radicais. Dos morros que a circundam surgem asas e paraglides, colorindo o c�u. Nas dunas, desde a Avenida das Rendeiras &#150; onde encontram-se os �ltimos redutos do artesanato da renda de bilro- at� a Joaquina, pratica-se o sandboard. Em suas �guas imperam as lanchas, jet-skis, pranchas de windsurf e barcos � vela de todos os tipos e tamanhos. A Lagoa esteve durante muito tempo ligada � v�rios n�cleos pr�ximos. Dentre estes est�o a Barra da Lagoa, a Costa da Lagoa e o Canto da Lagoa, comunidades de pescadores que preservam muitas das caracter�sticas culturais. No Canto da Lagoa, descobriu-se um sambaqui (cemit�rio ind�gena), onde foram recolhidos machados semi-polidos, cer�mica e outros vest�gios arqueol�gicos.
					</p>
					<p>
						A Costa da Lagoa � uma t�pica col�nia de pescadores. Sua origem est� ligada � ocupa��o da pr�pria Lagoa da Concei��o. Nos s�culos XVIII e XIX a Costa da Lagoa alcan�ou prest�gio pelo forte interc�mbio de mercadorias que ali eram produzidas e levadas para a Lagoa e outros n�cleos. A grande quantidade de cultivos e os muitos engenhos que ali existiram podem ser avaliados atualmente. H� ainda alguns engenhos t�picos, casar�es e sobrados ao longo do secular caminho de pedras que liga as duas comunidades. Aquele n�cleo de pescadores e rendeiras ainda desenvolve suas pr�ticas artesanalmente, como nas gera��es passadas. Um decreto municipal tombou a Costa da Lagoa como patrim�nio hist�rico cultural. 
					</p>
					<p>
						<b>Galheta</b> - A praia da Galheta, com acesso pela praia Mole, � o outro exemplo de praia selvagem que s� pode ser alcan�ada a p�. Para chegar at� ela � preciso seguir uma trilha de cerca de 300 metros localizada junto ao cost�o de pedras do lado esquerdo da Mole. A Galheta � um local paradis�aco e n�o apresenta nenhum tipo de edifica��o. Recentemente a C�mara dos Vereadores oficializou esse local como praia de nudismo. Al�m da bela paisagem que proporciona, o ambiente tamb�m � favor�vel � pr�tica do surf.
					</p>
					<p>
						<b>Barra da Lagoa</b> - Maior n�cleo pesqueiro da Ilha, a Barra da Lagoa tamb�m � uma das praias mais procuradas de Florian�polis com seus 2,50 km de faixa de areia. Distante 20 km do centro, a Barra � bem servida em bares e restaurantes. Suas �guas n�o chegam a ser violentas, embora em alguns dias seja poss�vel a pr�tica do surf, principalmente junto ao cost�o de pedras, do lado direito da praia. O lugar tem v�rias op��es de pousadas e casas de aluguel, al�m de campings. Em julho o local vive uma grande agita��o, com a Festa da Tainha, uma grande quermesse organizada pela comunidade local e que atrai visitantes de v�rias partes.
					</p>
					<p>
						<b>Mo�ambique</b> - Quase deserta, � a maior praia da ilha, com 12,5 km de areias macias. A quase inexist�ncia de ocupa��o urbana se deve ao fato de que esta praia situa-se pr�xima �s �reas de preserva��o permanente, como as dunas do Santinho e o Parque Florestal do Rio Vermelho, uma reserva de 400 mil m2 implantada nos anos 70. Tamb�m boa op��o para o surf, Mo�ambique ou Praia Grande, possui �guas frias. Seu nome adv�m do fato de suas areias serem repletas de mo�ambiques, um molusco semelhante � ostra. Distante 35 km do centro, tem uma diminuta estrutura comercial.
					</p>
					<p>
						<b>COSTA SUL</b>
						<br>
					</p>
					<p>
						Na Costa Sul encontram-se as praias com menor infra-estrutura tur�stica e que abrigam comunidades t�picas, al�m de v�rias trilhas selvagens que levam a praias de mar grosso quase desertas.
					</p>
					<p>
						<b>Campeche</b> - Localizada a 20 km do centro da cidade, � uma praia bastante larga, circundada por pequenas dunas e vegeta��o rasteira. � uma das boas op��es para o surf na Ilha. Sua extens�o � de 11,4 km e a estrada que d� acesso ao mar � bem servida em termos de bares e restaurantes. Em frente � praia est� a Ilha do Campeche, com exuberante vegeta��o nativa. De 1926 a 1939, o escritor e aviador franc�s Saint-Exup�ry (&quot;O Pequeno Pr�ncipe&quot;) fazia escala no aer�dromo natural do lugar. 
					</p>
					<p>
						<b>Naufragados</b> - Para se chegar at� esta praia percorre-se 28 km desde o centro at� o Ribeir�o da Ilha e mais 12 km por estrada de ch�o at� a Caieira da Barra do Sul. Depois � necess�ria uma caminhada de 3 km para se chegar ao destino final, que corresponde ao ponto mais extremo do sul da Ilha. Apesar do esfor�o, o passeio � agrad�vel. O nome da praia est� ligado a um naufr�gio ali ocorrido em 1715 com um barco de imigrantes a�orianos. Com uma extens�o de 1500 metros de areias claras, � uma praia de mar bravio e �guas geralmente frias. Muito procurada pelos adeptos do camping selvagem e surfistas. Para quem prefere maior comodidade, h� um acesso pelo mar a partir do Ribeir�o da Ilha. O passeio de barco � de rara beleza, pois o trajeto ocorre ao longo dos cost�es onde a vegeta��o da Ilha se apresenta muito preservada.
					</p>
					<p>
						<b>Morro das Pedras</b> - Igualmente de mar agitado, onde o banho exige alguns cuidados, esta praia, com 3.200 metros de extens�o e distante 20 km do centro, � liga��o entre o Campeche e a Arma��o. Paralela a ela est� a Lagoa do Peri. Na estrada que d� acesso � praia pode-se ver o choque das ondas contra o cost�o de pedras. No alto do morro fica a casa de retiro dos padres jesu�tas, constru�da com pedras extra�das do pr�prio local. Dali se tem uma bela vis�o da praia da Arma��o e do Parque da Lagoa do Peri.
					</p>
					<p>
						<b>Arma��o</b> - Com uma extens�o de 2,25 km, j� foi uma forte esta��o de ca�a �s baleias. Mesmo com boas ondas, n�o chega a ser perigosa para o banho. Possui restaurantes muito procurados, localizados junto � pra�a e � centen�ria capela a�oriana. Distante 25 km do centro, � uma das tradicionais col�nias de pescadores da Ilha. Ali tamb�m se localiza um dos mais importantes s�tios arqueol�gicos do Estado.
					</p>
					<p>
						<b>Matadeiro</b> - Separada da praia da Arma��o por um pequeno rio, esta praia possui apenas 200 metros e s� pode ser alcan�ada a p�. Al�m do surf, o local � excelente para a pr�tica do v�lei, frescobol e futebol etc. Possui bares que servem refei��es e lanches variados.
					</p>
					<p>
						<b>Lagoinha do Leste</b> - Outra praia selvagem, que s� pode ser alcan�ada a p�, a Lagoinha do Leste merece ser visitada. � acess�vel ap�s uma caminhada de aproximadamente uma hora pela trilha existente no morro. N�o possui bares ou restaurantes, mas � um para�so natural, de areias macias e mar consideravelmente agitado. Al�m disso tem uma lagoa localizada no lado esquerdo em cujas margens os campistas se instalam. Com uma extens�o de 1,25 km, � um aut�ntico santu�rio da natureza.
					</p>
					<p>
						<b>Saquinho</b> - Existem tr�s trilhas que levam � praia do Saquinho, partindo da Solid�o, da Caieira da Barra do Sul ou de Naufragados. S�o localizadas em �reas �ngremes e exigem um certo f�lego, mas vale o esfor�o ao revelar um peda�o de Mata Atl�ntica bastante preservado, ou, para quem sai da Solid�o, uma costa magnificamente recortada onde a presen�a das gaivotas � marcante. Esta praia tem formato de concha e apenas 200 metros de extens�o. � freq�entada por surfistas.
					</p>
					<p>
						<b>P�ntano do Sul</b> - Esta praia � uma das mais ricas col�nias de pescadores da Ilha, com boas op��es em restaurantes. Seu mar � relativamente agitado, mas com ondas n�o muito grandes e sem maiores riscos para banho. Distante 31 km do centro, ali est� localizada uma das reservas arqueol�gicas da Ilha, al�m do p�ntano, ecossistema que originou seu nome. A faixa de areia, na sua maioria �mida, � bastante larga. 
					 </p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(28)
					nm_figuras( 1) = "Armacao"
					nm_figuras( 2) = "Barra da Lagoa"
					nm_figuras( 3) = "Canasvieiras1"
					nm_figuras( 4) = "Canasvieiras2"
					nm_figuras( 5) = "Canasvieiras3"
					nm_figuras( 6) = "Costao da Ilha da Armacao"
					nm_figuras( 7) = "Costao do Santinho"
					nm_figuras( 8) = "Daniela"
					nm_figuras( 9) = "Hotel da Praia Brava"
					nm_figuras(10) = "Ilha da Armacao"
					nm_figuras(11) = "Ilha da Fortaleza da Aracatuba"
					nm_figuras(12) = "Ilha do Campeche"
					nm_figuras(13) = "Joaquina 1"
					nm_figuras(14) = "Joaquina"
					nm_figuras(15) = "Pantano do sul"
					nm_figuras(16) = "Piscina Natural na Praia Mole"
					nm_figuras(17) = "Ponta das Canas"
					nm_figuras(18) = "Ponta de Naufragados"
					nm_figuras(19) = "Ponta do Sambaqui"
					nm_figuras(20) = "Praia Brava 1"
					nm_figuras(21) = "Praia Brava 2"
					nm_figuras(22) = "Praia Deserta"
					nm_figuras(23) = "Praia do Matadeiro"
					nm_figuras(24) = "Praia do Ribeirao da Ilha"
					nm_figuras(25) = "Praia Mole 1"
					nm_figuras(26) = "Ribeirao da Ilha"

					redim alt_figuras(28)
					alt_figuras( 1) = "Praia da Arma��o"
					alt_figuras( 2) = "Barra da Lagoa"
					alt_figuras( 3) = "Praia de Canasveiras"
					alt_figuras( 4) = "Praia de Canasveiras"
					alt_figuras( 5) = "Praia de Canasveiras"
					alt_figuras( 6) = "Cost�o da Ilha da Arma��o"
					alt_figuras( 7) = "Cost�o do Santinho"
					alt_figuras( 8) = "Balne�rio Daniela"
					alt_figuras( 9) = "Hotel da Praia Brava"
					alt_figuras(10) = "Ilha da Arma��o"
					alt_figuras(11) = "Ilha da Fortaleza da Ara�atuba"
					alt_figuras(12) = "Ilha do Campeche"
					alt_figuras(13) = "Praia da Joaquina"
					alt_figuras(14) = "Praia da Joaquina"
					alt_figuras(15) = "Praia do P�ntano do Sul"
					alt_figuras(16) = "Piscina Natural na Praia Mole"
					alt_figuras(17) = "Ponta das Canas"
					alt_figuras(18) = "Ponta de Naufragados"
					alt_figuras(19) = "Ponta do Sambaqui"
					alt_figuras(20) = "Praia Brava"
					alt_figuras(21) = "Praia Brava"
					alt_figuras(22) = "Praia Deserta"
					alt_figuras(23) = "Praia do Matadeiro"
					alt_figuras(24) = "Praia do Ribeir�o da Ilha"
					alt_figuras(25) = "Praia Mole"
					alt_figuras(26) = "Ribeirao da Ilha"
					
					for i=1 to 26
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="51" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<% 
	else
		%>

		<hr id="hr1">

		<table border="0" id="tab4">
			<tr>
				<td class="textoComum" valign="top" WIDTH="436">
					<p>
						<b>BEACHES</b>
						<br>
					</p>
					<p>
						The municipalities that border Santa Catarina Island - Governador Celso Ramos, to the north; Palho�a, to the south; and S�o Jos� and Bigua��, to the west - have in common the Azorean colonization and the fact that they are home to companies of the metropolitan region. When it comes to beaches, the big competitors of Florian�polis are Palho�a and Governador Celso Ramos. 
					</p>
					<p>
						In the municipality of Palho�a, Praia de Fora, Enseada do Brito, Praia do Sonho and Pinheira are fishing communities that are popular with tourists. Pedras Altas is the official naturalist beach in the municipality, and Guarda do Emba� is recognized as one of the best for surfing. Governador Celso Ramos is better known for the polemical farra do boi &#150; or the running of oxes through the streets - than for its beautiful coast, with coves, bays and peninsulas. The municipality is also the largest producer of cultivated mussels in Brazil. In addition to the beaches of Palmas, Praia Grande, Arma��o, and Costeira, Anhatomirim Island and the Ba�a dos Golfinhos (Dolphin Bay) is part of the municipality. Still little explored by tourists and without many leisure options other than the beaches, Governador Celso Ramos is a trip worth taking to see the well-preserved nature. 
					</p>
					<p>
						At the center of attraction is Santa Catarina Island, with its mix of plateaus, dunes lagoons, mangrove swamps and mountains with Atlantic Forest that is displayed in various forms. Santa Catarina Island is surrounded by 30 small islands, among them those that house the forts of Anhatomirim and Ratones Grande, in addition to Ratones Pequena, Franc�s, Guar�s Grande, Diamante and Noivos, all in the North Bay. In the South Bay are Vinhas, Laranjeiras, Maria Francisca, do Largo, do Andrade and Carlos Islands. To the north stand out Deserta and Arvoredo Islands, which are part of a Marine Biology Reserve. To the east are the islands of Moleques do Norte, Mata-Fome, Badejo, das Aranhas, do Xavier and Campeche. On the latter, exuberant nature and the remains of an ancient pre-historic civilization are preserved. Sixteen hundred meters long, the island is a trip not to be missed. With help from a good guide one can see a lot in one day. There are prehistoric rock carvings and a cave full of bats, which cuts 30 meters into the island. One can hike and dive, or enjoy the beautiful beach. 
					</p>
					<p>
						To the south there are the Tr�s Irm�s, Moleques do Sul, do Coral, Ara�atuba and Papagaios Islands. On the north coast of Santa Catarina Island are the beaches with greater tourist infrastructure, calm, warm waters, ideal for families with children, except for Praia Brava.
					</p>
					<p>
						<b>THE NORTH COAST</b>
						<br>
					</p>
					<p>
						<b>Santinho</b> - On the open ocean and also good for surfing, it is 40 km from downtown and 2.5 km from the beach. One of its attractions is the rock carvings along the coast. There are inscriptions made in the rocks more than 5 thousands years ago by the Carij�s Indians. The strip of light sand is soft and wide. With its dunes, it forms an ecosystem of rare beauty.
					</p>
					<p>
						<b>Ingleses</b> - This beach, 36 kilometers from downtown, is the farthest north of Santa Catarina Island. Its name comes from an English ship that was stranded there in 1700. It is one of the beaches with the most complete tourist infrastructure. Although its waters are not as warm, it offers fine moments to bathers who seek leisure. It is approximately 5 kilometers long and one of the most sought after in summer. Brava - The rough cold sea, usually with strong waves, is great for surfing and doesn't impede a great dip in the sea. Located 38 kilometers from downtown, Brava is behind a hill from which there is a magnificent view on top, before reaching the beach. The 1.7 km strip of soft sand slopes deeply and the water is very deep right at the edge. The commercial options at this beach, which was recently developed with the construction of an imposing group of Mediterranean style buildings, have been increasing each year.
					</p>
					<p>
						<b>Lagoinha</b> - With a calm sea and cold waters, Lagoinha is located 37 kilometers from downtown. Its strip of sand is 900 meters long and has received increasingly more people in recent years. The access road is found just before that for Pontas das Canas. It is one of the most pleasant spots on Santa Catarina Island. It is considered a fine option for families with children.
					</p>
					<p>
						<b>Ponta das Canas and Canasvieiras</b> - Following the historic natural tendency on the north of the island, due to the need for defense from Spanish attacks, the region was settled and urbanized on a much greater scale than the south. Located 34 kilometers from the center of the city it has calm, usually warm waters. It is 1.9 kilometers long with fine, light colored sand. It is a good place for water sports, and one of the beaches most sought by Argentine and Uruguayan tourists. Cachoeira do Bom Jesus - Located between Ponta das Canas and Canasvieiras, this beach with calm seas has nearly 3 kilometers of white sand, and is located 27 kilometers from the city center. It is one of the beaches where the most beautiful houses of the city are found and it has a good set of commercial services. Canasvieiras - The principal spot for Argentineans in Florian�polis, Canasvieiras is the most cosmopolitan location on Santa Catarina Island. So much so that &quot;portunhol&quot; (a mixture of Spanish and Portuguese) is the official language during the summer months. It is one of the busiest points of the island, by day and by night. There are bars, restaurants, hotels, clubs, game rooms, camp grounds - all the facilities necessary for a comfortable stay with plenty of options. Equipment is also offered for various sports and there are many good tennis courts at the hotels, clubs, and posadas. Three kilometers long, the waters at Canasvieiras beach are extremely pleasant, warm and calm. One can rent a boat that makes the trip to Anhatomirim Island in 40 minutes. Even closer is Franc�s Island, where nearly everything is destined to the use of the only landowner. Other options at this beach, which is 27 kilometers from downtown, are the schooner trips and paddle boats for rent.
					</p>
					<p>
						<b>Daniela</b> - Three kilometers long and 26 kilometers from downtown, this beach is occupied mostly by summer houses. Its has clear waters on a warm sea, it is much sought after by families with children. There are not many services.
					</p>
					<p>
						<b>THE WEST COAST</b>
						<br>
					</p>
					<p>
						The west coast of Santa Catarina Island, known as Mar de Dentro (The Inner Sea), faces the mainland, and is divided between the north and south bays. In the north bay are found the beaches of Saco Grande, Santo Ant�nio de Lisboa, and Sambaqui, formed by communities located on bays with sheltered waters that conserve characteristics of Azorean immigrants. 
					</p>
					<p>
						<b>Sambaqui</b> - Part of the district of Santo Ant�nio de Lisboa, Sambaqui is an option for those who seek tranquility in one of the most pleasant areas for recreation and rest on the island. Seventeen kilometers from downtown, its beach is 1.1 km long. The name Sambaqui originates from the shell mounds in the area, calling the attention of researchers, and which from time to time have revealed fossilized animals or vegetable matter, in addition to the relics of the indigenous communities that inhabited the region. Its clear waters are calm, nearly still. The beach line is formed by rocky coasts. This is one place on the island where the folkloric boi-de-mam�o dance is still found. Sambaqui has various bars and good restaurants on the beach.
					</p>
					<p>
						<b>Santo Ant�nio de Lisboa</b> - The oldest neighborhood on the Island, Santo Ant�nio is an important center of Azorean culture. Its beach has warm, calm waters, It is 13 kilometers from downtown, noted for its extreme beauty and peacefulness, with coarse sand beaches. To substitute fishing, one of the activities that has been increasing is the cultivation of oysters and mussels. Another ideal place to seek peace is neighboring Cacup�. It has a beach 900 meters long. In addition to various restaurants, it is a fine option for a picnic or family outing. 
					</p>
					<p>
						<b>THE SOUTHERN PART OF THE ISLAND</b>
						<br>
					</p>
					<p>
						<b>Tapera</b> - Located on the southern coast of the Island, near the airport, it has clear, still waters. The beach is 1.5 km long and has firm, white, thick sand. There are a number of bars and it is a good place for an outing.
					</p>
					<p>
						<b>Ribeir�o da Ilha</b> - Located 36 km from downtown, Ribeir�o da Ilha is one of the oldest settlements of Florian�polis. Records indicate that in 1526 Spanish explorer Sebasti�o Caboto landed here and some of his sailors joined others already on the island because they were shipwrecked from an earlier expedition of Dias de Solis, in 1515. Ribeir�o is composed of various small beaches with calm waters with thick sand. It is one of the locations with the most traditional architecture, such as the Azorean style houses, the Nossa Senhora da Lapa do Ribeir�o Church and the Ethnology Museum, with a collection of rare artifacts related to Azorean colonization.
					</p>
					<p>
						<b>EAST COAST</b>
						<br>
					</p>
					<p>
						On the East Coast are located the beaches that face the Atlantic Ocean, settled above sandy banks. They are good for surfing and have clean, cold waters.
					</p>
					<p>
						<b>Joaquina</b> - World famous for its excellent surfing conditions and for the infinite number of surf competitions held there, the sand dunes of Joaquina attract a new type of sport - sandboarding. The beach's name comes from the legend of an old lacemaker named Joaquina, who was on the sand of the beach making lace when she was washed away by an enormous wave. Its sea are choppy. Seventeen km from downtown, it is called &quot;Joaca&quot; by the surfers. The beach is 3.5 km long and has one of the best facilities for visitors including hotels, restaurants, bars, a tourist terminal with a police and lifeguard post, public showers and bathrooms. Swimming is not recommended because of the large waves and current. Joaquina is invariably full of surfers and bodyboarders with their colorful clothes. There are always people playing soccer and volleyball in the sand. There is a large paid parking lot, as well as ice cream stands and crafts stores. An efficient lighting system allows surfing and other sports at night.
					</p>
					<p>
						<b>Mole</b> - A fine option for surfing, Praia Mole is not recommended for children. There are deep spots right at the edge. Located 15 kilometers from downtown between Lagoa da Concei��o and Barra da Lagoa, its name comes from its soft sands. It also has public parking, showers, bathrooms and a life guard post. There are bars with seafood and snacks. With its regularly cold waters, it is the hot spot of the city. With a predominantly young crowd, it is busy on summer nights with its bars with live music. It is also one of the favorite spots for those who play frescobol (or beach paddleball).
					</p>
					<p>
						<b>Lagoa da Concei��o</b> - Lagoa da Concei��o, along with the Herc�lio Luz Bridge, are the most famous symbols of the city. It can be said that there are different worlds that inhabit Florian�polis &#150; the cosmopolitan and the rural - they both mix in Lagoa. Although there are no waves, its is where the majority of surfers live. Although it has no libraries, theaters or cinemas, it is a center for artists and intellectuals. Although there is no large stage for shows, distinct rhythms sway the bars at night. Those who seek fun and art cannot miss visiting Lagoa. Nearby are some of the best beaches for surfing and other extreme sports. From the surrounding mountains rise hang-gliders and paragliders that color the sky. In the sand-dunes, between Avenida das Rendeiras - where bobbin lace is still made - to Joaquina, sandboarding is practiced. The lagoon waters are dominated by motorboats, jet-skis, windsurfers and sailboats of all types and sizes. For many years Lagoa was linked to a number of neighboring villages. Among these are Barra da Lagoa, Costa da Lagoa and Canto da Lagoa, communities of fishermen and their families which preserve many of the cultural characteristics. Canto da Lagoa has a sambaqui (Indigenous cemetery), where semi-polished ax heads, ceramic ware and other archeological remains were found.
					</p>
					<p>
						<b>Costa da Lagoa</b> is a fishing village. Its origin is linked to the settlement of the village of Lagoa da Concei��o. In the eighteenth and nineteenth centuries, it achieved prestige for the important exchange of goods that were produced there and taken to the Lagoa and other centers. Vestiges of the large quantity of crops and the many mills that existed there can still be noticed. There are still some traditional mills, large houses and colonial homes along the century-old stone route that links the villages of Costa da Lagoa with that of Lagoa da Concei��o. The local fishermen and lace-makers still conduct their traditional crafts, as in generations past. A municipal decree has preserved Costa da Lagoa as a historical-cultural landmark. 
					</p>
					<p>
						<b>Galheta</b> - Galheta beach, alongside Mole beach, is another example of a wild beach that can only be reached by foot. To get there one must follow a 300-meter trail located along the rocky coast on the northern side of Mole Beach. Galheta is a paradise with no buildings. The City Council recently declared the location to be a place for natural bathing. In addition to the beauty of the landscape, the environment is also good for surfing.
					</p>
					<p>
						<b>Barra da Lagoa</b> - The largest fishing center on the Island, Barra da Lagoa is also one of the most popular beaches in Florian�polis with its 2.50 km of sand. Twenty kilometers from downtown, Barra is well served by bars and restaurants. Its waters are not rough, although on some days it is possible to surf, principally near the rocky coast on the south side of the beach. It also has various types of posadas and houses for rent, in addition to camp grounds. In July the location becomes lively with the Tainha Festival, a large traditional event organized by the local community and which attracts visitors from near and far.
					</p>
					<p>
						<b>Mo�ambique</b> - Nearly deserted, it is the largest beach on the island, with 12.5 km of soft sands. The nearly nonexistent occupation is due to the fact that this beach is near preserved regions, such as the dunes of Santinho and the Rio Vermelho Forest Park, a reserve of 400 mil m2 established in 1970. Another good option for surfing is Mo�ambique or Praia Grande, which has cold waters. Its name comes from the fact that it has many mo�ambiques, a type of mollusk similar to an oyster. Located 35 km from downtown, it has very limited commerce.
					</p>
					<p>
						<b>SOUTH COAST</b>
						<br>
					</p>
					<p>
						On the southern coast are found beaches with less tourist facilities and which have traditional communities in addition to a number of rough trails that lead to nearly deserted open ocean beaches.
					</p>
					<p>
						<b>Campeche</b> - Located 20 km from the center of the city, it is a rather large beach, circled by small dunes and scrub vegetation. It is one of the best options for surfing on the island. It is 11.4 km long and the road that leads to the sea is well served by bars and restaurants. In front of the beach is Campeche Island, with exuberant native vegetation. From 1926 to 1939, the French writer and aviator Saint-Exup�ry (author of &quot;The Little Prince&quot;) landed at the natural air strip in the village. 
					</p>
					<p>
						<b>Naufragados</b> - To arrive at this beach one travels 28 km from downtown to Ribeir�o da Ilha and another 12 km by dirt road to Caieira da Barra do Sul. It is then necessary to walk 3 km to reach the final destination, which is the southern most point on the island. Despite the effort, the trip is pleasant. The name of the beach comes from a shipwreck (naufragados means shipwrecked in Portuguese) which occurred in 1715 with a boat of Azorean immigrants. With 1,500 meters of light colored sands, it is an open ocean beach with generally cold waters. It is much sought by campers and surfers. For those who prefer greater comfort, there is sea access from Ribeir�o da Ilha. The boat trip is one of rare beauty, for the trajectory runs along rocky coasts where the Island's vegetation is well preserved.
					</p>
					<p>
						<b>Morro das Pedras</b> - Also with rough seas, where one must swim with care, this beach that is 3,200 meters long and 20 km from downtown, connects Campeche and Arma��o. It is parallel to the Peri Lagoon. From the access road one can see the waves crashing against the rocky coast. On top of the hill is a retreat for Jesuit priests built from rocks taken from the site. There is a beautiful view of Arma��o and the Peri Lagoon Park.
					</p>
					<p>
						<b>Arma��o</b> - This beach of 2.25 km was a whaling center. Even with good waves, it is safe for swimming. There are popular restaurants along the plaza and a century-old Azorean chapel. Located 25 km from downtown, it is one of the most traditional fishing villages on the island. It also has one of the most important archeological sites in the state.
					</p>
					<p>
						<b>Matadeiro</b> - Separated from Arma��o beach by a small river, this beach is only 200 meters long and can only be reached by foot. In addition to surfing, it is excellent for volleyball, frescobol and soccer. There are small bars that have various snacks.
					</p>
					<p>
						<b>Lagoinha do Leste</b> - Another wild beach which can only be reached by foot, Lagoinha do Leste (The little lagoon of the east) is worth a visit. It is accessible after a walk of about an hour on a trail over the hill. There are no bars or restaurants, but it is a natural paradise, with soft sands and a rough seas. There is also a lagoon on the north side where campers set up tents on the margins. Its 1.25 km are a true sanctuary of nature.
					</p>
					<p>
						<b>Saquinho</b> - There are three trails that lead to Saquinho beach, leaving from Solid�o, from Caieira da Barra do Sul or from Naufragados. They are located in steep areas and require stamina, but are worth the effort because one finds a portion of well-preserved Atlantic Forest, or for those who leave Solid�o, a coast that is magnificently carved with a distinctive presence of seagulls. This beach is in the shape of a conch and is only 200 meters long. It is popular with surfers.
					</p>
					<p>
						<b>P�ntano do Sul</b> - This beach is one of the richest fishing villages on the island. It has good options for restaurants, and a relatively rough sea, but the waves are not big and it is safe for swimming. It is 31 km from downtown and has an important archeological site, in addition to a swamp, the ecosystem from where it took its name (P�ntano is swamp in Portuguese). The beach has a wide strip of damp sand. 
					 </p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(28)
					nm_figuras( 1) = "Armacao"
					nm_figuras( 2) = "Barra da Lagoa"
					nm_figuras( 3) = "Canasvieiras1"
					nm_figuras( 4) = "Canasvieiras2"
					nm_figuras( 5) = "Canasvieiras3"
					nm_figuras( 6) = "Costao da Ilha da Armacao"
					nm_figuras( 7) = "Costao do Santinho"
					nm_figuras( 8) = "Daniela"
					nm_figuras( 9) = "Hotel da Praia Brava"
					nm_figuras(10) = "Ilha da Armacao"
					nm_figuras(11) = "Ilha da Fortaleza da Aracatuba"
					nm_figuras(12) = "Ilha do Campeche"
					nm_figuras(13) = "Joaquina 1"
					nm_figuras(14) = "Joaquina"
					nm_figuras(15) = "Pantano do sul"
					nm_figuras(16) = "Piscina Natural na Praia Mole"
					nm_figuras(17) = "Ponta das Canas"
					nm_figuras(18) = "Ponta de Naufragados"
					nm_figuras(19) = "Ponta do Sambaqui"
					nm_figuras(20) = "Praia Brava 1"
					nm_figuras(21) = "Praia Brava 2"
					nm_figuras(22) = "Praia Deserta"
					nm_figuras(23) = "Praia do Matadeiro"
					nm_figuras(24) = "Praia do Ribeirao da Ilha"
					nm_figuras(25) = "Praia Mole 1"
					nm_figuras(26) = "Ribeirao da Ilha"

					redim alt_figuras(28)
					alt_figuras( 1) = "Arma��o Beach"
					alt_figuras( 2) = "Barra da Lagoa Beach"
					alt_figuras( 3) = "Praia de Canasveiras Beach"
					alt_figuras( 4) = "Praia de Canasveiras Beach"
					alt_figuras( 5) = "Praia de Canasveiras Beach"
					alt_figuras( 6) = "Coast Arma��o Island"
					alt_figuras( 7) = "Coast of Santinho"
					alt_figuras( 8) = "Daniela Beach"
					alt_figuras( 9) = "Hotel at Praia Brava"
					alt_figuras(10) = "Arma��o Island"
					alt_figuras(11) = "Ara�atuba Fort Island"
					alt_figuras(12) = "Campeche Island"
					alt_figuras(13) = "Joaquina Beach"
					alt_figuras(14) = "Joaquina Beach"
					alt_figuras(15) = "P�ntano do Sul Beach"
					alt_figuras(16) = "Natural Swimming pool at Mole Beach"
					alt_figuras(17) = "Ponta das Canas Beach"
					alt_figuras(18) = "Ponta de Naufragados Beach"
					alt_figuras(19) = "Ponta do Sambaqui Beach"
					alt_figuras(20) = "Brava Beach"
					alt_figuras(21) = "Brava Beach"
					alt_figuras(22) = "Deserta Beach"
					alt_figuras(23) = "Matadeiro Beach"
					alt_figuras(24) = "Ribeir�o da Ilha Beach"
					alt_figuras(25) = "Mole Beach"
					alt_figuras(26) = "Ribeirao da Ilha"
					
					for i=1 to 26
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="46" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<%
	end if 
case 5 
	
	if id_wk = 1 then
		%>

		<hr id="hr1">

		<table border="0" id="tab5">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>CONTINENTE TAMB�M MERECE UMA VISITA</b>
						<br>
					</p>
						No Continente, de frente para a Ilha, ficam belas praias de mar calmo. D�o nome a bairros da cidade e foram coqueluche nos anos 50 e 60. As dos bairros Coqueiros e Estreito n�o s�o habitualmente utilizadas para banhos. Em compensa��o, no caso de Coqueiros, oferecem �timas paisagens, como a praia da Saudade, do Sorriso, do Meio, Itagua�u e das Palmeiras, al�m da do Bom Abrigo, localizada no bairro hom�nimo. No Estreito encontra-se a praia do Balne�rio, uma op��o para caminhadas e pr�tica de esportes.
					<p>
					<p>
						<b>ENTORNO DE FLORIAN�POLIS</b>
						<br>
					</p>
					<p>
						Numa regi�o de transi��o entre o litoral e a serra, com clima ameno e temperado, Florian�polis tem na sua vizinhan�a duas cidades famosas por suas fontes minerais: �guas Mornas e Santo Amaro da Imperatriz, distantes 35 quil�metros do centro da capital catarinense. A paisagem campestre, que mistura-se � montanha, guarda a rusticidade de um tempo em que esses lugares eram caminhos de tropeiros entre a regi�o serrana e o litoral.
					</p>
					<p>
						De coloniza��o germ�nica, �guas Mornas foi emancipada politicamente em 1961, levada � categoria de munic�pio. Suas vilas e povoados tipicamente rurais criados pelos imigrantes alem�es tem tra�os ainda bem marcantes e facilmente percept�veis na arquitetura, idioma, h�bitos e costumes de seus habitantes.
					</p>
					<p>
						O visitante tem, al�m das op��es oferecidas pelos hot�is da regi�o, alternativas diversas, como passeios ao parque Estadual da Serra do Tabuleiro- local de preserva��o da fauna e flora, com destaque para um mini-zool�gico de esp�cies em extin��o. Tamb�m pr�ximas est�o algumas praias, como as da Pinheira e Guarda do Emba�, no munic�pio de Palho�a, distante 39 quil�metros de Florian�polis. 
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(6)
					nm_figuras( 1) = "Itaguacu"
					nm_figuras( 2) = "Praiadasaudade"
					nm_figuras( 3) = "praiadaspalmeiras"
					nm_figuras( 4) = "praiadaspalmeiras1"
					nm_figuras( 5) = "praiadomeio"

					redim alt_figuras(6)
					alt_figuras( 1) = "Praia de Itagua�u"
					alt_figuras( 2) = "Praia da Saudade"
					alt_figuras( 3) = "Praia das Palmeiras"
					alt_figuras( 4) = "Praia das Palmeiras"
					alt_figuras( 5) = "Praia do Assis (do meio)"
					
					for i=1 to 5
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="12" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<% 
	else
		%>

		<hr id="hr1">

		<table border="0" id="tab5">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>THE MAINLAND</b>
						<br>
					</p>
						On the mainland in front of the island are beautiful beaches with calm seas. They give name to the neighborhoods of the city and were very popular in the 1950s and 1960s. The Beaches in the neighborhoods of Coqueiros and Estreito are not regularly used for swimming. In compensation, in the case of Coqueiros, they offer beautiful views, such as that from the beaches of Saudade, do Sorriso, do Meio, Itagua�� and das Palmeiras, as well as that of Bom Abrigo, located in the neighborhood of the same name. In Estreito is found Balne�rio beach. It is not suitable for swimming, but is good for walking and sporting activities.
					</p>
					<p>
						In a transition region between the coast and the mountains, with a pleasant and temperate climate, Florian�polis has two neighboring cities famous for their mineral springs. �guas Mornas and Santo Amaro da Imperatriz, are 35 kilometers from the center of the state capital. The rural landscape that combines with the mountains, protects the rustic nature of a time when these places were routes for mule troops between the mountain region and the coast. 
					</p>
					<p>
						Of German settlement, �guas Mornas was declared a municipality in 1961. Its typically rural villages and settlements created by German immigrants still have strong marks of that culture which are easily noticed in the architecture, language, habits and customs of the residents. 
					</p>
					<p>
						In addition to the options offered by the hotels in the region, the visitor has various alternatives such as trips through the Serra do Tabuleiro mountains State Park, a location where the flora and fauna are preserved, highlighted by a small zoo with species threatened with extinction. There are also beaches nearby such as Pinheira and Guarda do Emba�, in the municipality of Palho�a, some 39 kilometers from Florian�polis.
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(6)
					nm_figuras( 1) = "Itaguacu"
					nm_figuras( 2) = "Praiadasaudade"
					nm_figuras( 3) = "praiadaspalmeiras"
					nm_figuras( 4) = "praiadaspalmeiras1"
					nm_figuras( 5) = "praiadomeio"

					redim alt_figuras(6)
					alt_figuras( 1) = "Itagua�u Beach"
					alt_figuras( 2) = "Saudade Beach"
					alt_figuras( 3) = "Palmeiras Beach"
					alt_figuras( 4) = "Palmeiras Beach"
					alt_figuras( 5) = "Assis Beach"
					
					for i=1 to 5
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="12" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<%
	end if 
case 6 
	
	if id_wk = 1 then
		%>

		<hr id="hr1">

		<table border="0" id="tab6">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>FORTALEZAS: TURISMO EDUCATIVO DESPERTA EM FLORIAN�POLIS</b>
						<br>
					</p>
					<p>
						Seculares, elas circundam a Ilha de Santa Catarina, distribu�das ao longo de sua costa de 172 quil�metros, recentemente revitalizadas e abertas � visita��o p�blica. S�o monumentais fortalezas que constitu�ram at� recentemente o principal projeto na �rea de patrim�nio cultural do Brasil. A restaura��o completa da maior delas, a de Santa Cruz de Anhatomirim, vem consolidando estes monumentos hist�ricos como principais pontos de atra��o tur�stica de Florian�polis. S�o milhares de visitantes de todo o pa�s e do exterior, principalmente de pa�ses vizinhos. A m�dia nos meses de janeiro e fevereiro ( alta temporada de ver�o ) tem sido de 400 mil pessoas.
					</p>
					<p>
						Tais monumentos constituem um conjunto de seis unidades que remontam ao s�culo XVIII, quando os espanh�is e portugueses entraram em conflito na regi�o do Prata e o governo portugu�s decidiu manter um comando �nico em toda costa sul brasileira at� a Col�nia de Sacramento, tendo a Ilha de Santa Catarina, ent�o chamada Nossa Senhora do Desterro, como ponto estrat�gico no Atl�ntico Sul.
					</p>
					<p>
						Dessa forma, o rei de Portugal, D. Jo�o V, determinou em 1738 a ida do brigadeiro Jos� da Silva Paes � Desterro para construir em seu redor um sistema de fortifica��es visando defend�-la. Engenheiro militar, Silva Paes projetou quatro: Santo Ant�nio, na ilha de Ratones Grande; Santa Cruz, na ilha de Anhatomirim; S�o Jos�, no norte da ilha de Santa Catarina e Nossa Senhora da Concei��o, ao sul desta, na pequena ilha de Ara�atuba.
					</p>
					<p>
						Do conjunto, a maior e a que sofreu menos danos ao longo dos s�culos foi a de Santa Cruz, que juntamente com as de Santo Ant�nio e S�o Jos�, formava o sistema defensivo triangular que guardava a entrada da barra norte da Ilha de Santa Catarina. Esse sistema de defesa tinha como base o cruzamento de fogos. Cada canh�o da fortaleza deveria atingir a metade da dist�ncia entre as outras duas de forma que fosse poss�vel o bloqueio do acesso aos inimigos.
					</p>
					<p>
						Segundo os historiadores, esta fortaleza n�o foi efetivamente utilizada do ponto de vista b�lico, nem mesmo durante a invas�o espanhola em 1777. Ap�s este epis�dio, o sistema entrou em descr�dito e Santa Cruz passou a ser progressivamente abandonada. Em 1894, durante a Revolu��o Federalista, serviu de pris�o e base de fuzilamento de revoltosos contra o governo de Floriano Peixoto. Em 1907, passou a pertencer ao Minist�rio da Marinha e voltou a ser utilizada como pris�o no desfecho da Revolu��o Constitucionalista de 1932. Funcionou como fortaleza at� o final da Segunda Guerra Mundial, quando o aparecimento de novas tecnologias b�licas tornou-a completamente obsoleta como unidade militar. Ent�o foi desativada.
					</p>
					<p>
						Apesar de tombada em 1938, somente em 1970 Santa Cruz veio � sofrer as primeiras interven��es de restauro feitos pela Universidade Federal de Santa Catarina, que reabriu-a oficialmente � visita��o p�blica em 1984. A restaura��o completa foi terminada em 1993.
					</p>
					<p>
						Como as demais edificadas no s�culo XVIII no Brasil, Santa Cruz tem tra�os de influ�ncia renascentista em sua arquitetura. Na ilha circundada por cost�es, seus edif�cios distribuem-se de maneira esparsa em diferentes n�veis, contidos por espessas muralhas, em cujo v�rtices se abrigam as proeminentes guaritas circulares de vigia. Entre os edif�cios mais significativos da fortaleza destacam-se a portada, que comp�e-se de um corredor formado por uma ab�boda de tijolos sobre os quais se elevam dois maci�os de alvenaria escalonadas; a casa do comandante, tipo c�mara e cadeia; e uma esp�cie de sobrado que foi a primeira sede do governo de Santa Catarina, onde residiu Silva Paes. O quartel da tropa � uma constru��o de grande destaque, representando o auge da impon�ncia das obras de Silva Paes. O estilo cl�ssico da constru��o � determinado por contornos retos, telhas coloniais e doze arcadas t�rreas de tal apuro que raramente deixavam de ser mencionadas por viajantes europeus em seus di�rios.
					</p>
					<p>
						Al�m desses edif�cios, encontram-se na ilha o paiol de p�lvora, a casa da farinha e o armaz�m da praia. Outros edif�cios foram constru�dos posteriormente, como a usina de eletricidade e a casa do tel�grafo. Para o visitante desta fortaleza h� o lado natural para ser apreciado. A maior parte do contorno da ilha de Anhatomirim � de cost�es que albergam uma fauna marinha muito peculiar. Tr�s pequenas praias arenosas completam o seu litoral. O rel�vo � modesto e a altitude m�xima � de 31 metros. Entre a ilha e o continente, que s�o separados por duas milhas, a profundidade � de no m�ximo cinco metros. Nas redondezas, numa ba�a, abrigam-se cerca de 500 golfinhos.
					</p>
					<p>
						A fortaleza de Santo Ant�nio, na ilha de Ratones Grande, � a segunda maior da regi�o. Sua constru��o teve in�cio em 1740, conclu�da quatro anos depois. Permaneceu em ru�nas at� 1990, quando foi iniciada sua recupera��o. Tamb�m com tra�os renascentistas, os principais edif�cios est�o implantados em linha, guarnecidos pela costa e voltados para o mar. O paiol de p�lvora, situado no ponto mais proeminente do t�rreo � a �nica constru��o com dois pavimentos. Os edif�cios, bem como as muralhas que os resguardam, foram constru�dos � base de cal. Entre os elementos arquitet�nicos mais significativos da fortaleza destacam-se a portada, a fonte de �gua e o aqueduto. Este, que une a casa do comandante ao quartel da tropa, integra um interessante e original sistema de capta��o, condu��o e aproveitamento das �guas pluviais provenientes dos telhados dos edif�cios principais.
					</p>
					<p>
						A fortaleza de S�o Jos� da Ponta Grossa, fica distante 25 quil�metros do centro de Florian�polis, na ilha de Santa Catarina. Estrategicamente situada no alto de um morro, emoldurada pela beleza dos cost�es e areia branca da praia do Forte, constitu�a o terceiro v�rtice do sistema triangular de defesa. A maioria de seus edif�cios completou sua restaura��o na Segunda metade da d�cada de 90. Entre os edif�cios, o mais significativo � a casa do comandante, constru��o curiosamente geminada ao paiol de p�lvora, formando o �nico conjunto com dois pavimentos no local. A for�a e a sobriedade da composi��o geradora do p�tio principal, delineado pelo sobrado colonial e a austera capela, espelham bem a import�ncia e a inter-rela��o dos poderes do Estado e a Igreja no s�culo XVIII.
					</p>
					<p>
						Al�m da pr�pria Universidade Federal de Santa Catarina, que organiza passeios com estudantes e estudiosos, empresas particulares exploram passeios tur�sticos para fortalezas que se localizam em ilhas (Anhatomirim e Ratones Grande). As excurs�es s�o feitas em escunas e a principal empresa do ramo � a Scuna Sul. Mesmo fora da temporada de ver�o, h� excurs�es di�rias em diferentes hor�rios e com servi�o de bordo. Alem destes servi�os, baleeiras s�o encontradas em diversas praias e na Lagoa da Concei��o para passeios nas proximidades.
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(9)
					nm_figuras( 1) = "Anhatomirim"
					nm_figuras( 2) = "Anhatomirim1"
					nm_figuras( 3) = "Anhatomirim2"
					nm_figuras( 4) = "PortalAnhatomirim"
					nm_figuras( 5) = "RatonesGrande"
					nm_figuras( 6) = "SaoJosedaPonta Grossa"
					nm_figuras( 7) = "TorreaoAnhatomirim"
					nm_figuras( 8) = "TuristasAnhatomirim"

					redim alt_figuras(9)
					alt_figuras( 1) = "Fortaleza de Santa Cruz de Anhatomirim"
					alt_figuras( 2) = "Fortaleza de Santa Cruz de Anhatomirim"
					alt_figuras( 3) = "Fortaleza de Santa Cruz de Anhatomirim"
					alt_figuras( 4) = "Fortaleza de Santa Cruz de Anhatomirim - Portal"
					alt_figuras( 5) = "Fortaleza de Santo Ant�nio - Ilha de Ratones Grande"
					alt_figuras( 6) = "Fortaleza de S�o Jos� da Ponta Grossa"
					alt_figuras( 7) = "Fortaleza de Santa Cruz de Anhatomirim - Torre�o"
					alt_figuras( 8) = "Fortaleza de Santa Cruz de Anhatomirim - Turistas"
					
					for i=1 to 8
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="65" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<% 
	else
		%>

		<hr id="hr1">

		<table border="0" id="tab6">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>FORTS</b>
						<br>
					</p>
					<p>
						Centuries old, the forts surround Santa Catarina Island, distributed along its 172-kilometer coastline, they were recently revitalized and open to public visits. They are monumental fortresses that until recently constituted the principal project in the field of cultural heritage in Brazil. The complete restoration of the largest, that of Santa Cruz de Anhatomirim, has consolidated these historic monuments as the main tourist attractions in Florian�polis. They have thousands of visitors from throughout the country and abroad, principally from neighboring countries. The average number of visitors in the months of January and February (the height of the summer season) has been 400,000 people.
					</p>
					<p>
						These monuments constitute a set of six units that date back to the seventeenth century when the Spanish and Portuguese entered into conflict in the region of the Plata River and the Portuguese government decided to maintain a single command on the entire southern Brazilian coast as far as the Col�nia de Sacramento. Santa Catarina Island, then called Nossa Senhora do Desterro, was the strategic point in the South Atlantic.
					</p>
					<p>
						In 1738, Portuguese King Dom Jo�o V determined that Brigadeiro Jos� da Silva Paes should go to Desterro to build a system of forts around the island. A military engineer, Silva Paes planned four: Santo Ant�nio de Lisboa, on Ratones Grande Island; Santa Cruz, on Anhatomirim Island; S�o Jos�, on the northern portion of Santa Catarina Island and Nossa Senhora da Concei��o, to the south, on the small island of Ara�atuba.
					</p>
					<p>
						Of the group, the largest and that which suffered less damage over the centuries was that of Santa Cruz, which together with that of Santo Ant�nio and S�o Jos� formed the triangular defensive system that protected the northern entrance of Santa Catarina Island. This basis of the defense was a system of cross-fire. Each cannon would be able to reach half the distance between the other two forts in order to block the access of enemies.
					</p>
					<p>
						According to historians, this fortress was not effectively utilized from a military point of view, not even during the Spanish invasion of 1777. After this episode the system entered into discredit and Santa Cruz came was progressively abandoned. In 1894, during the Federalist Revolution, it served as a prison and the place where those who revolted against the government of Floriano Peixoto were sent to the firing squad. In 1907, it was turned over to the Naval Ministry and went back to being utilized as a prison at the end of the Constitutionalist Revolution of 1932. It functioned as a fort until the end of World War II, when the appearance of new military technologies made it completely obsolete as a military unit. It was then disactivated.
					</p>
					<p>
						Although it was declared a landmark in 1938, only in 1970 were the first steps at restoration taken by the Federal University at Santa Catarina, which officially re-opened the fort to public visits in 1984. The complete restoration was terminated in 1993.
					</p>
					<p>
						As the other eighteenth century buildings in Brazil, Santa Cruz has renaissance traces in its architecture. On the Island circled by rocky coast, its buildings are distributed in a sparse manner on different levels, contained by thick walls, on top of which were sheltered the prominent circular lookout posts. Among the most significant buildings of the fort stand out the portal, which is composed of a corridor formed by a brick dome over which rise two stepped masses of masonry; the commandants house, a type of chamber and jail; and a two-story house that was the first government headquarters of Santa Catarina, where Silva Paes resided. The troop quarters is an important highlight, representing the peak of the majesty of the work of Silva Paes. The classic construction style is marked by straight lines, colonial roofs and 12 ground-level arcades built with such care that they rarely fail to be mentioned by European travelers in their diaries. In addition to these buildings, there is the powder house, the flour house and the beach warehouse.
					</p>
					<p>
						Other buildings were constructed later, such as the electrical plant and the telegraph house. Visitor's also have the natural side of the island to be appreciated. The greater part of the surroundings of Anhatomirim Island is formed by rocky coasts that protect a very special marine fauna. Three small sandy beaches complete the coast. The terrain is modest and the highest point is 31 meters above sea level. Between the island and the mainland, which are separated by two miles, the maximum depth is 5 meters.
					</p>
					<p>
						In the surroundings, there is a bay, which is home to some 500 dolphins. The Santo Ant�nio fort, on Ratones Grande Island, is the second largest of the region. Its construction began in 1740, and concluded four years later. It remained in ruins until 1990, when its restoration was initiated. It also had renaissance traces, the principle buildings are built in a row, protected by the coast and overlook the sea. The powder storage, located on the most prominent point of land is the only construction of two stories. The buildings, as well as the walls that protect them, were built with a lime base. Among the most significant architectural elements of the fort are the portada, the water well and the aqueduct which joins the commanders house to the troop quarters. It has an original and interesting method of retrieving, transporting and using rain water from the roofs of the main buildings.
					</p>
					<p>
						The S�o Jos� da Ponta Grossa fort is 25 kilometers from the center of Florian�polis on Santa Catarina Island. It is strategically located on top of a hill, framed by the beauty of the rocky coasts and the white sand of the Forte Beach, it constitutes the third flank of the triangular defense system. The largest of its buildings had their restoration completed in the second half of the 1990's. The most significant building is the commander's house which is curiously constructed with a common wall to the powder storage, forming the only two-floor structure at the location. The strength and sobriety of the composition generated by the principal patio, framed by the colonial house and the austere chapel, are a good reflection of the importance of the relationship of the powers of Church and State in the eighteenth century.
					</p>
					<p>
						In addition to the Federal University at Santa Catarina, which organizes excursions with students and scholars, private companies offer tourist trips to the island forts (Anhatomirim and Santo Ant�nio). The excursions are conducted on schooners and the principal company in the business is Escuna Sul. Even outside of the summer season, there are daily trips at different hours and with on-board service. In addition to the services, various whaling boats leave at any hour if there are passengers, from Lagoa da Concei��o, to the principal points of Santa Catarina Island and its surroundings, including the forts.
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(9)
					nm_figuras( 1) = "Anhatomirim"
					nm_figuras( 2) = "Anhatomirim1"
					nm_figuras( 3) = "Anhatomirim2"
					nm_figuras( 4) = "PortalAnhatomirim"
					nm_figuras( 5) = "RatonesGrande"
					nm_figuras( 6) = "SaoJosedaPonta Grossa"
					nm_figuras( 7) = "TorreaoAnhatomirim"
					nm_figuras( 8) = "TuristasAnhatomirim"

					redim alt_figuras(9)
					alt_figuras( 1) = "Santa Cruz de Anhatomirim Fort"
					alt_figuras( 2) = "Santa Cruz de Anhatomirim Fort"
					alt_figuras( 3) = "Santa Cruz de Anhatomirim Fort"
					alt_figuras( 4) = "Santa Cruz de Anhatomirim Fort - Doorway"
					alt_figuras( 5) = "Santo Ant�nio Fort - Ratones Grande Island"
					alt_figuras( 6) = "S�o Jos� da Ponta Grossa Fort"
					alt_figuras( 7) = "Santa Cruz de Anhatomirim Fort - Watchtower"
					alt_figuras( 8) = "Santa Cruz de Anhatomirim Fort - Tourists"
					
					for i=1 to 8
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="65" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<%
	end if 
case 7 
	
	if id_wk = 1 then
		%>

		<hr id="hr1">

		<table border="0" id="tab7">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>CULIN�RIA DA ILHA DE SANTA CATARINA PRIVILEGIA FRUTOS DO MAR</b>
						<br>
					</p>
					<p>
						A culin�ria da Ilha de Santa Catarina privilegia os frutos do mar. Peixes, camar�es e mariscos s�o alguns dos pratos servidos numa j� numerosa rede de restaurantes especializados, localizados tanto no centro da cidade como na maioria das praias mais freq�entadas. Os peixes s�o preparados nas mais variadas formas, dependendo da esp�cie. A tainha, o bagre e o linguado, por exemplo, podem ser servidos em fil�s ou cortados em postas. Os mariscos s�o servidos refogados logo ap�s seu recolhimento ou mesmo depois de secos.
					</p>
					<p>
						Os pratos mais comuns s�o o peixe ensopado, s� servido com sal e ervas de cheiro (salsa, cebolinha e louro) e consumido com pir�o de farinha de mandioca cozida no mesmo caldo de peixe; e o peixe frito, cortado em postas e envolto em farinha de mandioca ou trigo antes de fritar. A pr�pria gordura � aproveitada para fritura. Normalmente come-se o peixe frito com pir�o de mandioca. Outra variedade de prato muito comum � o peixe assado. Retiram-se apenas as v�sceras e conservam-se as escamas. H� tamb�m o peixe grelhado sobre brasas, enrolado em folhas de bananeira (n�o necessariamente). Quando pronto, o couro se desprende da carne com facilidade. Come-se com pir�o de feij�o . As ovas da tainha s�o outro prato muito apreciado, fritas e servidas com lim�o. No card�pio t�pico entra tamb�m o camar�o, frito, refogado, � milanesa e ao bafo, acompanhado de pir�o de peixe ou arroz. O caldo de camar�o, muito parecido com a sopa de legumes e verduras, tem largo consumo, quando n�o servido de base para o pir�o.
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(4)
					nm_figuras( 1) = "Box 32"
					nm_figuras( 2) = "Macarronada Italiana"
					nm_figuras( 3) = "Pizza Hut"
					nm_figuras( 4) = "Shopping"

					redim alt_figuras(4)
					alt_figuras( 1) = "Box 32 - Mercado P�blico"
					alt_figuras( 2) = "Macarronada Italiana"
					alt_figuras( 3) = "Pizza Hut"
					alt_figuras( 3) = "Pra�a de Alimenta��o - Shopping Beira Mar"
					
					for i=1 to 4
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="5" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<% 
	else
		%>

		<hr id="hr1">

		<table border="0" id="tab7">
			<tr>
				<td class="textoComum" width="436" valign="top">
					<p>
						<b>THE COOKING ON SANTA CATARINA ISLAND GIVES SPECIAL EMPHASIS TO SEAFOOD</b>
						<br>
					</p>
					<p>
						The cooking on Santa Catarina Island gives special emphasis to seafood. Fish, shrimp, and mussels are some of the dishes served in a now numerous group of specialized restaurants, located both in the center of the city as well as at most of the most popular beaches. Fish is prepared in many ways, depending on the species. Mullet, catfish and flounder for example can be served in fillets or steaks. Mussels are served stewed, whether they are fresh or dried.
					</p>
					<p>
						The most common dishes are fish broth, served only with salt and aromatic herbs (parsley, scallions and bay leaves) and eaten with cassava flour puree cooked in the fish broth; and fried fish, cut in steaks and dipped in cassava or wheat flour before frying. The fat of the fish is used for frying. Normally fried fish is eaten with cassava flour puree. Another common dish is fish - it is cleaned but the skin is left on.  Fish is also grilled over charcoal, and maybe rolled in banana leaves. When it is ready, the skin easily flakes off the meat. The dish is eaten with cassava flour puree with beans. Mullet row is another favorite dish, fried and served with lemon. A typical menu also has shrimp, both fried as well as stewed or cooked in batter, accompanied by cassava flour puree, cooked in fish and rice. The shrimp broth, enjoyed with a soup of vegetables and greens, is widely consumed, when not served as a base for cassava flour puree.
					</p>
				</td>
				<td class="textoComum" width="120" valign="top" align="right">
					<%
					redim nm_figuras(4)
					nm_figuras( 1) = "Box 32"
					nm_figuras( 2) = "Macarronada Italiana"
					nm_figuras( 3) = "Pizza Hut"
					nm_figuras( 4) = "Shopping"

					redim alt_figuras(4)
					alt_figuras( 1) = "Box 32 - Public Market"
					alt_figuras( 2) = "Macarronada Italiana"
					alt_figuras( 3) = "Pizza Hut"
					alt_figuras( 3) = "Shopping Beira Mar"
					
					for i=1 to 4
						%>
						<img SRC="images/fotos/<%=nm_figuras(i)%>_tn.jpg" ALT="<%=alt_figuras(i)%> - <%=strClick%>" vspace="5" onclick="ampliarFoto('<%=nm_figuras(i)%>','<%=alt_figuras(i)%>',<%=id_wk%>, 1);" onmouseover="this.style.cursor='hand';" onmouseout="this.style.cursor='default';">
						<br>
						<%
					next
					%>
					<br>&nbsp;
					<br>&nbsp;
				</td>
			</tr>
		</table>

		<%
	end if 
end select 
%>