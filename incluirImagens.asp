<% 
Server.ScriptTimeout = 450

Response.Flush
prefixo = request("prefixo")

op = request("op")
if op = "" then op = 0

desenvolvimento = false
if Request.ServerVariables("SERVER_NAME") = "marcelo" then desenvolvimento = true

%>
<!--!#include file="conn.asp"-->
<%
set Conn = server.CreateObject("ADODB.Connection")
set rs = server.CreateObject("ADODB.RecordSet")
	
Conn.Open cnpath

if op = 1 then

	Set upl = Server.CreateObject("SoftArtisans.FileUp")
	
	upl.Path = "e:\home\tavares\web\upload_admin\estomatologia"
	
	upl.SaveAs Cstr(prefixo) & "_" & upl.ShortFilename
	bytes = upl.TotalBytes
	fileName = upl.ShortFileName

	set rs = Conn.Execute("Select nu_seqImagem from TB_ImagensTrabalhos where cd_trabalho = " & prefixo)
	
	if rs.EOF then
		proximo = 1
	else
		while not rs.EOF
			proximo = rs("nu_seqImagem")
			rs.MoveNext
		wend
		proximo = proximo + 1
	end if
	
	strSQL = "INSERT INTO TB_ImagensTrabalhos (cd_trabalho, nu_seqImagem, " _
	& "de_nomeArquivo, de_legenda) VALUES (" _
	& prefixo & ", " _
	& proximo & ", '" _
	& fileName & "', '" _
	& upl.form("legenda") & "')"
	
	'Response.Write strSQL

	on error resume next
	conn.Execute(strSQL)

	if err.number <> 0 then
		%>
		<h5><%=err.Description%></h5>
		<%
	end if
	on error goto 0
else
	if op = 2 then
	
		strSQL = "DELETE from TB_ImagensTrabalhos where cd_trabalho = " & prefixo _
		& " and nu_seqImagem = " & request("nuSeqImagem")
		
		strNomeImagem = "e:\home\tavares\web\upload_admin\estomatologia\" _
		& prefixo & "_" & request("nomeArquivo")
		
		on error resume next
		
		Conn.BeginTrans
		Conn.Execute(strSQL)
		
		if err.number <> 0 then
			%>
			<h5>N�o foi poss�vel eliminar o Arquivo. (BD0010)</h5>
			<%
			conn.RollbackTrans
		else
			Set upl = Server.CreateObject("SoftArtisans.FileUp")
			upl.delete strNomeImagem
			if err.number <> 0 then
				%>
				<h5>N�o foi poss�vel eliminar o Arquivo. (UL0010)</h5>
				<%
				conn.RollbackTrans
			else
				conn.CommitTrans
			end if
		end if
	end if
end if
%>
<html>
	<head>
		<title>Inclus�o de Imagens</title>
		<link REL="stylesheet" HREF="estilos.css">
	</head>
	<body topmargin="10" leftmargin="10" onunload="finalizar()">
		<table class="TabelaFotoObra" WIDTH="580" HEIGHT="480" border="0">
			<tr height="50">
				<td align="center">
					<p class="Titulo" align="center">
						Inclus�o de Imagens
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top">
					<p class="textoComum">
						<form name="form_incluirImagens" enctype="multipart/form-data" method="post" action="incluirImagens.asp?prefixo=<%=prefixo%>&amp;op=1">
							<strong>Nome do Arquivo a incluir:</strong>
							&nbsp;
							<input type="file" name="f1" class="textbox01"><br><br>
							<strong>Legenda</strong><br>
							<textarea class="textarea01" name="legenda" cols="70" rows="3"></textarea>
							<br><br>
							<input type="submit" class="botaoPadrao" value="Incluir">
							<input type="hidden" id="finalizado" value="0">
							<hr noshade color="#7A64A5" width="80%">
							</form>
					</p>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top">
					<%
					set rs = Conn.Execute("select * from TB_ImagensTrabalhos where cd_trabalho = " & prefixo & " order by nu_seqImagem")
					if not rs.EOF then
						%>
						<form name="FormArquivos" method="post">
						<table width="80%">
							<tr class="textoComum">
								<td colspan="4" align="center"><strong>ARQUIVOS J� INCLU�DOS</strong><br><br></td>
							</tr>
							<tr class="textoComum">
								<td align="center"><strong>N�</strong></td>
								<td><strong>Nome do Arquivo</strong></td>
								<td><strong>Legenda</strong></td>
								<td align="center"><strong>Remover</strong></td>
							</tr>
							<%
							while not rs.EOF
								%>
								<tr class="textoComum">
									<td align="center"><%=rs("nu_seqImagem")%></td>
									<td><%=rs("de_nomeArquivo")%></td>
									<td><%=rs("de_legenda")%></td>
									<td align="center">
										<a href="javascript:deleteImagem('<%=rs("de_nomeArquivo")%>',<%=rs("nu_seqImagem")%>);">
											<img SRC="images/ico_remover.gif" border="0" WIDTH="15" HEIGHT="15">
										</a>
									</td>
								</tr>
								<%
								rs.MoveNext
							wend
							%>
						</table>
						<input type="hidden" name="nomeArquivo">
						<input type="hidden" name="nuSeqImagem">
						</form>
						<%
					else
						%>
							<tr class="textoComum">
								<td colspan="3" align="center">Nenhum arquivo incluido.</td>
							</tr>
						<%
					end if
					%>
				</td>
			</tr>
			<tr height="50">
				<td align="center">
					<p class="Titulo" align="center">
						<hr noshade color="#7A64A5" width="80%">
						<button class="botaoPadrao" onclick="encerrar()" id="button2" name="button2">Finalizar</button>
					</p>
				</td>
			</tr>
		</table>
	</body>
</html>
<script>
function finalizar(){
	if (document.form_incluirImagens.finalizado.value == 1)
		window.close();
	else {
		document.form_incluirImagens.finalizado.value = 1
	}
}

function encerrar(){
	finalizar();
	window.close();
}

function deleteImagem(arquivo, seq){
	document.FormArquivos.nomeArquivo.value = arquivo;
	document.FormArquivos.nuSeqImagem.value = seq;
	document.FormArquivos.action = 'incluirImagens.asp?prefixo=<%=prefixo%>&op=2';
	document.FormArquivos.submit();
}
</script>