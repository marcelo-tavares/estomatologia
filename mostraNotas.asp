<% 
nota = request("nota")
%>
<html>
	<head>
		<title>Nota Importante</title>
		<link REL="stylesheet" HREF="estilos.css">
	</head>
	<body topmargin="10" leftmargin="10">
		<%
		select case nota
			case 1
				strNota = "(1) ""III.3.i. A pesquisa em qualquer �rea do conhecimento, envolvendo seres humanos dever� prever procedimentos que assegurem a confidencialidade e a privacidade, a prote��o da imagem e a n�o estigmatiza��o, garantindo a n�o utiliza��o das informa��es em preju�zo das pessoas e/ou das comunidades, inclusive em termos de auto-estima, de prest�gio e/ou econ�mico-financeiro"".<br><a href=""http://www.bioetica.ufrgs.br/res19696.htm#III3i"" target=nova>(http://www.bioetica.ufrgs.br/res19696.htm#III3i)</a>"
				altura = 130
			case 2
				strNota = "(2) De acordo com a Resolu��o 196/96 (aprovada no Congresso Brasileiro de Bio�tica e pelo Conselho Nacional de Sa�de, 16/10/1996): �todo e qualquer projeto de pesquisa, em qualquer �rea, envolvendo seres humanos, dever� conter an�lise dos aspectos �ticos e ser aprovada por Comit� de �tica em Pesquisa da institui��o onde ser� realizada a pesquisa ou, na falta deste, pela Comiss�o Nacional de �tica em Pesquisa (CONEP). Entende-se por pesquisa envolvendo seres humanos aquela que, individual ou coletivamente, envolva o ser humano de forma direta ou indireta, em sua totalidade ou partes dele, incluindo o manejo de informa��es ou materiais.�<br><br>Levando em conta a dimens�o da rela��o pesquisador-sujeito da pesquisa, quanto � dignidade do ser humano, a Resolu��o 196/96 e o artigo 129 do C�digo de �tica M�dica determinam a necessidade de se obter o consentimento livre e esclarecido do sujeito da pesquisa ou do doador de �rg�o ou tecido para fins de tratamento de outros ou para fins de pesquisa (CONSELHO FEDERAL DE MEDICINA, 1998; CONSELHO FEDERAL DE ODONTOLOGIA, 2001).<br><br>De acordo com a Resolu��o 196/96, para a utiliza��o de qualquer �rg�o ou tecido humano, para fins de pesquisa ou trabalho cient�fico, h� necessidade de avalia��o e aprova��o do projeto por um comit� de �tica em pesquisa em seres humanos, atrav�s de um parecer consubstanciado (CONSELHO FEDERAL DE MEDICINA, 1998)."
				altura = 280
		end select
		%>
		<table class="TabelaFotoObra" WIDTH="580" HEIGHT="<%=altura%>">
			<tr valign=middle>
				<td valign="middle" align="center">
					<p class="textoComum">
						<%=strNota%>
					</p>
				</td>
			</tr>
		</table>
	</body>
</html>
