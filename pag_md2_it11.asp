<% if id_wk = 1 then %>
<p class="Titulo" align="center">Social Program</p>

<p class="textoComum" align="center">
	<A class=Link href="javascript:submeter(2,1,<%=id_wk%>,0)">Scientific Program</A>
	&nbsp;&nbsp;&nbsp;&nbsp;�&nbsp;&nbsp;&nbsp;&nbsp;
	<A class=Link href="javascript:submeter(2,1,<%=id_wk%>,2)">Tour Program</A>
</p>

<p class="textoComum" align="left">
	O 14th ICDMFR oferecer� as seguintes atividades sociais:
</p>

<p class="textoComum" align="left">
17 de maio (S�bado) �s 19:00h<br>
<b>Recep��o  do "Board of Directors" e "IADMFR Fellows"</b><br>(para membros do "IADMFR Board"
e "Fellows")<br>
Local: Clube Penhasco (Piano Bar) � Bairro Jos� Mendes
</p>

<p class="textoComum" align="left">
18 de maio (Domingo) �s 20:00h<br>
<b>Jantar do "Board of Directors"</b><br>(para membros do "Board of Directors")<br>
Local: Restaurante Pier 54 � Av. Beira Mar Norte, em baixo da Ponte Herc�lio Luz
</p>

<p class="textoComum" align="left">
19 de maio (Segunda-feira) �s 18:00h<br>
<b>Happy Hour</b><br>(para participantes do Pr�-Congresso)<br>
Local: Hall do Centro de Conven��es<br>
No fim das atividades cient�ficas do Pr�-Congresso, drinks e coquet�is leves
ser�o servidos, acompanhados por m�sica ao vivo. 
</p>

<p class="textoComum" align="left">
20 de maio (Ter�a-feira) �s 7:00h<br>
<b>Caf� da Manh� do Presidente</b><br>(somente para convidados)<br>
Local: Intercity Hotel � Av. Paulo Fontes, 1210 � Centro � Florian�polis
</p>

<p class="textoComum" align="left">
20 de maio (Ter�a-feira) �s 20:30h<br>
<b>Cerim�nia de Abertura e Recep��o de Boas Vindas</b><br>
Local: Centro Integrado de Cultura (CIC)<br>Av. Irineu Bornhausen, 5600 �
Agron�mica � Florian�polis<br>
A Cerim�nia de Abertura (Audit�rio principal do CIC) ser� seguida pela 
Recep��o de Boas Vindas, onde drinks e coquet�is leves ser�o servidos, 
na sala Lindolf Bell, piso t�rreo do CIC.
</p>

<p class="textoComum" align="left">
21 de maio (Quarta-feira) �s 21:00h<br>
<b>Festa da Cerveja</b><br>
Local: Associa��o Atl�tica Banco do Brasil (AABB) � Coqueiros
</p>

<p class="textoComum" align="left">
22 de maio (Quinta-feira) �s 20:30h<br>
<b>Banquete</b><br>
Local: Clube 12 de Agosto � Av. Herc�lio Luz � Centro
</p>

<p class="textoComum" align="left">
23 de maio (Sexta-feira) �s 12:30h<br>
<b>Festa de Despedida</b><br>
Local: Hall do Centro de Conven��es<br>
Um conv�vio informal, onde coquet�is leves e drinks ser�o servidos, 
para dar-nos uma possibilidade compartilhar dos �ltimos momentos 
de alegria com os amigos velhos e novos. </p>

<p class="textoComum" align="left">
	<i><b>Aten��o</b>:<br>Seu cart�o de identifica��o do 14th ICDMFR ser� seu passaporte para todas as atividades congresso, <b>incluindo as da programa��o social</b>. Portanto, lembre-se de mante-lo consigo durante todo o tempo.</i>
</p>

<p class="textoComum" align="left">
	As atividades sociais podem ser complementadas por outras programa��es sociais oferecidas pela cidade.
	<br><br>&nbsp;
</p>
<% else %>
<p class="Titulo" align="center">Social Program</p>

<p class="textoComum" align="center">
	<A class=Link href="javascript:submeter(2,1,<%=id_wk%>,0)">Scientific Program</A>
	&nbsp;&nbsp;&nbsp;&nbsp;�&nbsp;&nbsp;&nbsp;&nbsp;
	<A class=Link href="javascript:submeter(2,1,<%=id_wk%>,2)">Tour Program</A>
</p>

<p class="textoComum" align="left">
	The 14th ICDMFR will offer the following social activities:
</p>

<p class="textoComum" align="left">
17 May (Saturday) at 19:00h<br>
<b>Board of Directors and IADMFR Fellows� Reception</b><br>(for IADMFR Board members
and Fellows)<br>
Venue: Clube Penhasco (Piano Bar) � Bairro Jos� Mendes
</p>

<p class="textoComum" align="left">
18 May (Sunday) at 20:00h<br>
<b>Board of Directors� Dinner</b><br>(for Board members)<br>
Venue: Restaurant Pier 54 � Av. Beira Mar Norte, under Herc�lio Luz Bridge
</p>

<p class="textoComum" align="left">
19 May (Monday) at 18:00h<br>
<b>Happy Hour</b><br>(for participants of the Pre-Congress)<br>
Venue: Hall of the Convention Center<br>
At the end of the scientific activities of the Pre-Congress, drinks and
light refreshments will be served, accompanied by live music.
</p>

<p class="textoComum" align="left">
20 May (Tuesday) at 7:00h<br>
<b>President�s Breakfast</b><br>(per invitation only)<br>
Venue: Intercity Hotel � Av. Paulo Fontes, 1210 � Centro � Florian�polis
</p>

<p class="textoComum" align="left">
20 May (Tuesday) at 20:30h<br>
<b>Opening Ceremony and Welcome reception</b><br>
Venue: Centro Integrado de Cultura (CIC)<br>Av. Irineu Bornhausen, 5600 �
Agron�mica � Florian�polis<br>
The Opening Ceremony (main auditorium of CIC) will be followed by the
Welcome Reception, where drinks and light refreshments will be served, at
room Lindolf Bell (ground floor of CIC).
</p>

<p class="textoComum" align="left">
21 May (Wednesday) at 21:00h<br>
<b>Beer Party</b><br>
Venue: Associa��o Atl�tica Banco do Brasil (AABB) � Coqueiros
</p>

<p class="textoComum" align="left">
22 May (Thursday) at 20:30h<br>
<b>Banquet</b><br>
Venue: Clube 12 de Agosto � Av. Herc�lio Luz � Downtown
</p>

<p class="textoComum" align="left">
23 May (Friday) at 12:30h<br>
<b>Farewell Party</b><br>
Venue: Hall of the Convention Center<br>
An informal get-together, where light refreshments and drinks will be
served, to give us a chance to share the last moments of joy with old and
new friends.</p>

<p class="textoComum" align="left">
	<i><b>Attention</b>:<br>Your identification tag in the 14th ICDMFR will be your passport to attend all of the activities of the Congress, <b>including those of the Social Program</b>. Therefore, please remember to keep it in your possession during all times.</i>
</p>

<p class="textoComum" align="left">
	These social activities can be complemented by other optional social programs offered by the city.
	<br><br>&nbsp;
	<br><br>&nbsp;
</p>
<% end if %>



